package com.wi_vod.itstrucker;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.zip.GZIPInputStream;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;


import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import com.google.android.gms.security.ProviderInstaller;
import com.wi_vod.itstrucker.model.XMLparser;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;

public class LoginBackgroundService extends Service {
	String retval, ret, response_str=null;
	private SessionManager session;
	private static final String TAG = "LoginBackgroundService";
	//static int count=0;
	
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		//System.out.println("LoginBackgroundService onCreate Service Started");
		// something to do when the service is created
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		
		//System.out.println("LoginBackgroundService onStartCommand called");
		session = new SessionManager(LoginBackgroundService.this);
	
		if (Utils.isAppIsInBackground(LoginBackgroundService.this)) {
			Utils.stopalarmservice(LoginBackgroundService.this);
		}else{
			
			new VerifyLogin().execute();
		}
	
		return START_STICKY;
	}
	
	private class VerifyLogin extends AsyncTask<Void, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
		}

		@Override
		protected String doInBackground(Void... params) {
			try {
				//System.out.println(Utils.isInternetAvailable(ITSTruckerLogin.this));
				if (Utils.isInternetAvailable(LoginBackgroundService.this)) {
					// Log.i("IN BACKGROUND", "doInBackground");
					
					ProviderInstaller.installIfNeeded(LoginBackgroundService.this);
					SSLContext sslcontext = SSLContext.getInstance("TLSv1.2");
					sslcontext.init(null, null, null);
					String url = AppConstants.LOGIN_TEST_URL;
					URL obj = new URL(url);
					//URL obj = new URL("https://secure.truckstop.com");
					HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
				
					
					
					
				/*
					HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
					ITSApplication app = ((ITSApplication) getApplicationContext());
					//ITSApplication app= new ITSApplication();
					if (app.certifcate()==null) {
						return AppConstants.SERVER_CONNECTION_ERROR;
					}
					
					con.setSSLSocketFactory(app.certifcate().getSocketFactory());*/
					con.setRequestMethod("POST");
					con.setRequestProperty("Accept-Encoding", "gzip,deflate");
					con.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
					con.setRequestProperty("SOAPAction","http://tempuri.org/IWebServiceAdmin/VerifyItsAccountHandleAndPasswordGetIdAndProducts");
					con.setRequestProperty("Host", "webservices.truckstop.com");
					con.setRequestProperty("Connection", "Keep-Alive");
					con.setRequestProperty("User-Agent","Apache-HttpClient/4.1.1 (java 1.5)\\");
					
					String envolpe = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"";
					String uri = " xmlns:tem=\"http://tempuri.org/\">";
					String body = envolpe
							+ uri
							+ "\n<soapenv:Header/>\n"
							+ "<soapenv:Body>\n"
							+ "<tem:VerifyItsAccountHandleAndPasswordGetIdAndProducts>\n"
							+ "<tem:authtoken>\n"
							+ "<tem:UserName>"+ AppConstants.USERNAME+ "</tem:UserName>\n"
							+ "<tem:Password>"+ AppConstants.PASSWORD+ "</tem:Password>\n"
							+ "<tem:IntegrationId>"+ AppConstants.INTEGRARTION_ID+ "</tem:IntegrationId>\n"
							+ "</tem:authtoken>\n"
							+ "<tem:itsAccount>"+ session.getItsAccountNo()+ "</tem:itsAccount>\n"
							+ "<tem:handle>"+ session.getItsHandle()+ "</tem:handle>\n"
							+ "<tem:password>"+ session.getItsPassword()+ "</tem:password>\n"
					//	+ "<tem:password>luiulikj</tem:password>\n"
							+ "</tem:VerifyItsAccountHandleAndPasswordGetIdAndProducts>\n"
							+ "</soapenv:Body>\n" + "</soapenv:Envelope>";
				//System.out.println("BODY VALUE----" + body);
					con.setConnectTimeout(AppConstants.SOCKETTIMEOUT_ERROR);
					con.setDoOutput(true);
					DataOutputStream wr = new DataOutputStream(con.getOutputStream());
					wr.writeBytes(body);
					wr.flush();
					wr.close();
			 
					int responseCode = con.getResponseCode();
					
					if (responseCode!=200) {
						return AppConstants.SERVER_CONNECTION_ERROR;
					}else{
						
						InputStream stream = con.getInputStream();
						String contentEncoding = con.getHeaderField("Content-Encoding");
						
						if (contentEncoding != null&& contentEncoding.equalsIgnoreCase("gzip")) {
							final InputStream stream1 = new GZIPInputStream(stream);
							// System.out.println("i am here");
							BufferedReader reader = new BufferedReader(new InputStreamReader(stream1));
							String line;
							StringBuilder sbuilder = new StringBuilder();
							while ((line = reader.readLine()) != null) {
								sbuilder.append(line);
							}
							
							response_str="";
							response_str = sbuilder.toString();
							//System.out.println("in if statement\n      "+response_str);
							boolean result = XMLparser.parsexml(response_str);
							if (result) {
								return "success";
							}
						} else {
							BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
							String line;
							StringBuilder sbuilder = new StringBuilder();
							while ((line = reader.readLine()) != null) {
								sbuilder.append(line);
							}
							response_str="";
							response_str = sbuilder.toString();
					//	System.out.println("retval IN ELSE-----"+ response_str);
							boolean result = XMLparser.parsexml(response_str);
							if (result) {
								return "success";
							}
						}
					}
					
				}else {
					return AppConstants.INTERNET_CONNECTION_ERROR;

				}

			} catch(Exception e){
				return AppConstants.SERVER_CONNECTION_ERROR;
			}
			return AppConstants.SERVER_CONNECTION_ERROR;
		}
		/*@Override
		protected String doInBackground(Void... params) {
			try {
				//System.out.println(Utils.isInternetAvailable(ITSTruckerLogin.this));
				if (Utils.isInternetAvailable(LoginBackgroundService.this)) {
					// Log.i("IN BACKGROUND", "doInBackground");
					HttpPost post = new HttpPost(AppConstants.LOGIN_TEST_URL);
					//Log.i(TAG, AppConstants.LOGIN_TEST_URL);
					int timeoutConnection = 9000;
					HttpParams httpParameters = new BasicHttpParams();
					HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
					// Set the default socket timeout (SO_TIMEOUT) 
					// in milliseconds which is the timeout for waiting for data.
					int timeoutSocket = 5000;
					HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
					DefaultHttpClient client = new DefaultHttpClient(httpParameters);
					post.setHeader("Accept-Encoding", "gzip,deflate");
					post.setHeader("Content-Type", "text/xml;charset=UTF-8");
					post.setHeader("SOAPAction","http://tempuri.org/IWebServiceAdmin/VerifyItsAccountHandleAndPasswordGetIdAndProducts");
					post.setHeader("Host", "webservices.truckstop.com");
					post.setHeader("Connection", "Keep-Alive");
					post.setHeader("User-Agent","Apache-HttpClient/4.1.1 (java 1.5)\\");
		
					String envolpe = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"";
					String uri = " xmlns:tem=\"http://tempuri.org/\">";
					String body = envolpe
							+ uri
							+ "\n<soapenv:Header/>\n"
							+ "<soapenv:Body>\n"
							+ "<tem:VerifyItsAccountHandleAndPasswordGetIdAndProducts>\n"
							+ "<tem:authtoken>\n"
							+ "<tem:UserName>"+ AppConstants.USERNAME+ "</tem:UserName>\n"
							+ "<tem:Password>"+ AppConstants.PASSWORD+ "</tem:Password>\n"
							+ "<tem:IntegrationId>"+ AppConstants.INTEGRARTION_ID+ "</tem:IntegrationId>\n"
							+ "</tem:authtoken>\n"
							+ "<tem:itsAccount>"+ session.getItsAccountNo()+ "</tem:itsAccount>\n"
							+ "<tem:handle>"+ session.getItsHandle()+ "</tem:handle>\n"
							+ "<tem:password>"+ session.getItsPassword()+ "</tem:password>\n"
							//+ "<tem:password>luiulikj</tem:password>\n"
							+ "</tem:VerifyItsAccountHandleAndPasswordGetIdAndProducts>\n"
							+ "</soapenv:Body>\n" + "</soapenv:Envelope>";
				System.out.println("BODY VALUE----" + body);
					post.setEntity(new StringEntity(body));
					HttpResponse response = client.execute(post);
					StatusLine statuscode = response.getStatusLine();
					String statuscodes = Integer.toString(statuscode.getStatusCode());
				
					if (!statuscodes.equals("200")) {
						return AppConstants.SERVER_CONNECTION_ERROR;
					}else{
						InputStream stream = response.getEntity().getContent();
						Header contentEncoding = response.getFirstHeader("Content-Encoding");
						if (contentEncoding != null&& contentEncoding.getValue().equalsIgnoreCase("gzip")) {
							final InputStream stream1 = new GZIPInputStream(stream);
							// System.out.println("i am here");
							BufferedReader reader = new BufferedReader(new InputStreamReader(stream1));
							String line;
							StringBuilder sbuilder = new StringBuilder();
							while ((line = reader.readLine()) != null) {
								sbuilder.append(line);
							}
							
							response_str="";
							response_str = sbuilder.toString();
							System.out.println("in if statement\n      "+response_str);
							boolean result = XMLparser.parsexml(response_str);
							if (result) {
								return "success";
							}
						} else {
							BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
							String line;
							StringBuilder sbuilder = new StringBuilder();
							while ((line = reader.readLine()) != null) {
								sbuilder.append(line);
							}
							response_str="";
							response_str = sbuilder.toString();
							//System.out.println("retval IN ELSE-----"+ response_str);
							boolean result = XMLparser.parsexml(response_str);
							if (result) {
								return "success";
							}
						}
					}
					
				}else {
					//session.setnetwrokchangetracker(true);
					return AppConstants.INTERNET_CONNECTION_ERROR;

				}

			} catch (Exception e) {
				return AppConstants.SERVER_CONNECTION_ERROR;
			}
			return AppConstants.SERVER_CONNECTION_ERROR;
		}*/


		protected void onPostExecute(String result) {
		 if (result.equals(AppConstants.INTERNET_CONNECTION_ERROR)) {
				//Utils.showAlertMessage(AppConstants.INTERNET_CONNECTION_ERROR, ITSTruckerLogin.this);
				//Toast.makeText(ITSTruckerLogin.this,AppConstants.INTERNET_CONNECTION_ERROR ,Toast.LENGTH_LONG).show();
			}else if (result.equals(AppConstants.SERVER_CONNECTION_ERROR)) {
				//Utils.showAlertMessage(AppConstants.SERVER_CONNECTION_ERROR, ITSTruckerLogin.this);
				//Toast.makeText(ITSTruckerLogin.this,AppConstants.SERVER_CONNECTION_ERROR ,Toast.LENGTH_LONG).show();
				
			}else {
				
				//session.setnetwrokchangetracker(false);
				if (XMLparser.errorcode.equals("InActiveHandle")||XMLparser.accountverified.equals("false")) {
				
					if (!Utils.isAppIsInBackground(LoginBackgroundService.this)) {
						
						Intent registrationComplete = new Intent(Utils.LOGOUT_USER);
						LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(registrationComplete);
				
						//showAlertMessage();
						/*stopalarmservice();
						session.logoutUser();*/
					}else{
						//session.clearSession();
						Utils.stopalarmservice(LoginBackgroundService.this);
					}
				}else{
					if (Utils.isAppIsInBackground(LoginBackgroundService.this)) {
						Utils.stopalarmservice(LoginBackgroundService.this);
					}
				}
			}
		}
	}	

	
	/*//Verify whether the applicatication is in background or foreground.
	private boolean isAppIsInBackground(Context context) {
		boolean isInBackground = true;
		ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
			List<ActivityManager.RunningAppProcessInfo> runningProcesses = am
					.getRunningAppProcesses();
			for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
				if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
					for (String activeProcess : processInfo.pkgList) {
						if (activeProcess.equals(context.getPackageName())) {
							isInBackground = false;
						}
					}
				}
			}
		} else {
			List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
			ComponentName componentInfo = taskInfo.get(0).topActivity;
			if (componentInfo.getPackageName().equals(context.getPackageName())) {
				isInBackground = false;
			}
		}

		return isInBackground;
	}*/

	/*
	 *Alarmservice will be stopped and no more service requests will be generated by the broadcasts
	 */
	
	
	
	
}
