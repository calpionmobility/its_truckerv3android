/******************************************************************
 * CurrentPost.java
 * 
 * Description: Class for the showing the current posting Activity
 * 
 * @author: Wayne Y
 * 
 * @version: 1.0
 * 
 * History:
 * 
 * 
 ******************************************************************
 */
package com.wi_vod.itstrucker;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.wi_vod.itstrucker.ITSEulaScreen.HttpServiceListener;
import com.wi_vod.itstrucker.model.CurrentTruckPostsModel;
import com.wi_vod.itstrucker.model.XMLCurrentPostsparser;
import com.wi_vod.itstrucker.model.XMLTruckDeleteparser;

import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.zip.GZIPInputStream;

import javax.net.ssl.HttpsURLConnection;

//import android.view.ActionMode;

/************************************************************
 * Class: CurrentPost
 * 
 * Inherits: Fragment
 * 
 * Description: Set up the current post activity
 * -----------------------------------------------------------------
 */
public class CurrentPostScreen extends ActionBarActivity {

	private static final String TAG = "CURRENTPOST.CLASS";
	private ArrayList<Map<String, String>> list = new ArrayList<Map<String, String>>();
	private ArrayList<CurrentTruckPostsModel> truckpostitems = new ArrayList<CurrentTruckPostsModel>();
	private ProgressDialog progDialog; // Progress bar
	private String response_str;
	private StringBuilder builder = new StringBuilder();
	private String errormsg;
	private SessionManager session;
	private CustomAdapter adapter;
	int selectedlistpostion;
	private TextView listerrormsg, totalselectedtext;
	private ListView currentList;
	private String deleteTruckIds = "";
	static int checks = 0;
	private Resources res;
	boolean showdelete;
	private BroadcastReceiver mRegistrationBroadcastReceiver;
	

	// prevents orientation change from reloading results and crashing app
	@Override
	public void onConfigurationChanged(Configuration newConfig) {

		super.onConfigurationChanged(newConfig);
	}
	
	
	/*********************************************************
	 * Method: onCreate
	 * 
	 * Description: Called when the Activity is first created
	 * 
	 * @param: Bundle savedInstanceState - Current State
	 * 
	 * @returns: Void
	 * 
	 */
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.copycurrentpost);
		updateUI();
		 getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		res = getResources();
		session = new SessionManager(CurrentPostScreen.this);
		
		// Create a dialog for a process spinner
	/*	GetCurrentTrucks gettrucks = new GetCurrentTrucks();
		gettrucks.execute();*/
		GetCurrentTrucksFromServer();
		mRegistrationBroadcastReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Utils.logoutuser(AppConstants.LOGOUTUSER_ERRORMSG, CurrentPostScreen.this);
			}
		};
		
		//Log.i(TAG, "executed oncreateview");
	}

	public void updateUI() {
		listerrormsg = (TextView) findViewById(R.id.errormsg);
		currentList = (ListView) findViewById(R.id.currentlist);
		totalselectedtext = (TextView) findViewById(R.id.totalselected);
	}

	
	
	
	private void DeletePostRequest() {
		String envolpe = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\""
				+ " xmlns:v11=\"http://webservices.truckstop.com/v11\""
				+ " xmlns:web=\"http://schemas.datacontract.org/2004/07/WebServices\""
				+ " xmlns:web1=\"http://schemas.datacontract.org/2004/07/WebServices.Posting\""
				+ " xmlns:arr=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\""
				+ ">";
		String body = envolpe + "\n<soapenv:Header/>\n"
				+ "<soapenv:Body>\n" 
				+ "<v11:DeleteTrucks>\n"
				+ " <v11:trucks>\n"
				+ "<web:IntegrationId>"+session.getUserIntegrationID()+"</web:IntegrationId>\n" 
				+ "<web:Password>"+ AppConstants.PASSWORD+"</web:Password>\n"
				+ "<web:UserName>"+ AppConstants.USERNAME+"</web:UserName>\n"
				+ "<web1:Trucks>\n"
				
				+ deleteTruckIds 
				+ "</web1:Trucks>\n"
				+ "</v11:trucks>\n" 
				+ "</v11:DeleteTrucks>\n"
				+ "</soapenv:Body>\n" 
				+ "</soapenv:Envelope>\n";

		
		//System.out.println("BODY VALUE----" + body);
		
		
		String SOAPACTION="http://webservices.truckstop.com/v11/ITruckPosting/DeleteTrucks";
		
		HttpDeleteServiceListener listener = new HttpDeleteServiceListener();
		WebServiceConnectionService serv = new WebServiceConnectionService(CurrentPostScreen.this,body,
				SOAPACTION, listener,AppConstants.DELETEPOSTS_PROGRESS);
		serv.execute(AppConstants.GETTRUCKS_TEST_URL);
		
	}
	
	 protected class HttpDeleteServiceListener implements ConnectionServiceListener {

		@Override
		public void onServiceComplete(String response) {
			if (response.equals(AppConstants.INTERNET_CONNECTION_ERROR)) {
				Utils.showAlertMessage(AppConstants.INTERNET_CONNECTION_ERROR, CurrentPostScreen.this);
			} else if (response.equals(AppConstants.SERVER_CONNECTION_ERROR)) {
				Utils.showAlertMessage(AppConstants.SERVER_CONNECTION_ERROR, CurrentPostScreen.this);	
			} else if (response.equals(AppConstants.PLAYSERVICES_NOTFOUND)) {
				//Utils.showAlertMessage(AppConstants.PLAYSERVICES_NOTFOUND, CurrentPostScreen.this);
				Utils.updateGoogleplay(CurrentPostScreen.this);
			} else {
				try {
					boolean deletesucess = XMLTruckDeleteparser.parsexml(response);

					if (!deletesucess) {
						errormsg = XMLCurrentPostsparser.geterrormsg();
						//System.out.println("items in details---->"+ errormsg);
						Utils.showAlertMessage(errormsg, CurrentPostScreen.this);
						
					}else {
						Intent startsearch = new Intent(CurrentPostScreen.this,CurrentPostScreen.class);
						startsearch.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(startsearch);
					}
				} catch (Exception e) {
					Utils.showAlertMessage(AppConstants.SERVER_CONNECTION_ERROR, CurrentPostScreen.this);	
				}
				
				
				
			}
			
		}

		@Override
		public void onServiceComplete(String response, String reference) {
			// TODO Auto-generated method stub
			
		}
	
	 }
	
	
	
	
	
	
	
	
	
	/*********************************************************
	 * Class RequestDeletePost
	 * 
	 * Description: Task for processing the HTTP request
	 * 
	 * 
	 *//*
	private class RequestDeletePost extends AsyncTask<Void, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progDialog = new ProgressDialog(CurrentPostScreen.this);
			progDialog.setMessage(AppConstants.DELETEPOSTS_PROGRESS);
			// progDialog.setIndeterminate(false);
			progDialog.setCancelable(false);
			progDialog.show();
			if (CurrentPostScreen.this.getResources().getConfiguration().orientation==Configuration.ORIENTATION_PORTRAIT) {
				CurrentPostScreen.this.setRequestedOrientation(Configuration.ORIENTATION_PORTRAIT);
			}else{
				CurrentPostScreen.this.setRequestedOrientation(Configuration.ORIENTATION_LANDSCAPE);
			}
			
		}

		@Override
		protected String doInBackground(Void... params) {
			try {
				////System.out.println(Utils.isInternetAvailable(CurrentPostScreen.this));
				if (Utils.isInternetAvailable(CurrentPostScreen.this)) {
					// Log.i("IN BACKGROUND", "doInBackground");
					HttpPost post = new HttpPost(AppConstants.GETTRUCKS_TEST_URL);
					HttpParams httpParameters = new BasicHttpParams();
					HttpConnectionParams.setConnectionTimeout(httpParameters,AppConstants.TIMEOUTCONNECTION_ERROR);
					HttpConnectionParams.setSoTimeout(httpParameters,AppConstants.TIMEOUTCONNECTION_ERROR);
					DefaultHttpClient client = new DefaultHttpClient(httpParameters);
					post.setHeader("Accept-Encoding", "gzip,deflate");
					post.setHeader("Content-Type", "text/xml;charset=UTF-8");
					// SOAPAction:
					// "http://webservices.truckstop.com/v12/ILoadSearch/GetLoadSearchResults"
					post.setHeader("SOAPAction","http://webservices.truckstop.com/v11/ITruckPosting/DeleteTrucks");
					post.setHeader("Host", "webservices.truckstop.com");
					post.setHeader("Connection", "Keep-Alive");
					// post.setHeader("Content-Length", "2811");
					post.setHeader("User-Agent","Apache-HttpClient/4.1.1 (java 1.5)\\");
					
					 String url = AppConstants.GETTRUCKS_TEST_URL;
	                    Log.i(TAG, AppConstants.GETTRUCKS_TEST_URL);
						URL obj = new URL(url);
						HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
						ITSApplication app = ((ITSApplication) getApplicationContext());
						//ITSApplication app= new ITSApplication();
						if (app.certifcate()==null) {
							return AppConstants.SERVER_CONNECTION_ERROR;
						}
						
						con.setSSLSocketFactory(app.certifcate().getSocketFactory());
						con.setRequestMethod("POST");
						con.setRequestProperty("Accept-Encoding", "gzip,deflate");
						con.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
	                    // SOAPAction:
	                    // "http://webservices.truckstop.com/v12/ILoadSearch/GetLoadSearchResults"
						con.setRequestProperty("SOAPAction","http://webservices.truckstop.com/v11/ITruckPosting/DeleteTrucks");
						con.setRequestProperty("Host", "webservices.truckstop.com");
						con.setRequestProperty("Connection", "Keep-Alive");
						con.setRequestProperty("User-Agent", "Apache-HttpClient/4.1.1 (java 1.5)\\");
					
					String envolpe = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\""
							+ " xmlns:v11=\"http://webservices.truckstop.com/v11\""
							+ " xmlns:web=\"http://schemas.datacontract.org/2004/07/WebServices\""
							+ " xmlns:web1=\"http://schemas.datacontract.org/2004/07/WebServices.Posting\""
							+ " xmlns:arr=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\""
							+ ">";
					String body = envolpe + "\n<soapenv:Header/>\n"
							+ "<soapenv:Body>\n" 
							+ "<v11:DeleteTrucks>\n"
							+ " <v11:trucks>\n"
							+ "<web:IntegrationId>"+session.getUserIntegrationID()+"</web:IntegrationId>\n" 
							+ "<web:Password>"+ AppConstants.PASSWORD+"</web:Password>\n"
							+ "<web:UserName>"+ AppConstants.USERNAME+"</web:UserName>\n"
							+ "<web1:Trucks>\n"
							
							+ deleteTruckIds 
							+ "</web1:Trucks>\n"
							+ "</v11:trucks>\n" 
							+ "</v11:DeleteTrucks>\n"
							+ "</soapenv:Body>\n" 
							+ "</soapenv:Envelope>\n";

					//System.out.println("BODY VALUE----" + body);
					 con.setConnectTimeout(AppConstants.SOCKETTIMEOUT_ERROR);
						con.setDoOutput(true);
						DataOutputStream wr = new DataOutputStream(con.getOutputStream());
						//System.out.println("BODY VALUE----" + body);
						wr.writeBytes(body);
						wr.flush();
						wr.close();
				 
						int responseCode = con.getResponseCode();
	                    // System.out.println("Status coded-------" + statuscodes);
						if (responseCode!=200) {
	                        return AppConstants.SERVER_CONNECTION_ERROR;
	                    } else {
	                    	InputStream stream = con.getInputStream();
							String contentEncoding = con.getHeaderField("Content-Encoding");
						if (contentEncoding != null&& contentEncoding.equalsIgnoreCase("gzip")) {
							final InputStream stream1 = new GZIPInputStream(stream);
							// //System.out.println("i am here");
							BufferedReader reader = new BufferedReader(new InputStreamReader(stream1));
							String line;
							StringBuilder sbuilder = new StringBuilder();
							while ((line = reader.readLine()) != null) {

								sbuilder.append(line);
								response_str = sbuilder.toString();
								//System.out.println("retval IN IF-----"+ response_str);
								boolean deletesucess = XMLTruckDeleteparser.parsexml(response_str);

								if (!deletesucess) {
									errormsg = XMLCurrentPostsparser.geterrormsg();
									//System.out.println("items in details---->"+ errormsg);
									return "error";
								}
							}
						} else {
							BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
							String line;
							StringBuilder sbuilder = new StringBuilder();
							while ((line = reader.readLine()) != null) {
								sbuilder.append(line);
								response_str = "";
								response_str = sbuilder.toString();
								//System.out.println("retval IN ELSE-----"+ response_str);
								boolean deletesucess = XMLTruckDeleteparser.parsexml(response_str);
								if (!deletesucess) {
									errormsg = XMLCurrentPostsparser.geterrormsg();
									//System.out.println("items in details---->"+ errormsg);
									return "error";
								}
							}
						}
					}

				} else {
					return AppConstants.INTERNET_CONNECTION_ERROR;
				}

			} catch (Exception e) {
				e.printStackTrace();
				return AppConstants.SERVER_CONNECTION_ERROR;
			}
			return "success";
		}

		protected void onPostExecute(String result) {
			progDialog.dismiss();
			//CurrentPostScreen.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
			if (result.equals("error")) {
				Utils.showAlertMessage(errormsg, CurrentPostScreen.this);
				Toast.makeText(CopyOfCurrentPost.this,"Delete Unsuccessful.Try after some time",Toast.LENGTH_LONG).show();
				// finish();
			} else if (result.equals(AppConstants.INTERNET_CONNECTION_ERROR)) {
				Utils.showAlertMessage(AppConstants.INTERNET_CONNECTION_ERROR, CurrentPostScreen.this);
			} else if (result.equals(AppConstants.SERVER_CONNECTION_ERROR)) {
				Utils.showAlertMessage(AppConstants.SERVER_CONNECTION_ERROR, CurrentPostScreen.this);	
			} else {
				Intent startsearch = new Intent(CurrentPostScreen.this,CurrentPostScreen.class);
				startsearch.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(startsearch);
			}
		}
	}*/
	 
	 
	 private void GetCurrentTrucksFromServer(){
		 
		 String envolpe = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\""
					+ " xmlns:v11=\"http://webservices.truckstop.com/v11\""
					+ " xmlns:web=\"http://schemas.datacontract.org/2004/07/WebServices\""+ ">";
			String body = envolpe + "\n<soapenv:Header/>\n"
					+ "<soapenv:Body>\n" 
					+ "<v11:GetTrucks>\n"
					+ "<v11:listRequest>\n" 
					+ "<web:IntegrationId>"+session.getUserIntegrationID()+"</web:IntegrationId>\n" 
					+ "<web:Password>"+AppConstants.PASSWORD+"</web:Password>\n"
					+ "<web:UserName>"+AppConstants.USERNAME+"</web:UserName>\n" 
					+ "</v11:listRequest>\n"
					+ "</v11:GetTrucks>\n"
					+ "</soapenv:Body>\n"
					+ "</soapenv:Envelope>\n";

			//System.out.println("BODY VALUE----" + body);
			String SOAPACTION="http://webservices.truckstop.com/v11/ITruckPosting/GetTrucks";
			
			HttpCurrentPostServiceListener listener = new HttpCurrentPostServiceListener();
			WebServiceConnectionService serv = new WebServiceConnectionService(CurrentPostScreen.this,body,
					SOAPACTION, listener,AppConstants.CURRENTPOSTS_PROGRESS);
			serv.execute(AppConstants.GETTRUCKS_TEST_URL);
			
	 }
	 
	 
	 protected class HttpCurrentPostServiceListener implements ConnectionServiceListener {

		@Override
		public void onServiceComplete(String response) {
			if (response.equals(AppConstants.INTERNET_CONNECTION_ERROR)) {
				Utils.showErrorMessage(AppConstants.INTERNET_CONNECTION_ERROR,CurrentPostScreen.this);
				/*
				 * Toast.makeText(CopyOfCurrentPost.this,AppConstants.
				 * INTERNET_CONNECTION_ERROR,Toast.LENGTH_LONG).show();
				 * finish();
				 */
			} else if (response.equals(AppConstants.SERVER_CONNECTION_ERROR)) {
				Utils.showErrorMessage(AppConstants.SERVER_CONNECTION_ERROR,CurrentPostScreen.this);
				/*
				 * Toast.makeText(CopyOfCurrentPost.this,AppConstants.
				 * SERVER_CONNECTION_ERROR, Toast.LENGTH_LONG).show(); finish();
				 */
			} else if (response.equals(AppConstants.PLAYSERVICES_NOTFOUND)) {
				Utils.updateGoogleplay(CurrentPostScreen.this);
				/*
				 * Toast.makeText(CopyOfCurrentPost.this,AppConstants.
				 * SERVER_CONNECTION_ERROR, Toast.LENGTH_LONG).show(); finish();
				 */
			}else {
				try {
					boolean loadsucess = XMLCurrentPostsparser.parsexml(response);

					if (loadsucess) {
						truckpostitems.clear();
						truckpostitems = XMLCurrentPostsparser.getCurrentTrucks();
						// Log.i("Currentpost","truckpostitems size-------->"+truckpostitems.size());
						list.clear();
						if (truckpostitems.size() == 0) {
							//listerrormsg.setText("NO POSTS FOUND");
							Utils.showErrorMessage("You have no trucks currently posted!",CurrentPostScreen.this);
							// Toast.makeText(CurrentPost.this,"No Posts Found",Toast.LENGTH_LONG).show();
							// finish();
						} else {
							listerrormsg.setText("Total Posts: " + truckpostitems.size());
							adapter = new CustomAdapter(CurrentPostScreen.this,truckpostitems, res);
							currentList.setAdapter(adapter);
						}
						
					} else {
						errormsg = XMLCurrentPostsparser.geterrormsg();
						Utils.showAlertMessage(errormsg, CurrentPostScreen.this);
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
				
			}
			
		}

		@Override
		public void onServiceComplete(String response, String reference) {
			// TODO Auto-generated method stub
			
		}
		 
		 
		 
	 }

	 

/*	private class GetCurrentTrucks extends AsyncTask<Void, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progDialog = new ProgressDialog(CurrentPostScreen.this);
			progDialog.setMessage(AppConstants.CURRENTPOSTS_PROGRESS);
			// progDialog.setIndeterminate(false);
			progDialog.setCancelable(false);
			progDialog.show();
			if (CurrentPostScreen.this.getResources().getConfiguration().orientation==Configuration.ORIENTATION_PORTRAIT) {
				CurrentPostScreen.this.setRequestedOrientation(Configuration.ORIENTATION_PORTRAIT);
			}else{
				CurrentPostScreen.this.setRequestedOrientation(Configuration.ORIENTATION_LANDSCAPE);
			}
		}

		@Override
		protected String doInBackground(Void... params) {
			try {
				//System.out.println(Utils.isInternetAvailable(CurrentPostScreen.this));
				if (Utils.isInternetAvailable(CurrentPostScreen.this)) {
					// Log.i("IN BACKGROUND", "doInBackground");
					HttpPost post = new HttpPost(AppConstants.GETTRUCKS_TEST_URL);
					HttpParams httpParameters = new BasicHttpParams();
					HttpConnectionParams.setConnectionTimeout(httpParameters,AppConstants.TIMEOUTCONNECTION_ERROR);
					HttpConnectionParams.setSoTimeout(httpParameters,AppConstants.SOCKETTIMEOUT_ERROR);
					DefaultHttpClient client = new DefaultHttpClient(httpParameters);
					post.setHeader("Accept-Encoding", "gzip,deflate");
					post.setHeader("Content-Type", "text/xml;charset=UTF-8");
					// SOAPAction:
					// "http://webservices.truckstop.com/v12/ILoadSearch/GetLoadSearchResults"
					post.setHeader("SOAPAction","http://webservices.truckstop.com/v11/ITruckPosting/GetTrucks");
					post.setHeader("Host", "webservices.truckstop.com");
					post.setHeader("Connection", "Keep-Alive");
					// post.setHeader("Content-Length", "2811");
					post.setHeader("User-Agent","Apache-HttpClient/4.1.1 (java 1.5)\\");
					 String url = AppConstants.GETTRUCKS_TEST_URL;
	                   // Log.i(TAG, AppConstants.GETTRUCKS_TEST_URL);
						URL obj = new URL(url);
						HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
						ITSApplication app = ((ITSApplication) getApplicationContext());
						//ITSApplication app= new ITSApplication();
						if (app.certifcate()==null) {
							return AppConstants.SERVER_CONNECTION_ERROR;
						}
						
						con.setSSLSocketFactory(app.certifcate().getSocketFactory());
						con.setRequestMethod("POST");
						con.setRequestProperty("Accept-Encoding", "gzip,deflate");
						con.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
	                    // SOAPAction:
	                    // "http://webservices.truckstop.com/v12/ILoadSearch/GetLoadSearchResults"
						con.setRequestProperty("SOAPAction","http://webservices.truckstop.com/v11/ITruckPosting/GetTrucks");
						con.setRequestProperty("Host", "webservices.truckstop.com");
						con.setRequestProperty("Connection", "Keep-Alive");
						con.setRequestProperty("User-Agent", "Apache-HttpClient/4.1.1 (java 1.5)\\");

					String envolpe = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\""
							+ " xmlns:v11=\"http://webservices.truckstop.com/v11\""
							+ " xmlns:web=\"http://schemas.datacontract.org/2004/07/WebServices\""+ ">";
					String body = envolpe + "\n<soapenv:Header/>\n"
							+ "<soapenv:Body>\n" 
							+ "<v11:GetTrucks>\n"
							+ "<v11:listRequest>\n" 
							+ "<web:IntegrationId>"+session.getUserIntegrationID()+"</web:IntegrationId>\n" 
							+ "<web:Password>"+AppConstants.PASSWORD+"</web:Password>\n"
							+ "<web:UserName>"+AppConstants.USERNAME+"</web:UserName>\n" 
							+ "</v11:listRequest>\n"
							+ "</v11:GetTrucks>\n"
							+ "</soapenv:Body>\n"
							+ "</soapenv:Envelope>\n";

					//System.out.println("BODY VALUE----" + body);
					 con.setConnectTimeout(AppConstants.SOCKETTIMEOUT_ERROR);
						con.setDoOutput(true);
						DataOutputStream wr = new DataOutputStream(con.getOutputStream());
						//System.out.println("BODY VALUE----" + body);
						wr.writeBytes(body);
						wr.flush();
						wr.close();
				 
						int responseCode = con.getResponseCode();
	                    // System.out.println("Status coded-------" + statuscodes);
						if (responseCode!=200) {
	                        return AppConstants.SERVER_CONNECTION_ERROR;
	                    } else {
	                    	InputStream stream = con.getInputStream();
							String contentEncoding = con.getHeaderField("Content-Encoding");
						if (contentEncoding != null
								&& contentEncoding.equalsIgnoreCase(
										"gzip")) {
							final InputStream stream1 = new GZIPInputStream(
									stream);
							// //System.out.println("i am here");
							BufferedReader reader = new BufferedReader(
									new InputStreamReader(stream1));
							String line;
							while ((line = reader.readLine()) != null) {
								builder.append(line);
							}
							response_str = builder.toString();
							boolean loadsucess = XMLCurrentPostsparser.parsexml(response_str);

							if (loadsucess) {
								truckpostitems.clear();
								truckpostitems = XMLCurrentPostsparser.getCurrentTrucks();
								// Log.i("Currentpost","truckpostitems size-------->"+truckpostitems.size());
								return "success";
							} else {
								errormsg = XMLCurrentPostsparser
										.geterrormsg();
								return "error";
							}
						} else {
							BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
							String line;
							while ((line = reader.readLine()) != null) {
								builder.append(line);
							}
							response_str = builder.toString();
							boolean loadsucess = XMLCurrentPostsparser.parsexml(response_str);
							if (loadsucess) {
								truckpostitems.clear();
								truckpostitems = XMLCurrentPostsparser.getCurrentTrucks();
								
								return "success";
							} else {
								errormsg = XMLCurrentPostsparser.geterrormsg();
								return "error";
							}
						}
					}

				} else {
					return AppConstants.INTERNET_CONNECTION_ERROR;
				}

			} catch (Exception e) {
				e.printStackTrace();
				return AppConstants.SERVER_CONNECTION_ERROR;
			}
		}

		
		protected void onPostExecute(String result) {
			progDialog.dismiss();
			//CurrentPostScreen.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
			if (result.equals("error")) {
				
				Utils.showAlertMessage(errormsg, CurrentPostScreen.this);
				
				 * Toast.makeText(CurrentPost.this,errormsg,Toast.LENGTH_LONG).show
				 * (); finish();
				 
			} else if (result.equals(AppConstants.INTERNET_CONNECTION_ERROR)) {
				Utils.showErrorMessage(AppConstants.INTERNET_CONNECTION_ERROR,CurrentPostScreen.this);
				
				 * Toast.makeText(CopyOfCurrentPost.this,AppConstants.
				 * INTERNET_CONNECTION_ERROR,Toast.LENGTH_LONG).show();
				 * finish();
				 
			} else if (result.equals(AppConstants.SERVER_CONNECTION_ERROR)) {
				Utils.showErrorMessage(AppConstants.SERVER_CONNECTION_ERROR,CurrentPostScreen.this);
				
				 * Toast.makeText(CopyOfCurrentPost.this,AppConstants.
				 * SERVER_CONNECTION_ERROR, Toast.LENGTH_LONG).show(); finish();
				 
			} else {
				list.clear();
				if (truckpostitems.size() == 0) {
					//listerrormsg.setText("NO POSTS FOUND");
					Utils.showErrorMessage("You have no trucks currently posted!",CurrentPostScreen.this);
					// Toast.makeText(CurrentPost.this,"No Posts Found",Toast.LENGTH_LONG).show();
					// finish();
				} else {
					listerrormsg.setText("Total Posts: " + truckpostitems.size());
					adapter = new CustomAdapter(CurrentPostScreen.this,truckpostitems, res);
					currentList.setAdapter(adapter);
				}
			}
		}
	}
*/
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.contextual_menu, menu);
		MenuItem item = menu.findItem(R.id.item_delete);
		if (showdelete) {
			item.setVisible(true);
		} else {
			item.setVisible(false);
		}

		return super.onCreateOptionsMenu(menu);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
		//	NavUtils.navigateUpFromSameTask(this);
			finish();

			return true;

		case R.id.item_delete:
			int count = 0;
			deleteTruckIds = "";
			ArrayList<CurrentTruckPostsModel> countryList = truckpostitems;
			for (int i = 0; i < countryList.size(); i++) {
				CurrentTruckPostsModel country = countryList.get(i);
				if (country.isIsselected()) {
					deleteTruckIds = deleteTruckIds + "<arr:int>"
							+ country.getId() + "</arr:int>\n";
					//Log.i(TAG, country.getId());
					count++;
				}
			}
			confirmDeleteAlert(AppConstants.DELETETRUCK_CONFIRM);
			return true;

		}
		return super.onOptionsItemSelected(item);
	}

	/*********
	 * Adapter class extends with BaseAdapter and implements with
	 * OnClickListener
	 ************/
	public class CustomAdapter extends BaseAdapter implements OnClickListener {

		/*********** Declare Used Variables *********/
		private Activity activity;
		private ArrayList<CurrentTruckPostsModel> data;
		private LayoutInflater inflater = null;
		public Resources res;
		CurrentTruckPostsModel items = null;
		int i = 0;

		/************* CustomAdapter Constructor *****************/
		public CustomAdapter(Activity a, ArrayList<CurrentTruckPostsModel> d,
				Resources resLocal) {

			/********** Take passed values **********/
			activity = a;
			data = d;
			res = resLocal;

			/*********** Layout inflator to call external xml layout () ***********/
			inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		}

		/******** What is the size of Passed Arraylist Size ************/
		public int getCount() {

			if (data.size() <= 0)
				return 1;
			return data.size();
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		/********* Create a holder Class to contain inflated xml file elements *********/
		public class ViewHolder {

			public TextView origin;
			public TextView equipment;
			public TextView date;
			public TextView load;
			public CheckBox cb;

		}

		/******
		 * Depends upon data size called for each row , Create each ListView row
		 *****/
		public View getView(final int position, View convertView,
				ViewGroup parent) {

			View vi = convertView;
			ViewHolder holder;

			if (convertView == null) {

				/****** Inflate tabitem.xml file for each row ( Defined below ) *******/
				vi = inflater.inflate(R.layout.copycurrent_post, null);

				/****** View Holder Object to contain tabitem.xml file elements ******/

				holder = new ViewHolder();
				holder.origin = (TextView) vi.findViewById(R.id.postText1);
				holder.equipment = (TextView) vi.findViewById(R.id.postText2);
				holder.date = (TextView) vi.findViewById(R.id.postText3);
				holder.load = (TextView) vi.findViewById(R.id.postText4);
				holder.cb = (CheckBox) vi.findViewById(R.id.copycheck);

				/************ Set holder with LayoutInflater ************/
				vi.setTag(holder);
			} else
				holder = (ViewHolder) vi.getTag();

			if (data.size() <= 0) {
				holder.origin.setText("No Data");

			} else {
				/***** Get each Model object from Arraylist ********/
				items = null;
				items = (CurrentTruckPostsModel) data.get(position);

				/************ Set Model values in Holder elements ***********/
				holder.origin.setText(Utils.checkIsNull(items.getOriginCity()) + ", "+ Utils.checkIsNull(items.getOriginState())+" to "+Utils.checkIsNull(items.getDestinationCity()) + ", "+ Utils.checkIsNull(items.getDestinationState()));

				holder.equipment.setText(Utils.checkIsNull(items.getEquipment()));
				
				String newDateString = null;
				if (!Utils.checkIsNull(items.getDateAvailable()).equals("-")){
					newDateString=Utils.detailsDateFormat(items.getDateAvailable());
					holder.date.setText(newDateString);
				}else{
					holder.date.setText(AppConstants.EMPTY_STRING);
				}
				
				
				
				holder.load.setText(Utils.checkIsNull(items.getIsFull()));

				holder.cb.setTag(position);
				holder.cb.setChecked(data.get(position).isIsselected());
				holder.cb.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						CheckBox cb = (CheckBox) v;

						int getPosition = (Integer) cb.getTag();

						if (((CheckBox) v).isChecked()) {
							truckpostitems.get(getPosition).setIsselected(true);
							checks++;
						} else {
							truckpostitems.get(getPosition).setIsselected(false);
							checks--;
						}
						if (checks > 0) {
							showdelete = true;
							supportInvalidateOptionsMenu();
							// invalidateOptionsMenu();
							totalselectedtext.setText("Delete selected: "+ checks);
							totalselectedtext.setVisibility(View.VISIBLE);
							listerrormsg.setVisibility(View.GONE);
						} else {
							showdelete = false;
							supportInvalidateOptionsMenu();
							totalselectedtext.setVisibility(View.GONE);
							listerrormsg.setVisibility(View.VISIBLE);
						}
					}
				});

				/******** Set Item Click Listner for LayoutInflater for each row *******/

				vi.setOnClickListener(new OnItemClickListener(position));
			}
			return vi;
		}

		@Override
		public void onClick(View v) {
			//Log.v("CustomAdapter", "=====Row button clicked=====");
		}

		/********* Called when Item click in ListView ************/
		private class OnItemClickListener implements OnClickListener {
			private int mPosition;

			OnItemClickListener(int position) {
				mPosition = position;
			}

			@Override
			public void onClick(View arg0) {

				CurrentPostScreen sct = (CurrentPostScreen) activity;
				sct.onItemClick(mPosition);
			}
		}
	}

	
	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		Intent startsearch = new Intent(CurrentPostScreen.this,CurrentPostScreen.class);
		startsearch.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(startsearch);
	}
	
	

	/***************** This function used by adapter ****************/
	public void onItemClick(int mPosition) {
		CurrentTruckPostsModel selectedpost = (CurrentTruckPostsModel) truckpostitems.get(mPosition);
		Intent startpostdetailpage = new Intent(CurrentPostScreen.this,PostDetailActivity.class);
		String loadsize=Utils.checkIsNull(selectedpost.getIsFull());
		startpostdetailpage.putExtra("POSTLOADID", selectedpost.getId());
		startpostdetailpage.putExtra("LOADSIZE", loadsize);
		startActivity(startpostdetailpage);
	}
	
	
	
	
	
	
	
	public void confirmDeleteAlert(String msg) {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(CurrentPostScreen.this);

		// Setting Dialog Title
		alertDialog.setTitle("ITS TRUCKER");

		// Setting Dialog Message
		alertDialog.setMessage(msg);

		// Setting Icon to Dialog
		// alertDialog.setIcon(R.drawable.delete);

		// On pressing Ok button profile activity is started to enter profile home base
		alertDialog.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						/*RequestDeletePost requestDeletePost = new RequestDeletePost();
						requestDeletePost.execute();*/
						
						DeletePostRequest();
					}

					
				});

		// on pressing cancel button
		alertDialog.setNegativeButton("No",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		// Showing Alert Message
		alertDialog.show();
	}
	

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
	//	super.onBackPressed();
		finish();
	}
	
	 @Override
		public void onResume() {
			// TODO Auto-generated method stub
			super.onResume();
			Utils.appcameforeground(CurrentPostScreen.this);
			//Utils.startbackgroundservice(CurrentPostScreen.this);
			LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter(Utils.LOGOUT_USER));
			
		}

		@Override
		public void onPause() {
			LocalBroadcastManager.getInstance(this).unregisterReceiver( mRegistrationBroadcastReceiver);
			
			super.onPause();
		}
		@Override
		protected void onStop() {
			// TODO Auto-generated method stub
			super.onStop();
			Utils.appwentbackground(CurrentPostScreen.this);
			checks = 0;
		}
		
}
