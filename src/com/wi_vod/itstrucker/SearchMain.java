package com.wi_vod.itstrucker;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.NavUtils;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Toast;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;

import com.wi_vod.itstrucker.model.DataModel;
import com.wi_vod.itstrucker.model.DatabaseHandler;
import com.wi_vod.itstrucker.model.PlacesAutoCompleteAdapter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.widget.DatePicker;

public class SearchMain extends ActionBarActivity implements OnDialogCancelListener{
	
	private static String TAG="SEARCHMAIN";
	private Spinner originspinner,destinationspinner,searchequipmentspinner,searchtraileroptionspinner,
					hourOldspinner,sortoptionspinner,originareaspinner,destareaspinner;
	private AutoCompleteTextView originhomebase,destinationbase;
	private String statevalue,origincounrty,countryvalue,originstate="",origincity="",destinationcountry,
					destinationcity="",destinationstate="",searchradius="0",equipmenttype,searchdate,traileroption,
					hoursold,loadtype,sortype;
	private Button dateSearch,cleardate,searchtruck;
	private TextView radiustext,radius;
	private SeekBar radiuseekbar;
	private RadioGroup searchloadSizegroup;
	private RadioButton searchloadsizefull,searchloadsizeltl,searchloadsizeany,radioLoadButton;
	private CheckBox favourite;
	private int equipmenttypeindex, traileroptionindex,searchhoursindex,sorttypeindex;
	private DataModel currentsearch;
	private DatabaseHandler db;
	static boolean validated= true;
	static boolean destinationValidated= true;
	private SharedPreferences prefs;
	private int origincountryindex,profileradius,profileloadsize,profilehoursindex,searchtype,destinationcountryindex,gpsBased ;
	private String profilehomebase,intenthomebase="",originareaaselectedcode,destareaselectedcode;
	private RadioButton  searchradioLoadButton;
	private int intentcountryindex;
	private int originareaaselected=0,destareaselected=0;
	private String validationErrorMessage="";
	PlacesAutoCompleteAdapter adapter;
	String country_array[];
	String country_area[];
	String equipment[];
	String trailertype[];
	String sortoptionstype[];
	String countryforplaces[];
	String origincountryarray[]; 
	private Date todays;
	String areacode;
	private MultiSelectionSpinner equipmentType_multiSelect_Spinner,trailerOptions_multiSelect_Spinner;
	private String equipmentTypeStr, trailerOptionStr;
	private TextView txtEquipment,txtTrailerOption;
	boolean origin, destination;
	private String textwatcher;
	private boolean printDate;
	private DatePickerDialog dialog;
	private String corigin, cdest;
	private boolean tempDateFlag;
	int yy=0,mm=0,dd=0;
	private int year;
	private int month;
	private int day;
	boolean originvalidate,dstvalidate;
	private SessionManager session;
	
	static final int DATE_DIALOG_ID = 999;
	private BroadcastReceiver mRegistrationBroadcastReceiver;
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {

		View v = getCurrentFocus();
		boolean ret = super.dispatchTouchEvent(event);

		if (v instanceof EditText) {
			View w = getCurrentFocus();
			int scrcoords[] = new int[2];
			w.getLocationOnScreen(scrcoords);
			float x = event.getRawX() + w.getLeft() - scrcoords[0];
			float y = event.getRawY() + w.getTop() - scrcoords[1];

			if (event.getAction() == MotionEvent.ACTION_UP
					&& (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w
							.getBottom())) {

				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
						.getWindowToken(), 0);
			}
		}
		return ret;
	}
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.itssearch);
		UpdateUI();
		session = new SessionManager(SearchMain.this);
		prefs = getSharedPreferences("itstrucker", 0);
		db = new DatabaseHandler(this);
		 getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		 getSupportActionBar().setHomeButtonEnabled(true);
		getWindow().setSoftInputMode(WindowManager. LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		Bundle bundle = getIntent().getExtras();
		if (bundle != null) {
			currentsearch= bundle.getParcelable("searchobject");
			searchtype = bundle.getInt("Searchtype", 0);
			gpsBased=bundle.getInt("GPSBASED", 0);
			intenthomebase = bundle.getString("originname");
			intentcountryindex = bundle.getInt("countryindex");
		}
		if (searchtype!=4) {
			UpdateSearchValues();
		}else{
			UpdateCurrentsearchvalues();
		}
		
		mRegistrationBroadcastReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Utils.logoutuser(AppConstants.LOGOUTUSER_ERRORMSG, SearchMain.this);
			}
		};
	}
	
	private void initializeResources() {
		country_array = getResources().getStringArray(R.array.country_array);
		country_area = getResources().getStringArray(R.array.area_AB);
		equipment = getResources().getStringArray(R.array.type_AB_array);
		trailertype = getResources().getStringArray(R.array.option_array);
		sortoptionstype = getResources().getStringArray(R.array.sorttype_array);
		countryforplaces = getResources().getStringArray(R.array.googlecountry_array);
		origincountryarray= getResources().getStringArray( R.array.country_ab_array);
	}
	
	
	/**update Current search values.
	 * 
	 * 
	 */
	
	private void UpdateCurrentsearchvalues() {

		
		if (currentsearch.getOriginareaselected()==1) {
			origincountryindex= Arrays.asList(origincountryarray).indexOf(currentsearch.getOriginstate());
			originspinner.setSelection(origincountryindex);
			originhomebase.setText(currentsearch.getOriginstate());
			originhomebase.setEnabled(false);
		}else{
			 origincountryindex = Arrays.asList(origincountryarray).indexOf( currentsearch.getOrigincounrty());
			 originvalidate = true;
	            originspinner.setSelection(origincountryindex);
	            corigin = Utils.checkIsNullforPosts(currentsearch.getOrigincity()) + "," + Utils.checkIsNullforPosts(currentsearch.getOriginstate());
	            corigin = Utils.formatString(corigin);
	            
	                    originhomebase.setText(corigin);
	              
	            if (gpsBased == 1 || gpsBased == 2) {
	                originhomebase.setEnabled(false);
	                originspinner.setEnabled(false);
	            } else {
	                originhomebase.setEnabled(true);
	            }
			
		}
		
		if (currentsearch.getDestareaselected()==1) {
			destinationcountryindex= Arrays.asList(origincountryarray).indexOf(currentsearch.getDestinationstate());
			destinationspinner.setSelection(destinationcountryindex);
			destinationbase.setText(currentsearch.getDestinationstate());
			destinationbase.setEnabled(false);
		}else{
			destinationcountryindex= Arrays.asList(origincountryarray).indexOf(currentsearch.getDestinationcountry());
			
			destinationspinner.setSelection(destinationcountryindex);
			cdest = Utils.checkIsNullforPosts(currentsearch .getDestinationcity()) + ","+ Utils.checkIsNullforPosts(currentsearch.getDestinationstate());
	        cdest = Utils.formatString(cdest);
	        dstvalidate = true;
	       destinationbase.setText(cdest);
	              
	            
			if (gpsBased==1) {
				destinationbase.setEnabled(false);
				destinationspinner.setEnabled(false);
			}else{
				destinationbase.setEnabled(true);
			}
			
		}
		
		if (!Utils.checkIsNullforPosts(currentsearch.getEquipmenttype()).equals("")) {
			
		    if(currentsearch.getEquipmenttype().length() != 0){
		        txtEquipment.setVisibility(View.GONE);
		        equipmentType_multiSelect_Spinner.setVisibility(View.VISIBLE);
	            
    		    String equipTypeString = Utils.getSelectedEquipmentTypeNames(getApplicationContext(), currentsearch.getEquipmenttype()) ;
    		    equipmentType_multiSelect_Spinner.setSelection(Utils.getSelectedItems(equipTypeString,AppConstants.TOKEN_DELIMETER));
    		    equipmentType_multiSelect_Spinner.setCount(Utils.getSelectedItems(equipTypeString).size());
    	        equipmentType_multiSelect_Spinner.setSelectedIds(Utils.getSelectedEquipmentTypeIndices(getApplicationContext(), Utils.getSelectedItems(equipTypeString)));
		    }else{
		        txtEquipment.setVisibility(View.VISIBLE);
                equipmentType_multiSelect_Spinner.setVisibility(View.GONE);
		    }
		}
		
		if (!Utils.checkIsNullforPosts(currentsearch.getTraileroption()).equals("")) {

		    if(currentsearch.getTraileroption().length() != 0){
		        txtTrailerOption.setVisibility(View.GONE);
		        trailerOptions_multiSelect_Spinner.setVisibility(View.VISIBLE);
		        
		        ArrayList<String> selectedTrailerOptionList = Utils.getSelectedItems(currentsearch.getTraileroption(),",");
    		    trailerOptions_multiSelect_Spinner.setSelection(selectedTrailerOptionList);
    	        trailerOptions_multiSelect_Spinner.setCount(selectedTrailerOptionList.size());
    	        trailerOptions_multiSelect_Spinner.setSelectedIds(Utils.getSelectedTrailerOptionIndices(getApplicationContext(), Utils.getSelectedItems(currentsearch.getTraileroption())));
		    }else{
		        txtTrailerOption.setVisibility(View.VISIBLE);
		        trailerOptions_multiSelect_Spinner.setVisibility(View.GONE);
            }    
		}
		if (!Utils.checkIsNullforPosts(currentsearch.getHoursold()).equals("")) {
			//Log.i(TAG, currentsearch.getTraileroption());
			profilehoursindex = Arrays.asList(trailertype).indexOf(currentsearch.getHoursold());
			hourOldspinner.setSelection(profilehoursindex);
		}
		
		if (!Utils.checkIsNullforPosts(currentsearch.getSortype()).equals("")) {
			//Log.i(TAG, currentsearch.getSortype());
			sorttypeindex = Arrays.asList(sortoptionstype).indexOf(currentsearch.getSortype());
			sortoptionspinner.setSelection(sorttypeindex);
		}
		
		
		if (!Utils.checkIsNullforPosts(currentsearch.getSearchdate()).equals("")) {
			if (currentsearch.getSearchdate().equals("0000-00-00")) {
				dateSearch.setText(AppConstants.setdate);
				//cleardate.setVisibility(View.GONE);
			}else{
				dateSearch.setText(currentsearch.getSearchdate());
				//cleardate.setVisibility(View.VISIBLE);
			}
		}
		
		if (currentsearch.getLoadtype().equals("Full")) {
			searchloadsizefull.setChecked(true);
		}else if (currentsearch.getLoadtype().equals("Partial")) {
			searchloadsizeltl.setChecked(true);
		}else{
			searchloadsizeany.setChecked(true);
		}
		
		if (currentsearch.getSearchradius().equals("0")) {
			radiustext.setText("25 mi");
			searchradius="25";
		}else{
			radiuseekbar.setProgress(Integer.parseInt(currentsearch.getSearchradius())-25);
		}
		
		
	}


	/**update layout fields using sharedprefs values, Using profile saved values Search fields will be updated.
	 * 
	 * 
	 */
	public void UpdateSearchValues() {
		origincountryindex=prefs.getInt("country", 0);
		profilehomebase = prefs.getString("homebase", "");
		equipmentTypeStr = prefs.getString("equipmentindex", AppConstants.EMPTY_STRING);
        trailerOptionStr = prefs.getString("traileroptions", AppConstants.EMPTY_STRING);
		profileradius=prefs.getInt("radius", 0);
		profileloadsize=prefs.getInt("loadsizetype", 0);
		profilehoursindex=prefs.getInt("hoursold", 0);
		sorttypeindex=prefs.getInt("sorttype", 0);
		
		if (searchtype==1) {
			destinationspinner.setSelection(origincountryindex);
			destinationbase.setText(profilehomebase);	
			destinationspinner.setEnabled(false);
			destinationbase.setEnabled(false);
			updategooleplacesfordest(countryforplaces[origincountryindex],origincountryindex);
		}
		if (searchtype==1 ||searchtype==2) {
			originspinner.setSelection(intentcountryindex);
			originhomebase.setText(intenthomebase);
			originspinner.setEnabled(false);
			originhomebase.setEnabled(false);
			updategooleplacesfororigin(countryforplaces[intentcountryindex],intentcountryindex);
		}
		if (searchtype==3) {
			originspinner.setSelection(intentcountryindex);
		//	originhomebase.setText(intenthomebase);	
			 originvalidate = true ;
	         originhomebase.setText(intenthomebase);
	           
			updategooleplacesfororigin(countryforplaces[intentcountryindex],intentcountryindex);
		}
		// To retain the previously selected items
		    if(equipmentTypeStr.length() != 0 && !equipmentTypeStr.equals("-") ){
	            txtEquipment.setVisibility(View.GONE);
	            equipmentType_multiSelect_Spinner.setVisibility(View.VISIBLE);
		       
				ArrayList<String> selectedEquipmentTypeList = Utils.getSelectedItems(equipmentTypeStr);
				equipmentType_multiSelect_Spinner.setSelection(selectedEquipmentTypeList);
				equipmentType_multiSelect_Spinner.setCount(selectedEquipmentTypeList.size());
				equipmentType_multiSelect_Spinner.setSelectedIds(Utils.getSelectedEquipmentTypeIndices(getApplicationContext(), selectedEquipmentTypeList));
		    }else{
		        txtEquipment.setVisibility(View.VISIBLE);
                equipmentType_multiSelect_Spinner.setVisibility(View.GONE);
		    }
		    
		    if(trailerOptionStr.length() != 0 && !trailerOptionStr.equals("-")){
		        txtTrailerOption.setVisibility(View.GONE);
                trailerOptions_multiSelect_Spinner.setVisibility(View.VISIBLE);
                
				ArrayList<String> selectedTrailerOptionList = Utils.getSelectedItems(trailerOptionStr,",");
				trailerOptions_multiSelect_Spinner.setSelection(selectedTrailerOptionList);
				trailerOptions_multiSelect_Spinner.setCount(selectedTrailerOptionList.size());
				trailerOptions_multiSelect_Spinner.setSelectedIds(Utils.getSelectedTrailerOptionIndices(getApplicationContext(), selectedTrailerOptionList));
		    }else{
		        txtTrailerOption.setVisibility(View.VISIBLE);
		        trailerOptions_multiSelect_Spinner.setVisibility(View.GONE);
            }
		    
		hourOldspinner.setSelection(profilehoursindex);
		sortoptionspinner.setSelection(sorttypeindex);
		if (profileloadsize==1) {
			searchloadsizeltl.setChecked(true);
		}else if (profileloadsize==2) {
			searchloadsizefull.setChecked(true);
		}else{
			searchloadsizeany.setChecked(true);
		}
		
		if (profileradius==0) {
			radiustext.setText("25 mi");
			searchradius="25";
		}else{
			radiuseekbar.setProgress(profileradius-25);
		}
}

	/**Initialize views that was identified by the id attribute from the XML that was processed in onCreate.
	 * ie.,(R.layout.itssearch).
	 * 
	 */
	private void UpdateUI() {
		originspinner = (Spinner) findViewById(R.id.searchorigincountryspinner);
		destinationspinner = (Spinner) findViewById(R.id.searchdestcountryspinner);
		equipmentType_multiSelect_Spinner = (MultiSelectionSpinner)findViewById(R.id.equipmenttypes_multiselect_spinner);
        trailerOptions_multiSelect_Spinner = (MultiSelectionSpinner)findViewById(R.id.traileroptions_multiselect_spinner);
        
        txtEquipment = (TextView)findViewById(R.id.et_spinner_txt);
        
        txtEquipment.setOnClickListener(new OnClickListener() {
            
            @Override
            public void onClick(View v) {
                equipmentType_multiSelect_Spinner.setVisibility(View.VISIBLE);
                txtEquipment.setVisibility(View.GONE);
                equipmentType_multiSelect_Spinner.performClick();  
            }
        });
        
        txtTrailerOption = (TextView)findViewById(R.id.tr_spinner_txt);
        
        txtTrailerOption.setOnClickListener(new OnClickListener() {
            
            @Override
            public void onClick(View v) {
                trailerOptions_multiSelect_Spinner.setVisibility(View.VISIBLE);
                txtTrailerOption.setVisibility(View.GONE);
                trailerOptions_multiSelect_Spinner.performClick();  
            }
        });
        
		hourOldspinner = (Spinner) findViewById(R.id.searchhouroldspinner);
		sortoptionspinner = (Spinner) findViewById(R.id.searchsortoptions);
		originhomebase = (AutoCompleteTextView) findViewById(R.id.searchhomebase);
		destinationbase = (AutoCompleteTextView) findViewById(R.id.searchdestination);
		dateSearch = (Button) findViewById(R.id.searchpickdate);
		cleardate= (Button) findViewById(R.id.searchcleardate);
		searchloadSizegroup = (RadioGroup) findViewById(R.id.searchroadiogrouploadsize);
		searchloadsizefull = (RadioButton) findViewById(R.id.searchradioloadsizefull);
		searchloadsizeltl = (RadioButton) findViewById(R.id.searchradioloadsizeltl);
		searchloadsizeany = (RadioButton) findViewById(R.id.searchradioloadsizeany);
		radiuseekbar = (SeekBar) findViewById(R.id.searchradiusseekBar);
		radiustext = (TextView) findViewById(R.id.searchseekbarvalue);
		radius = (TextView) findViewById(R.id.searchradiusheader);
		
		favourite = (CheckBox) findViewById(R.id.checkBoxfavorite);
		searchtruck = (Button) findViewById(R.id.searchtruck);
		initializeResources();
		DateFormat df= new SimpleDateFormat("yyyy-MM-dd");
		todays=new Date();
		dateSearch.setText(df.format(todays));
		cleardate.setVisibility(View.GONE);
		
		searchtruck.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
					String origin=originhomebase.getText().toString();
					String destination=destinationbase.getText().toString();
					searchdate=dateSearch.getText().toString();
					
					// Selected Equipment OPtions
					List<Integer> selectedIndices = equipmentType_multiSelect_Spinner.getSelectedIndicies();
					equipmenttype = Utils.getSelectedEquipmentTypes(SearchMain.this, selectedIndices);
	                
					if (equipmenttype!=null && equipmenttype.contains(";")) {
					    equipmenttype = equipmenttype.replace(AppConstants.TOKEN_DELIMETER, ",");
                    }else if(equipmenttype == null || equipmenttype.length() == 0){
                        equipmenttype = AppConstants.EMPTY_STRING ;
                    }
					
					
					if (searchradius.equals("0")) {
						searchradius="25";
					}
					
	                 // Selected Trailer OPtions
	                traileroption = trailerOptions_multiSelect_Spinner.getSelectedItemsAsString();
	                
	                if (traileroption!=null && traileroption.contains(";")) {
	                    traileroption = traileroption.replace(AppConstants.TOKEN_DELIMETER, ",");
                    }else if(traileroption == null || traileroption.length() == 0){
                        traileroption = AppConstants.EMPTY_STRING ;
                    }
	                
					profileloadsize = searchloadSizegroup.getCheckedRadioButtonId();
					radioLoadButton = (RadioButton) findViewById(profileloadsize);
					loadtype=radioLoadButton.getText().toString();
					if (searchdate.equals(AppConstants.setdate)) {
						searchdate="0000-00-00";
					}
					if (loadtype.equals("LTL")) {
						loadtype="Partial";
					} else if (loadtype.equals("Any")) {
						loadtype="All";
					} 
					
					origincountryindex =originspinner.getSelectedItemPosition();
					StringBuilder originstr = new StringBuilder();
					if (origincountryindex>2) {
						origincounrty=getCountryForAreaSelected(origincountryindex);
						originstate=origincountryarray[origincountryindex];
						origincity="";
						originareaaselected=1;
						originareaaselectedcode=areacode;
					}else{
						origincounrty=getCountryForAreaSelected(origincountryindex);
						if (origin.isEmpty()) {
							originstate="";
							origincity="";
						}else{
							int count=0;
							StringTokenizer st = new StringTokenizer(origin, ",");
							while (st.hasMoreElements()) {
								String searchorigin=(String)st.nextElement();
								searchorigin=searchorigin.trim();
								if(!searchorigin.matches("^[a-zA-Z\\s]+")){
									validated=false;
								}else if (count==0 && !Utils.searchstate(SearchMain.this,searchorigin,origincountryindex)) {
									origincity=searchorigin;
								}else{
									if (!Utils.searchstate(SearchMain.this,searchorigin,origincountryindex)) {
										validated=false;
										//validationErrorMessage+=AppConstants.ORIGIN_NOTFOUND;
									}else{
										if (count==0 && Utils.searchstate(SearchMain.this,searchorigin,origincountryindex)) {
											origincity="";
										}
										originstr.append(","+Utils.statevalue);
									}
								}
								count++;
							}
							originstate=Utils.formatString(originstr.toString());
							if (!validated) {
								validationErrorMessage+=AppConstants.ORIGIN_NOTFOUND;
							}
						}
					}
					
					destinationcountryindex=destinationspinner.getSelectedItemPosition();
					StringBuilder dststr = new StringBuilder();
					if (destinationcountryindex>2) {
						destinationcountry=getCountryForAreaSelected(destinationcountryindex);
						destinationstate=origincountryarray[destinationcountryindex];
						destinationcity="";
						destareaselected=1;
						destareaselectedcode=areacode;
					}else{
						destinationcountry=getCountryForAreaSelected(destinationcountryindex);
						if (destination.isEmpty()) {
							destinationstate="";
							destinationcity="";
						}else{
							int count=0;
							StringTokenizer st = new StringTokenizer(destination, ",");
							while (st.hasMoreElements()) {
								String searchdestination=(String)st.nextElement();
								searchdestination=searchdestination.trim();
								if(!searchdestination.matches("^[a-zA-Z\\s]+")){
									destinationValidated=false;
								}else if (count==0 && !Utils.searchstate(SearchMain.this,searchdestination,destinationcountryindex)) {
									destinationcity=searchdestination;
								}else{
									if (!Utils.searchstate(SearchMain.this,searchdestination,destinationcountryindex)) {
										destinationValidated=false;
									}else{
										if (count==0 && Utils.searchstate(SearchMain.this,searchdestination,destinationcountryindex)) {
											destinationcity="";
										}
										dststr.append(","+Utils.statevalue);
									}
								}
								count++;
							}
							destinationstate=Utils.formatString(dststr.toString());
							if (!destinationValidated) {
								validationErrorMessage+="\n"+AppConstants.DESTINATION_NOTFOUND;
							}
						}
					}
					
				if (validated && destinationValidated) {
					StringBuilder stringBuilder = new StringBuilder();
					stringBuilder.append(origincounrty + originstate+ origincity + destinationcountry + destinationcity
							+ destinationstate + searchradius + equipmenttype+ searchdate + traileroption + hoursold 
							+ loadtype+ sortype);
					//Log.d(TAG, "traileroption--->"+traileroption);

					String md = Utils.md5(stringBuilder.toString());
					
					Log.d(TAG, origincounrty+" , " + originstate+" , "+ origincity+" , " + destinationcountry+" , " + destinationcity
							+" , "+ destinationstate+" , " + searchradius+" , " + equipmenttype+" , "+ searchdate+" , " + traileroption+" , " + hoursold 
							+" , "	+ loadtype+" , "+ sortype+" , "+md);
					
					DataModel fav = new DataModel(origincounrty, originstate,origincity, destinationcountry, destinationcity,
							destinationstate, searchradius, equipmenttype,searchdate, traileroption, hoursold, loadtype,
							sortype, originareaaselectedcode,destareaselectedcode, originareaaselected,destareaselected, md);
					
					
				//	Log.d(TAG, "md in searchmain -->"+md);
					if (favourite.isChecked()) {
						
						if (db.addTofavourite(fav, md)) {
							Toast.makeText(getApplicationContext(), AppConstants.ALREADY_FAVORITE, Toast.LENGTH_LONG).show();
						}else{
							Toast.makeText(getApplicationContext(), AppConstants.ADD_AS_FAVORITE, Toast.LENGTH_LONG).show();
						}
						
					}
					Intent startsearch = new Intent(SearchMain.this,SearchResultActivty.class);
					startsearch.putExtra("searchobject", fav);
					startsearch.putExtra("md5value", md);
					startActivity(startsearch);
					originstate = "";
					destinationstate = "";

				} else {
					validated = true;
					destinationValidated = true;
					Utils.showAlertMessage(validationErrorMessage,SearchMain.this);
					validationErrorMessage="";
					
					// Toast.makeText(SearchMain.this, AppConstants.INVALID_SERACHFORMAT ,Toast.LENGTH_SHORT).show();
				}
 			}
		});
		
		
		dateSearch.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
					DialogFragment newFragment = new SelectDateFragment();
					newFragment.show(getSupportFragmentManager(), "DatePicker");
				}else{
					showDialog(DATE_DIALOG_ID);
				}
				
			}
		});
		
		// Set Adapter for origincountry
		ArrayAdapter<CharSequence> originDataAdapter = ArrayAdapter.createFromResource(SearchMain.this, R.array.country_array, android.R.layout.simple_spinner_dropdown_item);
		originDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		originspinner.setAdapter(originDataAdapter);
		originspinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				origincountryindex = arg0.getSelectedItemPosition();
				origincounrty=origincountryarray[origincountryindex];
				if (origincountryindex>2) {
					originhomebase.setText(origincounrty);
					originhomebase.setEnabled(false);
				}else{
					if (gpsBased==1||gpsBased==2) {
						originhomebase.setEnabled(false);
					}else{
						originhomebase.setEnabled(true);
					if(originvalidate){
						originvalidate = false;
					}
					else{
						originhomebase.setText("");
					}
	                        updategooleplacesfororigin( countryforplaces[origincountryindex],  origincountryindex);
	                    }
	                }
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});
		// Set Adapter for destinationcountry
				ArrayAdapter<CharSequence> destinationDataAdapter = ArrayAdapter.createFromResource(SearchMain.this, R.array.country_array,android.R.layout.simple_spinner_dropdown_item);
				destinationDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				destinationspinner.setAdapter(destinationDataAdapter);
				destinationspinner.setOnItemSelectedListener(new OnItemSelectedListener() {
					@Override
					public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
						
						destinationcountryindex = arg0.getSelectedItemPosition();
						destinationcountry=origincountryarray[destinationcountryindex];
						if (destinationcountryindex>2) {
							destinationbase.setText(destinationcountry);
							destinationbase.setEnabled(false);
						}else{
							if (searchtype==1||gpsBased==1) {
								destinationbase.setEnabled(false);
							}else{
								destinationbase.setEnabled(true);
	                                if (dstvalidate) {
										dstvalidate=false;
									}else{
										 destinationbase.setText("");
									}
	                                updategooleplacesfordest(countryforplaces[destinationcountryindex],destinationcountryindex);
	                            }
	                        }
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {
					}
		});		
				
		radiuseekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {
				int yourStep = 25;
				int profileradius = ((int) Math.round(progress / yourStep))* yourStep;
				profileradius+=25;
				seekBar.setProgress(progress);
				radiustext.setText(String.valueOf(profileradius) + " mi");
				searchradius = String.valueOf(profileradius);
			}

			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			public void onStopTrackingTouch(SeekBar seekBar) {
			}
		});
		
		originhomebase.addTextChangedListener(new TextWatcher() {
			@Override
			   public void onTextChanged(CharSequence s, int start, int before,int count) {
			    textwatcher = String.valueOf(s);
			    //origin=true;
			    if (origincountryindex > 2 && destinationcountryindex>2) {
			    	origin = false;
			    	radiusVisibility();
			    }else if(textwatcher.equals("")){
			    	origin = false ;
			    	radiusVisibility();
			    }else if(textwatcher.contains(",")){
			     int num = textwatcher.replaceAll("[^,]","").length();
			     if (num==1) {
			      String[] tokens = textwatcher.split(",");
			      boolean statevalidated= Utils.searchstate(SearchMain.this,tokens[0],origincountryindex);
			      boolean statevalidated2=false;
			      try {
			        statevalidated2=Utils.searchstate(SearchMain.this,tokens[1],origincountryindex);
			      } catch (Exception e) {
			      }
			      if (!statevalidated&&statevalidated2) {
			    	  origin = true;
			    	  radiusVisibility();
			      }else{
			    	  origin = false;
			    	  radiusVisibility();
			      }
			     }
			     
			    }else{
			    	origin = false;
			    	radiusVisibility();
			    }
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}

		});
		destinationbase.addTextChangedListener(new TextWatcher() {

			
			@Override
			   public void onTextChanged(CharSequence s, int start, int before,int count) {
			    textwatcher = String.valueOf(s);
			    //origin=true;
			    if (destinationcountryindex > 2 && origincountryindex >2) {
			    	destination = false;
			    	radiusVisibility();
			    }else if(textwatcher.equals("")){
			    	destination = false ;
			    	radiusVisibility();
			    }else if(textwatcher.contains(",")){
			     int num = textwatcher.replaceAll("[^,]","").length();
			     if (num==1) {
			      String[] tokens = textwatcher.split(",");
			      boolean statevalidated= Utils.searchstate(SearchMain.this,tokens[0],destinationcountryindex);
			      boolean statevalidated2=false;
			      try {
			        statevalidated2=Utils.searchstate(SearchMain.this,tokens[1],destinationcountryindex);
			      } catch (Exception e) {
			      }
			      if (!statevalidated&&statevalidated2) {
			    	  destination = true;
			    	  radiusVisibility();
			      }else{
			    	  destination = false;
			    	  radiusVisibility();
			      }
			     }
			     
			    }else{
			    	destination = false;
			    	radiusVisibility();
			    }
			}
	
			

			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		
		
    		// Set Adapter for EquipmentType
            String[] equipString = getResources().getStringArray(R.array.type_array);
            int etSize = equipString.length ;
        
//        if(equipmentType_multiSelect_Spinner.getVisibility() == View.VISIBLE){
            if(null != equipString && etSize > 0){
                equipmentType_multiSelect_Spinner.setSelectType(AppConstants.SELECT_LIMIT);
                equipmentType_multiSelect_Spinner.setItems(equipString);
                equipmentType_multiSelect_Spinner.setmDialogCancelListener(this);
            }
//        }
    		// Set Adapter for TrailerOptions
            String[] trailerOptions = getResources().getStringArray(R.array.option_array);
            int trOpSize = trailerOptions.length ;
            if(null != trailerOptions && trOpSize > 0){
                trailerOptions_multiSelect_Spinner.setSelectType(AppConstants.SELECT_MAX);
                trailerOptions_multiSelect_Spinner.setItems(trailerOptions);
                trailerOptions_multiSelect_Spinner.setmDialogCancelListener(this);
            }

				// Set Adapter for HoursOld
				ArrayAdapter<CharSequence> hourOldDataAdapter = ArrayAdapter.createFromResource(SearchMain.this, R.array.hoursold_array,
								android.R.layout.simple_spinner_dropdown_item);
				hourOldDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				hourOldspinner.setAdapter(hourOldDataAdapter);
				hourOldspinner.setOnItemSelectedListener(new OnItemSelectedListener() {
					@Override
					public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
						searchhoursindex = arg0.getSelectedItemPosition();

						// storing string resources into Array 
						String hoursoldarray[] = getResources().getStringArray(R.array.hoursold_array);
						hoursold=hoursoldarray[searchhoursindex];
						if (hoursold.equals("Any")) {
							hoursold="0";
						}
						/*Toast.makeText(getActivity(), "You have selected : " +hoursoldarray[searchhoursindex], 
						Toast.LENGTH_SHORT).show();*/
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {
					}
				});

				// Set Adapter for TrailerOptions
				ArrayAdapter<CharSequence> sortTypeDataAdapter = ArrayAdapter.createFromResource(SearchMain.this, R.array.sorttype_array_regular,
								android.R.layout.simple_spinner_dropdown_item);
				sortTypeDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				sortoptionspinner.setAdapter(sortTypeDataAdapter);
				
				sortoptionspinner.setOnItemSelectedListener(new OnItemSelectedListener() {
					@Override
					public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
						sorttypeindex = arg0.getSelectedItemPosition();

						// storing string resources into Array 
						String sorttypearray[] = getResources().getStringArray(R.array.sorttype_array);
						sortype=sorttypearray[sorttypeindex];
						/*Toast.makeText(getActivity(), "You have selected : " +sorttype[sorttypeindex], 
						Toast.LENGTH_SHORT).show();*/
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {
					}
				});
	}
	

	public void clearDate(View v){
		dateSearch.setText(AppConstants.setdate);
		//cleardate.setVisibility(View.GONE);
	//	searchdate=dateSearch.getText().toString();
	}
	
	public  void populateSetDate(int year, int month, int day) {
		
		SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		try {
			Date date = outFormat.parse(String.valueOf(year)+"-"+String.valueOf(month)+"-"+String.valueOf(day));
			searchdate=outFormat.format(date).toString();
			//System.out.println("populateSetDate using date formattesr--"+searchdate);
			dateSearch.setText(searchdate);
			//cleardate.setVisibility(View.VISIBLE);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		}
	
	

	 @SuppressLint("ValidFragment")
	    public  class SelectDateFragment extends DialogFragment implements CustomDatePickerDialog.OnDateSetListener {
	        
	        @Override
	        public Dialog onCreateDialog(Bundle savedInstanceState) {
	            searchdate=dateSearch.getText().toString();
	            if(searchdate!=null && !searchdate.equals(AppConstants.setdate)){
	                
	                if (searchdate.equals(AppConstants.setdate)) {
	                    final Calendar calendar = Calendar.getInstance();
	                    yy = calendar.get(Calendar.YEAR);
	                    mm = calendar.get(Calendar.MONTH);
	                    dd = calendar.get(Calendar.DAY_OF_MONTH);
	                }else{
	                    String[]dateparts = searchdate.split("-");
	                    yy = Integer.parseInt(dateparts[0]); //  
	                    mm = Integer.parseInt(dateparts[1])-1; //  
	                    dd=Integer.parseInt(dateparts[2]);
	                }
	                
	             
	                if(Build.MANUFACTURER.contains(AppConstants.SAMSUNG) || Build.BRAND.contains(AppConstants.SAMSUNG)){
	 
	 
	                       dialog =new CustomDatePickerDialog(getActivity(), datePickerListener, yy, mm, dd);
	                       dialog.setButton(DialogInterface.BUTTON_NEUTRAL, "Clear", new DialogInterface.OnClickListener() {
	 
	                           public void onClick(DialogInterface dialog, int id) {
	                               tempDateFlag = false ;
	                               dateSearch.setText(AppConstants.setdate);
	                               /*DatePicker datePicker = ((DatePickerDialog) dialog).getDatePicker();
	                             datePickerListener.onDateSet(datePicker,
	                                       datePicker.getYear(),
	                                       datePicker.getMonth(),
	                                       datePicker.getDayOfMonth());*/
	                               dialog.dismiss();
	                           }
	                       });
	                       
	                      dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Set", new DialogInterface.OnClickListener() {
	 
	                          @SuppressLint("NewApi")
							public void onClick(DialogInterface dialog, int id) {
	                              tempDateFlag = true;
	                              DatePicker datePicker = ((DatePickerDialog) dialog).getDatePicker();
	                              datePickerListener.onDateSet(datePicker,
	                                      datePicker.getYear(),
	                                      datePicker.getMonth(),
	                                      datePicker.getDayOfMonth());
	                              dialog.dismiss();
	                          }
	                      });
	                }else{
	                    dialog =new CustomDatePickerDialog(getActivity(), this, yy, mm, dd);
	                    dialog.setButton(DialogInterface.BUTTON_NEUTRAL, "Clear", new DialogInterface.OnClickListener() {
	                        public void onClick(DialogInterface dialog, int id) {
	                            tempDateFlag = false ;
	                            dateSearch.setText(AppConstants.setdate);
	                            dialog.dismiss();
	                        }
	                    });
	                    
	                   dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Set", new DialogInterface.OnClickListener() {
	                       @SuppressLint("NewApi")
						public void onClick(DialogInterface dialog, int id) {
	                           tempDateFlag = true;
	                           DatePicker datePicker = ((DatePickerDialog) dialog).getDatePicker();
	                              datePickerListener.onDateSet(datePicker,
	                                      datePicker.getYear(),
	                                      datePicker.getMonth(),
	                                      datePicker.getDayOfMonth());
	                           dialog.dismiss();
	                       }
	                   });
	                }
	                  
	                  
	            }  
	            return  dialog;
	        }
	 
	        @Override
	        public void onDateSet(DatePicker dp, int yy, int mm, int dd) {
	 
	                if(tempDateFlag){
	                    populateSetDate(yy, mm + 1, dd);
	                }else{
	                    tempDateFlag = true;
	                }
	        }
	        
	        final DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
	            // when dialog box is closed, below method will be called.
	            public void onDateSet(DatePicker dp, int yy,
	                    int mm, int dd) {
	                if(tempDateFlag){
	                    populateSetDate(yy, mm + 1, dd);
	                }else{
	                    tempDateFlag = true;
	                }
	            }
	        };
	 
	    }
	 
	 
	 private DatePickerDialog dateDialog ;
	 
	 @Override
		protected Dialog onCreateDialog(int id) {
			switch (id) {
			case DATE_DIALOG_ID:
			   // set date picker as current date
			    searchdate=dateSearch.getText().toString();
                if(searchdate!=null && !searchdate.equals(AppConstants.setdate)){
                    
    			    if (searchdate.equals(AppConstants.setdate)) {
                        final Calendar calendar = Calendar.getInstance();
                        yy = calendar.get(Calendar.YEAR);
                        mm = calendar.get(Calendar.MONTH);
                        dd = calendar.get(Calendar.DAY_OF_MONTH);
                    }else{
                        String[]dateparts = searchdate.split("-");
                        yy = Integer.parseInt(dateparts[0]); //  
                        mm = Integer.parseInt(dateparts[1])-1; //  
                        dd=Integer.parseInt(dateparts[2]);
                    }
                }    
			   dateDialog = new DatePickerDialog(this, datePickerListener, 
	                         yy, mm, dd);
			   dateDialog.setCancelable(false);
			   dateDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
	                    new DialogInterface.OnClickListener() {
	                        @Override 
	                        public void onClick(DialogInterface dialog, int which) {
	                            dateSearch.setText(AppConstants.setdate);
	                         dialog.dismiss();
	 
	                        } 
	                    }); 
	            
	            return dateDialog;
			}
			return null;
		}
	 
	 
	 private DatePickerDialog.OnDateSetListener datePickerListener 
     = new DatePickerDialog.OnDateSetListener() {

// when dialog box is closed, below method will be called.
public void onDateSet(DatePicker view, int selectedYear,
		int selectedMonth, int selectedDay) {
	int year = selectedYear;
	int month = selectedMonth;
	int day = selectedDay;

	try {
		SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = outFormat.parse(String.valueOf(year)+"-"+String.valueOf(month+ 1)+"-"+String.valueOf(day));
		searchdate=outFormat.format(date).toString();
		//System.out.println("populateSetDate using date formattesr--"+searchdate);
		dateSearch.setText(searchdate);
		//cleardate.setVisibility(View.VISIBLE);
	} catch (ParseException e) {
		e.printStackTrace();
	}

}
};
	 
	 
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.search_main, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    // Respond to the action bar's Up/Home button
	    case android.R.id.home:
	        
	    	startHomeScreen();
	    	
	        return true;
	    }
	    return super.onOptionsItemSelected(item);
	}
	
	
	
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		startHomeScreen();
	}
	
	//start Home ITSMainActivity to refresh the screach main screen data.
	
	public void startHomeScreen(){
		NavUtils.navigateUpFromSameTask(this);
		Intent startMainActivity = new Intent(this, ITSMainScreen.class);
    	startMainActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(startMainActivity);
		finish();
	}
	
	
	public String  getCountryForAreaSelected(int index){
		String country;
		String originareaaselectedcode[]= getResources().getStringArray(R.array.area_array_select_code);
		 areacode=originareaaselectedcode[index];
		if (areacode.equals("Area CW")||areacode.equals("Area CW")||areacode.equals("Area CC")||areacode.equals("CAN")) {
			country="CAN";
		}else if (areacode.equals("Area Mx")||areacode.equals("MEX")) {
			country="MEX";
		} else {
			country="USA";
		}
		return country;
	}
	
	/*********************************************************
	 *Description: Updates  origin autocomplete for PlacesAutoCompleteAdapter with new googlecountry component and countryindex to search the entered
	 *value exists in existing state and abr list(Found in values folder)
	 * 
	 * 
	 */
	
	private void updategooleplacesfororigin(String googlecountry,int countryindex) {
		adapter=new PlacesAutoCompleteAdapter(SearchMain.this,googlecountry,countryindex, R.layout.list_item);
		originhomebase.setAdapter(adapter);
		
	}
	
	/*********************************************************
	 *Description: Updates  destination autocomplete for PlacesAutoCompleteAdapter with new googlecountry component and countryindex to search the entered
	 *value exists in existing state and abr list(Found in values folder)
	 * 
	 * 
	 */
	
	private void updategooleplacesfordest(String googlecountry,int countryindex) {
		adapter=new PlacesAutoCompleteAdapter(SearchMain.this,googlecountry,countryindex, R.layout.list_item);
		destinationbase.setAdapter(adapter);
		
	}


    @Override
    public void onDialogCancelled(int spinnerType,int type,boolean flag) {
        if(type == AppConstants.EQUIPMENT_TYPE && flag == false){
            equipmentType_multiSelect_Spinner.setVisibility(View.GONE);
            txtEquipment.setVisibility(View.VISIBLE);
        }else if(type == AppConstants.TRAILER_OPTION && flag == false){
            trailerOptions_multiSelect_Spinner.setVisibility(View.GONE);
            txtTrailerOption.setVisibility(View.VISIBLE);
            
        }
    }
    protected void radiusVisibility() {
        // TODO Auto-generated method stub
        if (origin || destination) {
            radiustext.setVisibility(View.VISIBLE);
            radiuseekbar.setVisibility(View.VISIBLE);
            radius.setVisibility(View.VISIBLE);
        } else {
            radiustext.setVisibility(View.GONE);
            radiuseekbar.setVisibility(View.GONE);
            radius.setVisibility(View.GONE);
        }
    }
    @Override
  	public void onResume() {
  		// TODO Auto-generated method stSystem.out.println(Utils.isAppIsInBackground(SearchMain.this));ub
    	
  		super.onResume();
  		//System.out.println(Utils.isAppIsInBackground(SearchMain.this));
  		Utils.appcameforeground(SearchMain.this);
  		LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter(Utils.LOGOUT_USER));
  		
  	}
    @Override
	protected void onPause() {
		
		LocalBroadcastManager.getInstance(this).unregisterReceiver( mRegistrationBroadcastReceiver);
		super.onPause();
		Utils.statevalue="";
	}
    
    @Override
    protected void onStop() {
    	//session.setappbackground(Utils.isAppIsInBackground(SearchMain.this));
    	Utils.appwentbackground(SearchMain.this);
		//System.out.println("onStop called");
    	// TODO Auto-generated method stub
    	super.onStop();
    }
  
    
 }
