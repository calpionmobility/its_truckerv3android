/******************************************************************
* ContactUs.java
* 
* Description: Class for the contact us Activity
* 
* @author: Byron C
* 
* @version: 1.0
* 
* History:
* 
* 
******************************************************************
*/
package com.wi_vod.itstrucker;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.json.JSONException;
import org.json.JSONObject;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.wi_vod.itstrucker.R;

public class ContactUs extends Fragment
{
    private static String COMPANY_PHONE = "8002032540";
    private static String EMAIL_ADDRESS = "itsmobile@truckstop.com";
    String sampleXml = "";
    
    /*********************************************************
     * Method: onCreate
     * 
     * Description: cCalled when the Activity is first created
     * 
     * @param: Bundle savedInstanceState - Current State
     * 
     * @returns: Void
     * 
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
    		Bundle savedInstanceState) {
    	
    	// TODO Auto-generated method stub
    	 View v = inflater.inflate(R.layout.contact, container, false);
    	 
    	 
    	 
    	
    	/* Button phoneButton = (Button) v.findViewById(R.id.bCallService);
         phoneButton.setOnClickListener(new View.OnClickListener()
         {

             public void onClick(View v)
             {
                 Intent dialIntent = new Intent();
                 dialIntent.setAction(Intent.ACTION_DIAL);
                 dialIntent.setData(Uri.parse("tel:" + COMPANY_PHONE));
                 startActivity(dialIntent);
             }
         }); 
         
         // Setup the event handler for the Email Customer Service button
         Button emailButton = (Button)v.findViewById(R.id.bEmailService);
         emailButton.setOnClickListener(new View.OnClickListener()
         {
             public void onClick(View v)
             {
                 Intent  mailIntent = new Intent();
                 mailIntent.setAction(Intent.ACTION_SEND);
                 mailIntent.setType("message/rfc822");
                 mailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {EMAIL_ADDRESS});
                 mailIntent.putExtra(Intent.EXTRA_SUBJECT, "Customer Service -- Sent From ITS Trucker For Android!");
                 
                 startActivity(mailIntent);
                 
             }
         }); 
         
         // Setup the event handler for the Feedback Email button
         Button feedbackButton = (Button) v.findViewById(R.id.bFeedbackEmail);
         feedbackButton.setOnClickListener(new View.OnClickListener()
         {
             public void onClick(View v)
             {
                 Intent  mailIntent = new Intent();
                 mailIntent.setAction(Intent.ACTION_SEND);
                 mailIntent.setType("message/rfc822");
                 mailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {EMAIL_ADDRESS});
                 mailIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback -- Sent From ITS Trucker For Android!");                
                 startActivity(mailIntent);
                 
             }
         }); 
         // Setup the event handler for the Call to Report a Bug button
         Button callBugButton = (Button) v.findViewById(R.id.bCallBug);
         callBugButton.setOnClickListener(new View.OnClickListener()
         {

             public void onClick(View v)
             {
                 Intent dialIntent = new Intent();
                 dialIntent.setAction(Intent.ACTION_DIAL);
                 dialIntent.setData(Uri.parse("tel:" + COMPANY_PHONE));
                 startActivity(dialIntent);
             }
         }); 
         
         // Setup the event handler for the Email to report a bug button
         Button emailBugButton = (Button) v.findViewById(R.id.bEmailBug);
         emailBugButton.setOnClickListener(new View.OnClickListener()
         {
             public void onClick(View v)
             {
                 Intent  mailIntent = new Intent();
                 mailIntent.setAction(Intent.ACTION_SEND);
                 mailIntent.setType("message/rfc822");
                 mailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {EMAIL_ADDRESS});
                 mailIntent.putExtra(Intent.EXTRA_SUBJECT, "BUG REPORT -- Sent From ITS Trucker For Android!");
                 
                 startActivity(mailIntent);
                 
             }
         }); */
         
    	return v;
    }
   
    
    
/*	private String readTextFile(InputStream inputStream) {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

		byte buf[] = new byte[1024];
		int len;
		try {
			while ((len = inputStream.read(buf)) != -1) {
				outputStream.write(buf, 0, len);
			}
			outputStream.close();
			inputStream.close();
		} catch (IOException e) {

		}
		return outputStream.toString();
	}*/
}
