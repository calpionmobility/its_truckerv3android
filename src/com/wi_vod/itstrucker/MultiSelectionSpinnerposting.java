
package com.wi_vod.itstrucker;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.util.AttributeSet;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import com.wi_vod.itstrucker.MultiSelectionSpinner.DialogButtonClickHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class MultiSelectionSpinnerposting extends Spinner implements
        OnMultiChoiceClickListener {
    String[] _items = null;
    boolean[] mSelection = null;
    int count = 0;
    List<Integer> selectedIds = new ArrayList<Integer>();
    int selectType = 0 ;
    String activityName = "";
    private static int ANY_OPTION_ID = 4 ;
    private static int NONE_OPTION_ID = 0 ;
    private static int PREVIOUS_SELECTED_OPTION_ID = -1 ;
    public OnDialogCancelListener mDialogCancelListener ;
    
    ArrayAdapter<CharSequence> simple_adapter;

    public MultiSelectionSpinnerposting(Context context) {
        super(context);

        simple_adapter = new ArrayAdapter<CharSequence>(context,
                android.R.layout.simple_spinner_dropdown_item);
        super.setAdapter(simple_adapter);
    }

    public MultiSelectionSpinnerposting(Context context, AttributeSet attrs) {
        super(context, attrs);

        simple_adapter = new ArrayAdapter<CharSequence>(context,
                android.R.layout.simple_spinner_dropdown_item);
        super.setAdapter(simple_adapter);
    }

    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
        
        if (mSelection != null && which < mSelection.length) {
            mSelection[which] = isChecked;
            
            if(mSelection[which]){
                count++;
                if(getSelectType() == AppConstants.SELECT_LIMIT){
                    
                    if(which == ANY_OPTION_ID  && isChecked){
                        count = 0;
                        for(int i=0 ; i<_items.length; i++){
                            if(i == ANY_OPTION_ID){
                                ((AlertDialog) dialog).getListView().setItemChecked(i, true);
                                mSelection[i] = true ;
                            }else{
                                ((AlertDialog) dialog).getListView().setItemChecked(i, false);
                                mSelection[i] = false ;
                            }
                        }
                    }else{
                        ((AlertDialog) dialog).getListView().setItemChecked(ANY_OPTION_ID, false);
                        mSelection[ANY_OPTION_ID] = false ;
                        selectedIds.remove(new Integer(ANY_OPTION_ID));
                    }
                    
                    if(count > 3){
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setMessage("Can't select more than 3 items")
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int pos) {
                                    }
                                })
                                .create().show();
                        mSelection[which] = false ;
                        ((AlertDialog) dialog).getListView().setItemChecked(which, false);
                        count--;
                        selectedIds.remove(new Integer(which));
                     }
                }else if(getSelectType() == AppConstants.SELECT_MAX){
                    if(which == NONE_OPTION_ID  && isChecked){
                        count = 0;
                        for(int i=0 ; i<_items.length; i++){
                            if(i == NONE_OPTION_ID){
                                ((AlertDialog) dialog).getListView().setItemChecked(i, true);
                                mSelection[i] = true ;
                            }else{
                                ((AlertDialog) dialog).getListView().setItemChecked(i, false);
                                mSelection[i] = false ;
                            }
                        }
                    }else{
                        ((AlertDialog) dialog).getListView().setItemChecked(NONE_OPTION_ID, false);
                        mSelection[NONE_OPTION_ID] = false ;
                        selectedIds.remove(new Integer(NONE_OPTION_ID));
                    }
                }else if(getSelectType() == AppConstants.SELECT_ONE){
                    if(PREVIOUS_SELECTED_OPTION_ID == -1){
                        PREVIOUS_SELECTED_OPTION_ID = which ;
                    }else{
                            if(PREVIOUS_SELECTED_OPTION_ID != which && isChecked){
                                for(int i=0 ; i<_items.length; i++){
                                    if(i == which){
                                        ((AlertDialog) dialog).getListView().setItemChecked(i, true);
                                        mSelection[i] = true ;
                                        selectedIds.remove(new Integer(PREVIOUS_SELECTED_OPTION_ID));  // Removing previous selected option
                                        PREVIOUS_SELECTED_OPTION_ID = i;
                                        
                                    }else{
                                        ((AlertDialog) dialog).getListView().setItemChecked(i, false);
                                        mSelection[i] = false ;
                                    }
                                }
                            }else if(PREVIOUS_SELECTED_OPTION_ID == which && !isChecked){
                                for(int i=0 ; i<_items.length; i++){
                                        ((AlertDialog) dialog).getListView().setItemChecked(i, false);
                                        mSelection[i] = false ;
                                        PREVIOUS_SELECTED_OPTION_ID = -1;
                                        selectedIds.remove(new Integer(PREVIOUS_SELECTED_OPTION_ID));
                                }
                            }
                     }      
                    
                }
                selectedIds.add(which);
            }else{
                if(null != selectedIds && selectedIds.size()>0){
                    if(getSelectType() == AppConstants.SELECT_ONE){
                        ((AlertDialog) dialog).getListView().setItemChecked(PREVIOUS_SELECTED_OPTION_ID, false);
                        mSelection[PREVIOUS_SELECTED_OPTION_ID] = false ;
                        selectedIds.remove(new Integer(PREVIOUS_SELECTED_OPTION_ID));
                    }else {
                        if(selectedIds.contains(which) && isChecked == false){
                            count--;
                            selectedIds.remove(new Integer(which));
                        }
                    }
                    
                }
            }
            simple_adapter.clear();
            simple_adapter.add(buildSelectedItemString());
        } else {
            throw new IllegalArgumentException(
                    "Argument 'which' is out of bounds.");
        }
    }

    @Override
    public boolean performClick() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMultiChoiceItems(_items, mSelection, this);
        builder.setPositiveButton("DONE", new DialogButtonClickHandler());
        builder.setCancelable(false);
        builder.show();
        builder.setCancelable(false);
        return true;
    }

    @Override
    public void setAdapter(SpinnerAdapter adapter) {
        throw new RuntimeException(
                "setAdapter is not supported by MultiSelectSpinner.");
    }

    public void setItems(String[] items) {
        _items = items;
        mSelection = new boolean[_items.length];
        simple_adapter.clear();
        simple_adapter.add(_items[0]);
        Arrays.fill(mSelection, false);
    }

    public void setItems(List<String> items) {
        _items = items.toArray(new String[items.size()]);
        mSelection = new boolean[_items.length];
        simple_adapter.clear();
        simple_adapter.add(_items[0]);
        Arrays.fill(mSelection, false);
    }

    public void setSelection(String[] selection) {
        for (String cell : selection) {
            for (int j = 0; j < _items.length; ++j) {
                if (_items[j].equals(cell)) {
                    mSelection[j] = true;
                }
            }
        }
    }

    public void setSelection(List<String> selection) {
        for (int i = 0; i < mSelection.length; i++) {
            mSelection[i] = false;
        }
        for (String sel : selection) {
            for (int j = 0; j < _items.length; ++j) {
                if (_items[j].equals(sel)) {
                    mSelection[j] = true;
                }
            }
        }
        simple_adapter.clear();
        simple_adapter.add(buildSelectedItemString());
    }

    public void setSelection(int index) {
        for (int i = 0; i < mSelection.length; i++) {
            mSelection[i] = false;
        }
        if (index >= 0 && index < mSelection.length) {
            mSelection[index] = true;
        } else {
            throw new IllegalArgumentException("Index " + index
                    + " is out of bounds.");
        }
        simple_adapter.clear();
        simple_adapter.add(buildSelectedItemString());
    }

    public void setSelection(int[] selectedIndicies) {
        for (int i = 0; i < mSelection.length; i++) {
            mSelection[i] = false;
        }
        for (int index : selectedIndicies) {
            if (index >= 0 && index < mSelection.length) {
                mSelection[index] = true;
            } else {
                throw new IllegalArgumentException("Index " + index
                        + " is out of bounds.");
            }
        }
        simple_adapter.clear();
        simple_adapter.add(buildSelectedItemString());
    }

    public List<String> getSelectedStrings() {
        List<String> selection = new LinkedList<String>();
        for (int i = 0; i < _items.length; ++i) {
            if (mSelection[i]) {
                selection.add(_items[i]);
            }
        }
        return selection;
    }

    public List<Integer> getSelectedIndicies() {
        List<Integer> selection = new LinkedList<Integer>();
        for (int i = 0; i < _items.length; ++i) {
            if (mSelection[i]) {
                selection.add(i);
            }
        }
        return selection;
    }

    private String buildSelectedItemString() {
        StringBuilder sb = new StringBuilder();
        boolean foundOne = false;

        for (int i = 0; i < _items.length; ++i) {
            if (mSelection[i]) {
                if (foundOne) {
                    sb.append("; ");
                }
                foundOne = true;
              
                sb.append(_items[i]);
            }
        }
        return sb.toString();
    }

    public String getSelectedItemsAsString() {
        StringBuilder sb = new StringBuilder();
        boolean foundOne = false;

        for (int i = 0; i < _items.length; ++i) {
            if (mSelection[i]) {
                if (foundOne) {
                    sb.append("; ");
                }
                foundOne = true;
             
                sb.append(_items[i]);
            }
        }
        return sb.toString();
    }
    
    public int getSelectType() {
        return selectType;
    }

    public void setSelectType(int selectType) {
        this.selectType = selectType;
    }

    public String[] get_items() {
        return _items;
    }

    public void set_items(String[] _items) {
        this._items = _items;
    }

    public boolean[] getmSelection() {
        return mSelection;
    }

    public void setmSelection(boolean[] mSelection) {
        this.mSelection = mSelection;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<Integer> getSelectedIds() {
        return selectedIds;
    }

    public void setSelectedIds(List<Integer> selectedIds) {
        this.selectedIds = selectedIds;
    }
    
    public OnDialogCancelListener getmDialogCancelListener() {
        return mDialogCancelListener;
    }

    public void setmDialogCancelListener(OnDialogCancelListener mDialogCancelListener) {
        this.mDialogCancelListener = mDialogCancelListener;
    }
  
    public class DialogButtonClickHandler implements DialogInterface.OnClickListener
    {
        public void onClick( DialogInterface dialog, int clicked ) 
        {
            switch(clicked) 
            {
                case DialogInterface.BUTTON_POSITIVE:
                    if( null != selectedIds){
                        if(getSelectType() == AppConstants.SELECT_LIMIT){
                            if(selectedIds.contains(new Integer(ANY_OPTION_ID))){
                                mDialogCancelListener.onDialogCancelled(AppConstants.SPINNER_TYPE_2,AppConstants.EQUIPMENT_TYPE,selectedIds.size() > 0);
                            }else{
                                mDialogCancelListener.onDialogCancelled(AppConstants.SPINNER_TYPE_2,AppConstants.EQUIPMENT_TYPE,count > 0);
                            }
                        }else if (getSelectType() == AppConstants.SELECT_ONE){
                            mDialogCancelListener.onDialogCancelled(AppConstants.SPINNER_TYPE_2,AppConstants.EQUIPMENT_TYPE,selectedIds.size() > 0);
                        }else{
                            mDialogCancelListener.onDialogCancelled(AppConstants.SPINNER_TYPE_2,AppConstants.TRAILER_OPTION,selectedIds.size() > 0);
                        }
                    }
                    break;
            }
        }
    }
    
    public void setUnSelection(int index) {
        if (index >= 0 && index < mSelection.length) {
            mSelection[index] = false;
        } else {
            throw new IllegalArgumentException("Index " + index
                    + " is out of bounds.");
        }
        simple_adapter.clear();
        simple_adapter.add(buildSelectedItemString());
    }
}
