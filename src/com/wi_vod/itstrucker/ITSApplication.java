package com.wi_vod.itstrucker;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.util.HashMap;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

public class ITSApplication extends Application {

	// The following line should be changed to include the correct property id.
	//private static final String PROPERTY_ID = "UA-43778067-4";

	// Logging TAG
	//private static final String TAG = "ITSApplication";

	public static int GENERAL_TRACKER = 0;

	public enum TrackerName {
		APP_TRACKER, // Tracker used only in this app.
		GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg: roll-up tracking.
		ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a company.
	}

	HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

	public ITSApplication() {
		super();
	}

	synchronized Tracker getTracker(TrackerName trackerId) {
		if (!mTrackers.containsKey(trackerId)) {

			GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
			Tracker t = analytics.newTracker(R.xml.app_tracker);
			mTrackers.put(trackerId, t);

		}
		return mTrackers.get(trackerId);
	}
	
	public SSLContext certifcate(){
		SSLContext context=null;
		
		try {
			// Load CAs from an InputStreamb (could be from a resource or ByteArrayInputStream or ...)
			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			// From https://www.washington.edu/itconnect/security/ca/load-der.crt
			//InputStream caInput = new BufferedInputStream(new InputStreamReader(getAssets().open("Go Daddy Class 2 Certification Authority.cer")));
			/*InputStream br = new BufferedReader(new InputStreamReader(getAssets().open("Go Daddy Class 2 Certification Authority.cer"), "UTF-8"));
			InputStream caInput= new BufferedInputStream(br*/
			AssetManager assetManager = getApplicationContext().getAssets();
			InputStream caInput = null;
			try {
				caInput = assetManager.open("GoDaddySecureCertificateAuthority-G2.cer");
			}
			catch (IOException e){
			    //Log.e("message: ",e.getMessage());
			    return null;
			}
			
			
			Certificate ca;
			try {
			    ca = cf.generateCertificate(caInput);
			   // System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
			} finally {
			    caInput.close();
			}

			// Create a KeyStore containing our trusted CAs
			String keyStoreType = KeyStore.getDefaultType();
			KeyStore keyStore = KeyStore.getInstance(keyStoreType);
			keyStore.load(null, null);
			keyStore.setCertificateEntry("ca", ca);

			// Create a TrustManager that trusts the CAs in our KeyStore
			String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
			TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
			tmf.init(keyStore);

			// Create an SSLContext that uses our TrustManager
			 context = SSLContext.getInstance("TLS");
			context.init(null, tmf.getTrustManagers(), null);
		} catch (Exception e) {
			return null;
		}
		
		return context;
	}
	
	
	public void startAlaramservice() {	
		Intent alarmIntent = new Intent(getApplicationContext(),LoginBackgroundService.class);	
		PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 0,alarmIntent, PendingIntent.FLAG_CANCEL_CURRENT);
		AlarmManager manager = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
		//int interval = Utils.SENDTRACKINGINFO_INTERVAL;
		//manager.setInexactRepeating(AlarmManager.RTC_WAKEUP,System.currentTimeMillis(), Utils.TRACKING_INTERVAL, pendingIntent);
		manager.setRepeating(AlarmManager.RTC_WAKEUP,System.currentTimeMillis(), Utils.TRACKING_INTERVAL, pendingIntent);
		//Toast.makeText(getApplicationContext(), "Alarm Called", Toast.LENGTH_SHORT).show();
		//System.out.println("startAlaramservice callled------------------>");
	}

	public void stopAlaramservice() {
		Intent alarmIntent = new Intent(getApplicationContext(),LoginBackgroundService.class);
		PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 0,alarmIntent, 0);
		AlarmManager manager = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
		manager.cancel(pendingIntent);
		//System.out.println("stopAlaramservice callled------------------>");		
	}	
}
