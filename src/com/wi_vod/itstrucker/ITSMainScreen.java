package com.wi_vod.itstrucker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.wi_vod.itstrucker.model.NavDrawerListAdapter;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ITSMainScreen extends ActionBarActivity  {

	Fragment Itsprofile;
	//private static String TAG="ITSMainScreen.class";
	
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;
	private TextView itsVersion;
	private BroadcastReceiver mRegistrationBroadcastReceiver;
	// nav drawer title
	private CharSequence mDrawerTitle;

	// used to store app title
	private CharSequence mTitle;

	// slide menu items
	private String[] navMenuTitles;
	private SharedPreferences prefs;

	private SessionManager session;
	private ArrayList<String> navDrawerItems = new ArrayList<String>();
	private NavDrawerListAdapter adapter;
	RelativeLayout drawer;
	private String[] mainArray=AppConstants.MAIN_MENU_LIST;
	static int  itemposition=0;
	static String itemname="";
	
	// ImageIds for respective menu items
	private ArrayList<Integer> navDrawerItemImageIds = new ArrayList<Integer>();
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {

		View v = getCurrentFocus();
		boolean ret = super.dispatchTouchEvent(event);

		if (v instanceof EditText) {
			View w = getCurrentFocus();
			int scrcoords[] = new int[2];
			w.getLocationOnScreen(scrcoords);
			float x = event.getRawX() + w.getLeft() - scrcoords[0];
			float y = event.getRawY() + w.getTop() - scrcoords[1];

			if (event.getAction() == MotionEvent.ACTION_UP
					&& (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w
							.getBottom())) {

				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
						.getWindowToken(), 0);
			}
		}
		return ret;
	}
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.itsmainscreen);
		session = new SessionManager(ITSMainScreen.this);
		prefs = getSharedPreferences("itstrucker", 0);
		//validate version,if version is not same then logout the user
		if (!validatepackageversion()) {
			session.logoutUser();
			/*Intent startloginscreenintent = new Intent(ITSMainScreen.this,ITSTruckerLogin.class);
			startloginscreenintent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(startloginscreenintent);*/
		}
		//startbackgroundservice();
		//initializing timer and background instance
		 session.setalertinstance(false);
		 session.setappbackground(false);
		 if (AppConstants.firstlaunch) {
			 Utils.startbackgroundservice(ITSMainScreen.this);//this will start alarm timer
			 AppConstants.firstlaunch=false;
		}
		
		 
		mTitle = mDrawerTitle = AppConstants.DRAWERTITLE;

		navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.list_slidermenu);
		drawer=(RelativeLayout) findViewById(R.id.drawer_view);
		itsVersion= (TextView) findViewById(R.id.itsversion);
		try {
			String versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
			if (!Utils.checkIsNullforPosts(versionName).equals("")) {
				if (AppConstants.DOMAIN.equals(AppConstants.TEST_SERVER)) {
					itsVersion.setText("v "+versionName);
					itsVersion.setTextColor(getResources().getColor(R.color.versioncolour));
				}else{
					itsVersion.setText("v "+versionName);
				}
			}
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (prefs.getInt("accessloadsearch", 0)!=0) {
			navDrawerItems.add(mainArray[0]);
			navDrawerItemImageIds.add(R.drawable.search_icon);
		}
		if (prefs.getInt("accessloadpost", 0)!=0) {
			navDrawerItems.add(mainArray[1]);
			navDrawerItemImageIds.add(R.drawable.post_icon);
		}
		mRegistrationBroadcastReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Utils.logoutuser(AppConstants.LOGOUTUSER_ERRORMSG, ITSMainScreen.this);
			}
		};
		
		navDrawerItems.add(mainArray[2]);
		navDrawerItems.add(mainArray[3]);
		
		// Adding ImageIds for respective menu items
		navDrawerItemImageIds.add(R.drawable.profile_icon);
		navDrawerItemImageIds.add(R.drawable.signout_icon);
		
	
		// Recycle the typed array

		mDrawerList.setOnItemClickListener(new SlideMenuClickListener());
		// setting the nav drawer list adapter
		adapter = new NavDrawerListAdapter(getApplicationContext(),navDrawerItems,navDrawerItemImageIds);
		mDrawerList.setAdapter(adapter);

		// enabling action bar app icon and behaving it as toggle button
		
		
		
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setLogo(R.drawable.ic_launcher);
		getSupportActionBar().setDisplayUseLogoEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, // nav menu toggle icon
				R.string.app_name, // nav drawer open - description for  accessibility
				R.string.app_name // nav drawer close - description for  accessibility
		) {
			public void onDrawerClosed(View view) {
				getSupportActionBar().setTitle(mTitle);
				// calling onPrepareOptionsMenu() to show action bar icons
				supportInvalidateOptionsMenu();
				
			}

			public void onDrawerOpened(View drawerView) {
				getSupportActionBar().setTitle(mDrawerTitle);
				// calling onPrepareOptionsMenu() to hide action bar icons
				supportInvalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState == null) {
			// on first time display view for first nav item
			if (prefs.getInt("accessloadsearch", 0)!=0) {
				updateDrawerView(navMenuTitles[0],0);
			}else{
				updateDrawerView(navMenuTitles[1],1);
			}
		}
	
	}
		
	
	/**
	 * Slide menu item click listener
	 * */
	private class SlideMenuClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			  String itemname=mDrawerList.getItemAtPosition(position).toString();
			 // System.out.println("str value selected--->"+itemname);
			  itemposition=position;
			  
			if (itemname.equals(mainArray[0])) {
				updateDrawerView(navMenuTitles[0], 0);
			}else if ( itemname.equals(mainArray[1])) {
				updateDrawerView(navMenuTitles[1],1);
				
			}else if (itemname.equals(mainArray[2])) {
				updateDrawerView(navMenuTitles[2],2);
			}else{
				displayView(3);
			}
		}
	}
	
	public void updateDrawerView(String selecteditem, int drawerposition){
		itemname=selecteditem;
		displayView(drawerposition);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		//getMenuInflater().inflate(R.menu.its_favmain, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// toggle nav drawer on selecting action bar app icon/title
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		// Handle action bar actions click
		switch (item.getItemId()) {
		
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/* *
	 * Called when invalidateOptionsMenu() is triggered
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		//boolean drawerOpen = mDrawerLayout.isDrawerOpen(drawer);
		//menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	/**
	 * Diplaying fragment view for selected nav drawer list item
	 * */
	private void displayView(int position) {
		// update the main content by replacing fragments
		Fragment fragment = null;
		switch (position) {
		case 0:
			if (session.isFirstTime()) {
				itemname=navMenuTitles[2];
				if (navDrawerItems.size()==4) {
					itemposition=2;
				}else{
					itemposition=1;
				}
				fragment = new ITSProfileActivity();
			}else{
				fragment = new SearchMainActivity();
			}
			break;
			
		case 1:
			if (session.isFirstTime()) {
				itemname=navMenuTitles[2];
				if (navDrawerItems.size()==4) {
					itemposition=2;
				}else{
					itemposition=1;
				}
				fragment = new ITSProfileActivity();
			}else{
				fragment = new PostTruckMain();
			}
			
			break;

		case 2:
			fragment = new ITSProfileActivity();
			break;
			
		case 3:
			session.logoutUser();
			 AppConstants.firstlaunch=true;
			Utils.stopalarmservice(ITSMainScreen.this);
			//send button click event if login is successful
			 Tracker t = ((ITSApplication) getApplication()).getTracker(ITSApplication.TrackerName.APP_TRACKER);
				
               t.send(new HitBuilders.EventBuilder().setCategory(getString( R.string.UICategory))
                       .setAction(getString( R.string.UIAction)).setLabel("Signout Button").build());
			finish();
			break;
			
		default:
			break;
		}

		if (fragment != null) {
			FragmentManager fragmentManager = getSupportFragmentManager();
			fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();
			if (session.isFirstTime()) {
				session.updateFirsttime(false);
			}
			mDrawerList.setItemChecked(itemposition, true);
			mDrawerList.setSelection(itemposition);
			setTitle(itemname);
		
			mDrawerLayout.closeDrawer(drawer);
		} 
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getSupportActionBar().setTitle(mTitle);
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}
	
	
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		itemposition=0;
	}
	
	 public  boolean validatepackageversion(){
     	try {
     	    PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
     	   String currentversion= packageInfo.versionName;
     	   
     	   if (!currentversion.equals(session.getPackageVersion())) {
			return false;
		}
     	   
     	   
     	} catch (NameNotFoundException e) {
     	    //Handle exception
     	}
     	return true;
     }
	
	 
	 @Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Utils.stopalarmservice(ITSMainScreen.this);
		 AppConstants.firstlaunch=true;
	}
	 
	 @Override
		public void onResume() {
			// TODO Auto-generated method stub
			super.onResume();
			//System.out.println("onresume called------------->");
		
			Utils.appcameforeground(ITSMainScreen.this);
			session.setnetwrokchangetracker(true);
			LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter(Utils.LOGOUT_USER));
		}

		@Override
		public void onPause() {
			LocalBroadcastManager.getInstance(this).unregisterReceiver( mRegistrationBroadcastReceiver);
			super.onPause();
		}
		
		@Override
		protected void onStop() {
			// TODO Auto-generated method stub
			super.onStop();
		
			Utils.appwentbackground(ITSMainScreen.this);
		
		}
	 
}
