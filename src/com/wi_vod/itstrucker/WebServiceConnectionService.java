package com.wi_vod.itstrucker;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;


import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import org.json.JSONObject;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.security.ProviderInstaller;
import com.wi_vod.itstrucker.model.XMLparser;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.CertPath;
import java.security.cert.CertPathValidatorException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.zip.GZIPInputStream;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSocketFactory;

public class WebServiceConnectionService extends AsyncTask<String, Void, String> {

    private static final String TAG = "WebServiceConnectionService";
	private Context _context;
	HttpURLConnection connection = null;
/*	private JSONObject _jsonObject ;
	private ConnectionServiceListener _connServiceListener ;
	private HashMap<String, String> mData = null;// post data
	private int i = 1;
	private String reference = null;*/
	private ProgressDialog dialog = null;
	private String progressMsg = null;
	private String requestbody=null;
	private String soapactionurl=null;
	String retval, ret, response_str=null;
	private ConnectionServiceListener _connServiceListener ;
	private String reference = null;
	
	public WebServiceConnectionService(Context context, String body,String soapactionurl,ConnectionServiceListener listener,String progressmessage){
		this._context = context ;
		this.requestbody = body ;
		this.soapactionurl = soapactionurl ;
		this._connServiceListener = listener ;
		this.dialog = new ProgressDialog(context);
		this.progressMsg=progressmessage;
	}
	
	
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		dialog.setMessage(progressMsg);
		dialog.setCancelable(false);
		if (progressMsg.equals("LOGFACT")) {
			dialog.hide();
		}else{
			dialog.show();
		}
		
		
	}
	
		@Override
	protected String doInBackground(String... params) {
			
			if (AppConstants.REQUESTTYPE.equals("HTTP_SERVICE")) {
				
				/*try {
					//System.out.println(Utils.isInternetAvailable(ITSTruckerLogin.this));
					if (Utils.isInternetAvailable(_context)) {
						//URL url = new URL(params[0]);
						URL url = new URL("http://secure.truckstop.com");
						connection = (HttpURLConnection) url.openConnection();
						connection.setConnectTimeout(30000);
						connection.setRequestMethod("POST");
						//connection.setRequestProperty("Content-Type","application/json");
						connection.setUseCaches(false);
						connection.setDoInput(true);
						connection.setDoOutput(true);
						connection.connect();
						// Send request
						DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
						wr.writeBytes(json.toString());
						wr.flush();
						wr.close();
						int HttpResult = connection.getResponseCode();
						if (HttpResult != 200) {
						// Log.i("IN BACKGROUND", "doInBackground");
							return AppConstants.SERVER_CONNECTION_ERROR;
					
						}else{
							
							InputStream stream = connection.getInputStream();
							
						
							String contentEncoding = connection.getHeaderField("Content-Encoding");
							
							//String contentEncoding = con.getHeaderField("Content-Encoding");
							
							//Header contentEncoding = con.getHeaderField("Content-Encoding").toString();
							
								BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
								String line;
								StringBuilder sbuilder = new StringBuilder();
								while ((line = reader.readLine()) != null) {
									sbuilder.append(line);
								}
								response_str="";
								response_str = sbuilder.toString();
								return response_str;
						}
						
					}else {
						return AppConstants.INTERNET_CONNECTION_ERROR;

					}

				} catch (Exception e) {
					return AppConstants.SERVER_CONNECTION_ERROR;
				}*/
				
				
				
				/*try {
					//System.out.println(Utils.isInternetAvailable(ITSTruckerLogin.this));
					if (Utils.isInternetAvailable(_context)) {
						// Log.i("IN BACKGROUND", "doInBackground");
						HttpPost post = new HttpPost(params[0]);
						
						if (params.length > 1) {
							reference = params[1];
						}
						
						
						//Log.i(TAG, AppConstants.LOGIN_TEST_URL);
						int timeoutConnection = 9000;
						HttpParams httpParameters = new BasicHttpParams();
						HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
						// Set the default socket timeout (SO_TIMEOUT) 
						// in milliseconds which is the timeout for waiting for data.
						int timeoutSocket = 5000;
						HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
						DefaultHttpClient client = new DefaultHttpClient(httpParameters);
						post.setHeader("Accept-Encoding", "gzip,deflate");
						post.setHeader("Content-Type", "text/xml;charset=UTF-8");
						post.setHeader("SOAPAction",this.soapactionurl);
						post.setHeader("Host", "webservices.truckstop.com");
						post.setHeader("Connection", "Keep-Alive");
						post.setHeader("User-Agent","Apache-HttpClient/4.1.1 (java 1.5)\\");
			
						
					System.out.println("BODY VALUE----" + this.requestbody);
						post.setEntity(new StringEntity(this.requestbody));
						HttpResponse response = client.execute(post);
						StatusLine statuscode = response.getStatusLine();
						String statuscodes = Integer.toString(statuscode.getStatusCode());
					
						if (!statuscodes.equals("200")) {
							return AppConstants.SERVER_CONNECTION_ERROR;
						}else{
							InputStream stream = response.getEntity().getContent();
							Header contentEncoding = response.getFirstHeader("Content-Encoding");
							if (contentEncoding != null&& contentEncoding.getValue().equalsIgnoreCase("gzip")) {
								final InputStream stream1 = new GZIPInputStream(stream);
								// System.out.println("i am here");
								BufferedReader reader = new BufferedReader(new InputStreamReader(stream1));
								String line;
								StringBuilder sbuilder = new StringBuilder();
								while ((line = reader.readLine()) != null) {
									sbuilder.append(line);
								}
								
								response_str="";
								response_str = sbuilder.toString();
								return response_str;
								System.out.println("in if statement\n      "+response_str);
								boolean result = XMLparser.parsexml(response_str);
								if (result) {
									return "success";
								}
							} else {
								BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
								String line;
								StringBuilder sbuilder = new StringBuilder();
								while ((line = reader.readLine()) != null) {
									sbuilder.append(line);
								}
								response_str="";
								response_str = sbuilder.toString();
								return response_str;
								//System.out.println("retval IN ELSE-----"+ response_str);
								boolean result = XMLparser.parsexml(response_str);
								if (result) {
									return "success";
								}
							}
						}
						
					}else {
						return AppConstants.INTERNET_CONNECTION_ERROR;

					}

				} catch (Exception e) {
					return AppConstants.SERVER_CONNECTION_ERROR;
				}*/
				
				
				
				
				
			
			}else if(AppConstants.REQUESTTYPE.equals("HTTPS_SERVICE")){
				if (checkPlayServices()) {
					try {
						//System.out.println(Utils.isInternetAvailable(ITSTruckerLogin.this));
						if (Utils.isInternetAvailable(_context)) {
						// Log.i("IN BACKGROUND", "doInBackground");
							ProviderInstaller.installIfNeeded(_context);
							SSLContext sslcontext = SSLContext.getInstance("TLSv1.2");
							sslcontext.init(null, null, null);
							URL obj = new URL(params[0]);
							//URL obj = new URL("https://secure.truckstop.com");
							HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
						
							con.setRequestMethod("POST");
							con.setRequestProperty("Accept-Encoding", "gzip,deflate");
							con.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
							con.setRequestProperty("SOAPAction",this.soapactionurl);
							con.setRequestProperty("Host", "webservices.truckstop.com");
							con.setRequestProperty("Connection", "Keep-Alive");
							con.setRequestProperty("User-Agent","Apache-HttpClient/4.1.1 (java 1.5)\\");
							
							//System.out.println("BODY VALUE this----" + this.requestbody);
							con.setConnectTimeout(AppConstants.SOCKETTIMEOUT_ERROR);
							
							con.setDoOutput(true);
							DataOutputStream wr = new DataOutputStream(con.getOutputStream());
							wr.writeBytes(this.requestbody);
							wr.flush();
							wr.close();
					 
							int responseCode = con.getResponseCode();
							
							if (responseCode!=200) {
								return AppConstants.SERVER_CONNECTION_ERROR;
							}else{
								InputStream stream = con.getInputStream();
								String contentEncoding = con.getHeaderField("Content-Encoding");
								
								//Header contentEncoding = con.getHeaderField("Content-Encoding").toString();
								if (contentEncoding != null&& contentEncoding.equalsIgnoreCase("gzip")) {
									final InputStream stream1 = new GZIPInputStream(stream);
									// System.out.println("i am here");
									BufferedReader reader = new BufferedReader(new InputStreamReader(stream1));
									String line;
									StringBuilder sbuilder = new StringBuilder();
									while ((line = reader.readLine()) != null) {
										sbuilder.append(line);
									}
									
									response_str="";
									response_str = sbuilder.toString();
									return response_str;
									//System.out.println("in if statement\n      "+response_str);
									
								} else {
									BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
									String line;
									StringBuilder sbuilder = new StringBuilder();
									while ((line = reader.readLine()) != null) {
										sbuilder.append(line);
									}
									response_str="";
									response_str = sbuilder.toString();
									return response_str;
							//	System.out.println("retval IN ELSE-----"+ response_str);
								}
							}
							
						}else {
							return AppConstants.INTERNET_CONNECTION_ERROR;

						}

					} catch(Exception e){
						e.printStackTrace();
						return AppConstants.SERVER_CONNECTION_ERROR;
					}
				}else{
					return AppConstants.PLAYSERVICES_NOTFOUND;
				}
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
			/*	try {
					//System.out.println(Utils.isInternetAvailable(ITSTruckerLogin.this));
					if (Utils.isInternetAvailable(_context)) {
						// Log.i("IN BACKGROUND", "doInBackground");
						
						URL obj = new URL(params[0]);
						HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
						ITSApplication app = ((ITSApplication) _context.getApplicationContext());
						//ITSApplication app= new ITSApplication();
						if (app.certifcate()==null) {
							return AppConstants.SERVER_CONNECTION_ERROR;
						}
						
						con.setSSLSocketFactory(app.certifcate().getSocketFactory());
						con.setRequestMethod("POST");
						con.setRequestProperty("Accept-Encoding", "gzip,deflate");
						con.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
						con.setRequestProperty("SOAPAction",this.soapactionurl);
						con.setRequestProperty("Host", "webservices.truckstop.com");
						con.setRequestProperty("Connection", "Keep-Alive");
						con.setRequestProperty("User-Agent","Apache-HttpClient/4.1.1 (java 1.5)\\");
						
					System.out.println("BODY VALUE----" + this.requestbody);
						con.setConnectTimeout(AppConstants.SOCKETTIMEOUT_ERROR);
						con.setDoOutput(true);
						DataOutputStream wr = new DataOutputStream(con.getOutputStream());
						wr.writeBytes(this.requestbody);
						wr.flush();
						wr.close();
				 
						int responseCode = con.getResponseCode();
						
						if (responseCode!=200) {
							return AppConstants.SERVER_CONNECTION_ERROR;
						}else{
							InputStream stream = con.getInputStream();
							String contentEncoding = con.getHeaderField("Content-Encoding");
							
							//Header contentEncoding = con.getHeaderField("Content-Encoding").toString();
							if (contentEncoding != null&& contentEncoding.equalsIgnoreCase("gzip")) {
								final InputStream stream1 = new GZIPInputStream(stream);
								// System.out.println("i am here");
								BufferedReader reader = new BufferedReader(new InputStreamReader(stream1));
								String line;
								StringBuilder sbuilder = new StringBuilder();
								while ((line = reader.readLine()) != null) {
									sbuilder.append(line);
								}
								
								response_str="";
								response_str = sbuilder.toString();
								return response_str;
								//System.out.println("in if statement\n      "+response_str);
								
							} else {
								BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
								String line;
								StringBuilder sbuilder = new StringBuilder();
								while ((line = reader.readLine()) != null) {
									sbuilder.append(line);
								}
								response_str="";
								response_str = sbuilder.toString();
								return response_str;
						//	System.out.println("retval IN ELSE-----"+ response_str);
							}
						}
						
					}else {
						return AppConstants.INTERNET_CONNECTION_ERROR;

					}

				} catch(Exception e){
					return AppConstants.SERVER_CONNECTION_ERROR;
				}*/
				
				
				//code with out SSL certificate
				/*try {
					//System.out.println(Utils.isInternetAvailable(ITSTruckerLogin.this));
					if (Utils.isInternetAvailable(_context)) {
						// Log.i("IN BACKGROUND", "doInBackground");
						
						URL obj = new URL("https://secure.truckstop.com");
						HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
						ITSApplication app = ((ITSApplication) _context.getApplicationContext());
						//ITSApplication app= new ITSApplication();
						if (app.certifcate()==null) {
							return AppConstants.SERVER_CONNECTION_ERROR;
						}
						
						//con.setSSLSocketFactory(app.certifcate().getSocketFactory());
						con.setRequestMethod("GET");
						con.setRequestProperty("Accept-Encoding", "gzip,deflate");
						con.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
						con.setRequestProperty("SOAPAction",this.soapactionurl);
						con.setRequestProperty("Host", "webservices.truckstop.com");
						con.setRequestProperty("Connection", "Keep-Alive");
						con.setRequestProperty("User-Agent","Apache-HttpClient/4.1.1 (java 1.5)\\");
						
					System.out.println("BODY VALUE----" + this.requestbody);
						con.setConnectTimeout(AppConstants.SOCKETTIMEOUT_ERROR);
						con.setDoOutput(true);
						con.connect();
						DataOutputStream wr = new DataOutputStream(con.getOutputStream());
						wr.writeBytes(this.requestbody);
						wr.flush();
						wr.close();
				 
						int responseCode = con.getResponseCode();
						
						if (responseCode!=200) {
							return AppConstants.SERVER_CONNECTION_ERROR;
						}else{
							InputStream stream = con.getInputStream();
							//String contentEncoding = con.getHeaderField("Content-Encoding");
							
							//Header contentEncoding = con.getHeaderField("Content-Encoding").toString();
							
								BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
								String line;
								StringBuilder sbuilder = new StringBuilder();
								while ((line = reader.readLine()) != null) {
									sbuilder.append(line);
								}
								response_str="";
								response_str = sbuilder.toString();
								return response_str;
						}
						
					}else {
						return AppConstants.INTERNET_CONNECTION_ERROR;

					}

				} catch(Exception e){
					e.printStackTrace();
					//SSLHandshakeException sslEx=(SSLHandshakeException) e;
				
					CertificateException certEx = (CertificateException)sslEx.getCause();
					
					
					System.out.println(certEx);
					
					CertPathValidatorException certEx1 = (CertPathValidatorException)certEx.getCause();
					 List certList = certEx1.getCertPath().getCertificates();
					 for (   Object   cert : certList) {
						 
						 
						 System.out.println(cert.toString());
						
					}
				

					 System.out.println( certEx1.getIndex());

					
					

					
					
					return AppConstants.SERVER_CONNECTION_ERROR;
				}
				
				*/
				
				
				
				
				
				
				
				
				
				
			}
			return AppConstants.SERVER_CONNECTION_ERROR;
	}
		
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if(this.dialog!=null && this.dialog.isShowing()){
			this.dialog.dismiss();
		}
		
		if (reference != null) {
			_connServiceListener.onServiceComplete(result, reference);
		} else {
			_connServiceListener.onServiceComplete(result);
		}
	}
	
	
	 private boolean checkPlayServices() {
	        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(_context);
	        if (resultCode != ConnectionResult.SUCCESS) {
	           
	            return false;
	        }
	        return true;
	    }

}
