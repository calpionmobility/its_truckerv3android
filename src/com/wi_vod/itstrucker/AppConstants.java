package com.wi_vod.itstrucker;

public class AppConstants {

	/*public static String USERNAME = "ITSMOBILEWS";
	public static String PASSWORD = "ITSMI2ITS";
	public static int INTEGRARTION_ID = 245873;*/
	
	public static String USERNAME = "ITTRUWSQ4";
	public static String PASSWORD = "HY50Cq11";
	public static int INTEGRARTION_ID = 281962;
	public static String LOGIN_ERROR_CODE = "";
	public static String LOGIN_ERROR_MESSAGE = "";
	public static String LOGIN_INTEGRATION = "";
	
	
	public static boolean firstlaunch=true;
	
	//HTTPS 
	
	public static String TEST_SERVER = "http://testws.truckstop.com:8080/V13/";//test Server URL
	//public static String PRODUCTION_SERVER = "http://webservices.truckstop.com/V13/";//Production Server URL
	public static String PRODUCTION_SERVER = "https://webservices.truckstop.com/V13/";//Production Server URL
	public static String HTTPS_STAGE_SERVER = "https://testws.truckstop.com/V13/";//Production Server URL
	
	public static String REQUESTTYPE = "HTTPS_SERVICE";//HTTPS_SERVICE or HTTP_SERVICE
	
	
	public static String DOMAIN = TEST_SERVER;
//	public static String DOMAIN = PRODUCTION_SERVER;
	public static String LOGIN_TEST_URL = DOMAIN+"Admin/WebserviceAdmin.svc";
	
	public static String SEARCHLOADS_TEST_URL = DOMAIN+"Searching/LoadSearch.svc"; 
	public static String GETTRUCKS_TEST_URL = DOMAIN+"Posting/TruckPosting.svc";
	
	//Google places Constants
	public static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
	public static final String TYPE_AUTOCOMPLETE = "/autocomplete";
	public static final String OUT_JSON = "/json";

	public static final String API_KEY = "AIzaSyAj-YCzb7FsSgP1qwP2pNL8C8ovXzIRtcA";
	
	

	public static String NO_ADDRESS_FOUND="No address found for location";
	public static final String SERVER_CONNECTION_ERROR = "Connection to the server not available. Please try again.";
	public static final String INTERNET_CONNECTION_ERROR = "Please check your internet connection and try again";
	public static final String SERVER_ERROR = "Encountered an unexpected error. Please try again";
	public static final String SERVER_REQUEST_ERROR = "Request to the server failed";
	public static final String  NOGPSFOUND_ERROR="GPS might be turned off or we are unable to detect current location!";
	public static final String PLAYSERVICES_NOTFOUND = "Google Play services might not be installed.Please Install and try again later.";
	
	public static final String EMPTY_STRING="-";
	public static final String POSTEMPTY_STRING="";
	
	public static String DESTINATION_NOTFOUND = "State not found or invalid Destination!";
	public static String ORIGIN_NOTFOUND = "State not found or invalid Origin!";
	public static String INVALID_SERACHFORMAT = "Not valid City,state format";
	
	public static String PROFILEINVALID_SERACHFORMAT ="State not found or invalid Home Base!";
	
	//EULA screen constants
	public static final String EULA_PROGRESS="Accepting EULA...";
		
	//ITS MainScreen Activity
	public static String DRAWERTITLE="ITS Trucker";
	public static String MAIN_MENU_LIST[] = { "Search","Post","Profile","Sign Out" };
	
	public static String PRODUCT_DENIED="You don't have access to this feature. Please contact customer service at 800-203-2540.";
	public static String PRODUCT_DENIED_LOGIN="You don't have access. Please contact customer service at 800-203-2540.";
	
	//Post Activity Constants
	public static final String POST_SUCESS = "Truck Posted Successfully with Posting-Id:";
	public static final String REPOST_SUCESS = "Truck Updated Successfully with Posting-Id:";
	public static final String POST_PROGRESS = "Posting truck ...";

	//Post Detail Activity Constants
	public static final String POSTDETAIL_PROGRESS = "Retrieving Details...";
	
	//Login Activity constants
	public static String LOGIN_PROGRESS = "Signing In...";
	public static String LOGOUTUSER_ERRORMSG = "Unable to verify credentials. Please login again.";
	public static String LOGIN_CREDENTIALSREQUIRED = "Please enter your credentials.";
	
	//Search Main screen COnstatnts
	public static String RECENTSEARCH_ERRORMESSAGE = "No load search has been made recently";
	public static String PROFILE_SETTINGERRORMSG ="Home Base and Equipment Type has not been set! Would you like to set it now?";
	public static String HOMEBASE_ERRORMESSAGE ="Home Base has not been set! Would you like to set it now?";
	public static String SEARCHHOMEEQP_ERRORMESSAGE ="Equipment Type has not been set! Would you like to set it now?";
	
	//Search Load results constatnts
	public static String SEARCHLOADS_PROGRESS = "Searching Loads...";
	public static int SEARCHLOADS_PAGESIZE = 50;
	
	//Search Detail Screen Constants
	public static String DETAILLOADS_PROGRESS="Retrieving Details...";
	
	//Favorites
	public static final String ALREADY_FAVORITE = "Already a favorite";
	public static final String NO_FAVORITE = "No Favorites to show ";
	public static final String ADD_AS_FAVORITE = "Added as favorite";
	
	
	//Search Main List Items
	public static String SEARCH_ITEM_LIST[] = { "Current Search", "Get me Home","Go Anywhere","Favorite Searches", "New Search" };
	
	//Post Main List Items
	//public static String POST_ITEM_LIST[] = { "Get me Home","Go Anywhere","Current Postings","Favorite Post", "New Post" };
	public static String POST_ITEM_LIST[] = { "Get me Home","Go Anywhere","Current Postings", "New Post" };
	 
	//Current Post screen COnstatnts
	public static final String DELETEPOSTS_PROGRESS = "Deleting Post(s)...";
	public static final String CURRENTPOSTS_PROGRESS = "Retrieving Current Posts...";
	public static final String DELETETRUCK_CONFIRM="Do you want to delete the selected Trucks?";
	
	//Internet Error
	public static int TIMEOUTCONNECTION_ERROR = 30000;// Set the default  timeout (SO_TIMEOUT) in milliseconds which is the timeout for waiting for data.
	public static int SOCKETTIMEOUT_ERROR = 30000;// Set the default socket timeout (SO_TIMEOUT) in milliseconds which is the timeout for waiting for data.
	
	
	// For MultiSelect Spinner ( Equipment Types and Trailer Options)
		public static final int SELECT_LIMIT = 1;
		public static final int SELECT_MAX = 2;
		public static final int SELECT_ONE = 3;
		public static final int EQUIPMENT_TYPE = 1;
		public static final int TRAILER_OPTION = 2;
		
		public static final int SPINNER_TYPE_1 = 1;
        public static final int SPINNER_TYPE_2 = 2;
        
		/**
		 * Trailer Options
		 */
	    public static final String OPTION_TARPS = "Tarps";
	    public static final String OPTION_HAZARDOUS = "Hazardous" ; 
	    public static final String OPTION_PALLETEXCHANGE = "Pallet Exchange" ;
	    public static final String OPTION_TEAM = "Team" ;
	    public static final String OPTION_EXPEDITED = "Expedited" ;
	    public static final String TOKEN_DELIMETER = ";";
        public static final String NONE = "None"; 
        public static String setdate = "Select Date";
        public static final String SAMSUNG = "samsung";
        public static final String DETAIL_MOREINFO ="Call for more info";
        public static final String TERMS_CONDITIONS_URL = "https://app.truckstop.com//content/mobile/Responsive-TermsAndConditions.html";//"https://truckstop.com/terms/";// old url
        public static final String TNC_MSG_CONTENT_1 = "The terms & conditions and privacy policy for our web site and application have been updated to address newer security provisions on our application.  After reading the terms, click �Accept� and you will be able to proceed with your use of the application.";
        public static final String TNC_MSG_CONTENT_2 = "The terms & conditions must be accepted before proceeding.  For questions, please contact Customer Support at support@truckstop.com.";

	
}