package com.wi_vod.itstrucker.model;

public class DrawerItemsModel {
	
	String Title;
	int showitem;
	public DrawerItemsModel() {
		// TODO Auto-generated constructor stub
	}
	
	
	public DrawerItemsModel(String title, int showitem) {
		super();
		Title = title;
		this.showitem = showitem;
	}


	public String getTitle() {
		return Title;
	}
	public void setTitle(String title) {
		Title = title;
	}
	public int getShowitem() {
		return showitem;
	}
	public void setShowitem(int showitem) {
		this.showitem = showitem;
	}
	
	

}
