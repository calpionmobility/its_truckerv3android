package com.wi_vod.itstrucker.model;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class XMLSearchLoadDetailParser {
	static private SearchDetailLoadModel items=null;
	static private SeachDetailEquipmentTypes equipmenttypes=null;
	static private List<SearchDetailLoadModel> detailitems=new ArrayList<SearchDetailLoadModel>();
	
	public static String errormessage=""; 
	public static SearchDetailLoadModel  getSearchItems(){
		return items;
	}
	
	public static String geterrormmsg(){
		//System.out.println("errormessage in geterrormmsg method               "+errormessage);
		return errormessage;
	}
	
	public static boolean parsexml(String xmlstring) throws XmlPullParserException {
		XmlPullParserFactory factory = null;
		XmlPullParser parser = null;
		String text = null;
	
		try {
			factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			parser = factory.newPullParser();
			parser.setInput(new StringReader(xmlstring));

			int eventType = parser.getEventType();
			while (eventType != XmlPullParser.END_DOCUMENT) {
				String tagname = parser.getName();
				switch (eventType) {
				case XmlPullParser.START_TAG:
					if (tagname.equalsIgnoreCase("Error")) {
					//	System.out.println("errormessagea  " + text);
					}
					if (tagname.equalsIgnoreCase("GetLoadSearchDetailResultResult")) {
						detailitems.clear();
					}
					if (tagname.equalsIgnoreCase("LoadDetail")) {
						items=new SearchDetailLoadModel();
					}
					if (tagname.equalsIgnoreCase("EquipmentTypes")) {
						equipmenttypes=new SeachDetailEquipmentTypes();
					}
					break;

				case XmlPullParser.TEXT:
					text = parser.getText();
					break;

				case XmlPullParser.END_TAG:
					if (tagname.equalsIgnoreCase("ErrorMessage")) {
						errormessage=text;
						return false;
					}else {
						
						if (tagname.equalsIgnoreCase("LoadDetail")) {
							detailitems.add(items);
						} else if (tagname.equalsIgnoreCase("Age")) {
							
							items.setAge(text);
							text="";
							//System.out.println("" + text);
						} else if (tagname.equalsIgnoreCase("Bond")) {
							items.setBond(text);
							text="";
							//System.out.println("" + text);
						}  else if (tagname.equalsIgnoreCase("BondTypeID")) {
							items.setBondTypeID(text);
							text="";
							//System.out.println("" + text);
						}else if (tagname.equalsIgnoreCase("Credit")) {
							items.setCredit(text);
							text="";
							//System.out.println("" + text);
						} else if (tagname.equalsIgnoreCase("DOTNumber")) {
							items.setDotNumber(text);
							text="";
						} else if (tagname.equalsIgnoreCase("DeletedId")) {
							items.setDeletedId(text);
							text="";
						}else if (tagname.equalsIgnoreCase("DeletedId")) {
							items.setDeletedId(text);
							text="";
						}else if (tagname.equalsIgnoreCase("DeliveryDate")) {
							items.setDeliveryDate(text);
							text="";
						} else if (tagname.equalsIgnoreCase("DeliveryTime")) {
							items.setDeliveryTime(text);
							text="";
						}else if (tagname.equalsIgnoreCase("DestinationCity")) {
							items.setDestinationCity(text);
							text="";
						}else if (tagname.equalsIgnoreCase("DestinationCountry")) {
							items.setDestinationCountry(text);
							text="";
						}else if (tagname.equalsIgnoreCase("DestinationState")) {
							items.setDestinationState(text);
							text="";
						}else if (tagname.equalsIgnoreCase("DestinationZip")) {
							items.setDestinationZip(text);
							text="";
						}else if (tagname.equalsIgnoreCase("Distance")) {
							items.setDistance(text);
							text="";
						}else if (tagname.equalsIgnoreCase("Entered")) {
							items.setEntered(text);
							text="";
						}else if (tagname.equalsIgnoreCase("Equipment")) {
							items.setEquipment(text);
							text="";
						}else if (tagname.equalsIgnoreCase("TrailerOptionType")) {
							if (items.getTrailerOptionType()!=null) {
								items.setTrailerOptionType(items.getTrailerOptionType()+","+text);
							}else{
								items.setTrailerOptionType(text);
							}
							text="";
						}else if (tagname.equalsIgnoreCase("EquipmentTypes")) {
							items.setEquipmenttypes(equipmenttypes);
							text="";
						}else if (tagname.equalsIgnoreCase("Category")) {
							equipmenttypes.setCategory(text);
							text="";
						}else if (tagname.equalsIgnoreCase("CategoryId")) {
							equipmenttypes.setCategoryId(text);
							text="";
						}else if (tagname.equalsIgnoreCase("Code")) {
							equipmenttypes.setCode(text);
							text="";
						}else if (tagname.equalsIgnoreCase("Description")) {
							equipmenttypes.setDescription(text);
							text="";
							//System.out.println("equipmenttypes.setDescription(text)    "+text);
						}else if (tagname.equalsIgnoreCase("FullLoad")) {
							equipmenttypes.setFullLoad(text);
							text="";
						}else if (tagname.equalsIgnoreCase("Id")) {
							equipmenttypes.setId(text);
							text="";
						}else if (tagname.equalsIgnoreCase("IsCategorizable")) {
							equipmenttypes.setCategorizable(Boolean.valueOf(text));
							text="";
						}else if (tagname.equalsIgnoreCase("IsCombo")) {
							equipmenttypes.setCombo(Boolean.valueOf(text));
							text="";
						}else if (tagname.equalsIgnoreCase("IsTruckPost")) {
							equipmenttypes.setTruckPost(Boolean.valueOf(text));
							text="";
						}else if (tagname.equalsIgnoreCase("MapToId")) {
							equipmenttypes.setMapToId(text);
							text="";
						}else if (tagname.equalsIgnoreCase("RequiredOption")) {
							equipmenttypes.setRequiredOption(text);
							text="";
						}else if (tagname.equalsIgnoreCase("WebserviceOnly")) {
							equipmenttypes.setWebserviceOnly(Boolean.valueOf(text));
							text="";
						}else if (tagname.equalsIgnoreCase("ExperienceFactor")) {
							
						}else if (tagname.equalsIgnoreCase("FuelCost")) {
							items.setFuelCost(text);
							text="";
						}else if (tagname.equalsIgnoreCase("HandleName")) {
							items.setHandleName(text);
							text="";
						}else if (tagname.equalsIgnoreCase("HasBonding")) {
							items.setHasBonding(text);
							text="";
						}else if (tagname.equalsIgnoreCase("ID")) {
							items.setiD(text);
						//	System.out.println("" + text);
							text="";
						}else if (tagname.equalsIgnoreCase("IsDeleted")) {
							items.setDeleted(Boolean.valueOf(text));
							text="";
						}
						else if (tagname.equalsIgnoreCase("IsFriend")) {
							items.setIsFriend(text);
							text="";
							
						}else if (tagname.equalsIgnoreCase("Length")) {
							items.setLength(text);
							text="";
							
						}else if (tagname.equalsIgnoreCase("LoadType")) {
							items.setLoadType(text);
							text="";
						}else if (tagname.equalsIgnoreCase("MCNumber")) {
							items.setMcNumber(text);
							text="";
						}else if (tagname.equalsIgnoreCase("Mileage")) {
							items.setMileage(text);
							text="";
						}else if (tagname.equalsIgnoreCase("OriginCity")) {
							items.setOriginCity(text);
							text="";
						}else if (tagname.equalsIgnoreCase("OriginCountry")) {
							items.setOriginCountry(text);
							text="";
						}else if (tagname.equalsIgnoreCase("OriginState")) {
							items.setOriginState(text);
							text="";
						}else if (tagname.equalsIgnoreCase("OriginZip")) {
							items.setOriginZip(text);
							text="";
						}else if (tagname.equalsIgnoreCase("PaymentAmount")) {
							items.setPaymentAmount(text);
							text="";
						}else if (tagname.equalsIgnoreCase("PickUpDate")) {
							items.setPickupDate(text);
							text="";
						}else if (tagname.equalsIgnoreCase("PickupTime")) {
							items.setPickupTime(text);
							text="";
						}else if (tagname.equalsIgnoreCase("PointOfContact")) {
							items.setPointOfContact(text);
							text="";
						}else if (tagname.equalsIgnoreCase("PointOfContactPhone")) {
							items.setPointOfContactPhone(text);
							//System.out.println("" + text);
							text="";
						}else if (tagname.equalsIgnoreCase("PricePerGallon")) {
							items.setPricePerGallon(text);
							text="";
						}else if (tagname.equalsIgnoreCase("Quantity")) {
							items.setQuantity(text);
							text="";
						}else if (tagname.equalsIgnoreCase("SpecInfo")) {
							items.setSpecInfo(text);
							text="";
						}else if (tagname.equalsIgnoreCase("Stops")) {
							items.setStops(text);
							text="";
						}else if (tagname.equalsIgnoreCase("TMCNumber")) {
							items.settMCNumber(text);
							text="";
						}else if (tagname.equalsIgnoreCase("TruckCompanyCity")) {
							items.setTruckCompanyCity(text);
							text="";
						}else if (tagname.equalsIgnoreCase("TruckCompanyEmail")) {
							items.setTruckCompanyEmail(text);
							text="";
						}else if (tagname.equalsIgnoreCase("TruckCompanyFax")) {
							items.setTruckCompanyFax(text);
							text="";
						}else if (tagname.equalsIgnoreCase("TruckCompanyName")) {
							items.setTruckCompanyName(text);
							text="";
						}else if (tagname.equalsIgnoreCase("TruckCompanyPhone")) {
							items.setTruckCompanyPhone(text);
							text="";
						}else if (tagname.equalsIgnoreCase("TruckCompanyState")) {
							items.setTruckCompanyState(text);
							text="";
						}else if (tagname.equalsIgnoreCase("Weight")) {
							items.setWeight(text);
							text="";
						}else if (tagname.equalsIgnoreCase("Width")) {
							items.setWidth(text);
							text="";
						}
						else if (tagname.equalsIgnoreCase("TruckCompanyMobile")) {
							items.setTruckCompanyMobile(text);
							text="";
						}
					}

					break;

				default:
					break;
				}
				eventType = parser.next();
			}

		} catch (XmlPullParserException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;

	}

}
