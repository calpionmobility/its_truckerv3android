package com.wi_vod.itstrucker.model;

public class LoginProducts {
	   /*<ProductID>0</ProductID>
       <Name>LOADPOST</Name>
       <ProductStatus>Have</ProductStatus>
       <EndDate>0001-01-01T00:00:00</EndDate>*/
	
	private String productid;
	private String name;
	private String productstatus;
	private String enddate;
	public LoginProducts() {
		// TODO Auto-generated constructor stub
	}
	
	public LoginProducts(String productid, String name, String productstatus,String enddate) {
		super();
		this.productid = productid;
		this.name = name;
		this.productstatus = productstatus;
		this.enddate = enddate;
	}
	public String getProductid() {
		return productid;
	}
	public void setProductid(String productid) {
		this.productid = productid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getProductstatus() {
		return productstatus;
	}
	public void setProductstatus(String productstatus) {
		this.productstatus = productstatus;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	
	

}
