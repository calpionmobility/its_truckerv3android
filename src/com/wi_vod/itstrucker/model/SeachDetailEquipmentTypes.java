package com.wi_vod.itstrucker.model;

public class SeachDetailEquipmentTypes {

	private String category;
	private String categoryId;
	private String code;
	private String description;
	private String fullLoad;
	private String id;
	private boolean isCategorizable;
	private boolean isCombo;
	private boolean isTruckPost;
	private String MapToId;
	private String RequiredOption;
	private boolean WebserviceOnly;
	public SeachDetailEquipmentTypes() {
		
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getFullLoad() {
		return fullLoad;
	}
	public void setFullLoad(String fullLoad) {
		this.fullLoad = fullLoad;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public boolean isCategorizable() {
		return isCategorizable;
	}
	public void setCategorizable(boolean isCategorizable) {
		this.isCategorizable = isCategorizable;
	}
	public boolean isCombo() {
		return isCombo;
	}
	public void setCombo(boolean isCombo) {
		this.isCombo = isCombo;
	}
	public boolean isTruckPost() {
		return isTruckPost;
	}
	public void setTruckPost(boolean isTruckPost) {
		this.isTruckPost = isTruckPost;
	}
	public String getMapToId() {
		return MapToId;
	}
	public void setMapToId(String mapToId) {
		MapToId = mapToId;
	}
	public String getRequiredOption() {
		return RequiredOption;
	}
	public void setRequiredOption(String requiredOption) {
		RequiredOption = requiredOption;
	}
	public boolean isWebserviceOnly() {
		return WebserviceOnly;
	}
	public void setWebserviceOnly(boolean webserviceOnly) {
		WebserviceOnly = webserviceOnly;
	}
	
	
}
