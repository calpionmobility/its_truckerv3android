package com.wi_vod.itstrucker.model;


import java.util.ArrayList;

import com.wi_vod.itstrucker.R;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class NavDrawerListAdapter extends BaseAdapter {
	
	private Context context;
	private ArrayList<String> navDrawerItems;
	private ArrayList<Integer> navDrawerItemImageIds;
	
	public NavDrawerListAdapter(Context context, ArrayList<String> navDrawerItems){
		this.context = context;
		this.navDrawerItems = navDrawerItems;
	}

	public NavDrawerListAdapter(Context context, ArrayList<String> navDrawerItems, ArrayList<Integer> navDrawerItemImageIds){
        this.context = context;
        this.navDrawerItems = navDrawerItems;
        this.navDrawerItemImageIds = navDrawerItemImageIds ;
    }

	@Override
	public int getCount() {
		return navDrawerItems.size();
	}

	@Override
	public Object getItem(int position) {		
		return navDrawerItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.drawer_list_item, null);
        }
         
        ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.title);

//        imgIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_launcher));
        
        imgIcon.setImageDrawable(context.getResources().getDrawable(navDrawerItemImageIds.get(position)));
        txtTitle.setText(navDrawerItems.get(position));
        
        // displaying count
        // check whether it set visible or not
        
        
        return convertView;
	}

}
