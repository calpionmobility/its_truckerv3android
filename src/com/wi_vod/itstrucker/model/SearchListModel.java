package com.wi_vod.itstrucker.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.wi_vod.itstrucker.R;
import com.wi_vod.itstrucker.SearchDetailActivity;
import com.wi_vod.itstrucker.SearchResultActivty;
import com.wi_vod.itstrucker.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class SearchListModel extends BaseAdapter implements OnClickListener {
	
	
	public static String TAG="SearchListModel.class";
	private Activity activity;
	private ArrayList<LoadSearchItem> data;
	private static LayoutInflater inflater = null;
	public Resources res;
	private LoadSearchItem seachlist = null;
	int i = 0;

	public SearchListModel(Activity a, ArrayList<LoadSearchItem> d,Resources resLocal) {

		activity = a;
		data = d;
		res = resLocal;
		inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	@Override
	public int getCount() {
		if (data.size() <= 0)
			return 1;
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	/********* Create a holder Class to contain inflated xml file elements *********/
	public static class ViewHolder {

		public TextView companyName;
		public TextView amount;
		public TextView origin;
		public TextView destintaion;
		public TextView originmiles;
		public TextView destintaionmiles;
		public TextView loadage;
		public TextView equipmenttype;
		public TextView loadsize;
		public TextView fuelprice;
		public TextView traileroptions;
		
		public TextView totalmileage;
		public TextView day2pay;
		public TextView expfactor;
		public Button dateButton;
		public ImageView callcompany;

	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		ViewHolder holder;

		if (convertView == null) {

			vi = inflater.inflate(R.layout.custom_row_viewmodel, null);

			holder = new ViewHolder();
			holder.companyName = (TextView) vi.findViewById(R.id.loadcompanyname);
			holder.amount = (TextView) vi.findViewById(R.id.loadpayment);
			holder.origin = (TextView) vi.findViewById(R.id.loadorigin);
			holder.destintaion = (TextView) vi.findViewById(R.id.loaddestination);
			holder.originmiles = (TextView) vi.findViewById(R.id.loadoriginmiles);
			holder.destintaionmiles = (TextView) vi.findViewById(R.id.loaddestinationmiles);
			holder.loadage = (TextView) vi.findViewById(R.id.loadage);
			holder.equipmenttype = (TextView) vi.findViewById(R.id.loadeqptype);
			holder.loadsize = (TextView) vi.findViewById(R.id.customloadsize);
			holder.fuelprice = (TextView) vi.findViewById(R.id.loadfuelprice);
			holder.traileroptions = (TextView) vi.findViewById(R.id.loadtraileroptions);
			holder.totalmileage = (TextView) vi.findViewById(R.id.loadtotalmileage);
			holder.day2pay = (TextView) vi.findViewById(R.id.loaddaystopay);
			holder.expfactor = (TextView) vi.findViewById(R.id.loadexpfactor);
			holder.dateButton = (Button) vi.findViewById(R.id.datebutton);
			holder.callcompany = (ImageView) vi.findViewById(R.id.callbutton);
			vi.setTag(holder);
		} else
			holder = (ViewHolder) vi.getTag();

		if (data.size() <= 0) {
			holder.companyName.setText("No Data");

		} else {
			/***** Get each Model object from Arraylist ********/
			seachlist = null;
			seachlist = (LoadSearchItem) data.get(position);
			/************ Set Model values in Holder elements ***********/
			holder.companyName.setText(Utils.checkIsNull(seachlist.getCompanyname()));
			
			if (!Utils.checkIsNull(seachlist.getPayment()).equals("-")) {
				holder.amount.setText("$"+Utils.checkIsNull(seachlist.getPayment()));
			}else{
				holder.amount.setText(Utils.checkIsNull(seachlist.getPayment()));
			}
			
			
			holder.origin.setText(Utils.checkIsNull(seachlist.getOriginCity())+","+Utils.checkIsNull(seachlist.getOriginState()));
			holder.destintaion.setText(Utils.checkIsNull(seachlist.getDestinationcity())+", "+Utils.checkIsNull(seachlist.getDestinationState()));
			if (!Utils.checkIsNull(seachlist.getOriginDistance()).equals("-")) {
				holder.originmiles.setText(Utils.checkIsNull(seachlist.getOriginDistance())+ " miles");
			}else{
				holder.originmiles.setText(Utils.checkIsNull(seachlist.getOriginDistance()));
			}
			
			
		
			
			if (!Utils.checkIsNull(seachlist.getDestinationDistance()).equals("-")) {
				holder.destintaionmiles.setText(Utils.checkIsNull(seachlist.getDestinationDistance())+ " miles");
			}else{
				holder.destintaionmiles.setText(Utils.checkIsNull(seachlist.getDestinationDistance()));
			}
			
			
			
				holder.loadage.setText("Age: "+Utils.checkIsNull(seachlist.getAge()));

			holder.equipmenttype.setText(Utils.checkIsNull(seachlist.getEquipment()));
			holder.loadsize.setText(Utils.checkIsNull(seachlist.getLoadType()));
			holder.fuelprice.setText("Fuel: "+Utils.checkIsNull(seachlist.getFuelCost()));
			
			holder.traileroptions.setText(Utils.checkIsNull(seachlist.getTrialerOptionType()));
			holder.totalmileage.setText("Total Miles:"+Utils.checkIsNull(seachlist.getMiles()));
			
			
			
			
			
			if(Utils.checkIsNull(seachlist.getDays2pay()).equals("----")||Utils.checkIsNull(seachlist.getDays2pay()).equals("-"))
				holder.day2pay.setText("D2P:-");
			else
				holder.day2pay.setText("D2P:"+Utils.checkIsNull(seachlist.getDays2pay()));
				
			
			
			
			
			
			
			
			
			holder.expfactor.setText("EXP:"+Utils.checkIsNull(seachlist.getExperienceFactor()));
			
			
			holder.callcompany.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					

					try {
						seachlist=(LoadSearchItem) data.get(position);
						if (!Utils.checkIsNull(seachlist.getPointOfContactPhone()).equals("-")) {
							Intent callIntent = new Intent(Intent.ACTION_DIAL);
							callIntent.setData(Uri.parse("tel:"+seachlist.getPointOfContactPhone()));
							/*String p = "tel:" + seachlist.getPointOfContactPhone();
							callIntent.setData(Uri.parse(p));*/
							//activity.startActivity(callIntent);
							PackageManager packageManager =activity.getPackageManager();
							List<ResolveInfo> activities = packageManager
									.queryIntentActivities(callIntent, 0);
							boolean isIntentSafe = activities.size() > 0;
							if (isIntentSafe) {
								activity.startActivity(callIntent);
							} else {
								Toast.makeText(activity,
										"There is no dial pad installed.",
										Toast.LENGTH_SHORT).show();
							}
						}
						
						
						
					} catch (android.content.ActivityNotFoundException ex) {
						Toast.makeText(activity,
								"There is no dial pad installed.",
								Toast.LENGTH_SHORT).show();
					}
					
				}
			});
			String datefromresults=seachlist.getPickupDate();
			if (datefromresults!=null && datefromresults.contains("/")) {
				String replacement = datefromresults.substring(0, 5);
			//	Log.i(TAG, replacement);
				holder.dateButton.setText(replacement);
				}else{
					holder.dateButton.setText(datefromresults);
				}
			
			
			/******** Set Item Click Listner for LayoutInflater for each row *******/
			vi.setOnClickListener(new OnItemClickListener(position));
		}
		return vi;
	}

	/********* Called when Item click in ListView ************/
	private class OnItemClickListener implements OnClickListener {
		private int mPosition;

		OnItemClickListener(int position) {
			mPosition = position;

		}
		@Override
		public void onClick(View arg0) {
			SearchResultActivty sct = (SearchResultActivty) activity;
				sct.ononItemClick(mPosition);
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

}
