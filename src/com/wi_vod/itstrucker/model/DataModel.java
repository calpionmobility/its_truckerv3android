package com.wi_vod.itstrucker.model;



import android.os.Parcel;
import android.os.Parcelable;

public class DataModel implements Parcelable {

	private String origincounrty,originstate,origincity,destinationcountry,destinationcity,destinationstate,searchradius,equipmenttype,searchdate,traileroption,
	hoursold,loadtype,sortype,width,length,weight,quantity,mindist,ratepermile,specialInfo,originareacode,destinationareacode,md5value;
	int originareaselected,destareaselected;
	
private int id;
private int isselected;
public DataModel() {
	// TODO Auto-generated constructor stub
}

	public DataModel(String origincounrty, String originstate,
		String origincity, String destinationcountry, String destinationcity,
		String destinationstate, String searchradius, String equipmenttype,
		String searchdate, String traileroption, String hoursold,
		String loadtype, String sortype,String originareacd,String dstareacd,int originselected,int destselected,String md5value) {
	super();
	this.origincounrty = origincounrty;
	this.originstate = originstate;
	this.origincity = origincity;
	this.destinationcountry = destinationcountry;
	this.destinationcity = destinationcity;
	this.destinationstate = destinationstate;
	this.searchradius = searchradius;
	this.equipmenttype = equipmenttype;
	this.searchdate = searchdate;
	this.traileroption = traileroption;
	this.hoursold = hoursold;
	this.loadtype = loadtype;
	this.sortype = sortype;
	
	this.originareacode = originareacd;
	this.destinationareacode = dstareacd;
	this.originareaselected = originselected;
	this.destareaselected = destselected;
	this.md5value=md5value;
	
	
}

	
	
	
	public DataModel(String origincounrty, String originstate,String origincity, String destinationcountry, String destinationcity,
			String destinationstate, String searchradius, String equipmenttype,
			String searchdate, String traileroption, String hoursold,
			String loadtype, String width,String length,String weight,String mindist,String ratepermile,String quantity,String specialInfo,String md5value) {
		super();
		this.origincounrty = origincounrty;
		this.originstate = originstate;
		this.origincity = origincity;
		this.destinationcountry = destinationcountry;
		this.destinationcity = destinationcity;
		this.destinationstate = destinationstate;
		this.searchradius = searchradius;
		this.equipmenttype = equipmenttype;
		this.searchdate = searchdate;
		this.traileroption = traileroption;
		this.hoursold = hoursold;
		this.loadtype = loadtype;
		this.width = width;
		this.length = length;
		this.weight = weight;
		this.mindist = mindist;
		this.ratepermile = ratepermile;
		this.quantity=quantity;
		this.specialInfo = specialInfo;
		/*this.originareacode = originareacd;
		this.destinationareacode = dstareacd;
		this.originareaselected = originselected;
		this.destareaselected = destselected;*/
		this.md5value=md5value;
	}
	
	
	

	public DataModel(Parcel in)
	{
		readFromParcel(in); 
	}
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
	
		 out.writeString(origincounrty);
		 out.writeString(originstate);
		 out.writeString(origincity);
		 out.writeString(destinationcountry);
		 out.writeString(destinationcity);
		 out.writeString(destinationstate);
		 out.writeString(searchradius);
		 out.writeString(equipmenttype);
		 out.writeString(searchdate);
		 out.writeString(traileroption);
		 out.writeString(hoursold);
		 out.writeString(loadtype);
		 out.writeString(sortype);
		 out.writeString(originareacode);
		 out.writeString(destinationareacode);
		 out.writeInt(originareaselected);
		 out.writeInt(destareaselected);
		 /*width,length,weight,mindist,ratepermile,specialInfo*/
		 out.writeString(width);
		 out.writeString(length);
		 out.writeString(weight);
		 out.writeString(mindist);
		 out.writeString(quantity);
		 out.writeString(ratepermile);
		 out.writeString(specialInfo);
		 out.writeInt(isselected);
		 out.writeString(md5value);
		 
		 
		 
		// out.writeString(originalrecord);
	}
	
	private void readFromParcel(Parcel in) {  
		origincounrty = in.readString();
		originstate= in.readString();
		origincity= in.readString();
		destinationcountry= in.readString();
		destinationcity= in.readString();
		destinationstate= in.readString();
		searchradius= in.readString();
		equipmenttype= in.readString();
		searchdate= in.readString();
		traileroption= in.readString();
		hoursold= in.readString();
		loadtype= in.readString();
		sortype= in.readString();
		originareacode= in.readString();
		destinationareacode= in.readString();
		originareaselected= in.readInt();
		destareaselected= in.readInt();
		width= in.readString();
		length= in.readString();
		weight= in.readString();
		mindist= in.readString();
		quantity=in.readString();
		ratepermile= in.readString();
		specialInfo= in.readString();
		isselected= in.readInt();
		md5value= in.readString();
		
	}
	
	
	
	
	
	
	
	
	public String getMd5value() {
		return md5value;
	}

	public void setMd5value(String md5value) {
		this.md5value = md5value;
	}

	public int getIsselected() {
		return isselected;
	}

	public void setIsselected(int isselected) {
		this.isselected = isselected;
	}

	public String getOriginareacode() {
		return originareacode;
	}

	public void setOriginareacode(String originareacode) {
		this.originareacode = originareacode;
	}

	public String getDestinationareacode() {
		return destinationareacode;
	}

	public void setDestinationareacode(String destinationareacode) {
		this.destinationareacode = destinationareacode;
	}

	public int getOriginareaselected() {
		return originareaselected;
	}

	public void setOriginareaselected(int originareaselected) {
		this.originareaselected = originareaselected;
	}

	public int getDestareaselected() {
		return destareaselected;
	}

	public void setDestareaselected(int destareaselected) {
		this.destareaselected = destareaselected;
	}

	public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

	public String getOrigincounrty() {
		return origincounrty;
	}

	public void setOrigincounrty(String origincounrty) {
		this.origincounrty = origincounrty;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getOriginstate() {
		return originstate;
	}

	public void setOriginstate(String originstate) {
		this.originstate = originstate;
	}

	public String getOrigincity() {
		return origincity;
	}

	public void setOrigincity(String origincity) {
		this.origincity = origincity;
	}

	public String getDestinationcountry() {
		return destinationcountry;
	}

	public void setDestinationcountry(String destinationcountry) {
		this.destinationcountry = destinationcountry;
	}

	public String getDestinationcity() {
		return destinationcity;
	}

	public void setDestinationcity(String destinationcity) {
		this.destinationcity = destinationcity;
	}

	public String getDestinationstate() {
		return destinationstate;
	}

	public void setDestinationstate(String destinationstate) {
		this.destinationstate = destinationstate;
	}

	public String getSearchradius() {
		return searchradius;
	}

	public void setSearchradius(String searchradius) {
		this.searchradius = searchradius;
	}

	public String getEquipmenttype() {
		return equipmenttype;
	}

	public void setEquipmenttype(String equipmenttype) {
		this.equipmenttype = equipmenttype;
	}

	public String getSearchdate() {
		return searchdate;
	}

	public void setSearchdate(String searchdate) {
		this.searchdate = searchdate;
	}

	public String getTraileroption() {
		return traileroption;
	}

	public void setTraileroption(String traileroption) {
		this.traileroption = traileroption;
	}

	public String getHoursold() {
		return hoursold;
	}

	public void setHoursold(String hoursold) {
		this.hoursold = hoursold;
	}

	public String getLoadtype() {
		return loadtype;
	}

	public void setLoadtype(String loadtype) {
		this.loadtype = loadtype;
	}

	public String getSortype() {
		return sortype;
	}

	public void setSortype(String sortype) {
		this.sortype = sortype;
	}
	
	

	
	
	
	
	
	
	
	public String getSpecialInfo() {
		return specialInfo;
	}

	public void setSpecialInfo(String specialInfo) {
		this.specialInfo = specialInfo;
	}

	public String getWidth() {
		return width;
	}

	public void setWidth(String width) {
		this.width = width;
	}

	public String getLength() {
		return length;
	}

	public void setLength(String length) {
		this.length = length;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getMindist() {
		return mindist;
	}

	public void setMindist(String mindist) {
		this.mindist = mindist;
	}

	public String getRatepermile() {
		return ratepermile;
	}

	public void setRatepermile(String ratepermile) {
		this.ratepermile = ratepermile;
	}
	/** This field is needed for Android to be able to 
	 * create new objects, individually or as arrays. 
 	 */ 
	
	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
	public DataModel createFromParcel(Parcel in) {
			return new DataModel(in); 
		}  
		public DataModel[] newArray(int size) { 
			return new DataModel[size]; 
		} 
	}; 
	
	
}
