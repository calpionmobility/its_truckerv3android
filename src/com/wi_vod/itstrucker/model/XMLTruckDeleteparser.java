package com.wi_vod.itstrucker.model;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import com.wi_vod.itstrucker.AppConstants;

public class XMLTruckDeleteparser {
	public static String errormessage=null;
	static private LoginProducts loginproducts=null;
	static private List<LoginProducts> products=new ArrayList<LoginProducts>();
	public static List<LoginProducts> getproducts(){
		return products;
	}
	
	
	public static String geterrormsg(){
		return errormessage;
	}
	public static boolean parsexml(String xmlstring) throws XmlPullParserException {
		XmlPullParserFactory factory = null;
		XmlPullParser parser = null;
		String text = null;
		
		try {
			factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			parser = factory.newPullParser();
			parser.setInput(new StringReader(xmlstring));

			int eventType = parser.getEventType();
			while (eventType != XmlPullParser.END_DOCUMENT) {
				String tagname = parser.getName();
				switch (eventType) {
				case XmlPullParser.START_TAG:
					break;

				case XmlPullParser.TEXT:
					text = parser.getText();
					break;

				case XmlPullParser.END_TAG:

				 if (tagname.equalsIgnoreCase("ErrorMessage")) {
						errormessage = text;
						 return false;
					}

					break;

				default:
					break;
				}
				eventType = parser.next();
			}

		} catch (XmlPullParserException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;

	}

}
