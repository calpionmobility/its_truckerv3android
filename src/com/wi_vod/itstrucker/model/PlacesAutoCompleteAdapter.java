package com.wi_vod.itstrucker.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.wi_vod.itstrucker.R;
import com.wi_vod.itstrucker.Utils;

import java.util.ArrayList;

public class PlacesAutoCompleteAdapter extends ArrayAdapter<String> implements Filterable,ListAdapter {
    private ArrayList<String> resultList;
    private Context context;
    private String countryab;
    private int  countryindex;
    
    private ArrayList<String> finalList;
    public PlacesAutoCompleteAdapter(Context context,String country, int countryindex,int textViewResourceId) {
        super(context,textViewResourceId);
        this.context=context;
        this.countryab=country;
        this.resultList=new ArrayList<String>();
        this.countryindex=countryindex;
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public String getItem(int index) {
        return resultList.get(index);
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    // Retrieve the autocomplete results.
                	
                    resultList = Utils.autocomplete(constraint.toString(), countryab,countryindex, context);
                    		//autocomplete(constraint.toString());
                    
                    // Footer
                    resultList.add("----");
                    
                    // Assign the data to the FilterResults
                    filterResults.values = resultList;
                    filterResults.count = resultList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                }
                else {
                    notifyDataSetInvalidated();
                }
            }};
        return filter;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
      //if (convertView == null) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
 
        if (position != (resultList.size() - 1))
            view = inflater.inflate(R.layout.list_item, null);
        else
            view = inflater.inflate(R.layout.autocomplete_google_logo, null);
    //}
    //else {
    //    view = convertView;
    //}
 
        if (position != (resultList.size() - 1)) {
            TextView autocompleteTextView = (TextView)view.findViewById(R.id.autocompleteText);
            autocompleteTextView.setText(resultList.get(position));
        }
        else {
            ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
            // not sure what to do <img class="emoji" draggable="false" alt="😀" src="http://s.w.org/images/core/emoji/72x72/1f600.png">
        }
 
    return view;
    }
    
}