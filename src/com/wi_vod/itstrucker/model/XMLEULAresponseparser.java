package com.wi_vod.itstrucker.model;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import com.wi_vod.itstrucker.AppConstants;

public class XMLEULAresponseparser {
	public static String errormessage=null;
	
	private static String eulaAcceptedid=null; 
	public static String getEULAId(){
		return eulaAcceptedid;
	}
	
	
	public static String geterrormsg(){
		return errormessage;
	}
	public static boolean parsexml(String xmlstring) throws XmlPullParserException {
		XmlPullParserFactory factory = null;
		XmlPullParser parser = null;
		String text = null;
		
		try {
			factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			parser = factory.newPullParser();
			parser.setInput(new StringReader(xmlstring));

			int eventType = parser.getEventType();
			while (eventType != XmlPullParser.END_DOCUMENT) {
				String tagname = parser.getName();
				switch (eventType) {
				case XmlPullParser.START_TAG:
					if (tagname.equalsIgnoreCase("ErrorMessage")) {
						//System.out.println("errormessage  " + text);
						//AppConstants.LOGIN_ERROR_MESSAGE = text;
					}
					break;

				case XmlPullParser.TEXT:
					text = parser.getText();
					break;

				case XmlPullParser.END_TAG:
				 if (tagname.equalsIgnoreCase("ErrorMessage")) {
					//	System.out.println("errormessage in XMLPARSER  " + text);
						errormessage = text;
						 return false;
					} else if (tagname.equalsIgnoreCase("ID")) {
						eulaAcceptedid=text;
					}
					break;

				default:
					break;
				}
				eventType = parser.next();
			}

		} catch (XmlPullParserException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;

	}

}
