package com.wi_vod.itstrucker.model;

public class SearchDetailLoadModel {

	private String age;
	private String bond;
	private String bondTypeID;
	private String credit;
	private String dotNumber;
	private String deletedId;
	private String deliveryDate;
	private String deliveryTime;
	private String destinationCity;
	private String destinationCountry;
	private String destinationState;
	private String destinationZip;
	private String distance;
	private String entered;
	private String equipment;
	private String trailerOptionType;
	private String experienceFactor;
	private String fuelCost;
	private String handleName;
	private String hasBonding;
	private String iD;
	private String miles;
	private boolean isDeleted;
	


	private String isFriend;
	private String length;
	private String loadType;
	private String mcNumber;
	private String mileage;
	private String originCity;
	private String originCountry;
	private String originState;
	private String originZip;
	private String paymentAmount;
	private String pickupDate;
	private String pickupTime;
	private String pointOfContact;
	private String pointOfContactPhone;
	private String pricePerGallon;
	private String quantity;
	private String specInfo;
	private String stops;
	private String tMCNumber;
	private String truckCompanyCity;
	private String truckCompanyEmail;
	private String truckCompanyFax;
	private String truckCompanyName;
	private String truckCompanyPhone;
	private String truckCompanyMobile;
	private String truckCompanyState;
	private String weight;
	private String width;
	private String postrated;
	private String ld_miles;
	private SeachDetailEquipmentTypes equipmenttypes;
	
	
	public String getEquipment() {
		return equipment;
	}


	public void setEquipment(String equipment) {
		this.equipment = equipment;
	}

	public SearchDetailLoadModel() {
		// TODO Auto-generated constructor stub
	}


	public String getAge() {
		return age;
	}


	public void setAge(String age) {
		this.age = age;
	}


	public SeachDetailEquipmentTypes getEquipmenttypes() {
		return equipmenttypes;
	}


	public void setEquipmenttypes(SeachDetailEquipmentTypes equipmenttypes) {
		this.equipmenttypes = equipmenttypes;
	}


	public String getBond() {
		return bond;
	}


	public void setBond(String bond) {
		this.bond = bond;
	}


	public String getBondTypeID() {
		return bondTypeID;
	}


	public void setBondTypeID(String bondTypeID) {
		this.bondTypeID = bondTypeID;
	}


	public String getCredit() {
		return credit;
	}


	public void setCredit(String credit) {
		this.credit = credit;
	}

	public String getMiles() {
		return miles;
	}


	public void setMiles(String miles) {
		this.miles = miles;
	}
	public String getDotNumber() {
		return dotNumber;
	}


	public void setDotNumber(String dotNumber) {
		this.dotNumber = dotNumber;
	}


	public String getDeletedId() {
		return deletedId;
	}


	public void setDeletedId(String deletedId) {
		this.deletedId = deletedId;
	}


	public String getDeliveryDate() {
		return deliveryDate;
	}


	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}


	public String getDeliveryTime() {
		return deliveryTime;
	}


	public void setDeliveryTime(String deliveryTime) {
		this.deliveryTime = deliveryTime;
	}


	public String getDestinationCity() {
		return destinationCity;
	}


	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}


	public String getDestinationCountry() {
		return destinationCountry;
	}


	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}


	public String getDestinationState() {
		return destinationState;
	}


	public void setDestinationState(String destinationState) {
		this.destinationState = destinationState;
	}


	public String getDestinationZip() {
		return destinationZip;
	}


	public void setDestinationZip(String destinationZip) {
		this.destinationZip = destinationZip;
	}


	public String getDistance() {
		return distance;
	}


	public void setDistance(String distance) {
		this.distance = distance;
	}


	public String getEntered() {
		return entered;
	}


	public void setEntered(String entered) {
		this.entered = entered;
	}


	public String getTrailerOptionType() {
		return trailerOptionType;
	}


	public void setTrailerOptionType(String trailerOptionType) {
		this.trailerOptionType = trailerOptionType;
	}


	public String getExperienceFactor() {
		return experienceFactor;
	}


	public void setExperienceFactor(String experienceFactor) {
		this.experienceFactor = experienceFactor;
	}


	public String getFuelCost() {
		return fuelCost;
	}


	public void setFuelCost(String fuelCost) {
		this.fuelCost = fuelCost;
	}


	public String getHandleName() {
		return handleName;
	}


	public void setHandleName(String handleName) {
		this.handleName = handleName;
	}


	public String getHasBonding() {
		return hasBonding;
	}


	public void setHasBonding(String hasBonding) {
		this.hasBonding = hasBonding;
	}


	public String getiD() {
		return iD;
	}


	public void setiD(String iD) {
		this.iD = iD;
	}


	public boolean isDeleted() {
		return isDeleted;
	}


	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}


	public String getIsFriend() {
		return isFriend;
	}


	public void setIsFriend(String isFriend) {
		this.isFriend = isFriend;
	}


	public String getLength() {
		return length;
	}


	public void setLength(String length) {
		this.length = length;
	}


	public String getLoadType() {
		return loadType;
	}


	public void setLoadType(String loadType) {
		this.loadType = loadType;
	}


	public String getMcNumber() {
		return mcNumber;
	}


	public void setMcNumber(String mcNumber) {
		this.mcNumber = mcNumber;
	}


	public String getMileage() {
		return mileage;
	}


	public void setMileage(String mileage) {
		this.mileage = mileage;
	}


	public String getOriginCity() {
		return originCity;
	}


	public void setOriginCity(String originCity) {
		this.originCity = originCity;
	}


	public String getOriginCountry() {
		return originCountry;
	}


	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}


	public String getOriginState() {
		return originState;
	}


	public void setOriginState(String originState) {
		this.originState = originState;
	}


	public String getOriginZip() {
		return originZip;
	}


	public void setOriginZip(String originZip) {
		this.originZip = originZip;
	}


	public String getPaymentAmount() {
		return paymentAmount;
	}


	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}


	public String getPickupDate() {
		return pickupDate;
	}


	public void setPickupDate(String pickupDate) {
		this.pickupDate = pickupDate;
	}


	public String getPickupTime() {
		return pickupTime;
	}


	public void setPickupTime(String pickupTime) {
		this.pickupTime = pickupTime;
	}


	public String getPointOfContact() {
		return pointOfContact;
	}


	public void setPointOfContact(String pointOfContact) {
		this.pointOfContact = pointOfContact;
	}


	public String getPointOfContactPhone() {
		return pointOfContactPhone;
	}


	public void setPointOfContactPhone(String pointOfContactPhone) {
		this.pointOfContactPhone = pointOfContactPhone;
	}


	public String getPricePerGallon() {
		return pricePerGallon;
	}


	public void setPricePerGallon(String pricePerGallon) {
		this.pricePerGallon = pricePerGallon;
	}


	public String getQuantity() {
		return quantity;
	}


	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}


	public String getSpecInfo() {
		return specInfo;
	}


	public void setSpecInfo(String specInfo) {
		this.specInfo = specInfo;
	}


	public String getStops() {
		return stops;
	}


	public void setStops(String stops) {
		this.stops = stops;
	}


	public String gettMCNumber() {
		return tMCNumber;
	}


	public void settMCNumber(String tMCNumber) {
		this.tMCNumber = tMCNumber;
	}


	public String getTruckCompanyCity() {
		return truckCompanyCity;
	}


	public void setTruckCompanyCity(String truckCompanyCity) {
		this.truckCompanyCity = truckCompanyCity;
	}


	public String getTruckCompanyEmail() {
		return truckCompanyEmail;
	}


	public void setTruckCompanyEmail(String truckCompanyEmail) {
		this.truckCompanyEmail = truckCompanyEmail;
	}


	public String getTruckCompanyFax() {
		return truckCompanyFax;
	}


	public void setTruckCompanyFax(String truckCompanyFax) {
		this.truckCompanyFax = truckCompanyFax;
	}


	public String getTruckCompanyName() {
		return truckCompanyName;
	}


	public void setTruckCompanyName(String truckCompanyName) {
		this.truckCompanyName = truckCompanyName;
	}


	public String getTruckCompanyPhone() {
		return truckCompanyPhone;
	}


	public void setTruckCompanyPhone(String truckCompanyPhone) {
		this.truckCompanyPhone = truckCompanyPhone;
	}


	
	public String getTruckCompanyMobile() {
		return truckCompanyMobile;
	}


	public void setTruckCompanyMobile(String truckCompanyMobile) {
		this.truckCompanyMobile = truckCompanyMobile;
	}

	
	
	public String getTruckCompanyState() {
		return truckCompanyState;
	}


	public void setTruckCompanyState(String truckCompanyState) {
		this.truckCompanyState = truckCompanyState;
	}


	public String getWeight() {
		return weight;
	}


	public void setWeight(String weight) {
		this.weight = weight;
	}


	public String getWidth() {
		return width;
	}


	public void setWidth(String width) {
		this.width = width;
	}
	
	
	public String getpostrated() {
		return postrated;
	}


	public void setpostrated(String postrated) {
		this.postrated = postrated;
	}
	
	public String getld_miles() {
		return ld_miles;
	}


	public void setld_miles(String ld_miles) {
		this.ld_miles = ld_miles;
	}
}
