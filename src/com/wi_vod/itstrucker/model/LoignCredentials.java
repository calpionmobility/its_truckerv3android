package com.wi_vod.itstrucker.model;

public class LoignCredentials {
	
	public int id;
	public String userName;
	public String handle;
	public String password;
	
public LoignCredentials() {
	// TODO Auto-generated constructor stub
}

public LoignCredentials(int id, String userName, String handle, String password) {
	super();
	this.id = id;
	this.userName = userName;
	this.handle = handle;
	this.password = password;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getUserName() {
	return userName;
}

public void setUserName(String userName) {
	this.userName = userName;
}

public String getHandle() {
	return handle;
}

public void setHandle(String handle) {
	this.handle = handle;
}

public String getPassword() {
	return password;
}

public void setPassword(String password) {
	this.password = password;
}



}
