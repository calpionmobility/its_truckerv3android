package com.wi_vod.itstrucker.model;

import android.os.Parcel;
import android.os.Parcelable;

public class PostDetailLoadModel implements Parcelable{
//pd stands for postdetail;
	private String pd_carrierRating; //<a:CarrierRating/>
	private String pd_companyId; // <a:CompanyID>00000000-0000-0000-0000-000000000000</a:CompanyID>
	private String pd_dotNumber;//  <a:DOTNumber/>
	private String pd_dateTruckAvailable;//<a:DateTruckAvailable>6/17/14</a:DateTruckAvailable>
	private String pd_destinationCity;// <a:DestinationCity/>
	private String pd_destinationCountry;//  <a:DestinationCountry>USA</a:DestinationCountry>
	private String  pd_destinationState;;// <a:DestinationState>AL</a:DestinationState>
	private String pd_dispatchname;//   <a:DispatchName/>
	private String pd_dispatchPhone;// <a:DispatchPhone/>
	private String pd_entered; // <a:Entered>2014-06-16T06:06:56.667</a:Entered>
	private String pd_equipment;// <a:Equipment>ANIM</a:Equipment>
	private String pd_trailerOptionType;//<b:TrailerOptionType>Expedited</b:TrailerOptionType>
	private SeachDetailEquipmentTypes equipmenttypes; 
	private String pd_hasFileCheck;// <a:HasFileCheck>false</a:HasFileCheck>
	private String pd_loadid; //<a:ID>46955822</a:ID>
	private String pd_isFriend;//<a:IsFriend>false</a:IsFriend>
	private String pd_isFullTruck; // <a:IsFullTruck>true</a:IsFullTruck>
	private String pd_length;// <a:Length>6.00</a:Length>
	private String pd_mcNumber;//<a:MCNumber/>
	private String pd_MinMiles;//  <a:MinMiles>8</a:MinMiles>
	private String pd_originCity;//<a:OriginCity>Boise</a:OriginCity>
	private String pd_originCountry;//  <a:OriginCountry>USA</a:OriginCountry>
	private String pd_originState;// <a:OriginState>ID</a:OriginState>
	private String pd_perMile; //<a:PerMile>6</a:PerMile>
	private String pd_percentFull;//<a:PercentFull/>
	private String pd_pointOfContact;// <a:PointOfContact/>
	private String pd_pointOfContactPhone;// <a:PointOfContactPhone/>
	private String pd_quantity;//<a:Quantity>5</a:Quantity>
	private String pd_specInfo;// <a:SpecInfo>vb</a:SpecInfo>
	private String pd_TCAMember;// <a:TCAMember>false</a:TCAMember>
	private String pd_tMCNumber;//<a:TMCNumber/>  
	private String pd_timeTruckAvailable;//<a:TimeTruckAvailable/>
	private String pd_truckCompanyCity;//<a:TruckCompanyCity>NEW PLYMOUTH</a:TruckCompanyCity>
	private String pd_truckCompanyEmail;//<a:TruckCompanyEmail>zackw@truckstop.com</a:TruckCompanyEmail>
	private String pd_truckCompanyFax;// <a:TruckCompanyFax/>
	private String pd_truckCompanyName;//  <a:TruckCompanyName>INTERNET TRUCKSTOP / ITS MOBILE</a:TruckCompanyName>
	private String pd_truckCompanyPhone;//  <a:TruckCompanyPhone/>
	private String pd_truckCompanyState;// <a:TruckCompanyState>ID</a:TruckCompanyState>
	private String pd_TruckDesiredDirection;//  <a:TruckDesiredDirection/>
	private String pd_truckNumber;//<a:TruckNumber i:nil="true"/>
	private String pd_weight;//   <a:Weight>8</a:Weight>
	private String pd_width;//  <a:Width>5</a:Width>
	private String pd_equipmenttypesdescription;//    <b:Description>Animal Carrier</b:Description>
	private String pd_equipmenttypescode;
   
	
	public String getPd_equipmenttypescode() {
		return pd_equipmenttypescode;
	}
	public void setPd_equipmenttypescode(String pd_equipmenttypescode) {
		this.pd_equipmenttypescode = pd_equipmenttypescode;
	}
	public String getPd_equipmenttypesdescription() {
		return pd_equipmenttypesdescription;
	}
	public void setPd_equipmenttypesdescription(String pd_equipmenttypesdescription) {
		this.pd_equipmenttypesdescription = pd_equipmenttypesdescription;
	}
	public String getPd_carrierRating() {
		return pd_carrierRating;
	}
	public void setPd_carrierRating(String pd_carrierRating) {
		this.pd_carrierRating = pd_carrierRating;
	}
	public String getPd_companyId() {
		return pd_companyId;
	}
	public void setPd_companyId(String pd_companyId) {
		this.pd_companyId = pd_companyId;
	}
	public String getPd_dotNumber() {
		return pd_dotNumber;
	}
	public void setPd_dotNumber(String pd_dotNumber) {
		this.pd_dotNumber = pd_dotNumber;
	}
	public String getPd_dateTruckAvailable() {
		return pd_dateTruckAvailable;
	}
	public void setPd_dateTruckAvailable(String pd_dateTruckAvailable) {
		this.pd_dateTruckAvailable = pd_dateTruckAvailable;
	}
	public String getPd_destinationCity() {
		return pd_destinationCity;
	}
	public void setPd_destinationCity(String pd_destinationCity) {
		this.pd_destinationCity = pd_destinationCity;
	}
	public String getPd_destinationCountry() {
		return pd_destinationCountry;
	}
	public void setPd_destinationCountry(String pd_destinationCountry) {
		this.pd_destinationCountry = pd_destinationCountry;
	}
	public String getPd_destinationState() {
		return pd_destinationState;
	}
	public void setPd_destinationState(String pd_destinationState) {
		this.pd_destinationState = pd_destinationState;
	}
	public String getPd_dispatchname() {
		return pd_dispatchname;
	}
	public void setPd_dispatchname(String pd_dispatchname) {
		this.pd_dispatchname = pd_dispatchname;
	}
	public String getPd_dispatchPhone() {
		return pd_dispatchPhone;
	}
	public void setPd_dispatchPhone(String pd_dispatchPhone) {
		this.pd_dispatchPhone = pd_dispatchPhone;
	}
	public String getEntered() {
		return pd_entered;
	}
	public void setEntered(String entered) {
		this.pd_entered = entered;
	}
	public String getEquipment() {
		return pd_equipment;
	}
	public void setEquipment(String equipment) {
		this.pd_equipment = equipment;
	}
	public String getTrailerOptionType() {
		return pd_trailerOptionType;
	}
	public void setTrailerOptionType(String trailerOptionType) {
		this.pd_trailerOptionType = trailerOptionType;
	}
	public SeachDetailEquipmentTypes getEquipmenttypes() {
		return equipmenttypes;
	}
	public void setEquipmenttypes(SeachDetailEquipmentTypes equipmenttypes) {
		this.equipmenttypes = equipmenttypes;
	}
	public String getPd_hasFileCheck() {
		return pd_hasFileCheck;
	}
	public void setPd_hasFileCheck(String pd_hasFileCheck) {
		this.pd_hasFileCheck = pd_hasFileCheck;
	}
	public String getPd_loadid() {
		return pd_loadid;
	}
	public void setPd_loadid(String pd_loadid) {
		this.pd_loadid = pd_loadid;
	}
	public String getPd_isFriend() {
		return pd_isFriend;
	}
	public void setPd_isFriend(String pd_isFriend) {
		this.pd_isFriend = pd_isFriend;
	}
	public String getPd_isFullTruck() {
		return pd_isFullTruck;
	}
	public void setPd_isFullTruck(String pd_isFullTruck) {
		this.pd_isFullTruck = pd_isFullTruck;
	}
	public String getPd_length() {
		return pd_length;
	}
	public void setPd_length(String pd_length) {
		this.pd_length = pd_length;
	}
	public String getPd_mcNumber() {
		return pd_mcNumber;
	}
	public void setPd_mcNumber(String pd_mcNumber) {
		this.pd_mcNumber = pd_mcNumber;
	}
	public String getPd_MinMiles() {
		return pd_MinMiles;
	}
	public void setPd_MinMiles(String pd_MinMiles) {
		this.pd_MinMiles = pd_MinMiles;
	}
	public String getPd_originCity() {
		return pd_originCity;
	}
	public void setPd_originCity(String pd_originCity) {
		this.pd_originCity = pd_originCity;
	}
	public String getPd_originCountry() {
		return pd_originCountry;
	}
	public void setPd_originCountry(String pd_originCountry) {
		this.pd_originCountry = pd_originCountry;
	}
	public String getPd_originState() {
		return pd_originState;
	}
	public void setPd_originState(String pd_originState) {
		this.pd_originState = pd_originState;
	}
	public String getPd_perMile() {
		return pd_perMile;
	}
	public void setPd_perMile(String pd_perMile) {
		this.pd_perMile = pd_perMile;
	}
	public String getPd_percentFull() {
		return pd_percentFull;
	}
	public void setPd_percentFull(String pd_percentFull) {
		this.pd_percentFull = pd_percentFull;
	}
	public String getPd_pointOfContact() {
		return pd_pointOfContact;
	}
	public void setPd_pointOfContact(String pd_pointOfContact) {
		this.pd_pointOfContact = pd_pointOfContact;
	}
	public String getPd_pointOfContactPhone() {
		return pd_pointOfContactPhone;
	}
	public void setPd_pointOfContactPhone(String pd_pointOfContactPhone) {
		this.pd_pointOfContactPhone = pd_pointOfContactPhone;
	}
	public String getPd_quantity() {
		return pd_quantity;
	}
	public void setPd_quantity(String pd_quantity) {
		this.pd_quantity = pd_quantity;
	}
	public String getPd_specInfo() {
		return pd_specInfo;
	}
	public void setPd_specInfo(String pd_specInfo) {
		this.pd_specInfo = pd_specInfo;
	}
	public String getPd_TCAMember() {
		return pd_TCAMember;
	}
	public void setPd_TCAMember(String pd_TCAMember) {
		this.pd_TCAMember = pd_TCAMember;
	}
	public String getPd_tMCNumber() {
		return pd_tMCNumber;
	}
	public void setPd_tMCNumber(String pd_tMCNumber) {
		this.pd_tMCNumber = pd_tMCNumber;
	}
	public String getPd_timeTruckAvailable() {
		return pd_timeTruckAvailable;
	}
	public void setPd_timeTruckAvailable(String pd_timeTruckAvailable) {
		this.pd_timeTruckAvailable = pd_timeTruckAvailable;
	}
	public String getPd_truckCompanyCity() {
		return pd_truckCompanyCity;
	}
	public void setPd_truckCompanyCity(String pd_truckCompanyCity) {
		this.pd_truckCompanyCity = pd_truckCompanyCity;
	}
	public String getPd_truckCompanyEmail() {
		return pd_truckCompanyEmail;
	}
	public void setPd_truckCompanyEmail(String pd_truckCompanyEmail) {
		this.pd_truckCompanyEmail = pd_truckCompanyEmail;
	}
	public String getPd_truckCompanyFax() {
		return pd_truckCompanyFax;
	}
	public void setPd_truckCompanyFax(String pd_truckCompanyFax) {
		this.pd_truckCompanyFax = pd_truckCompanyFax;
	}
	public String getPd_truckCompanyName() {
		return pd_truckCompanyName;
	}
	public void setPd_truckCompanyName(String pd_truckCompanyName) {
		this.pd_truckCompanyName = pd_truckCompanyName;
	}
	public String getPd_truckCompanyPhone() {
		return pd_truckCompanyPhone;
	}
	public void setPd_truckCompanyPhone(String pd_truckCompanyPhone) {
		this.pd_truckCompanyPhone = pd_truckCompanyPhone;
	}
	public String getPd_truckCompanyState() {
		return pd_truckCompanyState;
	}
	public void setPd_truckCompanyState(String pd_truckCompanyState) {
		this.pd_truckCompanyState = pd_truckCompanyState;
	}
	public String getPd_TruckDesiredDirection() {
		return pd_TruckDesiredDirection;
	}
	public void setPd_TruckDesiredDirection(String pd_TruckDesiredDirection) {
		this.pd_TruckDesiredDirection = pd_TruckDesiredDirection;
	}
	public String getPd_truckNumber() {
		return pd_truckNumber;
	}
	public void setPd_truckNumber(String pd_truckNumber) {
		this.pd_truckNumber = pd_truckNumber;
	}
	public String getPd_weight() {
		return pd_weight;
	}
	public void setPd_weight(String pd_weight) {
		this.pd_weight = pd_weight;
	}
	public String getPd_width() {
		return pd_width;
	}
	public void setPd_width(String pd_width) {
		this.pd_width = pd_width;
	}
	
	
	

	public PostDetailLoadModel() {
		// TODO Auto-generated constructor stub
	}
	
	public PostDetailLoadModel(Parcel in)
	{
		readFromParcel(in); 
	}
	


	@Override
	public void writeToParcel(Parcel out, int flags) {
	
		 out.writeString(pd_carrierRating);
		 out.writeString(pd_companyId );
		 out.writeString(pd_dotNumber);
		 out.writeString(pd_dateTruckAvailable);
		 out.writeString(pd_destinationCity);
		 out.writeString(pd_destinationCountry);
		 out.writeString(pd_destinationState);
		 out.writeString(pd_dispatchname);
		 out.writeString(pd_dispatchPhone);
		 out.writeString(pd_entered );
		 out.writeString(pd_equipment);
		 out.writeString(pd_trailerOptionType);
		 out.writeString(pd_equipmenttypesdescription);
		 out.writeString(pd_equipmenttypescode);
		 
		 out.writeString(pd_hasFileCheck);
		 out.writeString(pd_loadid);
		 out.writeString(pd_isFriend);
		 out.writeString(pd_isFullTruck );
		 out.writeString(pd_length);
		 out.writeString(pd_mcNumber);
		 out.writeString(pd_MinMiles);
		 out.writeString(pd_originCity);
		 out.writeString(pd_originCountry);
		 out.writeString(pd_originState);
		 out.writeString(pd_perMile );
		 out.writeString(pd_percentFull);
		 out.writeString(pd_pointOfContact);
		 out.writeString(pd_pointOfContactPhone);
		 out.writeString(pd_quantity);
		 
		 
		 out.writeString(pd_specInfo);
		 out.writeString(pd_TCAMember);
		 out.writeString(pd_tMCNumber);
		 
		 out.writeString(pd_timeTruckAvailable);
		 out.writeString(pd_truckCompanyCity);
		 
		 out.writeString(pd_truckCompanyEmail);
		 out.writeString(pd_truckCompanyFax);
		 out.writeString(pd_truckCompanyName);
		 out.writeString(pd_truckCompanyPhone);
		 out.writeString(pd_truckCompanyState);
		 
		 out.writeString(pd_TruckDesiredDirection);
		 out.writeString(pd_truckNumber);
		 out.writeString(pd_weight);
		 out.writeString(pd_width);
		
	}
	
	private void readFromParcel(Parcel in) {  
		pd_carrierRating = in.readString();
		pd_companyId  = in.readString();
		pd_dotNumber = in.readString();
		pd_dateTruckAvailable = in.readString();
		pd_destinationCity = in.readString();
		pd_destinationCountry = in.readString();
		pd_destinationState = in.readString();
		pd_dispatchname = in.readString();
		pd_dispatchPhone = in.readString();
		pd_entered = in.readString();
		pd_equipment = in.readString();
		pd_trailerOptionType = in.readString();
		pd_equipmenttypesdescription  = in.readString();
		pd_equipmenttypescode= in.readString();
		pd_hasFileCheck = in.readString();
		pd_loadid  = in.readString();
		pd_isFriend = in.readString();
		pd_isFullTruck  = in.readString();
		pd_length = in.readString();
		pd_mcNumber = in.readString();
		pd_MinMiles = in.readString();
		pd_originCity = in.readString();
		pd_originCountry = in.readString();
		pd_originState = in.readString();
		pd_perMile  = in.readString();
		pd_percentFull = in.readString();
		pd_pointOfContact = in.readString();
		pd_pointOfContactPhone = in.readString();
		pd_quantity = in.readString();
		pd_specInfo = in.readString();
		pd_TCAMember = in.readString();
		pd_tMCNumber = in.readString();
		pd_timeTruckAvailable = in.readString();
		pd_truckCompanyCity = in.readString();
		pd_truckCompanyEmail = in.readString();
		pd_truckCompanyFax = in.readString();
		pd_truckCompanyName = in.readString();
		pd_truckCompanyPhone = in.readString();
		pd_truckCompanyState = in.readString();
		
		pd_TruckDesiredDirection = in.readString();
		pd_truckNumber = in.readString();
		pd_weight = in.readString();
		pd_width = in.readString();
		
		
		//originalrecord = in.readString();
		
	}
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	/** This field is needed for Android to be able to 
	 * create new objects, individually or as arrays. 
 	 */ 
	
	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
	public PostDetailLoadModel createFromParcel(Parcel in) {
			return new PostDetailLoadModel(in); 
		}  
		public PostDetailLoadModel[] newArray(int size) { 
			return new PostDetailLoadModel[size]; 
		} 
	}; 

	
}
