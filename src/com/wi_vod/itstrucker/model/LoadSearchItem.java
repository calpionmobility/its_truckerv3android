package com.wi_vod.itstrucker.model;



import android.os.Parcel;
import android.os.Parcelable;

public class LoadSearchItem implements Parcelable {
	private String age ;  				 //<a:Age>9+</a:Age>
	private String bond ;			// <a:Bond>0</a:Bond>
	private String bondenabled ;		//  <a:BondEnabled>false</a:BondEnabled>
	private String bondtypeid ;	// <a:BondTypeID>9999</a:BondTypeID>
	private String companyname ;			//<a:CompanyName>INDUSTRI</a:CompanyName>
	private String days2pay ;			// <a:Days2Pay/>
	private String destinationcity ;			// <a:DestinationCity>RANCHO CUCAMONGA</a:DestinationCity>
	private String destinationCountry ;			// <a:DestinationCountry>USA</a:DestinationCountry>
	private String destinationDistance ;			//  <a:DestinationDistance>0</a:DestinationDistance>
	private String destinationState ;			//  <a:DestinationState>CA</a:DestinationState>
	private String equipment ;			//  <a:Equipment>F</a:Equipment>
	private String trialerOptionType ;			//   <b:TrailerOptionType>Tarps</b:TrailerOptionType>
	private String experienceFactor ;			//    <a:ExperienceFactor/>
	private String fuelCost ;			//  <a:FuelCost/>
	private String loadID ;			//  <a:ID>813224646</a:ID>
	private String isFriend ;			//   <a:IsFriend>false</a:IsFriend>
	private String length ;			//    <a:Length></a:Length>
	private String loadType ;			//   <a:LoadType>Full</a:LoadType>
	private String miles ;			//      <a:Miles>832</a:Miles>
	private String originCity ;			//   <a:OriginCity>CALDWELL</a:OriginCity>
	private String originCountry ;			//     <a:OriginCountry>USA</a:OriginCountry>
	private String originDistance ;			//    <a:OriginDistance>0</a:OriginDistance>
	private String originState ;			//      <a:OriginState>ID</a:OriginState>
	private String payment ;			//     <a:Payment>0</a:Payment>
	private String pickupDate ;			//     <a:PickUpDate>01/21/14</a:PickUpDate>
	private String pointOfContactPhone ;			//       <a:PointOfContactPhone>406-363-1681</a:PointOfContactPhone>
	private String pricePerGall ;			//     <a:PricePerGall>0</a:PricePerGall>
	private String weight ;			//    <a:Weight></a:Weight>
	
	
	public LoadSearchItem() {
		// TODO Auto-generated constructor stub
	}
	
	public LoadSearchItem(Parcel in)
	{
		readFromParcel(in); 
	}
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
	
		 out.writeString(age);
		 out.writeString(bond);
		 out.writeString(bondenabled);
		 out.writeString(bondtypeid);
		 out.writeString(companyname);
		 out.writeString(days2pay);
		 out.writeString(destinationcity);
		 out.writeString(destinationCountry);
		 out.writeString(destinationDistance);
		 out.writeString(destinationState);
		 out.writeString(equipment);
		 out.writeString(trialerOptionType);
		 out.writeString(experienceFactor);
		 out.writeString(fuelCost);
		 out.writeString(loadID);
		 out.writeString(isFriend);
		 out.writeString(length);
		 out.writeString(loadType);
		 out.writeString(miles);
		 out.writeString(originCity);
		 out.writeString(originCountry);
		 out.writeString(originDistance);
		 out.writeString(originState);
		 out.writeString(payment);
		 out.writeString(pickupDate);
		 out.writeString(pointOfContactPhone);
		 out.writeString(pricePerGall);
		 out.writeString(weight);
		
	}
	
	private void readFromParcel(Parcel in) {  
		age = in.readString();
		bond = in.readString();
		bondenabled = in.readString();
		bondtypeid = in.readString();
		companyname = in.readString();
		days2pay = in.readString();
		destinationcity = in.readString();
		destinationCountry = in.readString();
		destinationDistance = in.readString();
		destinationState = in.readString();
		equipment = in.readString();
		trialerOptionType = in.readString();
		experienceFactor = in.readString();
		fuelCost = in.readString();
		loadID = in.readString();
		isFriend = in.readString();
		length = in.readString();
		loadType = in.readString();
		miles = in.readString();
		originCity = in.readString();
		originCountry = in.readString();
		originDistance = in.readString();
		originState = in.readString();
		payment = in.readString();
		pickupDate = in.readString();
		pointOfContactPhone = in.readString();
		pricePerGall = in.readString();
		weight = in.readString();
		
		
		//originalrecord = in.readString();
		
	}
	
	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getBond() {
		return bond;
	}

	public void setBond(String bond) {
		this.bond = bond;
	}

	public String getBondenabled() {
		return bondenabled;
	}

	public void setBondenabled(String bondenabled) {
		this.bondenabled = bondenabled;
	}

	public String getBondtypeid() {
		return bondtypeid;
	}

	public void setBondtypeid(String bondtypeid) {
		this.bondtypeid = bondtypeid;
	}

	public String getCompanyname() {
		return companyname;
	}

	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}

	public String getDays2pay() {
		return days2pay;
	}

	public void setDays2pay(String days2pay) {
		this.days2pay = days2pay;
	}

	public String getDestinationcity() {
		return destinationcity;
	}

	public void setDestinationcity(String destinationcity) {
		this.destinationcity = destinationcity;
	}

	public String getDestinationCountry() {
		return destinationCountry;
	}

	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}

	public String getDestinationDistance() {
		return destinationDistance;
	}

	public void setDestinationDistance(String destinationDistance) {
		this.destinationDistance = destinationDistance;
	}

	public String getDestinationState() {
		return destinationState;
	}

	public void setDestinationState(String destinationState) {
		this.destinationState = destinationState;
	}

	public String getEquipment() {
		return equipment;
	}

	public void setEquipment(String equipment) {
		this.equipment = equipment;
	}

	public String getTrialerOptionType() {
		return trialerOptionType;
	}

	public void setTrialerOptionType(String trialerOptionType) {
		this.trialerOptionType = trialerOptionType;
	}

	public String getExperienceFactor() {
		return experienceFactor;
	}

	public void setExperienceFactor(String experienceFactor) {
		this.experienceFactor = experienceFactor;
	}

	public String getFuelCost() {
		return fuelCost;
	}

	public void setFuelCost(String fuelCost) {
		this.fuelCost = fuelCost;
	}

	public String getLoadID() {
		return loadID;
	}

	public void setLoadID(String loadID) {
		this.loadID = loadID;
	}

	public String getIsFriend() {
		return isFriend;
	}

	public void setIsFriend(String isFriend) {
		this.isFriend = isFriend;
	}

	public String getLength() {
		return length;
	}

	public void setLength(String length) {
		this.length = length;
	}

	public String getLoadType() {
		return loadType;
	}

	public void setLoadType(String loadType) {
		this.loadType = loadType;
	}

	public String getMiles() {
		return miles;
	}

	public void setMiles(String miles) {
		this.miles = miles;
	}

	public String getOriginCity() {
		return originCity;
	}

	public void setOriginCity(String originCity) {
		this.originCity = originCity;
	}

	public String getOriginCountry() {
		return originCountry;
	}

	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}

	public String getOriginDistance() {
		return originDistance;
	}

	public void setOriginDistance(String originDistance) {
		this.originDistance = originDistance;
	}

	public String getOriginState() {
		return originState;
	}

	public void setOriginState(String originState) {
		this.originState = originState;
	}

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	public String getPickupDate() {
		return pickupDate;
	}

	public void setPickupDate(String pickupDate) {
		this.pickupDate = pickupDate;
	}

	public String getPointOfContactPhone() {
		return pointOfContactPhone;
	}

	public void setPointOfContactPhone(String pointOfContactPhone) {
		this.pointOfContactPhone = pointOfContactPhone;
	}

	public String getPricePerGall() {
		return pricePerGall;
	}

	public void setPricePerGall(String pricePerGall) {
		this.pricePerGall = pricePerGall;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}





	




	/** This field is needed for Android to be able to 
	 * create new objects, individually or as arrays. 
 	 */ 
	
	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
	public LoadSearchItem createFromParcel(Parcel in) {
			return new LoadSearchItem(in); 
		}  
		public LoadSearchItem[] newArray(int size) { 
			return new LoadSearchItem[size]; 
		} 
	}; 
}
