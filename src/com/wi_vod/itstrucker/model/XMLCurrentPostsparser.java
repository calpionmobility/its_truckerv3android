package com.wi_vod.itstrucker.model;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import com.wi_vod.itstrucker.AppConstants;

public class XMLCurrentPostsparser {
	public static String errormessage = null;
	static private CurrentTruckPostsModel posts = null;
	static private ArrayList<CurrentTruckPostsModel> currentposts = new ArrayList<CurrentTruckPostsModel>();

	public static ArrayList<CurrentTruckPostsModel> getCurrentTrucks() {
		return currentposts;
	}

	public static String geterrormsg() {
		return errormessage;
	}

	public static boolean parsexml(String xmlstring)
			throws XmlPullParserException {
		XmlPullParserFactory factory = null;
		XmlPullParser parser = null;
		String text = null;

		try {
			factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			parser = factory.newPullParser();
			parser.setInput(new StringReader(xmlstring));

			int eventType = parser.getEventType();
			while (eventType != XmlPullParser.END_DOCUMENT) {
				String tagname = parser.getName();
				switch (eventType) {
				case XmlPullParser.START_TAG:
					if (tagname.equalsIgnoreCase("ErrorMessage")) {
						//System.out.println("errormessage  " + text);
						// AppConstants.LOGIN_ERROR_MESSAGE = text;
					}
					
					if (tagname.equalsIgnoreCase("PostedTrucks")) {
						currentposts.clear();
					}
					if (tagname.equalsIgnoreCase("TruckPost")) {
						posts = new CurrentTruckPostsModel();
					}
					break;

				case XmlPullParser.TEXT:
					text = parser.getText();
					break;

				case XmlPullParser.END_TAG:

					if (tagname.equalsIgnoreCase("ErrorMessage")) {
						System.out .println("errormessage in XMLPARSER  " + text);
						errormessage = text;
						return false;

					} else {
						if (tagname.equalsIgnoreCase("TruckPost")) {
							currentposts.add(posts);
						} else if (tagname.equalsIgnoreCase("DateAvailable")) {
							posts.setDateAvailable(text);
							text="";

						} else if (tagname.equalsIgnoreCase("DestinationCity")) {
							posts.setDestinationCity(text);
							text="";

						} else if (tagname
								.equalsIgnoreCase("DestinationCountry")) {
							posts.setDestinationCountry(text);
							text="";

						} else if (tagname.equalsIgnoreCase("DestinationState")) {
							posts.setDestinationState(text);
							text="";
						} else if (tagname.equalsIgnoreCase("Equipment")) {
							posts.setEquipment(text);
							text="";
						} else if (tagname
								.equalsIgnoreCase("TrailerOptionType")) {
							posts.setTrailerOptionType(text);
							text="";
						} else if (tagname.equalsIgnoreCase("Handle")) {
							posts.setHandle(text);
							text="";
						} else if (tagname.equalsIgnoreCase("Id")) {
							posts.setId(text);
							text="";
						} else if (tagname.equalsIgnoreCase("IsDaily")) {
							posts.setIsDaily(Boolean.valueOf(text));
							text="";

						} else if (tagname.equalsIgnoreCase("IsFull")) {
							if (!text.equals("")||text!=null) {
								if (text.equals("false")) {
									posts.setIsFull("LTL");
								}else{
									posts.setIsFull("Full");
								}
							}
							text="";

						} else if (tagname.equalsIgnoreCase("OriginCity")) {
							posts.setOriginCity(text);
							text="";
						} else if (tagname.equalsIgnoreCase("OriginCountry")) {
							posts.setOriginCountry(text);
							text="";
						} else if (tagname.equalsIgnoreCase("OriginState")) {
							posts.setOriginState(text);
							text="";
						} else if (tagname.equalsIgnoreCase("TruckMultiDest")) {
							posts.setTruckMultiDest(text);
							text="";
						} else if (tagname.equalsIgnoreCase("TruckNumber")) {
							posts.setTruckNumber(text);
							text="";
						}
					}

					break;

				default:
					break;
				}
				eventType = parser.next();
			}

		} catch (XmlPullParserException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;

	}

}
