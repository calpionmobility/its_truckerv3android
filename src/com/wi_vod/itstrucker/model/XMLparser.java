package com.wi_vod.itstrucker.model;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import com.wi_vod.itstrucker.AppConstants;

public class XMLparser {
	public static String errormessage = null;
	public static int integrationId;
	public static String errorcode = null;
	public static String accountverified = null;
	
	public static String companyID = null;
	public static String handleID = null;

	static private LoginProducts loginproducts = null;
	static private List<LoginProducts> products = new ArrayList<LoginProducts>();

	public static List<LoginProducts> getproducts() {
		return products;
	}

	public static String geterrormsg() {
		return errormessage;
	}

	public static int getintegrationId() {
		return integrationId;
	}
	public static String getcompanyID() {
		return companyID;
	}
	public static String gethandleID() {
		return handleID;
	}
	
	
	
	public static boolean parsexml(String xmlstring)
			throws XmlPullParserException {
		XmlPullParserFactory factory = null;
		XmlPullParser parser = null;
		String text = null;

		try {
			factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			parser = factory.newPullParser();
			parser.setInput(new StringReader(xmlstring));

			int eventType = parser.getEventType();
			while (eventType != XmlPullParser.END_DOCUMENT) {
				String tagname = parser.getName();
				switch (eventType) {
				case XmlPullParser.START_TAG:
					if (tagname.equalsIgnoreCase("ErrorMessage")) {
						//System.out.println("errormessage  " + text);
						// AppConstants.LOGIN_ERROR_MESSAGE = text;
					}

					if (tagname.equalsIgnoreCase("Product")) {
						loginproducts = new LoginProducts();
					}
					break;

				case XmlPullParser.TEXT:
					text = parser.getText();
					break;

				case XmlPullParser.END_TAG:
					if (tagname.equalsIgnoreCase("CompanyID")) {
						companyID = text;
					}
					if (tagname.equalsIgnoreCase("HandleID")) {
						handleID = text;
					}
					if (tagname.equalsIgnoreCase("ErrorCode")) {
						errorcode = text;
					}
					if (tagname.equalsIgnoreCase("Verified")) {
						accountverified = text;
					}

					if (tagname.equalsIgnoreCase("IntegrationID")) {
					//	System.out.println("IntegrationID  " + text);
						integrationId = Integer.valueOf(text);
					}
					if (tagname.equalsIgnoreCase("ErrorMessage")) {
					//	System.out.println("errormessage in XMLPARSER  " + text);
						errormessage = text;
						//return false;

					} else {
						if (tagname.equalsIgnoreCase("Product")) {
							products.add(loginproducts);
						} else if (tagname.equalsIgnoreCase("ProductID")) {
							loginproducts.setProductid(text);

						} else if (tagname.equalsIgnoreCase("Name")) {
							loginproducts.setName(text);

						} else if (tagname.equalsIgnoreCase("ProductStatus")) {
							loginproducts.setProductstatus(text);

						} else if (tagname.equalsIgnoreCase("EndDate")) {
							loginproducts.setEnddate(text);

						}
					}

					break;

				default:
					break;
				}
				eventType = parser.next();
			}

		} catch (XmlPullParserException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;

	}

}
