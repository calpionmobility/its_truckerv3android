package com.wi_vod.itstrucker.model;

public class CurrentTruckPostsModel {
	
	private String dateAvailable;
	private String destinationCity;
	private String destinationCountry;
	private String destinationState;
	
	private String equipment;
	private String trailerOptionType;
	private String handle;
	private String id;
	private boolean isDaily;
	private String isFull;
	private String originCity;
	private String originCountry;
	private String originState;
	private String truckMultiDest;
	private String truckNumber;
	
	private boolean isselected;
	
	public boolean isIsselected() {
		return isselected;
	}

	public void setIsselected(boolean isselected) {
		this.isselected = isselected;
	}

	public void setDaily(boolean isDaily) {
		this.isDaily = isDaily;
	}

	public CurrentTruckPostsModel() {
		// TODO Auto-generated constructor stub
	}
	

	public String getDateAvailable() {
		return dateAvailable;
	}

	public void setDateAvailable(String dateAvailable) {
		this.dateAvailable = dateAvailable;
	}

	public String getDestinationCity() {
		return destinationCity;
	}

	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}

	public String getDestinationCountry() {
		return destinationCountry;
	}

	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}

	public String getDestinationState() {
		return destinationState;
	}

	public void setDestinationState(String destinationState) {
		this.destinationState = destinationState;
	}

	public String getEquipment() {
		return equipment;
	}

	public void setEquipment(String equipment) {
		this.equipment = equipment;
	}

	public String getTrailerOptionType() {
		return trailerOptionType;
	}

	public void setTrailerOptionType(String trailerOptionType) {
		this.trailerOptionType = trailerOptionType;
	}

	public String getHandle() {
		return handle;
	}

	public void setHandle(String handle) {
		this.handle = handle;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean getIsDaily() {
		return isDaily;
	}

	public void setIsDaily(boolean isDaily) {
		this.isDaily = isDaily;
	}

	public String getIsFull() {
		return isFull;
	}

	public void setIsFull(String isFull) {
		this.isFull = isFull;
	}

	public String getOriginCity() {
		return originCity;
	}

	public void setOriginCity(String originCity) {
		this.originCity = originCity;
	}

	public String getOriginCountry() {
		return originCountry;
	}

	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}

	public String getOriginState() {
		return originState;
	}

	public void setOriginState(String originState) {
		this.originState = originState;
	}

	public String getTruckMultiDest() {
		return truckMultiDest;
	}

	public void setTruckMultiDest(String truckMultiDest) {
		this.truckMultiDest = truckMultiDest;
	}

	public String getTruckNumber() {
		return truckNumber;
	}

	public void setTruckNumber(String truckNumber) {
		this.truckNumber = truckNumber;
	}
	


}
