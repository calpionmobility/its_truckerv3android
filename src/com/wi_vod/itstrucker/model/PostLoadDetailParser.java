package com.wi_vod.itstrucker.model;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class PostLoadDetailParser {
	static private PostDetailLoadModel items=null;
	static private SeachDetailEquipmentTypes equipmenttypes=null;
	static private List<PostDetailLoadModel> postdetailitems=new ArrayList<PostDetailLoadModel>();
	
	public static String errormessage=""; 
	public static PostDetailLoadModel  getpostdetailItems(){
		return items;
	}
	
	public static String geterrormmsg(){
		//System.out.println("errormessage in geterrormmsg method               "+errormessage);
		return errormessage;
	}
	
	public static boolean parsexml(String xmlstring) throws XmlPullParserException {
		XmlPullParserFactory factory = null;
		XmlPullParser parser = null;
		String text = null;
	
		try {
			factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			parser = factory.newPullParser();
			parser.setInput(new StringReader(xmlstring));

			int eventType = parser.getEventType();
			while (eventType != XmlPullParser.END_DOCUMENT) {
				String tagname = parser.getName();
				switch (eventType) {
				case XmlPullParser.START_TAG:
					if (tagname.equalsIgnoreCase("Error")) {
						System.out.println("errormessagea  " + text);
					}
					if (tagname.equalsIgnoreCase("GetTruckDetailResultsResult")) {
						postdetailitems.clear();
					}
					if (tagname.equalsIgnoreCase("TruckDetail")) {
						items=new PostDetailLoadModel();
					}
					if (tagname.equalsIgnoreCase("EquipmentTypes")) {
						equipmenttypes=new SeachDetailEquipmentTypes();
					}
					break;

				case XmlPullParser.TEXT:
					text = parser.getText();
					break;

				case XmlPullParser.END_TAG:
					if (tagname.equalsIgnoreCase("ErrorMessage")) {
						errormessage=text;
						return false;
					}else {
						
						if (tagname.equalsIgnoreCase("TruckDetail")) {
							postdetailitems.add(items);
						} else if (tagname.equalsIgnoreCase("CarrierRating")) {
							
							items.setPd_carrierRating(text);
							text="";
							//System.out.println("" + text);
						} else if (tagname.equalsIgnoreCase("CompanyID")) {
							items.setPd_companyId(text);
							text="";
							//System.out.println("" + text);
						}  else if (tagname.equalsIgnoreCase("DOTNumber")) {
							items.setPd_dotNumber(text);
							text="";
							//System.out.println("" + text);
						}else if (tagname.equalsIgnoreCase("DateTruckAvailable")) {
							items.setPd_dateTruckAvailable(text);
							text="";
							//System.out.println("" + text);
						} else if (tagname.equalsIgnoreCase("DestinationCity")) {
							items.setPd_destinationCity(text);
							text="";
						} else if (tagname.equalsIgnoreCase("DestinationCountry")) {
							items.setPd_destinationCountry(text);
							text="";
						}else if (tagname.equalsIgnoreCase("DestinationState")) {
							items.setPd_destinationState(text);
							text="";
						}else if (tagname.equalsIgnoreCase("DispatchName")) {
							items.setPd_dispatchname(text);
							text="";
						} else if (tagname.equalsIgnoreCase("DispatchPhone")) {
							items.setPd_dispatchPhone(text);
							text="";
						}else if (tagname.equalsIgnoreCase("Entered")) {
							items.setEntered(text);
							text="";
						}else if (tagname.equalsIgnoreCase("Equipment")) {
							items.setEquipment(text);
							text="";
						}else if (tagname.equalsIgnoreCase("TrailerOptionType")) {
							if (items.getTrailerOptionType()!=null) {
								items.setTrailerOptionType(items.getTrailerOptionType()+","+text);
							}else{
								items.setTrailerOptionType(text);
								
							}
							text="";
						
						}else if (tagname.equalsIgnoreCase("EquipmentTypes")) {
							items.setEquipmenttypes(equipmenttypes);
							text="";
						}else if (tagname.equalsIgnoreCase("Category")) {
							equipmenttypes.setCategory(text);
							text="";
						}else if (tagname.equalsIgnoreCase("CategoryId")) {
							equipmenttypes.setCategoryId(text);
							text="";
						}else if (tagname.equalsIgnoreCase("Code")) {
							items.setPd_equipmenttypescode(text);
							equipmenttypes.setCode(text);
							text="";
						}else if (tagname.equalsIgnoreCase("Description")) {
							equipmenttypes.setDescription(text);
							items.setPd_equipmenttypesdescription(text);
							text="";
							//System.out.println("equipmenttypes.setDescription(text)    "+text);
						}else if (tagname.equalsIgnoreCase("FullLoad")) {
							equipmenttypes.setFullLoad(text);
							text="";
						}else if (tagname.equalsIgnoreCase("Id")) {
							equipmenttypes.setId(text);
							text="";
						}else if (tagname.equalsIgnoreCase("IsCategorizable")) {
							equipmenttypes.setCategorizable(Boolean.valueOf(text));
							text="";
						}else if (tagname.equalsIgnoreCase("IsCombo")) {
							equipmenttypes.setCombo(Boolean.valueOf(text));
							text="";
						}else if (tagname.equalsIgnoreCase("IsTruckPost")) {
							equipmenttypes.setTruckPost(Boolean.valueOf(text));
							text="";
						}else if (tagname.equalsIgnoreCase("MapToId")) {
							equipmenttypes.setMapToId(text);
							text="";
						}else if (tagname.equalsIgnoreCase("RequiredOption")) {
							equipmenttypes.setRequiredOption(text);
							text="";
						}else if (tagname.equalsIgnoreCase("WebserviceOnly")) {
							equipmenttypes.setWebserviceOnly(Boolean.valueOf(text));
							text="";
						}else if (tagname.equalsIgnoreCase("HasFileCheck")) {
							items.setPd_hasFileCheck(text);
							text="";
						}else if (tagname.equalsIgnoreCase("ID")) {
							items.setPd_loadid(text);
							text="";
						}else if (tagname.equalsIgnoreCase("IsFriend")) {
							items.setPd_isFriend(text);
							text="";
						}else if (tagname.equalsIgnoreCase("IsFullTruck")) {
							items.setPd_isFullTruck(text);
							
							text="";
						}else if (tagname.equalsIgnoreCase("Length")) {
							items.setPd_length(text);
							text="";
							
						}else if (tagname.equalsIgnoreCase("MCNumber")) {
							items.setPd_mcNumber(text);
							text="";
						}else if (tagname.equalsIgnoreCase("MinMiles")) {
							items.setPd_MinMiles(text);
							text="";
						}else if (tagname.equalsIgnoreCase("OriginCity")) {
							items.setPd_originCity(text);
							text="";
						}else if (tagname.equalsIgnoreCase("OriginCountry")) {
							items.setPd_originCountry(text);
							text="";
						}else if (tagname.equalsIgnoreCase("OriginState")) {
							items.setPd_originState(text);
							text="";
						}else if (tagname.equalsIgnoreCase("PerMile")) {
							items.setPd_perMile(text);
							text="";
						}else if (tagname.equalsIgnoreCase("PercentFull")) {
							items.setPd_percentFull(text);
							text="";
						}else if (tagname.equalsIgnoreCase("PointOfContact")) {
							items.setPd_pointOfContact(text);
							text="";
						}else if (tagname.equalsIgnoreCase("PointOfContactPhone")) {
							items.setPd_pointOfContactPhone(text);
							text="";
						}else if (tagname.equalsIgnoreCase("Quantity")) {
							items.setPd_quantity(text);
							text="";
						}else if (tagname.equalsIgnoreCase("SpecInfo")) {
							items.setPd_specInfo(text);
							text="";
						}else if (tagname.equalsIgnoreCase("TCAMember")) {
							items.setPd_TCAMember(text);
							text="";
						}else if (tagname.equalsIgnoreCase("TMCNumber")) {
							items.setPd_tMCNumber(text);
							text="";
						}else if (tagname.equalsIgnoreCase("TimeTruckAvailable")) {
							items.setPd_timeTruckAvailable(text);
							text="";
						}else if (tagname.equalsIgnoreCase("TruckCompanyCity")) {
							items.setPd_truckCompanyCity(text);
							text="";
						}else if (tagname.equalsIgnoreCase("TruckCompanyEmail")) {
							items.setPd_truckCompanyEmail(text);
							text="";
						}else if (tagname.equalsIgnoreCase("TruckCompanyFax")) {
							items.setPd_truckCompanyFax(text);
							text="";
						}else if (tagname.equalsIgnoreCase("TruckCompanyName")) {
							items.setPd_truckCompanyName(text);
							text="";
						}else if (tagname.equalsIgnoreCase("TruckCompanyPhone")) {
							items.setPd_truckCompanyPhone(text);
							text="";
						}else if (tagname.equalsIgnoreCase("TruckCompanyState")) {
							items.setPd_truckCompanyState(text);
							text="";
						}else if (tagname.equalsIgnoreCase("Weight")) {
							items.setPd_weight(text);
							text="";
						}else if (tagname.equalsIgnoreCase("Width")) {
							items.setPd_width(text);
							text="";
						}
					}

					break;

				default:
					break;
				}
				eventType = parser.next();
			}

		} catch (XmlPullParserException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;

	}

}
