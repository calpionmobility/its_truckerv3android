package com.wi_vod.itstrucker.model;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class XMLSearchLoadParser {
	static private LoadSearchItem items=null;
	static private List<LoadSearchItem> searchitems=new ArrayList<LoadSearchItem>();
	
	public static String errormessage=""; 
	public static List<LoadSearchItem> getSearchItems(){
		return searchitems;
	}
	
	public static String geterrormmsg(){
		return errormessage;
	}
	
	
	public static boolean parsexml(String xmlstring) throws XmlPullParserException {
		XmlPullParserFactory factory = null;
		XmlPullParser parser = null;
		String text = null;
	
		try {
			factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			parser = factory.newPullParser();
			parser.setInput(new StringReader(xmlstring));

			int eventType = parser.getEventType();
			while (eventType != XmlPullParser.END_DOCUMENT) {
				
				
				String tagname = parser.getName();
				switch (eventType) {
				case XmlPullParser.START_TAG:
					if (tagname.equalsIgnoreCase("Error")) {
						///System.out.println("errormessagea  " + text);
					}
					if (tagname.equalsIgnoreCase("SearchResults")) {
						searchitems.clear();

					}
					if (tagname.equalsIgnoreCase("LoadSearchItem")) {
						
						items=new LoadSearchItem();
					}
					break;

				case XmlPullParser.TEXT:
					text = parser.getText();
					break;

				case XmlPullParser.END_TAG:
					if (tagname.equalsIgnoreCase("ErrorMessage")) {
						
						errormessage=text;
						//System.out.println("errormessage  " + text+" static err msg  "+errormessage);
						return false;
					}
					if (tagname.equalsIgnoreCase("SearchResults")) {
						//System.out.println("SearchResults  " + text);
						
						// return false;

					} else {
						
						if (tagname.equalsIgnoreCase("LoadSearchItem")) {
							searchitems.add(items);
							text="";
						} else if (tagname.equalsIgnoreCase("Age")) {
							items.setAge(text);
							text="";
							//System.out.println("" + text);
						} else if (tagname.equalsIgnoreCase("Bond")) {
							items.setBond(text);
							text="";
							//System.out.println("" + text);
						} else if (tagname.equalsIgnoreCase("BondEnabled")) {
							items.setBondenabled(text);
							text="";
							//System.out.println("" + text);
						} else if (tagname.equalsIgnoreCase("BondTypeID")) {
							items.setBondtypeid(text);
							text="";
							//System.out.println("" + text);
						} else if (tagname.equalsIgnoreCase("CompanyName")) {
							items.setCompanyname(text);
							text="";
							//System.out.println("" + text);
						} else if (tagname.equalsIgnoreCase("Days2Pay")) {
							if (text!=null) {
								items.setDays2pay(text);
								text="";
							}
							//System.out.println("" + text);
						} else if (tagname.equalsIgnoreCase("DestinationCity")) {
							items.setDestinationcity(text);
							text="";
							//System.out.println("" + text);
						} else if (tagname.equalsIgnoreCase("DestinationCountry")) {
							items.setDestinationCountry(text);
							text="";
							//System.out.println("" + text);
						} else if (tagname.equalsIgnoreCase("DestinationDistance")) {
							items.setDestinationDistance(text);
							text="";
							//System.out.println("" + text);
						} else if (tagname.equalsIgnoreCase("DestinationState")) {
							items.setDestinationState(text);
							text="";
							//System.out.println("" + text);
						} else if (tagname.equalsIgnoreCase("Equipment")) {
							items.setEquipment(text);
							text="";
							//System.out.println("" + text);
							
						} else if (tagname.equalsIgnoreCase("ExperienceFactor")) {
							items.setExperienceFactor(text);
							text="";
							//System.out.println("" + text);
						}else if (tagname.equalsIgnoreCase("TrailerOptionType")) {
							items.setTrialerOptionType(text);
							text="";
							//System.out.println("" + text);
						}else if (tagname.equalsIgnoreCase("FuelCost")) {
							items.setFuelCost(text);
							text="";
							//System.out.println("" + text);
						}else if (tagname.equalsIgnoreCase("ID")) {
							items.setLoadID(text);
							text="";
							//System.out.println("" + text);
						}else if (tagname.equalsIgnoreCase("IsFriend")) {
							items.setIsFriend(text);
							text="";
							//System.out.println("" + text);
						} else if (tagname.equalsIgnoreCase("Length")) {
							items.setLength(text);
							text="";
							//System.out.println("" + text);
						}else if (tagname.equalsIgnoreCase("LoadType")) {
							items.setLoadType(text);
							text="";
							//System.out.println("" + text);
						}else if (tagname.equalsIgnoreCase("Miles")) {
							items.setMiles(text);
							text="";
							//System.out.println("" + text);
						}else if (tagname.equalsIgnoreCase("OriginCity")) {
							items.setOriginCity(text);
							text="";
							//System.out.println("" + text);
						}else if (tagname.equalsIgnoreCase("OriginCountry")) {
							items.setOriginCountry(text);
							text="";
							//System.out.println("" + text);
						}else if (tagname.equalsIgnoreCase("OriginDistance")) {
							items.setOriginDistance(text);
							text="";
							//System.out.println("" + text);
						}else if (tagname.equalsIgnoreCase("OriginState")) {
							items.setOriginState(text);
							text="";
							//System.out.println("" + text);
						}else if (tagname.equalsIgnoreCase("Payment")) {
							items.setPayment(text);
							text="";
							//System.out.println("" + text);
						}else if (tagname.equalsIgnoreCase("PickUpDate")) {
							items.setPickupDate(text);
							text="";
							//System.out.println("" + text);
						}else if (tagname.equalsIgnoreCase("PointOfContactPhone")) {
							//System.out.println("PointOfContactPhone" + text);
							items.setPointOfContactPhone(text);
							text="";
							
						}else if (tagname.equalsIgnoreCase("PricePerGall")) {
							items.setPricePerGall(text);
							text="";
							//System.out.println("" + text);
						}else if (tagname.equalsIgnoreCase("Weight")) {
							items.setWeight(text);
							text="";
							//System.out.println("" + text);
						}
					}

					break;

				default:
					break;
				}
				eventType = parser.next();
			}

		} catch (XmlPullParserException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;

	}

}
	
	
	
	