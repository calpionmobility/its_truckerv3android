package com.wi_vod.itstrucker.model;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

public class DatabaseHandler extends SQLiteOpenHelper {

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 8;

	// Database Name
	private static final String DATABASE_NAME = "itstrucker";

	// Favourite table name
	private static final String TABLE_FAVOURITE = "favourites";
	private static final String TABLE_POST_FAVOURITE = "postfavourites";
	/*
	 * origincounrty,originstate,origincity,destinationcountry,destinationcity,
	 * destinationstate
	 * ,searchradius,equipmenttype,searchdate,traileroption,hoursold
	 * ,loadtype,sortype;
	 */
	// Contacts Table Columns names
	private static final String KEY_ID = "id";
	private static final String KEY_ORIGINCITY = "origincity";
	private static final String KEY_ORIGINCOUNTRY = "origincounrty";
	private static final String KEY_ORIGINSTATE = "originstate";

	private static final String KEY_DESTINATIONCOUNTRY = "destinationcountry";
	private static final String KEY_DESTINATIONCITY = "destinationcity";
	private static final String KEY_DESTINATIONSTATE = "destinationstate";
	private static final String KEY_SEARCHRADIUS = "searchradius";
	private static final String KEY_EQUIPMENTTYPE = "equipmenttype";
	private static final String KEY_SEARCHDATE = "searchdate";
	private static final String KEY_TRAILEROPTION = "traileroption";
	private static final String KEY_HOURSOLD = "hoursold";
	private static final String KEY_LOADTYPE = "loadtype";
	private static final String KEY_SORTYPE = "sortype";
	private static final String KEY_WIDTH = "width";
	private static final String KEY_LENGTH = "length";
	private static final String KEY_WEIGHT = "weight";
	private static final String KEY_MINDIST = "mindist";
	private static final String KEY_RATEPERMILE = "ratepermile";
	private static final String KEY_QUANTITY = "quantity";
	private static final String KEY_SPECIALINFO = "specialinfo";
	private static final String KEY_MDSTRING = "mdvalue";
	
	private static final String CREATE_CONTACTS_TABLE = "CREATE TABLE "
			+ TABLE_FAVOURITE + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
			+ KEY_ORIGINCITY + " TEXT," + KEY_ORIGINSTATE + " TEXT,"
			+ KEY_ORIGINCOUNTRY + " TEXT," + KEY_DESTINATIONCITY + " TEXT,"
			+ KEY_DESTINATIONSTATE + " TEXT," + KEY_DESTINATIONCOUNTRY
			+ " TEXT," + KEY_SEARCHRADIUS + " TEXT," + KEY_EQUIPMENTTYPE
			+ " TEXT," + KEY_SEARCHDATE + " TEXT," + KEY_TRAILEROPTION
			+ " TEXT," + KEY_HOURSOLD + " TEXT," + KEY_LOADTYPE + " TEXT,"
			+ KEY_SORTYPE + " TEXT," + KEY_MDSTRING + " TEXT" + ")";

	private static final String CREATE_POSTCONTACTS_TABLE = "CREATE TABLE "
			+ TABLE_POST_FAVOURITE + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
			+ KEY_ORIGINCITY + " TEXT," + KEY_ORIGINSTATE + " TEXT,"
			+ KEY_ORIGINCOUNTRY + " TEXT," + KEY_DESTINATIONCITY + " TEXT,"
			+ KEY_DESTINATIONSTATE + " TEXT," + KEY_DESTINATIONCOUNTRY
			+ " TEXT," + KEY_SEARCHRADIUS + " TEXT," + KEY_EQUIPMENTTYPE
			+ " TEXT," + KEY_SEARCHDATE + " TEXT," + KEY_TRAILEROPTION
			+ " TEXT," + KEY_HOURSOLD + " TEXT," + KEY_LOADTYPE + " TEXT,"
			+ KEY_WIDTH + " TEXT," + KEY_LENGTH + " TEXT," + KEY_WEIGHT
			+ " TEXT," + KEY_MINDIST + " TEXT," + KEY_RATEPERMILE
			+ " TEXT," + KEY_QUANTITY + " TEXT," + KEY_SPECIALINFO + " TEXT,"
			+ KEY_MDSTRING + " TEXT" + ")";

	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {

		db.execSQL(CREATE_CONTACTS_TABLE);
		db.execSQL(CREATE_POSTCONTACTS_TABLE);
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_FAVOURITE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_POST_FAVOURITE);
		// Create tables again
		onCreate(db);
	}

	/**
	 * All CRUD(Create, Read, Update, Delete) Operations Adding new favorite
	 * search record
	 */
	public boolean addTofavourite(DataModel fav, String md) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();

		Cursor cursor = db.query(TABLE_FAVOURITE, new String[] {
				KEY_ORIGINCITY, KEY_ORIGINSTATE, KEY_ORIGINCOUNTRY },
				KEY_MDSTRING + "=?", new String[] { md }, null, null, null,
				null);

		int rowcount = cursor.getCount();

		// Verify if mdstring matches in the records.If found return true or
		// else update new record
		if (rowcount == 1) {
			db.close();
			return true;
		} else {
			values.put(KEY_ORIGINCITY, fav.getOrigincity()); // Contact Name
			values.put(KEY_ORIGINSTATE, fav.getOriginstate()); // Contact Phone
			values.put(KEY_ORIGINCOUNTRY, fav.getOrigincounrty());
			values.put(KEY_DESTINATIONCITY, fav.getDestinationcity());
			values.put(KEY_DESTINATIONSTATE, fav.getDestinationstate());
			values.put(KEY_DESTINATIONCOUNTRY, fav.getDestinationcountry());
			values.put(KEY_SEARCHRADIUS, fav.getSearchradius());
			values.put(KEY_EQUIPMENTTYPE, fav.getEquipmenttype());
			values.put(KEY_SEARCHDATE, fav.getSearchdate());
			values.put(KEY_TRAILEROPTION, fav.getTraileroption());
			values.put(KEY_HOURSOLD, fav.getHoursold());
			values.put(KEY_LOADTYPE, fav.getLoadtype());
			values.put(KEY_SORTYPE, fav.getSortype());
			values.put(KEY_MDSTRING, md);
			// Inserting Row
			db.insert(TABLE_FAVOURITE, null, values);
			db.close(); // Closing database connection
			return false;
		}
	}

	public boolean addTopostfavourite(DataModel fav, String md) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();

		Cursor cursor = db.query(TABLE_POST_FAVOURITE, new String[] {
				KEY_ORIGINCITY, KEY_ORIGINSTATE, KEY_ORIGINCOUNTRY },
				KEY_MDSTRING + "=?", new String[] { md }, null, null, null,
				null);

		int rowcount = cursor.getCount();

		// Verify if mdstring matches in the records.If found return true or
		// else update new record
		if (rowcount == 1) {
			db.close();
			return true;
		} else {
			values.put(KEY_ORIGINCITY, fav.getOrigincity()); // Contact Name
			values.put(KEY_ORIGINSTATE, fav.getOriginstate()); // Contact Phone
			values.put(KEY_ORIGINCOUNTRY, fav.getOrigincounrty());
			values.put(KEY_DESTINATIONCITY, fav.getDestinationcity());
			values.put(KEY_DESTINATIONSTATE, fav.getDestinationstate());
			values.put(KEY_DESTINATIONCOUNTRY, fav.getDestinationcountry());
			values.put(KEY_SEARCHRADIUS, fav.getSearchradius());
			values.put(KEY_EQUIPMENTTYPE, fav.getEquipmenttype());
			values.put(KEY_SEARCHDATE, fav.getSearchdate());
			values.put(KEY_TRAILEROPTION, fav.getTraileroption());
			values.put(KEY_HOURSOLD, fav.getHoursold());
			values.put(KEY_LOADTYPE, fav.getLoadtype());
			values.put(KEY_WIDTH, fav.getWidth());
			values.put(KEY_LENGTH, fav.getLength());
			values.put(KEY_WEIGHT, fav.getWeight());
			values.put(KEY_MINDIST, fav.getMindist());
			Log.i("getMindist", fav.getMindist().toString());
			values.put(KEY_RATEPERMILE, fav.getRatepermile());
			values.put(KEY_QUANTITY, fav.getQuantity());
			values.put(KEY_SPECIALINFO, fav.getSpecialInfo());
			values.put(KEY_MDSTRING, md);
			// Inserting Row
			db.insert(TABLE_POST_FAVOURITE, null, values);
			db.close(); // Closing database connection
			return false;
		}
	}

	public boolean alreadyFavorite(DataModel fav, String md) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();

		Cursor cursor = db.query(TABLE_FAVOURITE, new String[] {
				KEY_ORIGINCITY, KEY_ORIGINSTATE, KEY_ORIGINCOUNTRY },
				KEY_MDSTRING + "=?", new String[] { md }, null, null, null,
				null);

		int rowcount = cursor.getCount();

		// Verify if mdstring matches in the records.If found return true or
		// else update new record
		if (rowcount == 1) {
			db.close();
			return true;
		} else {
			return false;
		}

	}

	/**
	 * Reading all favorites to show in favorites activity
	 * 
	 */
	public ArrayList<DataModel> getAllfav() {
		ArrayList<DataModel> favList = new ArrayList<DataModel>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_FAVOURITE;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				DataModel fav = new DataModel();
				fav.setId(Integer.parseInt(cursor.getString(0)));
				fav.setOrigincity(cursor.getString(1));
				fav.setOriginstate(cursor.getString(2));
				fav.setOrigincounrty(cursor.getString(3));
				fav.setDestinationcity(cursor.getString(4));
				fav.setDestinationstate(cursor.getString(5));
				fav.setDestinationcountry(cursor.getString(6));
				/*
				 * searchradius,equipmenttype,searchdate,traileroption,hoursold,
				 * loadtype,sortype
				 */
				fav.setSearchradius(cursor.getString(7));
				fav.setEquipmenttype(cursor.getString(8));
				fav.setSearchdate(cursor.getString(9));
				fav.setTraileroption(cursor.getString(10));
				fav.setHoursold(cursor.getString(11));
				fav.setLoadtype(cursor.getString(12));
				fav.setSortype(cursor.getString(13));
				fav.setMd5value(cursor.getString(14));
				Log.i("FAV MD5", cursor.getString(14));
				Log.i("FAV MD5", cursor.getString(14));
				// Adding contact to list
				favList.add(fav);
			} while (cursor.moveToNext());
		}
		db.close();
		// return contact list
		return favList;
	}

	/**
	 * Reading all favorites to show in postfavorites activity
	 * 
	 */
	public ArrayList<DataModel> getAllpostfav() {
		ArrayList<DataModel> favList = new ArrayList<DataModel>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_POST_FAVOURITE;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				DataModel fav = new DataModel();
				fav.setId(Integer.parseInt(cursor.getString(0)));
				fav.setOrigincity(cursor.getString(1));
				fav.setOriginstate(cursor.getString(2));
				fav.setOrigincounrty(cursor.getString(3));
				fav.setDestinationcity(cursor.getString(4));
				fav.setDestinationstate(cursor.getString(5));
				fav.setDestinationcountry(cursor.getString(6));
				fav.setSearchradius(cursor.getString(7));
				fav.setEquipmenttype(cursor.getString(8));
				fav.setSearchdate(cursor.getString(9));
				fav.setTraileroption(cursor.getString(10));
				fav.setHoursold(cursor.getString(11));
				fav.setLoadtype(cursor.getString(12));
				fav.setWidth(cursor.getString(13));
				fav.setLength(cursor.getString(14));
				fav.setWeight(cursor.getString(15));
				fav.setMindist(cursor.getString(16));
				//Log.i("setMindist", cursor.getString(16));
				fav.setRatepermile(cursor.getString(17));
				fav.setQuantity(cursor.getString(18));
				fav.setSpecialInfo(cursor.getString(19));
				fav.setMd5value(cursor.getString(20));
				Log.i("specialinfo", cursor.getString(19));
				Log.i("FAV MD5", cursor.getString(20));
				favList.add(fav);
			} while (cursor.moveToNext());
		}
		db.close();
		// return contact list
		return favList;
	}

	/**
	 * Deleting one favorite record
	 * 
	 */
	public void deletefav(DataModel fav) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_FAVOURITE, KEY_ID + " = ?",
				new String[] { String.valueOf(fav.getId()) });
		db.close();
	}

	public boolean deletefavorites(ArrayList<DataModel> fav) {

		SQLiteDatabase db = this.getWritableDatabase();
		System.out.println("fav size--->" + fav.size());
		for (DataModel favorites : fav) {
			System.out.println(favorites.getId());
			db.delete(TABLE_FAVOURITE, KEY_ID + " = ?",
					new String[] { String.valueOf(favorites.getId()) });
		}
		db.close();

		return true;
	}

	
	
	public void dropFavoriteTable() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_FAVOURITE, null, null);
		db.close();
	}
	
	
	
	
	/**
	 * Getting favorites count
	 * 
	 */
	public int getfavouritesCount() {
		String countQuery = "SELECT  * FROM " + TABLE_FAVOURITE;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		int count = cursor.getCount();
		cursor.close();

		// return count
		return count;
	}
	/**
	 * Deleting one postfavorite record
	 * 
	 */
	public void deletepostfav(DataModel fav) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_POST_FAVOURITE, KEY_ID + " = ?",
				new String[] { String.valueOf(fav.getId()) });
		db.close();
	}
	public boolean deletepostfavorites(ArrayList<DataModel> fav) {

		SQLiteDatabase db = this.getWritableDatabase();
		System.out.println("fav size--->" + fav.size());
		for (DataModel favorites : fav) {
			System.out.println(favorites.getId());
			db.delete(TABLE_POST_FAVOURITE, KEY_ID + " = ?",
					new String[] { String.valueOf(favorites.getId()) });
		}
		db.close();

		return true;
	}
	/**
	 * Getting postfavorites count
	 * 
	 */
	public int getpostfavouritesCount() {
		String countQuery = "SELECT  * FROM " + TABLE_POST_FAVOURITE;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		int count = cursor.getCount();
		cursor.close();

		// return count
		return count;
	}
}