package com.wi_vod.itstrucker;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.AsyncTask.Status;
import android.support.v4.app.NavUtils;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.wi_vod.itstrucker.ITSTruckerLogin.HttpServiceListener;
import com.wi_vod.itstrucker.Utils.TrailerOptions;
import com.wi_vod.itstrucker.model.DataModel;
import com.wi_vod.itstrucker.model.DatabaseHandler;
import com.wi_vod.itstrucker.model.LoadSearchItem;
import com.wi_vod.itstrucker.model.SearchListModel;
import com.wi_vod.itstrucker.model.XMLSearchLoadParser;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.xmlpull.v1.XmlPullParserException;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.zip.GZIPInputStream;

import javax.net.ssl.HttpsURLConnection;

public class SearchResultActivty extends ActionBarActivity {
    private static String TAG="SearchResultActivty";
    private String errormsg;
    private String searchDate = "";
    private String response_str;
    // private String trailerOptionsAttributeType="";
    private String dateAttribute = "<arr:dateTime>" + searchDate
            + "</arr:dateTime>\n";
    private String trailerOptionsAttribute = "";
    private StringBuilder builder = new StringBuilder();
    private ArrayList<LoadSearchItem> sercheditems = new ArrayList<LoadSearchItem>();
    private ArrayList<LoadSearchItem> resultitems = new ArrayList<LoadSearchItem>();

    DataModel searchdata;
    private TextView resultNo, route;
    private Resources res;
    private SearchListModel adapter;
    private ProgressDialog progDialog; // Progress bar
    public SearchResultActivty seachload = null;
    private ListView listview;
    private SessionManager session;
    private DatabaseHandler db;
    private int loadResultPageNumber = 1;
    Button btnLoadMore;
    private String mdvalue = null;
    Bundle bundle;
    private int intentorigin;
    boolean showdelete;
    String routeorigin;
    String routedst;
    int gpsBased;
   // SearchLoads loadsearch;
	private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.searchresult);
        listview = (ListView) findViewById(R.id.searchlistView);
//selected 
        route = (TextView) findViewById(R.id.searchresultroute);

        btnLoadMore = new Button(this);
        btnLoadMore.setText("Load More");
        btnLoadMore.setBackgroundResource(R.drawable.save_button_states);
        btnLoadMore.setTextColor(Color.BLACK);
        btnLoadMore.setPadding(5, 5, 5, 5);
        listview.addFooterView(btnLoadMore);

        session = new SessionManager(SearchResultActivty.this);
        db = new DatabaseHandler(SearchResultActivty.this);
        bundle = getIntent().getExtras();
        if (bundle != null) {
            searchdata = bundle.getParcelable("searchobject");
            mdvalue = bundle.getString("md5value");
            intentorigin = bundle.getInt("intentorigin");
            gpsBased = bundle.getInt("GPSBASED");

          /*  loadsearch = new SearchLoads();
            loadsearch.execute();*/
            
            sendsearchrequest();
            
            
        }

        if (searchdata.getOriginareaselected() == 1) {
            routeorigin = Utils.checkIsNull(searchdata.getOriginareacode());
        } else {
            routeorigin = Utils.checkIsNull(searchdata.getOrigincity()) + ","
                    + Utils.checkIsNull(searchdata.getOriginstate());
        }

        if (searchdata.getDestareaselected() == 1) {
            routedst = Utils.checkIsNull(searchdata.getDestinationareacode());
        } else {
            routedst = Utils.checkIsNull(searchdata.getDestinationcity()) + ","
                    + Utils.checkIsNull(searchdata.getDestinationstate());
        }
        route.setText(routeorigin + " to " + routedst);

        if (db.alreadyFavorite(searchdata, searchdata.getMd5value())) {
            showdelete = false;
            supportInvalidateOptionsMenu();
        } else {
            showdelete = true;
            supportInvalidateOptionsMenu();
        }

        btnLoadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // Starting a new async task
                loadResultPageNumber++;
                sendsearchrequest();
             /*   SearchLoads loadsearch = new SearchLoads();
                loadsearch.execute();*/
                
                
                
                
            }
        });
    	mRegistrationBroadcastReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				  /*if (loadsearch != null && loadsearch.getStatus() == Status.RUNNING) {
			            loadsearch.cancel(true);
			        }*/
				Utils.logoutuser(AppConstants.LOGOUTUSER_ERRORMSG, SearchResultActivty.this);
			}
		};
    }

    private void sendsearchrequest() {
    	 searchDate = searchdata.getSearchdate();
         // System.out.println(searchDate);

         DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
         Date startDate = null;
         String newDateString = null;
         String newDateString2 = null;
         try {
             startDate = df.parse(searchDate);
             // startDate = df.parse("2014-06-12");
             newDateString = df.format(startDate);
             // System.out.println(newDateString);
         } catch (ParseException e) {
             e.printStackTrace();
         }
         Date date2 = new Date();

         int returnvalue = Utils.compareDates(startDate, date2);
         if (intentorigin == 1 || intentorigin == 2) {
             // If searchrequest is made by currentsearch and
             // favorite ,verify date field
             if (returnvalue == -1) {
                 dateAttribute = "";
             } else {
                 dateAttribute = "<arr:dateTime>" + searchDate
                         + "</arr:dateTime>\n";
             }
         } else {
             // If searchrequest is made by searchmain.class ,verify
             // date field
             if (searchDate.equals("0000-00-00")) {
                 dateAttribute = "";
             } else {
                 dateAttribute = "<arr:dateTime>" + searchDate
                         + "</arr:dateTime>\n";
             }
         }

         //
         if (searchdata.getTraileroption().equals(
                 AppConstants.EMPTY_STRING)
                 || searchdata.getTraileroption().equals(
                         AppConstants.NONE)) {
             trailerOptionsAttribute = "";
         } else {
             List<String> trailerOptions = new ArrayList<String>();
             trailerOptions = Utils.getSelectedItems(
                     searchdata.getTraileroption(), ",");
             if (null != trailerOptions && trailerOptions.size() > 0) {
                 Iterator<String> iterator = trailerOptions
                         .iterator();
                 while (iterator.hasNext()) {
                     String trailerOpt = iterator.next();

                     if (trailerOpt
                             .equals(AppConstants.OPTION_TARPS)) {
                         trailerOptionsAttribute += "<truc:TrailerOptionType>"
                                 + TrailerOptions.Tarps
                                 + "</truc:TrailerOptionType>\n";
                     } else if (trailerOpt
                             .equals(AppConstants.OPTION_HAZARDOUS)) {
                         trailerOptionsAttribute += "<truc:TrailerOptionType>"
                                 + TrailerOptions.Hazardous
                                 + "</truc:TrailerOptionType>\n";
                     } else if (trailerOpt
                             .equals(AppConstants.OPTION_PALLETEXCHANGE)) {
                         trailerOptionsAttribute += "<truc:TrailerOptionType>"
                                 + TrailerOptions.PalletExchange
                                 + "</truc:TrailerOptionType>\n";
                     } else if (trailerOpt
                             .equals(AppConstants.OPTION_TEAM)) {
                         trailerOptionsAttribute += "<truc:TrailerOptionType>"
                                 + TrailerOptions.Team
                                 + "</truc:TrailerOptionType>\n";
                     } else if (trailerOpt
                             .equals(AppConstants.OPTION_EXPEDITED)) {
                         trailerOptionsAttribute += "<truc:TrailerOptionType>"
                                 + TrailerOptions.Expedited
                                 + "</truc:TrailerOptionType>\n";
                     }
                 }
             }
         }
         
         
         

         String envolpe = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\""
                 + " xmlns:v12=\"http://webservices.truckstop.com/v12\""
                 + " xmlns:web=\"http://schemas.datacontract.org/2004/07/WebServices\""
                 + " xmlns:web1=\"http://schemas.datacontract.org/2004/07/WebServices.Searching\""
                 + " xmlns:truc=\"http://schemas.datacontract.org/2004/07/Truckstop2.Objects\""
                 + " xmlns:arr=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\""
                 + ">";
         String body = envolpe
                 + "\n<soapenv:Header/>\n"
                 + "<soapenv:Body>\n"
                 + "<v12:GetLoadSearchResults>\n"
                 + "<v12:searchRequest>\n"
                 + "<web:IntegrationId>"
                 + session.getUserIntegrationID()
                 + "</web:IntegrationId>\n"
              
                 + "<web:Password>" + AppConstants.PASSWORD  + "</web:Password>\n"
                    + "<web:UserName>" + AppConstants.USERNAME + "</web:UserName>\n"
                 + "<web1:Criteria>\n"
                 + "<web1:DestinationCity>"
                 + searchdata.getDestinationcity()
                 + "</web1:DestinationCity>\n"
                 + "<web1:DestinationCountry>"
                 + searchdata.getDestinationcountry()
                 + "</web1:DestinationCountry>\n"
                 + "<web1:DestinationRange>"
                 + searchdata.getSearchradius()
                 + "</web1:DestinationRange>\n"
                 + "<web1:DestinationState>"
                 + searchdata.getDestinationstate()
                 + "</web1:DestinationState>\n"
                 // +"<web1:DestinationState>CA</web1:DestinationState>\n"
                 + "<web1:EquipmentOptions>\n"
                 + trailerOptionsAttribute
                 + "</web1:EquipmentOptions>\n"
                 + "<web1:EquipmentType>"
                 + searchdata.getEquipmenttype()
                 + "</web1:EquipmentType>\n"
                 + "<web1:HoursOld>"
                 + searchdata.getHoursold()
                 + "</web1:HoursOld>\n"
                 + "<web1:LoadType>"
                 + searchdata.getLoadtype()
                 + "</web1:LoadType>\n"
                 + "<web1:OriginCity>"
                 + searchdata.getOrigincity()
                 + "</web1:OriginCity>\n"
                 + "<web1:OriginCountry>"
                 + searchdata.getOrigincounrty()
                 + "</web1:OriginCountry>\n"
                 + "<web1:OriginLatitude>0</web1:OriginLatitude>\n"
                 + "<web1:OriginLongitude>0</web1:OriginLongitude>\n"
                 + "<web1:OriginRange>"
                 + searchdata.getSearchradius()
                 + "</web1:OriginRange>\n"
                 // +"<web1:OriginState>"+searchdata.getOriginstate()+"</web1:OriginState>\n"
                 + "<web1:OriginState>"
                 + searchdata.getOriginstate()
                 + "</web1:OriginState>\n"
                 + "<web1:PageNumber>"
                 + loadResultPageNumber
                 + "</web1:PageNumber>\n"
                 + "<web1:PageSize>"
                 + AppConstants.SEARCHLOADS_PAGESIZE
                 + "</web1:PageSize>\n"
                 + "<web1:PickupDates>\n"
                 + dateAttribute
                 + "</web1:PickupDates>\n"
                 + "<web1:SortBy>"
                 + searchdata.getSortype()
                 + "</web1:SortBy>\n"
                 + "<web1:SortDescending>"
                 + Utils.getSortOrder(SearchResultActivty.this,
                         searchdata.getSortype())
                 + "</web1:SortDescending>\n" + "</web1:Criteria>\n"
                 + "</v12:searchRequest>\n"
                 + "</v12:GetLoadSearchResults>\n"
                 + "</soapenv:Body>\n" + "</soapenv:Envelope>\n";
        // System.out.println("BODY VALUE----" + body);
         String SOAPACTION="http://webservices.truckstop.com/v12/ILoadSearch/GetLoadSearchResults";
         
         HttpServiceListener listener = new HttpServiceListener();
			WebServiceConnectionService serv = new WebServiceConnectionService(SearchResultActivty.this,body,
					SOAPACTION, listener,AppConstants.SEARCHLOADS_PROGRESS);
			serv.execute(AppConstants.SEARCHLOADS_TEST_URL);         
	}

    
    
    
    
    
    
    protected class HttpServiceListener implements ConnectionServiceListener {

		@Override
		public void onServiceComplete(String response) {
			if (response.equals(AppConstants.INTERNET_CONNECTION_ERROR)) {
                Utils.showErrorMessage(AppConstants.INTERNET_CONNECTION_ERROR, SearchResultActivty.this);
             
            } else if (response.equals(AppConstants.SERVER_CONNECTION_ERROR)) {
                Utils.showErrorMessage(AppConstants.SERVER_CONNECTION_ERROR,SearchResultActivty.this);
               
            }else if (response.equals(AppConstants.PLAYSERVICES_NOTFOUND)) {
               // Utils.showErrorMessage(AppConstants.PLAYSERVICES_NOTFOUND,SearchResultActivty.this);
            	Utils.updateGoogleplay(SearchResultActivty.this);
               
            } else {
            	 boolean loadsucess=false;
				try {
					loadsucess = XMLSearchLoadParser.parsexml(response);
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					 Utils.showErrorMessage("Unable to process request", SearchResultActivty.this);
				}
                 if (loadsucess) {
                     resultitems = (ArrayList<LoadSearchItem>) XMLSearchLoadParser .getSearchItems();
                     
                     if (resultitems.size() == 0) {

                         Utils.showErrorMessage(
                                 "No result found for the search criteria made.",
                                 SearchResultActivty.this);
                         /*
                          * Toast.makeText(SearchResultActivty.this,"No result found",Toast
                          * .LENGTH_LONG).show(); finish();
                          */
                     } else {
                         int currentPosition = listview.getFirstVisiblePosition();
                         sercheditems.addAll(resultitems);
                         // System.out.println("paidhistoryresultviewArr sizre in history----->"+
                         // paidhistoryresultviewArr.size()+ "count value" + paidcount);
                         seachload = SearchResultActivty.this;
                         adapter = new SearchListModel(seachload, sercheditems, res);
                         listview.setAdapter(adapter);
                         if (loadResultPageNumber != 1) {
                             listview.setSelectionFromTop(currentPosition + 1, 0);
                         }

                         if (resultitems.size() < AppConstants.SEARCHLOADS_PAGESIZE) {
                             btnLoadMore.setVisibility(View.GONE);
                             listview.removeFooterView(btnLoadMore);
                         }
                         /*
                          * resultNo = (TextView) findViewById(R.id.ResultNumber);
                          * resultNo.setText(String.valueOf(sercheditems.size()));
                          */

                         if (intentorigin != 2) {
                             Gson gson = new Gson();
                             String json = gson.toJson(searchdata);
                             // System.out.println("json.tostring------->" + json);
                             session.updateCurrentSearch(json);
                         }

                     }
                     
                   
                 } else {
                     errormsg = XMLSearchLoadParser.geterrormmsg();
                     Utils.showErrorMessage(errormsg, SearchResultActivty.this);
                    
                 }
            }
			
		}

		@Override
		public void onServiceComplete(String response, String reference) {
			// TODO Auto-generated method stub
			
		}
    	
    	
    }
    
    
    
	/*private class SearchLoads extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progDialog = new ProgressDialog(SearchResultActivty.this);
            progDialog.setMessage(AppConstants.SEARCHLOADS_PROGRESS);
            progDialog.setCancelable(false);
            progDialog.show();
          if (SearchResultActivty.this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                SearchResultActivty.this
                        .setRequestedOrientation(Configuration.ORIENTATION_PORTRAIT);
            } else {
                SearchResultActivty.this
                        .setRequestedOrientation(Configuration.ORIENTATION_LANDSCAPE);
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try {

                // System.out.println(Utils.isInternetAvailable(SearchResultActivty.this));
                if (Utils.isInternetAvailable(SearchResultActivty.this)) {
                    searchDate = searchdata.getSearchdate();
                    // System.out.println(searchDate);

                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    Date startDate = null;
                    String newDateString = null;
                    String newDateString2 = null;
                    try {
                        startDate = df.parse(searchDate);
                        // startDate = df.parse("2014-06-12");
                        newDateString = df.format(startDate);
                        // System.out.println(newDateString);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Date date2 = new Date();

                    int returnvalue = Utils.compareDates(startDate, date2);
                    if (intentorigin == 1 || intentorigin == 2) {
                        // If searchrequest is made by currentsearch and
                        // favorite ,verify date field
                        if (returnvalue == -1) {
                            dateAttribute = "";
                        } else {
                            dateAttribute = "<arr:dateTime>" + searchDate
                                    + "</arr:dateTime>\n";
                        }
                    } else {
                        // If searchrequest is made by searchmain.class ,verify
                        // date field
                        if (searchDate.equals("0000-00-00")) {
                            dateAttribute = "";
                        } else {
                            dateAttribute = "<arr:dateTime>" + searchDate
                                    + "</arr:dateTime>\n";
                        }
                    }

                    //
                    if (searchdata.getTraileroption().equals(
                            AppConstants.EMPTY_STRING)
                            || searchdata.getTraileroption().equals(
                                    AppConstants.NONE)) {
                        trailerOptionsAttribute = "";
                    } else {
                        List<String> trailerOptions = new ArrayList<String>();
                        trailerOptions = Utils.getSelectedItems(
                                searchdata.getTraileroption(), ",");
                        if (null != trailerOptions && trailerOptions.size() > 0) {
                            Iterator<String> iterator = trailerOptions
                                    .iterator();
                            while (iterator.hasNext()) {
                                String trailerOpt = iterator.next();

                                if (trailerOpt
                                        .equals(AppConstants.OPTION_TARPS)) {
                                    trailerOptionsAttribute += "<truc:TrailerOptionType>"
                                            + TrailerOptions.Tarps
                                            + "</truc:TrailerOptionType>\n";
                                } else if (trailerOpt
                                        .equals(AppConstants.OPTION_HAZARDOUS)) {
                                    trailerOptionsAttribute += "<truc:TrailerOptionType>"
                                            + TrailerOptions.Hazardous
                                            + "</truc:TrailerOptionType>\n";
                                } else if (trailerOpt
                                        .equals(AppConstants.OPTION_PALLETEXCHANGE)) {
                                    trailerOptionsAttribute += "<truc:TrailerOptionType>"
                                            + TrailerOptions.PalletExchange
                                            + "</truc:TrailerOptionType>\n";
                                } else if (trailerOpt
                                        .equals(AppConstants.OPTION_TEAM)) {
                                    trailerOptionsAttribute += "<truc:TrailerOptionType>"
                                            + TrailerOptions.Team
                                            + "</truc:TrailerOptionType>\n";
                                } else if (trailerOpt
                                        .equals(AppConstants.OPTION_EXPEDITED)) {
                                    trailerOptionsAttribute += "<truc:TrailerOptionType>"
                                            + TrailerOptions.Expedited
                                            + "</truc:TrailerOptionType>\n";
                                }
                            }
                        }
                    }
                    //

                    HttpPost post = new HttpPost( AppConstants.SEARCHLOADS_TEST_URL);
                    Log.i(TAG, AppConstants.SEARCHLOADS_TEST_URL);
                    
                    HttpParams httpParameters = new BasicHttpParams();
                    HttpConnectionParams.setConnectionTimeout(httpParameters, AppConstants.TIMEOUTCONNECTION_ERROR);
                    // Set the default socket timeout (SO_TIMEOUT)
                    // in milliseconds which is the timeout for waiting for
                    // data.
                    HttpConnectionParams.setSoTimeout(httpParameters,AppConstants.SOCKETTIMEOUT_ERROR);
                    DefaultHttpClient client = new DefaultHttpClient(httpParameters);
                    post.con.setRequestProperty("Accept-Encoding", "gzip,deflate");
                    post.setHeader("Content-Type", "text/xml;charset=UTF-8");
                    // SOAPAction:
                    // "http://webservices.truckstop.com/v12/ILoadSearch/GetLoadSearchResults"
                    post.setHeader("SOAPAction",
                            "http://webservices.truckstop.com/v12/ILoadSearch/GetLoadSearchResults");
                    post.setHeader("Host", "webservices.truckstop.com");
                    post.setHeader("Connection", "Keep-Alive");
                    post.setHeader("User-Agent",
                            "Apache-HttpClient/4.1.1 (java 1.5)\\");
                    
                    
                    
                    
                    
                    String url = AppConstants.SEARCHLOADS_TEST_URL;
                   // Log.i(TAG, AppConstants.SEARCHLOADS_TEST_URL);
					URL obj = new URL(url);
					HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
					ITSApplication app = ((ITSApplication) getApplicationContext());
					//ITSApplication app= new ITSApplication();
					if (app.certifcate()==null) {
						return AppConstants.SERVER_CONNECTION_ERROR;
					}
					
					con.setSSLSocketFactory(app.certifcate().getSocketFactory());
					con.setRequestMethod("POST");
					con.setRequestProperty("Accept-Encoding", "gzip,deflate");
					con.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
                    // SOAPAction:
                    // "http://webservices.truckstop.com/v12/ILoadSearch/GetLoadSearchResults"
					con.setRequestProperty("SOAPAction",
                            "http://webservices.truckstop.com/v12/ILoadSearch/GetLoadSearchResults");
					con.setRequestProperty("Host", "webservices.truckstop.com");
					con.setRequestProperty("Connection", "Keep-Alive");
					con.setRequestProperty("User-Agent", "Apache-HttpClient/4.1.1 (java 1.5)\\");
                    
                    
                    
                   
                    String envolpe = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\""
                            + " xmlns:v12=\"http://webservices.truckstop.com/v12\""
                            + " xmlns:web=\"http://schemas.datacontract.org/2004/07/WebServices\""
                            + " xmlns:web1=\"http://schemas.datacontract.org/2004/07/WebServices.Searching\""
                            + " xmlns:truc=\"http://schemas.datacontract.org/2004/07/Truckstop2.Objects\""
                            + " xmlns:arr=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\""
                            + ">";
                    String body = envolpe
                            + "\n<soapenv:Header/>\n"
                            + "<soapenv:Body>\n"
                            + "<v12:GetLoadSearchResults>\n"
                            + "<v12:searchRequest>\n"
                            + "<web:IntegrationId>"
                            + session.getUserIntegrationID()
                            + "</web:IntegrationId>\n"
                         
                            + "<web:Password>" + AppConstants.PASSWORD  + "</web:Password>\n"
                               + "<web:UserName>" + AppConstants.USERNAME + "</web:UserName>\n"
                            + "<web1:Criteria>\n"
                            + "<web1:DestinationCity>"
                            + searchdata.getDestinationcity()
                            + "</web1:DestinationCity>\n"
                            + "<web1:DestinationCountry>"
                            + searchdata.getDestinationcountry()
                            + "</web1:DestinationCountry>\n"
                            + "<web1:DestinationRange>"
                            + searchdata.getSearchradius()
                            + "</web1:DestinationRange>\n"
                            + "<web1:DestinationState>"
                            + searchdata.getDestinationstate()
                            + "</web1:DestinationState>\n"
                            // +"<web1:DestinationState>CA</web1:DestinationState>\n"
                            + "<web1:EquipmentOptions>\n"
                            + trailerOptionsAttribute
                            + "</web1:EquipmentOptions>\n"
                            + "<web1:EquipmentType>"
                            + searchdata.getEquipmenttype()
                            + "</web1:EquipmentType>\n"
                            + "<web1:HoursOld>"
                            + searchdata.getHoursold()
                            + "</web1:HoursOld>\n"
                            + "<web1:LoadType>"
                            + searchdata.getLoadtype()
                            + "</web1:LoadType>\n"
                            + "<web1:OriginCity>"
                            + searchdata.getOrigincity()
                            + "</web1:OriginCity>\n"
                            + "<web1:OriginCountry>"
                            + searchdata.getOrigincounrty()
                            + "</web1:OriginCountry>\n"
                            + "<web1:OriginLatitude>0</web1:OriginLatitude>\n"
                            + "<web1:OriginLongitude>0</web1:OriginLongitude>\n"
                            + "<web1:OriginRange>"
                            + searchdata.getSearchradius()
                            + "</web1:OriginRange>\n"
                            // +"<web1:OriginState>"+searchdata.getOriginstate()+"</web1:OriginState>\n"
                            + "<web1:OriginState>"
                            + searchdata.getOriginstate()
                            + "</web1:OriginState>\n"
                            + "<web1:PageNumber>"
                            + loadResultPageNumber
                            + "</web1:PageNumber>\n"
                            + "<web1:PageSize>"
                            + AppConstants.SEARCHLOADS_PAGESIZE
                            + "</web1:PageSize>\n"
                            + "<web1:PickupDates>\n"
                            + dateAttribute
                            + "</web1:PickupDates>\n"
                            + "<web1:SortBy>"
                            + searchdata.getSortype()
                            + "</web1:SortBy>\n"
                            + "<web1:SortDescending>"
                            + Utils.getSortOrder(SearchResultActivty.this,
                                    searchdata.getSortype())
                            + "</web1:SortDescending>\n" + "</web1:Criteria>\n"
                            + "</v12:searchRequest>\n"
                            + "</v12:GetLoadSearchResults>\n"
                            + "</soapenv:Body>\n" + "</soapenv:Envelope>\n";
                    System.out.println("BODY VALUE----" + body);
                 //  System.out.println("BODY VALUE----" + body);
                    post.setEntity(new StringEntity(body));

                    HttpResponse response = client.execute(post);
                    StatusLine statuscode = response.getStatusLine();
                    String statuscodes = Integer.toString(statuscode
                            .getStatusCode());
                    con.setConnectTimeout(AppConstants.SOCKETTIMEOUT_ERROR);
					con.setDoOutput(true);
					DataOutputStream wr = new DataOutputStream(con.getOutputStream());
				//	System.out.println("BODY VALUE----" + body);
					wr.writeBytes(body);
					wr.flush();
					wr.close();
					  if (isCancelled()) return null; //used to stop progress bar and stop async task http://stackoverflow.com/questions/4748964/android-cancel-asynctask-forcefully
					int responseCode = con.getResponseCode();
                    // System.out.println("Status coded-------" + statuscodes);
					if (responseCode!=200) {
                        return AppConstants.SERVER_CONNECTION_ERROR;
                    } else {
                    	  if (isCancelled()) return null; //used to stop progress bar and stop async task http://stackoverflow.com/questions/4748964/android-cancel-asynctask-forcefully
                        InputStream stream = response.getEntity().getContent();
                        Header contentEncoding = response
                                .getFirstHeader("Content-Encoding");
                    	InputStream stream = con.getInputStream();
						String contentEncoding = con.getHeaderField("Content-Encoding");
                        if (contentEncoding != null && contentEncoding.equalsIgnoreCase("gzip")) {
                        	  if (isCancelled()) return null; //used to stop progress bar and stop async task http://stackoverflow.com/questions/4748964/android-cancel-asynctask-forcefully
                            final InputStream stream1 = new GZIPInputStream(  stream);
                            // System.out.println("i am here");
                            BufferedReader reader = new BufferedReader( new InputStreamReader(stream1));
                            String line;
                            while ((line = reader.readLine()) != null) {
                                builder.append(line);
                                
                            }
                            response_str="";
                            response_str = builder.toString();
                            //System.out.println("retval IN IF-----"+ response_str);
                            boolean loadsucess = XMLSearchLoadParser.parsexml(response_str);
                            if (loadsucess) {
                                resultitems = (ArrayList<LoadSearchItem>) XMLSearchLoadParser .getSearchItems();
                                return "success";
                            } else {
                                errormsg = XMLSearchLoadParser .geterrormmsg();
                                // System.out.println("errormsg IN IF-----"+errormsg);
                                return "error";
                            }
                        } else {
                        	  if (isCancelled()) return null; //used to stop progress bar and stop async task http://stackoverflow.com/questions/4748964/android-cancel-asynctask-forcefully
                            BufferedReader reader = new BufferedReader( new InputStreamReader(stream));
                            String line;
                            StringBuilder builder1 = new StringBuilder();
                            while ((line = reader.readLine()) != null) {
                                builder1.append(line);
                               
                            }
                            response_str="";
                            response_str = builder1.toString();
                            // System.out.println("retval IN ELSE-----"+ response_str);
                                boolean loadsucess = XMLSearchLoadParser
                                        .parsexml(response_str);
                                if (loadsucess) {
                                    resultitems = (ArrayList<LoadSearchItem>) XMLSearchLoadParser .getSearchItems();
                                    return "success";
                                } else {
                                    errormsg = XMLSearchLoadParser .geterrormmsg();
                                    return "error";
                                }  
                        }
                    }

                } else {
                    return AppConstants.INTERNET_CONNECTION_ERROR;
                }

            } catch (Exception e) {
                return AppConstants.SERVER_CONNECTION_ERROR;
            }
           
        }

        protected void onPostExecute(String result) {
            progDialog.dismiss();
            //SearchResultActivty.this .setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
            if (result.equals("error")) {
                Utils.showErrorMessage(errormsg, SearchResultActivty.this);
                
                 * Toast.makeText(SearchResultActivty.this,errormsg,Toast.
                 * LENGTH_LONG).show(); finish();
                 
            } else if (result.equals(AppConstants.INTERNET_CONNECTION_ERROR)) {
                Utils.showErrorMessage(AppConstants.INTERNET_CONNECTION_ERROR,
                        SearchResultActivty.this);
                
                 * Toast.makeText(SearchResultActivty.this,AppConstants.
                 * INTERNET_CONNECTION_ERROR,Toast.LENGTH_LONG).show();
                 * finish();
                 
            } else if (result.equals(AppConstants.SERVER_CONNECTION_ERROR)) {
                Utils.showErrorMessage(AppConstants.SERVER_CONNECTION_ERROR,
                        SearchResultActivty.this);
                
                 * Toast.makeText(SearchResultActivty.this,AppConstants.
                 * SERVER_CONNECTION_ERROR, Toast.LENGTH_LONG).show(); finish();
                 
            } else if (resultitems.size() == 0) {

                Utils.showErrorMessage(
                        "No result found for the search criteria made.",
                        SearchResultActivty.this);
                
                 * Toast.makeText(SearchResultActivty.this,"No result found",Toast
                 * .LENGTH_LONG).show(); finish();
                 
            } else {
                int currentPosition = listview.getFirstVisiblePosition();
                sercheditems.addAll(resultitems);
                // System.out.println("paidhistoryresultviewArr sizre in history----->"+
                // paidhistoryresultviewArr.size()+ "count value" + paidcount);
                seachload = SearchResultActivty.this;
                adapter = new SearchListModel(seachload, sercheditems, res);
                listview.setAdapter(adapter);
                if (loadResultPageNumber != 1) {
                    listview.setSelectionFromTop(currentPosition + 1, 0);
                }

                if (resultitems.size() < AppConstants.SEARCHLOADS_PAGESIZE) {
                    btnLoadMore.setVisibility(View.GONE);
                    listview.removeFooterView(btnLoadMore);
                }
                
                 * resultNo = (TextView) findViewById(R.id.ResultNumber);
                 * resultNo.setText(String.valueOf(sercheditems.size()));
                 

                if (intentorigin != 2) {
                    Gson gson = new Gson();
                    String json = gson.toJson(searchdata);
                    // System.out.println("json.tostring------->" + json);
                    session.updateCurrentSearch(json);
                }

            }
        }
    }
*/
    public void ononItemClick(int mPosition) {

        LoadSearchItem searchloadid = (LoadSearchItem) sercheditems
                .get(mPosition);
        String Loadid = searchloadid.getLoadID();

        String daystopay = Utils.checkIsNull(searchloadid.getDays2pay());
        String experiencefactor = Utils.checkIsNull(searchloadid.getExperienceFactor());
        Intent startdetailpage = new Intent(this, SearchDetailActivity.class);
        startdetailpage.putExtra("LOADID", Loadid);
        startdetailpage.putExtra("days2pay", daystopay);
        startdetailpage.putExtra("experiencefactor", experiencefactor);
        startActivity(startdetailpage);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.searchresult_menu, menu);
        MenuItem item = menu.findItem(R.id.action_addfavourite);

        if (showdelete) {
            item.setVisible(true);
        } else {
            item.setVisible(false);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
        case android.R.id.home:
        	
            startSearchActivity();

            return true;

        case R.id.action_refreshsearch:
              Tracker t = ((ITSApplication) getApplication()).getTracker(ITSApplication.TrackerName.APP_TRACKER);
                
              t.send(new HitBuilders.EventBuilder().setCategory(getString( R.string.UICategory))
                      .setAction(getString( R.string.UIAction)).setLabel("Search List Refresh Button").build());
            /*
             * loadResultPageNumber=1; sercheditems.clear(); //onCreate(bundle);
             */Intent startsearch = new Intent(this, SearchResultActivty.class);
            startsearch.putExtra("searchobject", searchdata);
            startsearch.putExtra("md5value", mdvalue);
            startsearch.putExtra("intentorigin", bundle.getInt("intentorigin"));
            startsearch.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(startsearch);

            return true;

        case R.id.action_addfavourite:

            if (db.addTofavourite(searchdata, searchdata.getMd5value())) {
                Toast.makeText(getApplicationContext(),
                        AppConstants.ALREADY_FAVORITE, Toast.LENGTH_LONG)
                        .show();
            } else {
                Toast.makeText(getApplicationContext(),
                        AppConstants.ADD_AS_FAVORITE, Toast.LENGTH_LONG).show();
            }
            return true;

        default:
            return super.onOptionsItemSelected(item);
        }

    }

    private void startSearchActivity() {
    	//NavUtils.navigateUpFromSameTask(this);
        if (bundle.getInt("intentorigin") == 1
                && bundle.getInt("GPSBASED") != 0) {
            Intent startSearchMainActivity = new Intent(this, SearchMain.class);
            startSearchMainActivity.putExtra("Searchtype", 4);
            startSearchMainActivity.putExtra("GPSBASED", gpsBased);
            startSearchMainActivity.putExtra("searchobject", searchdata);
           // startSearchMainActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(startSearchMainActivity);

        }else{
            Intent startSearchMainActivity = new Intent(this, SearchMain.class);
            startSearchMainActivity.putExtra("Searchtype", 4);
            startSearchMainActivity.putExtra("GPSBASED", 0);
            startSearchMainActivity.putExtra("searchobject", searchdata);
            startActivity(startSearchMainActivity);            
            finish();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
     Log.i("TAG", "onBackPressed called");
       /* if (loadsearch != null && loadsearch.getStatus() == Status.RUNNING) {
            loadsearch.cancel(true);
            loadsearch=null;
        }*/
        startSearchActivity();
        
    }
    
    
    @Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Utils.appcameforeground(SearchResultActivty.this);
		LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter(Utils.LOGOUT_USER));
		
	}

	@Override
	public void onPause() {
		LocalBroadcastManager.getInstance(this).unregisterReceiver( mRegistrationBroadcastReceiver);
		
		super.onPause();
	}
	
	 @Override
	    protected void onStop() {
	    	//session.setappbackground(Utils.isAppIsInBackground(SearchMain.this));
	    	Utils.appwentbackground(SearchResultActivty.this);
			//System.out.println("onStop called");
	    	// TODO Auto-generated method stub
	    	super.onStop();
	    }
}