package com.wi_vod.itstrucker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class NetworkStatusReceiver extends BroadcastReceiver {

	public static String TAG = "NetworkStatusReceiver.java";
	private SessionManager session;
	/*
	 * StevenSettings prefsSettings; DatabaseHandler db;
	 */
	Context broadcastcontext;
	boolean networkstatus;
	String timestamp;

	@Override
	public void onReceive(Context context, Intent intent) {
		
		//broadcastcontext = context;
		session=new SessionManager(context);
		//System.out.println("NetworkStatusReceiver called-------------->"+"     session.getnetwrokchangetracker()     "+session.getnetwrokchangetracker());
		if (Utils.isInternetAvailable(context) && session.getnetwrokchangetracker() && !session.getItsPassword().equals("")) {
			// networkstatus="ON";
			//IF netwrokchangetracker is true then this  condition will be executed,this will be set 
			//to true when app is launched for first time in ITSMainscreen.java 
				//System.out.println("calling LoginBackgroundService from NetworkStatusReceiver.java ");
				Intent serviceIntent = new Intent(context,LoginBackgroundService.class);
				context.startService(serviceIntent);
				session.setnetwrokchangetracker(false);
			// System.out.println("networkstatus--->ON");
		}else{
			session.setnetwrokchangetracker(true);
		}

	}

}
