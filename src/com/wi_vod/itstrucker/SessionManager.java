  package com.wi_vod.itstrucker;

import java.util.HashMap;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SessionManager {
	// Shared Preferences
	SharedPreferences pref;
	// Editor for Shared preferences
	Editor editor;
	// Context
	Context _context;
	// Shared pref mode
	int PRIVATE_MODE = 0;
	// Sharedpref file name
	private static final String PREF_NAME = "itstrucker";
	// All Shared Preferences Keys
	private static final String IS_LOGIN = "IsLoggedIn";
	// User name (make variable public to access from outside)
	public static final String ITS_HANDLE = "handle";
	public static final String ITS_ACCOUNTNUMBER = "accountnumber";
	public static final String ITS_INTEGRATIONID = "integrationid";
	public static final String ITS_PASSWORD = "password";
	public static final String PROFILE_COUNTRY = "country";
	public static final String PROFILE_HOMEBASE = "homebase";
	public static final String PROFILE_EQUIPMENTTYPEINDEX = "equipmentindex";
	public static final String PROFILE_TRAILEROPTIONSINDEX = "traileroptions";
	public static final String PROFILE_RADIUS = "radius";
	public static final String PROFILE_LOADSIZE = "loadsizetype";
	
	public static final String PROFILE_SEARCHLOADSIZESTRING = "loadsizetypestring";
	
	public static final String POSTING_EQUIPMENTTYPEINDEX = "ppequipmentindex";
	public static final String POSTING_TRAILEROPTIONSINDEX = "pptraileroptions";
	public static final String POSTING_RADIUS = "ppradius";
	public static final String POSTING_LOADSIZE = "pploadsizetype";
	
	public static final String PROFILE_HOUROLDINDEX = "hoursold";
	public static final String PROFILE_SEARCHHOUROLDSTRING = "hoursold";
	
	public static final String PROFILE_WIDTH = "width";
	public static final String PROFILE_LENGTH = "length";
	public static final String PROFILE_WEIGHT= "weight";
	public static final String PROFILE_MINDIST= "mindist";
	public static final String PROFILE_QUANTITY= "quantity";
	public static final String PROFILE_RATEPERMILE= "ratepermile";
	public static final String PROFILE_SORTTYPEINDEX= "sorttype";
	public static final String PROFILE_SEARCHSORTTYPESTRING= "sorttypestring";
	
	public static final String PROFILE_FISRTTIME = "firsttime";
	public static final String PROFILE_PREVIOUSUSER = "previoususer";
	
	public static final String ACCESS_LOADPOST = "accessloadpost";
	public static final String ACCESS_LOADSEARCH = "accessloadsearch";
	public static final String FIRSTLAUNCH = "firstlaunch";
	
	public static final String POSTDETAIL_LOADID = "postloadid";
	
	public static final String CURRENTSEARCH = "currentsearch";
	public static final String CURRENTVERSION = "currentversion";
	public static final String BROADCASTCOUNT = "broadcastcount";
	public static final String PSPROFILESEARCHOBJECT = "profilesearch";
	
	public static final String BACKGRNDLOGIN_NETWORKCHANGE = "networkchange";
	
	public static final String SINGLEALERT_INSTANCE = "alertinstance";
	public static final String ISAPP_BACKGROUND= "appbackground";
	
	// Constructor
	public SessionManager(Context context) {
		this._context = context;
		pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
	}

	/**
	 * Create login session
	 * */
	public void createLoginSession(String handle, String accountnumber,String password,
			int integrationid,boolean firsttime) {
		// Storing login value as TRUE
		editor.putBoolean(IS_LOGIN, true);
		editor.putBoolean(PROFILE_FISRTTIME, firsttime);
		// Storing name in pref
		editor.putString(ITS_HANDLE, handle);
		editor.putString(ITS_ACCOUNTNUMBER, accountnumber);
		editor.putInt(ITS_INTEGRATIONID, integrationid);
		editor.putString(ITS_PASSWORD, password);
		// commit changes
		editor.commit();
	}
	
	public void updateloadpostaccess() {
		editor.putInt(ACCESS_LOADPOST, 1);
		editor.commit();
	}
	
	
	public void updateloadsearhaccess() {
		editor.putInt(ACCESS_LOADSEARCH, 1);
		editor.commit();
	}
	
	
	public void postLoadId(String loadid) {
		editor.putString(POSTDETAIL_LOADID, loadid);
		editor.commit();
	}
	
	public String getpostedLoadId() {
		return pref.getString(POSTDETAIL_LOADID, "");
	}

	
	
	public void updatefirsttime(boolean updatelaunch) {
		editor.putBoolean(FIRSTLAUNCH, updatelaunch);
		editor.commit();
	}
	
	
	/**
	 * Check login method wil check user login status If false it will redirect
	 * user to login page Else won't do anything
	 * */
	public void checkLogin() {
		// Check login status
		if (!this.isLoggedIn()) {
			// user is not logged in redirect him to Login Activity
			Intent i = new Intent(_context, ITSTruckerLogin.class);
			// Closing all the Activities
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			// Add new Flag to start new Activity
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			// Staring Login Activity
			_context.startActivity(i);
		}

	}
	
	/**
	 * Get stored ITS Handle data
	 **/
	public String getItsHandle() {
		// return ItsHandle
		return pref.getString(ITS_HANDLE, "");
	}
	
	/**
	 * Get stored ITS Handle data
	 **/
	public String getItsPassword() {
		// return ItsHandle
		return pref.getString(ITS_PASSWORD, "");
	}
	
	
	/**
	 * Get stored ITS Account data
	 **/
	public String getItsAccountNo() {
		// return accountnumber
		return pref.getString(ITS_ACCOUNTNUMBER, "");
	}
	
	/**
	 * Get stored ITS Integration ID details
	 **/
	public int getUserIntegrationID() {
		// return accountnumber
		return pref.getInt(ITS_INTEGRATIONID, 0);
	}
	
	
	/**
	 * Get stored ITS Handle data
	 **/
	public String getpreviousUser() {
		// return ItsHandle
		return pref.getString(PROFILE_PREVIOUSUSER, "");
	}
	
	/**
	 * 
	 **/
	public boolean isFirstTime() {
		// return ItsHandle
		return pref.getBoolean(PROFILE_FISRTTIME, true);
	}

	
	public void updateFirsttime(boolean firsttime) {
		// return ItsHandle
		editor.putBoolean(PROFILE_FISRTTIME, firsttime);
		editor.commit();
	}
	/**
	 * Clear session details
	 * */
	public void logoutUser() {
		// Clearing all data from Shared Preferences
		editor.remove(ITS_INTEGRATIONID);
		editor.remove(IS_LOGIN);
		editor.remove(ITS_PASSWORD);
		String user=getItsHandle();
		editor.putString(PROFILE_PREVIOUSUSER, user);
	    //editor.putString(CURRENTSEARCH, null);
		
		editor.putInt(ACCESS_LOADPOST, 0);
		editor.putInt(ACCESS_LOADSEARCH, 0);
		
		
		// editor.clear();
		editor.commit();
	
		// set the new task and clear flags
		
		
		// After logout redirect user to Loing Activity
		Intent i = new Intent(_context, ITSTruckerLogin.class);
		// Closing all the Activities
		//i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		// Add new Flag to start new Activity
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		// Staring Login Activity
		_context.startActivity(i);
	}
	
	
	
	/**
	 * Clear session details
	 * */
	public void clearSession() {
		// Clearing all data from Shared Preferences
		editor.remove(ITS_INTEGRATIONID);
		editor.remove(IS_LOGIN);
		editor.remove(ITS_PASSWORD);
		String user=getItsHandle();
		editor.putString(PROFILE_PREVIOUSUSER, user);
		editor.putString(CURRENTSEARCH, null);
		
		editor.putInt(ACCESS_LOADPOST, 0);
		editor.putInt(ACCESS_LOADSEARCH, 0);
		
		
		// editor.clear();
		editor.commit();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	/**
	 * Quick check for login
	 * **/
	// Get Login State
	public boolean isLoggedIn() {
		return pref.getBoolean(IS_LOGIN, false);
	}

	/**
	 * Create login session
	 * */
	public void createsearchProfile(int countrySlection,String homebase, String equipmenttype,int radius,String traileroption,int loadsize,
			int hoursold,int sorttype) {
		editor.putInt(PROFILE_COUNTRY, countrySlection);
		editor.putString(PROFILE_HOMEBASE, homebase);
		editor.putString(PROFILE_EQUIPMENTTYPEINDEX, equipmenttype);
		editor.putInt(PROFILE_RADIUS, radius);
		editor.putString(PROFILE_TRAILEROPTIONSINDEX, traileroption);
		editor.putInt(PROFILE_LOADSIZE, loadsize);
		editor.putInt(PROFILE_HOUROLDINDEX, hoursold);
		editor.putInt(PROFILE_SORTTYPEINDEX, sorttype);

		//PROFILE_SEARCHLOADSIZESTRING,PROFILE_SEARCHHOUROLDSTRING
		// commit changes
		editor.commit();
	}
	
	
	public void createpostingProfile(String equipmenttype,int radius,String traileroption,int loadsize,
			String width,String length,String weight,String mindist,String ratePerMile) {
		// Storing login value as TRUE
		
		editor.putString(POSTING_EQUIPMENTTYPEINDEX, equipmenttype);
		editor.putInt(POSTING_RADIUS, radius);
		editor.putString(POSTING_TRAILEROPTIONSINDEX, traileroption);
		editor.putInt(POSTING_LOADSIZE, loadsize);
		editor.putString(PROFILE_WIDTH, width);
		editor.putString(PROFILE_LENGTH, length);
		editor.putString(PROFILE_WEIGHT, weight);
		editor.putString(PROFILE_MINDIST, mindist);
		editor.putString(PROFILE_RATEPERMILE, ratePerMile);
		// commit changes
		editor.commit();
	}
	
	
	
	public void clearprofilevalues(){
		editor.remove(PROFILE_COUNTRY);
		editor.remove(PROFILE_HOMEBASE);
		editor.remove(PROFILE_EQUIPMENTTYPEINDEX);
		editor.remove(PROFILE_RADIUS);
		editor.remove(PROFILE_TRAILEROPTIONSINDEX);
		editor.remove(PROFILE_LOADSIZE);
		editor.remove(PROFILE_HOUROLDINDEX);
		editor.remove(PROFILE_SORTTYPEINDEX);
		
		editor.remove(POSTING_EQUIPMENTTYPEINDEX);
		editor.remove(POSTING_RADIUS);
		editor.remove(POSTING_TRAILEROPTIONSINDEX);
		editor.remove(POSTING_LOADSIZE);
		editor.remove(PROFILE_WIDTH);
		editor.remove(PROFILE_LENGTH);
		editor.remove(PROFILE_WEIGHT);
		editor.remove(PROFILE_MINDIST);
		editor.remove(PROFILE_RATEPERMILE);
		editor.putString(CURRENTSEARCH, null);
		editor.commit();
	}
	
	
	
	public void updateCurrentSearch(String object){
		//System.out.println("object--------------"+object);
		editor.putString(CURRENTSEARCH, object);
		editor.commit();
		//System.out.println("pref.getString(CURRENTSEARCH, ---------"+pref.getString(CURRENTSEARCH, ""));
	}
	
	public String getCurrentSearch(){
		//System.out.println("pref.getString(CURRENTSEARCH called, ---------"+pref.getString(CURRENTSEARCH, ""));
		return pref.getString(CURRENTSEARCH, "");
		
	}
	
	
	
	
	
	
	
	
	
	public void updateSearchprofileSearch(String profileobject){
		//System.out.println("object--------------"+object);
		editor.putString(PSPROFILESEARCHOBJECT, profileobject);
		editor.commit();
		//System.out.println("pref.getString(CURRENTSEARCH, ---------"+pref.getString(CURRENTSEARCH, ""));
	}
	
	public String getSearchprofileSearch(){
		//System.out.println("pref.getString(CURRENTSEARCH called, ---------"+pref.getString(CURRENTSEARCH, ""));
		return pref.getString(PSPROFILESEARCHOBJECT, "");
		
	}
	
	
	
	
	
	
	
	public void setpackageVersion(String version){
		//System.out.println("object--------------"+object);
		editor.putString(CURRENTVERSION, version);
		editor.commit();
		//System.out.println("pref.getString(CURRENTSEARCH, ---------"+pref.getString(CURRENTSEARCH, ""));
	}
	
	public String getPackageVersion(){
		//System.out.println("pref.getString(CURRENTSEARCH called, ---------"+pref.getString(CURRENTSEARCH, ""));
		return pref.getString(CURRENTVERSION, "");
		
	}
	
	
	
	public void setbroadcastcount(int count){
		//System.out.println("object--------------"+object);
		editor.putInt(BROADCASTCOUNT, count);
		editor.commit();
		//System.out.println("pref.getString(CURRENTSEARCH, ---------"+pref.getString(CURRENTSEARCH, ""));
	}
	
	public int getbroadcastcount(){
		//System.out.println("pref.getString(CURRENTSEARCH called, ---------"+pref.getString(CURRENTSEARCH, ""));
		return pref.getInt(BROADCASTCOUNT, 0);
		
	}
	
	public void setnetwrokchangetracker(boolean networkchange){
		//System.out.println("object--------------"+object);
		editor.putBoolean(BACKGRNDLOGIN_NETWORKCHANGE, networkchange);
		editor.commit();
		//System.out.println("pref.getString(CURRENTSEARCH, ---------"+pref.getString(CURRENTSEARCH, ""));
	}
	
	public boolean getnetwrokchangetracker(){
		//System.out.println("object--------------"+object);
		return pref.getBoolean(BACKGRNDLOGIN_NETWORKCHANGE, false);
		
		//System.out.println("pref.getString(CURRENTSEARCH, ---------"+pref.getString(CURRENTSEARCH, ""));
	}
	
	public void setalertinstance(boolean networkchange){
		//System.out.println("object--------------"+object);
		editor.putBoolean(SINGLEALERT_INSTANCE, networkchange);
		editor.commit();
		//System.out.println("pref.getString(CURRENTSEARCH, ---------"+pref.getString(CURRENTSEARCH, ""));
	}
	public boolean getalertinstance(){
		//System.out.println("object--------------"+object);
		return pref.getBoolean(SINGLEALERT_INSTANCE, false);
		//System.out.println("pref.getString(CURRENTSEARCH, ---------"+pref.getString(CURRENTSEARCH, ""));
	}
	
	
	public void setappbackground(boolean appbackground){
		//System.out.println("object--------------"+object);
		editor.putBoolean(ISAPP_BACKGROUND, appbackground);
		editor.commit();
		//System.out.println("pref.getString(CURRENTSEARCH, ---------"+pref.getString(CURRENTSEARCH, ""));
	}
	public boolean getisappbackground(){
		//System.out.println("object--------------"+object);
		return pref.getBoolean(ISAPP_BACKGROUND, false);
		//System.out.println("pref.getString(CURRENTSEARCH, ---------"+pref.getString(CURRENTSEARCH, ""));
	}
	
	
}
