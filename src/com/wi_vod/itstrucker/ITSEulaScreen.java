package com.wi_vod.itstrucker;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.RenderPriority;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.wi_vod.itstrucker.ITSTruckerLogin.HttpServiceListener;
import com.wi_vod.itstrucker.model.DatabaseHandler;
import com.wi_vod.itstrucker.model.LoginProducts;
import com.wi_vod.itstrucker.model.XMLEULAresponseparser;
import com.wi_vod.itstrucker.model.XMLparser;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.zip.GZIPInputStream;

import javax.net.ssl.HttpsURLConnection;


import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.xmlpull.v1.XmlPullParserException;

public class ITSEulaScreen extends Activity {
	private static String TAG="ITSEulaScreen";
	TextView eulaText;
	Button acceptbtn, declinebtn;
	private ProgressDialog progDialog;
	String retval, ret, response_str=null;
	StringBuilder builder = new StringBuilder();
	List<LoginProducts> products = new ArrayList<LoginProducts>();
	private String errormsg;
	private SessionManager session;
	private SharedPreferences prefs;
	private String companyId;
	private String handleId;
	private String itsusername;
	private String itshandle;
	private String itspassword;
	
	private WebView webView ;
	private ProgressDialog mProgressDialog;
	final Activity activity = this;
	DatabaseHandler db;
	
	 //prevents orientation change from reloading results and crashing app
    @Override 
    public void onConfigurationChanged(Configuration newConfig) { 
      
      super.onConfigurationChanged(newConfig); 
    } 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.itseula_screen);
		Bundle bundle = getIntent().getExtras();
		session = new SessionManager(ITSEulaScreen.this);
		prefs = getSharedPreferences("itstrucker", 0);
		db = new DatabaseHandler(this);
		if (bundle != null) {
			companyId = bundle.getString("COMPANYID");
			handleId = bundle.getString("HANDLEID");
			 itsusername= bundle.getString("ITSACCOUNT");
			 itshandle= bundle.getString("HANDLE");
			 itspassword= bundle.getString("PASSWORD");
		}
		
//		eulaText = (TextView)findViewById(R.id.eulatext);
	
		webView = (WebView)findViewById(R.id.webview);
		acceptbtn = (Button) findViewById(R.id.accept);
		declinebtn = (Button) findViewById(R.id.decline);

		/*StringBuilder text = new StringBuilder();
		try {
			// Construct BufferedReader from InputStreamReader
			BufferedReader br = new BufferedReader(new InputStreamReader(getAssets().open("Terms.txt"), "UTF-8"));
			String line = null;
			while ((line = br.readLine()) != null) {
				text.append(line);
				text.append("\n");
			}
			br.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
		eulaText.setText(text.toString());*/
		
		if(Utils.isInternetAvailable(activity)){
		    
		    showTNCAlert1();
		    
    		mProgressDialog = new ProgressDialog(this); 
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL); 
            mProgressDialog.setMessage("Please wait..."); 
            mProgressDialog.setCancelable(false); 
            mProgressDialog.setMax(100); 
            mProgressDialog.show(); 
            
    		webView.getSettings().setJavaScriptEnabled(true);
    		webView.getSettings().setUseWideViewPort(false);

    		webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
    		
    		webView.setWebChromeClient(new WebChromeClient() {
                 public void onProgressChanged(WebView view, final int progress)
                 { 
                     activity.setTitle("Loading..."); 
                     activity.setProgress(progress * 100);
                     mProgressDialog.setProgress(progress );
    
                     if(progress == 100)
                     { 
                         activity.setTitle(R.string.app_name);
                         mProgressDialog.dismiss(); 
                     } 
                 } 
             }); 
    
             webView.setWebViewClient(new WebViewClient() {
                 @Override 
                 public void onReceivedError(WebView view, int errorCode, String description, String failingUrl){
                     AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
                     alertDialog.setTitle("Error");
                     alertDialog.setMessage(AppConstants.INTERNET_CONNECTION_ERROR);
                     alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                         public void onClick(DialogInterface dialog,int which) {
                              dialog.cancel();
                              Intent i = new Intent(getApplicationContext(),ITSTruckerLogin.class);
                              startActivity(i);
                              finish();
                         }
                     });
                     alertDialog.show();
                    
                 } 
    
                 @Override 
                 public boolean shouldOverrideUrlLoading(WebView view, String url)
                 { 
                     view.loadUrl(url);
                     return true; 
                 } 
             }); 
             
              webView.loadUrl(AppConstants.TERMS_CONDITIONS_URL);
//            webView.loadUrl("file:///android_asset/.html");
		}
		else{
		    Utils.showAlertMessage(AppConstants.INTERNET_CONNECTION_ERROR, ITSEulaScreen.this);
		    
		}
	}

	public void startAcceptedRequest(View v) {
		
		/*new SendAcceptTermsRequest().execute();*/
		SendAcceptTermsRequest();
	}

	private void SendAcceptTermsRequest() {
		String envolpe = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"";
		String uri = " xmlns:tem=\"http://tempuri.org/\">";
		String body = envolpe
				+ uri
				+ "\n<soapenv:Header/>\n"
				+ "<soapenv:Body>\n"
				+ "<tem:RecordUserHandleAcceptsAUP>\n"
				+ "<tem:authtoken>\n"
				+ "<tem:UserName>"+ AppConstants.USERNAME+ "</tem:UserName>\n"
				+ "<tem:Password>"+ AppConstants.PASSWORD+ "</tem:Password>\n"
				+ "<tem:IntegrationId>"+ AppConstants.INTEGRARTION_ID+ "</tem:IntegrationId>\n"
				+ "</tem:authtoken>\n"
				+ "<tem:itsAccount_ID>"+companyId+"</tem:itsAccount_ID>\n"
				+ "<tem:acceptorsHandle_ID>"+handleId+"</tem:acceptorsHandle_ID>\n"
				+ "<tem:itsAccount_ID>1656833</tem:itsAccount_ID>\n"
				+ "<tem:acceptorsHandle_ID>1040464</tem:acceptorsHandle_ID>\n"
				+ "</tem:RecordUserHandleAcceptsAUP>\n"
				+ "</soapenv:Body>\n" + "</soapenv:Envelope>";
		//System.out.println("BODY VALUE----" + body);
		
		
		String SOAPACTION="http://tempuri.org/IWebServiceAdmin/RecordUserHandleAcceptsAUP";
		
		HttpEULAServiceListener listener = new HttpEULAServiceListener();
		WebServiceConnectionService serv = new WebServiceConnectionService(ITSEulaScreen.this,body,
				SOAPACTION, listener,AppConstants.EULA_PROGRESS);
		serv.execute(AppConstants.LOGIN_TEST_URL);
		
	}
	
	
	 protected class HttpEULAServiceListener implements ConnectionServiceListener {
		@Override
		public void onServiceComplete(String response) {
			if (response.equals(AppConstants.INTERNET_CONNECTION_ERROR)) {
				startlogin(AppConstants.INTERNET_CONNECTION_ERROR);
				/*
				 * Toast.makeText(PostActivity.this,AppConstants.
				 * INTERNET_CONNECTION_ERROR,Toast.LENGTH_LONG).show();
				 * finish();
				 */
			} else if (response.equals(AppConstants.SERVER_CONNECTION_ERROR)) {
				startlogin(AppConstants.SERVER_CONNECTION_ERROR);
				/*
				 * Toast.makeText(PostActivity.this,AppConstants.
				 * SERVER_CONNECTION_ERROR, Toast.LENGTH_LONG).show(); finish();
				 */
			}else if (response.equals(AppConstants.PLAYSERVICES_NOTFOUND)) {
				startlogin(AppConstants.PLAYSERVICES_NOTFOUND);
				/*
				 * Toast.makeText(PostActivity.this,AppConstants.
				 * SERVER_CONNECTION_ERROR, Toast.LENGTH_LONG).show(); finish();
				 */
			} else {
				
				boolean result;
				try {
					result = XMLEULAresponseparser.parsexml(response);
					if (!result) {
						errormsg=XMLparser.geterrormsg();
						startlogin(errormsg);
					} else {
						products.clear();
						sendLoginRequest();
					}
					
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					startlogin(AppConstants.SERVER_CONNECTION_ERROR);
				}
			}
		}

		

		@Override
		public void onServiceComplete(String response, String reference) {
			// TODO Auto-generated method stub
			
		}
		 
	 }
	
	 
	 
	 private void sendLoginRequest() {
		 String envolpe = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"";
			String uri = " xmlns:tem=\"http://tempuri.org/\">";
			String body = envolpe
					+ uri
					+ "\n<soapenv:Header/>\n"
					+ "<soapenv:Body>\n"
					+ "<tem:VerifyItsAccountHandleAndPasswordGetIdAndProducts>\n"
					+ "<tem:authtoken>\n"
					+ "<tem:UserName>"+ AppConstants.USERNAME+ "</tem:UserName>\n"
					+ "<tem:Password>"+ AppConstants.PASSWORD+ "</tem:Password>\n"
					+ "<tem:IntegrationId>"+ AppConstants.INTEGRARTION_ID+ "</tem:IntegrationId>\n"
					+ "</tem:authtoken>\n"
					+ "<tem:itsAccount>"+ itsusername+ "</tem:itsAccount>\n"
					+ "<tem:handle>"+ itshandle+ "</tem:handle>\n"
					+ "<tem:password>"+ itspassword+ "</tem:password>\n"
			//	+ "<tem:password>luiulikj</tem:password>\n"
					+ "</tem:VerifyItsAccountHandleAndPasswordGetIdAndProducts>\n"
					+ "</soapenv:Body>\n" + "</soapenv:Envelope>";
			String SOAPACTION="http://tempuri.org/IWebServiceAdmin/VerifyItsAccountHandleAndPasswordGetIdAndProducts";
			
			HttpServiceListener listener = new HttpServiceListener();
			WebServiceConnectionService serv = new WebServiceConnectionService(ITSEulaScreen.this,body,
					SOAPACTION, listener,AppConstants.LOGIN_PROGRESS);
			serv.execute(AppConstants.LOGIN_TEST_URL);
			//new VerifyLogin().execute();
		}
	 
	 
	public void declined(View v) {
//		startLoginScreen();
	    showTNCAlert2();
	}
	
	
	
	
	/*********************************************************
     * Class PostTruck
     * 
     * Description: Task for processing the HTTP request
     *//*
    
    private class SendAcceptTermsRequest extends AsyncTask<Void, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progDialog = new ProgressDialog(ITSEulaScreen.this);
			progDialog.setMessage(AppConstants.EULA_PROGRESS);
			progDialog.setCancelable(false);
			progDialog.show();

		}

		@Override
		protected String doInBackground(Void... params) {
			
			try {
				//System.out.println(Utils.isInternetAvailable(ITSTruckerLogin.this));
				if (Utils.isInternetAvailable(ITSEulaScreen.this)) {
					// Log.i("IN BACKGROUND", "doInBackground");
					HttpPost post = new HttpPost(AppConstants.LOGIN_TEST_URL);
					int timeoutConnection = 9000;
					HttpParams httpParameters = new BasicHttpParams();
					HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
					// Set the default socket timeout (SO_TIMEOUT) 
					// in milliseconds which is the timeout for waiting for data.
					int timeoutSocket = 5000;
					HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
					DefaultHttpClient client = new DefaultHttpClient(httpParameters);
					post.setHeader("Accept-Encoding", "gzip,deflate");
					post.setHeader("Content-Type", "text/xml;charset=UTF-8");
					post.setHeader("SOAPAction","http://tempuri.org/IWebServiceAdmin/RecordUserHandleAcceptsAUP");
					post.setHeader("Host", "webservices.truckstop.com");
					post.setHeader("Connection", "Keep-Alive");
					post.setHeader("User-Agent","Apache-HttpClient/4.1.1 (java 1.5)\\");
					
					
					 String url = AppConstants.LOGIN_TEST_URL;
	                    //Log.i(TAG, AppConstants.LOGIN_TEST_URL);
						URL obj = new URL(url);
						HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
						ITSApplication app = ((ITSApplication) getApplicationContext());
						//ITSApplication app= new ITSApplication();
						if (app.certifcate()==null) {
							return AppConstants.SERVER_CONNECTION_ERROR;
						}
						
						con.setSSLSocketFactory(app.certifcate().getSocketFactory());
						con.setRequestMethod("POST");
						con.setRequestProperty("Accept-Encoding", "gzip,deflate");
						con.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
	                    // SOAPAction:
	                    // "http://webservices.truckstop.com/v12/ILoadSearch/GetLoadSearchResults"
						con.setRequestProperty("SOAPAction","http://tempuri.org/IWebServiceAdmin/RecordUserHandleAcceptsAUP");
						con.setRequestProperty("Host", "webservices.truckstop.com");
						con.setRequestProperty("Connection", "Keep-Alive");
						con.setRequestProperty("User-Agent", "Apache-HttpClient/4.1.1 (java 1.5)\\");
					
						String envolpe = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"";
						String uri = " xmlns:tem=\"http://tempuri.org/\">";
						String body = envolpe
								+ uri
								+ "\n<soapenv:Header/>\n"
								+ "<soapenv:Body>\n"
								+ "<tem:RecordUserHandleAcceptsAUP>\n"
								+ "<tem:authtoken>\n"
								+ "<tem:UserName>"+ AppConstants.USERNAME+ "</tem:UserName>\n"
								+ "<tem:Password>"+ AppConstants.PASSWORD+ "</tem:Password>\n"
								+ "<tem:IntegrationId>"+ AppConstants.INTEGRARTION_ID+ "</tem:IntegrationId>\n"
								+ "</tem:authtoken>\n"
								+ "<tem:itsAccount_ID>"+companyId+"</tem:itsAccount_ID>\n"
								+ "<tem:acceptorsHandle_ID>"+handleId+"</tem:acceptorsHandle_ID>\n"
								+ "<tem:itsAccount_ID>1656833</tem:itsAccount_ID>\n"
								+ "<tem:acceptorsHandle_ID>1040464</tem:acceptorsHandle_ID>\n"
								+ "</tem:RecordUserHandleAcceptsAUP>\n"
								+ "</soapenv:Body>\n" + "</soapenv:Envelope>";
						//System.out.println("BODY VALUE----" + body);
					post.setEntity(new StringEntity(body));
					HttpResponse response = client.execute(post);
					StatusLine statuscode = response.getStatusLine();
					String statuscodes = Integer.toString(statuscode.getStatusCode());
				
					if (!statuscodes.equals("200")) {
						return AppConstants.SERVER_CONNECTION_ERROR;
					}else{
						InputStream stream = response.getEntity().getContent();
						Header contentEncoding = response.getFirstHeader("Content-Encoding");
						if (contentEncoding != null&& contentEncoding.getValue().equalsIgnoreCase("gzip")) {
					
					
					 con.setConnectTimeout(AppConstants.SOCKETTIMEOUT_ERROR);
						con.setDoOutput(true);
						DataOutputStream wr = new DataOutputStream(con.getOutputStream());
						//System.out.println("BODY VALUE----" + body);
						wr.writeBytes(body);
						wr.flush();
						wr.close();
				 
						int responseCode = con.getResponseCode();
	                    // System.out.println("Status coded-------" + statuscodes);
						if (responseCode!=200) {
	                        return AppConstants.SERVER_CONNECTION_ERROR;
	                    } else {
	                    	InputStream stream = con.getInputStream();
							String contentEncoding = con.getHeaderField("Content-Encoding");
						if (contentEncoding != null&& contentEncoding.equalsIgnoreCase("gzip")) {
							final InputStream stream1 = new GZIPInputStream(stream);
							// System.out.println("i am here");
							BufferedReader reader = new BufferedReader(new InputStreamReader(stream1));
							String line;
							StringBuilder sbuilder = new StringBuilder();
							while ((line = reader.readLine()) != null) {
								sbuilder.append(line);
								
								response_str="";
								response_str = sbuilder.toString();
								//System.out.println("in if statement\n"+response_str);
								boolean result = XMLEULAresponseparser.parsexml(response_str);
								if (!result) {
									errormsg=XMLparser.geterrormsg();
									return "error";
								} else {
									return "success";
								}
							}
						} else {
							BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
							String line;
							StringBuilder sbuilder = new StringBuilder();
							while ((line = reader.readLine()) != null) {
								sbuilder.append(line);
								response_str="";
								response_str = sbuilder.toString();
								//System.out.println("retval IN ELSE-----"+ response_str);
								boolean result = XMLEULAresponseparser.parsexml(response_str);
								if (!result) {
									errormsg=XMLEULAresponseparser.geterrormsg();
									return "error";
								} else {
									//employeesq=XMLparser.getproducts();
									return "success";
								}
							}
						}
					}
					
				}else {
					return AppConstants.INTERNET_CONNECTION_ERROR;

				}

			} catch (Exception e) {
				return AppConstants.SERVER_CONNECTION_ERROR;
			}
			return AppConstants.SERVER_CONNECTION_ERROR;
		}
		@Override
		protected String doInBackground(Void... params) {
			
			try {
				//System.out.println(Utils.isInternetAvailable(ITSTruckerLogin.this));
				if (Utils.isInternetAvailable(ITSEulaScreen.this)) {
					// Log.i("IN BACKGROUND", "doInBackground");
					HttpPost post = new HttpPost(AppConstants.LOGIN_TEST_URL);
					int timeoutConnection = 9000;
					HttpParams httpParameters = new BasicHttpParams();
					HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
					// Set the default socket timeout (SO_TIMEOUT) 
					// in milliseconds which is the timeout for waiting for data.
					int timeoutSocket = 5000;
					HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
					DefaultHttpClient client = new DefaultHttpClient(httpParameters);
					post.setHeader("Accept-Encoding", "gzip,deflate");
					post.setHeader("Content-Type", "text/xml;charset=UTF-8");
					post.setHeader("SOAPAction","http://tempuri.org/IWebServiceAdmin/RecordUserHandleAcceptsAUP");
					post.setHeader("Host", "webservices.truckstop.com");
					post.setHeader("Connection", "Keep-Alive");
					post.setHeader("User-Agent","Apache-HttpClient/4.1.1 (java 1.5)\\");
					
					String envolpe = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"";
					String uri = " xmlns:tem=\"http://tempuri.org/\">";
					String body = envolpe
							+ uri
							+ "\n<soapenv:Header/>\n"
							+ "<soapenv:Body>\n"
							+ "<tem:RecordUserHandleAcceptsAUP>\n"
							+ "<tem:authtoken>\n"
							+ "<tem:UserName>"+ AppConstants.USERNAME+ "</tem:UserName>\n"
							+ "<tem:Password>"+ AppConstants.PASSWORD+ "</tem:Password>\n"
							+ "<tem:IntegrationId>"+ AppConstants.INTEGRARTION_ID+ "</tem:IntegrationId>\n"
							+ "</tem:authtoken>\n"
							+ "<tem:itsAccount_ID>"+companyId+"</tem:itsAccount_ID>\n"
							+ "<tem:acceptorsHandle_ID>"+handleId+"</tem:acceptorsHandle_ID>\n"
							+ "<tem:itsAccount_ID>1656833</tem:itsAccount_ID>\n"
							+ "<tem:acceptorsHandle_ID>1040464</tem:acceptorsHandle_ID>\n"
							+ "</tem:RecordUserHandleAcceptsAUP>\n"
							+ "</soapenv:Body>\n" + "</soapenv:Envelope>";
					//System.out.println("BODY VALUE----" + body);
					post.setEntity(new StringEntity(body));
					HttpResponse response = client.execute(post);
					StatusLine statuscode = response.getStatusLine();
					String statuscodes = Integer.toString(statuscode.getStatusCode());
				
					if (!statuscodes.equals("200")) {
						return AppConstants.SERVER_CONNECTION_ERROR;
					}else{
						InputStream stream = response.getEntity().getContent();
						Header contentEncoding = response.getFirstHeader("Content-Encoding");
						if (contentEncoding != null&& contentEncoding.getValue().equalsIgnoreCase("gzip")) {
							final InputStream stream1 = new GZIPInputStream(stream);
							// System.out.println("i am here");
							BufferedReader reader = new BufferedReader(new InputStreamReader(stream1));
							String line;
							StringBuilder sbuilder = new StringBuilder();
							while ((line = reader.readLine()) != null) {
								sbuilder.append(line);
								
								response_str="";
								response_str = sbuilder.toString();
								//System.out.println("in if statement\n"+response_str);
								boolean result = XMLEULAresponseparser.parsexml(response_str);
								if (!result) {
									errormsg=XMLparser.geterrormsg();
									return "error";
								} else {
									return "success";
								}
							}
						} else {
							BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
							String line;
							StringBuilder sbuilder = new StringBuilder();
							while ((line = reader.readLine()) != null) {
								sbuilder.append(line);
								response_str="";
								response_str = sbuilder.toString();
								//System.out.println("retval IN ELSE-----"+ response_str);
								boolean result = XMLEULAresponseparser.parsexml(response_str);
								if (!result) {
									errormsg=XMLEULAresponseparser.geterrormsg();
									return "error";
								} else {
									//employeesq=XMLparser.getproducts();
									return "success";
								}
							}
						}
					}
					
				}else {
					return AppConstants.INTERNET_CONNECTION_ERROR;

				}

			} catch (Exception e) {
				return AppConstants.SERVER_CONNECTION_ERROR;
			}
			return AppConstants.SERVER_CONNECTION_ERROR;
		}


		protected void onPostExecute(String result) {
			progDialog.dismiss();
			if (result.equals("error")) {
				Log.i("error msg in ITSEULASCREEN--->", errormsg);
				Utils.showAlertMessage(errormsg, ITSEulaScreen.this);	
				startlogin(errormsg);
			} else if (result.equals(AppConstants.INTERNET_CONNECTION_ERROR)) {
				startlogin(AppConstants.INTERNET_CONNECTION_ERROR);
				
				 * Toast.makeText(PostActivity.this,AppConstants.
				 * INTERNET_CONNECTION_ERROR,Toast.LENGTH_LONG).show();
				 * finish();
				 
			} else if (result.equals(AppConstants.SERVER_CONNECTION_ERROR)) {
				startlogin(AppConstants.SERVER_CONNECTION_ERROR);
				
				 * Toast.makeText(PostActivity.this,AppConstants.
				 * SERVER_CONNECTION_ERROR, Toast.LENGTH_LONG).show(); finish();
				 
			} else {
				products.clear();
				String envolpe = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"";
				String uri = " xmlns:tem=\"http://tempuri.org/\">";
				String body = envolpe
						+ uri
						+ "\n<soapenv:Header/>\n"
						+ "<soapenv:Body>\n"
						+ "<tem:VerifyItsAccountHandleAndPasswordGetIdAndProducts>\n"
						+ "<tem:authtoken>\n"
						+ "<tem:UserName>"+ AppConstants.USERNAME+ "</tem:UserName>\n"
						+ "<tem:Password>"+ AppConstants.PASSWORD+ "</tem:Password>\n"
						+ "<tem:IntegrationId>"+ AppConstants.INTEGRARTION_ID+ "</tem:IntegrationId>\n"
						+ "</tem:authtoken>\n"
						+ "<tem:itsAccount>"+ itsusername+ "</tem:itsAccount>\n"
						+ "<tem:handle>"+ itshandle+ "</tem:handle>\n"
						+ "<tem:password>"+ itspassword+ "</tem:password>\n"
				//	+ "<tem:password>luiulikj</tem:password>\n"
						+ "</tem:VerifyItsAccountHandleAndPasswordGetIdAndProducts>\n"
						+ "</soapenv:Body>\n" + "</soapenv:Envelope>";
				String SOAPACTION="http://tempuri.org/IWebServiceAdmin/VerifyItsAccountHandleAndPasswordGetIdAndProducts";
				
				HttpServiceListener listener = new HttpServiceListener();
				WebServiceConnectionService serv = new WebServiceConnectionService(ITSEulaScreen.this,body,
						SOAPACTION, listener,AppConstants.LOGIN_PROGRESS);
				serv.execute(AppConstants.LOGIN_TEST_URL);
				//new VerifyLogin().execute();
			}
		}
	}*/
    
    
    /*start loginscreen activity based on the user event or webresponse
	 * 
	 * 
	 */
	public void startlogin(String msg) {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(ITSEulaScreen.this);
		// Setting Dialog Title
		alertDialog.setTitle("Error");
		// Setting Dialog Message
		alertDialog.setMessage(msg);
		// On pressing OK button start login screen
		alertDialog.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
						startLoginScreen();
					}
				});
		 alertDialog.show();
	}

    public void startLoginScreen(){
    	Intent startloginscreen = new Intent(getApplicationContext(),ITSTruckerLogin.class);
		startloginscreen.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(startloginscreen);
		finish();
    }
    
    
    public void showTNCAlert1(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ITSEulaScreen.this);
        alertDialog.setMessage(AppConstants.TNC_MSG_CONTENT_1);
        alertDialog.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
        });
        alertDialog.show();
    }
    
    public void showTNCAlert1_showLoginScreen(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ITSEulaScreen.this);
        alertDialog.setMessage(AppConstants.TNC_MSG_CONTENT_1);
        alertDialog.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startLoginScreen();
                    }
        });
        alertDialog.show();
    }
    
    
    public void showTNCAlert2(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ITSEulaScreen.this);
        alertDialog.setMessage(AppConstants.TNC_MSG_CONTENT_2);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        showTNCAlert1_showLoginScreen();
                    }
        });
        alertDialog.show();
    }
    
    protected class HttpServiceListener implements ConnectionServiceListener {

		@Override
		public void onServiceComplete(String response) {
			
			 if (response.equals(AppConstants.INTERNET_CONNECTION_ERROR)) {
					Utils.showAlertMessage(AppConstants.INTERNET_CONNECTION_ERROR, ITSEulaScreen.this);
					//Toast.makeText(ITSTruckerLogin.this,AppConstants.INTERNET_CONNECTION_ERROR ,Toast.LENGTH_LONG).show();
				}else if (response.equals(AppConstants.SERVER_CONNECTION_ERROR)) {
					Utils.showAlertMessage(AppConstants.SERVER_CONNECTION_ERROR, ITSEulaScreen.this);
					//Toast.makeText(ITSTruckerLogin.this,AppConstants.SERVER_CONNECTION_ERROR ,Toast.LENGTH_LONG).show();
				}else if (response.equals(AppConstants.PLAYSERVICES_NOTFOUND)) {
					//Utils.showAlertMessage(AppConstants.PLAYSERVICES_NOTFOUND, ITSEulaScreen.this);
					Utils.updateGoogleplay(ITSEulaScreen.this);
					//Toast.makeText(ITSTruckerLogin.this,AppConstants.SERVER_CONNECTION_ERROR ,Toast.LENGTH_LONG).show();
				}else {
					try {
						boolean result = XMLparser.parsexml(response);
						
						if (result) {
							if (XMLparser.accountverified.equals("true") && XMLparser.errorcode.equals("OKtoLogin")) {
								products=XMLparser.getproducts();
								Iterator<LoginProducts> iterator = products.iterator();
								while (iterator.hasNext()) {
									LoginProducts loginProductsResults=(LoginProducts)iterator.next();
									if (loginProductsResults.getName().equals("LOADPOST")&&loginProductsResults.getProductstatus().equals("Have")) {
										session.updateloadpostaccess();
										
									}
									if (loginProductsResults.getName().equals("LOADSEARCH")&&loginProductsResults.getProductstatus().equals("Have")) {
									
										session.updateloadsearhaccess();
									}
									//System.out.println(err.getName()+"   "+err.getProductstatus());
								}
								//clear user preference if the current logged-in user is not same as previous user.
								if (!itshandle.equals(session.getItsHandle())) {
									session.clearprofilevalues();
									db.dropFavoriteTable();
								}
								session.updatefirsttime(true);
								if (prefs.getInt("accessloadsearch", 0)!=0||prefs.getInt("accessloadpost", 0)!=0) {
									//System.out.println("AppConstants.INTEGRARTION_ID+++"+XMLparser.getintegrationId());
									if (session.isFirstTime()) {
										session.createLoginSession(itshandle, itsusername,itspassword, XMLparser.getintegrationId(),true);
									}else{
										session.createLoginSession(itshandle, itsusername,itspassword, XMLparser.getintegrationId(),false);
									}
									session.setpackageVersion(Utils.validatepackageversion(ITSEulaScreen.this));
									Intent i = new Intent(getApplicationContext(),ITSMainScreen.class);
									startActivity(i);
									finish();
								}else{
									Utils.showAlertMessage(AppConstants.PRODUCT_DENIED_LOGIN, ITSEulaScreen.this);
								}
							}else if (XMLparser.accountverified.equals("false") && XMLparser.errorcode.equals("TermsNotYetAccepted")) {
								//startEula(XMLparser.geterrormsg(),XMLparser.getcompanyID(),XMLparser.gethandleID());
								//Utils.showAlertMessage(errormsg, ITSTruckerLogin.this);	
							}else if (XMLparser.errorcode.equals("InActiveHandle")) {
								Utils.showErrorMessage(AppConstants.LOGOUTUSER_ERRORMSG, ITSEulaScreen.this);
								//Utils.showAlertMessage(AppConstants.LOGOUTUSER_ERRORMSG, ITSEulaScreen.this);
								//Utils.showAlertMessage(errormsg, ITSTruckerLogin.this);	
							}else{
								Utils.showErrorMessage(XMLparser.geterrormsg(), ITSEulaScreen.this);
								//Utils.showAlertMessage(XMLparser.geterrormsg(), ITSEulaScreen.this);	
								//startEula(errormsg,XMLparser.getcompanyID(),XMLparser.gethandleID());
							}
						}else{
							Utils.showAlertMessage(AppConstants.SERVER_CONNECTION_ERROR, ITSEulaScreen.this);
						}
						
						
					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				
				}
			}

		@Override
		public void onServiceComplete(String response, String reference) {
			// TODO Auto-generated method stub
			
		}
	}

    
    
    
    
    
    
    
    
    
    
    
    /*private class VerifyLogin extends AsyncTask<Void, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			  progDialog = new ProgressDialog(ITSEulaScreen.this);
			  progDialog.setMessage(AppConstants.LOGIN_PROGRESS);
			  progDialog.setCancelable(false); 
			  progDialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {
			try {
				//System.out.println(Utils.isInternetAvailable(ITSTruckerLogin.this));
				if (Utils.isInternetAvailable(ITSEulaScreen.this)) {
					// Log.i("IN BACKGROUND", "doInBackground");
					
					String url = AppConstants.LOGIN_TEST_URL;
					URL obj = new URL(url);
					HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
					ITSApplication app = ((ITSApplication) getApplicationContext());
					//ITSApplication app= new ITSApplication();
					if (app.certifcate()==null) {
						return AppConstants.SERVER_CONNECTION_ERROR;
					}
					
					con.setSSLSocketFactory(app.certifcate().getSocketFactory());
					con.setRequestMethod("POST");
					con.setRequestProperty("Accept-Encoding", "gzip,deflate");
					con.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
					con.setRequestProperty("SOAPAction","http://tempuri.org/IWebServiceAdmin/VerifyItsAccountHandleAndPasswordGetIdAndProducts");
					con.setRequestProperty("Host", "webservices.truckstop.com");
					con.setRequestProperty("Connection", "Keep-Alive");
					con.setRequestProperty("User-Agent","Apache-HttpClient/4.1.1 (java 1.5)\\");
					
		
					String envolpe = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"";
					String uri = " xmlns:tem=\"http://tempuri.org/\">";
					String body = envolpe
							+ uri
							+ "\n<soapenv:Header/>\n"
							+ "<soapenv:Body>\n"
							+ "<tem:VerifyItsAccountHandleAndPasswordGetIdAndProducts>\n"
							+ "<tem:authtoken>\n"
							+ "<tem:UserName>"+ AppConstants.USERNAME+ "</tem:UserName>\n"
							+ "<tem:Password>"+ AppConstants.PASSWORD+ "</tem:Password>\n"
							+ "<tem:IntegrationId>"+ AppConstants.INTEGRARTION_ID+ "</tem:IntegrationId>\n"
							+ "</tem:authtoken>\n"
							
							+ "<tem:itsAccount>"+ itsusername+ "</tem:itsAccount>\n"
							+ "<tem:handle>"+ itshandle+ "</tem:handle>\n"
							+ "<tem:password>"+ itspassword+ "</tem:password>\n"
					//	+ "<tem:password>luiulikj</tem:password>\n"
							+ "</tem:VerifyItsAccountHandleAndPasswordGetIdAndProducts>\n"
							+ "</soapenv:Body>\n" + "</soapenv:Envelope>";
				//System.out.println("BODY VALUE----" + body);
					con.setConnectTimeout(AppConstants.SOCKETTIMEOUT_ERROR);
					con.setDoOutput(true);
					DataOutputStream wr = new DataOutputStream(con.getOutputStream());
					wr.writeBytes(body);
					wr.flush();
					wr.close();
			 
					int responseCode = con.getResponseCode();
					
					
				
					if (responseCode!=200) {
						return AppConstants.SERVER_CONNECTION_ERROR;
					}else{
						
						
						InputStream stream = con.getInputStream();
						String contentEncoding = con.getHeaderField("Content-Encoding");
						
						if (contentEncoding != null&& contentEncoding.equalsIgnoreCase("gzip")) {
							final InputStream stream1 = new GZIPInputStream(stream);
							// System.out.println("i am here");
							BufferedReader reader = new BufferedReader(new InputStreamReader(stream1));
							String line;
							StringBuilder sbuilder = new StringBuilder();
							while ((line = reader.readLine()) != null) {
								sbuilder.append(line);
							}
							
							response_str="";
							response_str = sbuilder.toString();
							//System.out.println("in if statement\n      "+response_str);
							boolean result = XMLparser.parsexml(response_str);
							if (result) {
								return "success";
							}
						} else {
							BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
							String line;
							StringBuilder sbuilder = new StringBuilder();
							while ((line = reader.readLine()) != null) {
								sbuilder.append(line);
							}
							response_str="";
							response_str = sbuilder.toString();
					//	System.out.println("retval IN ELSE-----"+ response_str);
							boolean result = XMLparser.parsexml(response_str);
							if (result) {
								return "success";
							}
						}
					}
					
				}else {
					return AppConstants.INTERNET_CONNECTION_ERROR;

				}

			} catch(Exception e){
				return AppConstants.SERVER_CONNECTION_ERROR;
			}
			return AppConstants.SERVER_CONNECTION_ERROR;
		}
		@Override
		protected String doInBackground(Void... params) {
			try {
				//System.out.println(Utils.isInternetAvailable(ITSTruckerLogin.this));
				if (Utils.isInternetAvailable(ITSEulaScreen.this)) {
					// Log.i("IN BACKGROUND", "doInBackground");
					HttpPost post = new HttpPost(AppConstants.LOGIN_TEST_URL);
					//Log.i(TAG, AppConstants.LOGIN_TEST_URL);
					int timeoutConnection = 9000;
					HttpParams httpParameters = new BasicHttpParams();
					HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
					// Set the default socket timeout (SO_TIMEOUT) 
					// in milliseconds which is the timeout for waiting for data.
					int timeoutSocket = 5000;
					HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
					DefaultHttpClient client = new DefaultHttpClient(httpParameters);
					post.setHeader("Accept-Encoding", "gzip,deflate");
					post.setHeader("Content-Type", "text/xml;charset=UTF-8");
					post.setHeader("SOAPAction","http://tempuri.org/IWebServiceAdmin/VerifyItsAccountHandleAndPasswordGetIdAndProducts");
					post.setHeader("Host", "webservices.truckstop.com");
					post.setHeader("Connection", "Keep-Alive");
					post.setHeader("User-Agent","Apache-HttpClient/4.1.1 (java 1.5)\\");
		
					String envolpe = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"";
					String uri = " xmlns:tem=\"http://tempuri.org/\">";
					String body = envolpe
							+ uri
							+ "\n<soapenv:Header/>\n"
							+ "<soapenv:Body>\n"
							+ "<tem:VerifyItsAccountHandleAndPasswordGetIdAndProducts>\n"
							+ "<tem:authtoken>\n"
							+ "<tem:UserName>"+ AppConstants.USERNAME+ "</tem:UserName>\n"
							+ "<tem:Password>"+ AppConstants.PASSWORD+ "</tem:Password>\n"
							+ "<tem:IntegrationId>"+ AppConstants.INTEGRARTION_ID+ "</tem:IntegrationId>\n"
							+ "</tem:authtoken>\n"
							 itsusername= bundle.getString("ITSACCOUNT");
							 itshandle= bundle.getString("HANDLE");
							 itspassword= bundle.getString("PASSWORD");
							+ "<tem:itsAccount>"+ itsusername+ "</tem:itsAccount>\n"
							+ "<tem:handle>"+ itshandle+ "</tem:handle>\n"
							+ "<tem:password>"+ itspassword+ "</tem:password>\n"
					//	+ "<tem:password>luiulikj</tem:password>\n"
							+ "</tem:VerifyItsAccountHandleAndPasswordGetIdAndProducts>\n"
							+ "</soapenv:Body>\n" + "</soapenv:Envelope>";
				System.out.println("BODY VALUE----" + body);
					post.setEntity(new StringEntity(body));
					HttpResponse response = client.execute(post);
					StatusLine statuscode = response.getStatusLine();
					String statuscodes = Integer.toString(statuscode.getStatusCode());
				
					if (!statuscodes.equals("200")) {
						return AppConstants.SERVER_CONNECTION_ERROR;
					}else{
						InputStream stream = response.getEntity().getContent();
						Header contentEncoding = response.getFirstHeader("Content-Encoding");
						if (contentEncoding != null&& contentEncoding.getValue().equalsIgnoreCase("gzip")) {
							final InputStream stream1 = new GZIPInputStream(stream);
							// System.out.println("i am here");
							BufferedReader reader = new BufferedReader(new InputStreamReader(stream1));
							String line;
							StringBuilder sbuilder = new StringBuilder();
							while ((line = reader.readLine()) != null) {
								sbuilder.append(line);
							}
							
							response_str="";
							response_str = sbuilder.toString();
							System.out.println("in if statement\n      "+response_str);
							boolean result = XMLparser.parsexml(response_str);
							if (result) {
								return "success";
							}
						} else {
							BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
							String line;
							StringBuilder sbuilder = new StringBuilder();
							while ((line = reader.readLine()) != null) {
								sbuilder.append(line);
							}
							response_str="";
							response_str = sbuilder.toString();
							//System.out.println("retval IN ELSE-----"+ response_str);
							boolean result = XMLparser.parsexml(response_str);
							if (result) {
								return "success";
							}
						}
					}
					
				}else {
					return AppConstants.INTERNET_CONNECTION_ERROR;

				}

			} catch (Exception e) {
				return AppConstants.SERVER_CONNECTION_ERROR;
			}
			return AppConstants.SERVER_CONNECTION_ERROR;
		}
		protected void onPostExecute(String result) {
			
			progDialog.dismiss();
			
		 if (result.equals(AppConstants.INTERNET_CONNECTION_ERROR)) {
				Utils.showAlertMessage(AppConstants.INTERNET_CONNECTION_ERROR, ITSEulaScreen.this);
				//Toast.makeText(ITSTruckerLogin.this,AppConstants.INTERNET_CONNECTION_ERROR ,Toast.LENGTH_LONG).show();
			}else if (result.equals(AppConstants.SERVER_CONNECTION_ERROR)) {
				Utils.showAlertMessage(AppConstants.SERVER_CONNECTION_ERROR, ITSEulaScreen.this);
				//Toast.makeText(ITSTruckerLogin.this,AppConstants.SERVER_CONNECTION_ERROR ,Toast.LENGTH_LONG).show();
			}else {
				
				if (XMLparser.accountverified.equals("true") && XMLparser.errorcode.equals("OKtoLogin")) {
					products=XMLparser.getproducts();
					Iterator<LoginProducts> iterator = products.iterator();
					while (iterator.hasNext()) {
						LoginProducts loginProductsResults=(LoginProducts)iterator.next();
						if (loginProductsResults.getName().equals("LOADPOST")&&loginProductsResults.getProductstatus().equals("Have")) {
							session.updateloadpostaccess();
							
						}
						if (loginProductsResults.getName().equals("LOADSEARCH")&&loginProductsResults.getProductstatus().equals("Have")) {
						
							session.updateloadsearhaccess();
						}
						//System.out.println(err.getName()+"   "+err.getProductstatus());
					}
					//clear user preference if the current logged-in user is not same as previous user.
					if (!itshandle.equals(session.getItsHandle())) {
						session.clearprofilevalues();
						db.dropFavoriteTable();
					}
					session.updatefirsttime(true);
					if (prefs.getInt("accessloadsearch", 0)!=0||prefs.getInt("accessloadpost", 0)!=0) {
						//System.out.println("AppConstants.INTEGRARTION_ID+++"+XMLparser.getintegrationId());
						if (session.isFirstTime()) {
							session.createLoginSession(itshandle, itsusername,itspassword, XMLparser.getintegrationId(),true);
						}else{
							session.createLoginSession(itshandle, itsusername,itspassword, XMLparser.getintegrationId(),false);
						}
						session.setpackageVersion(Utils.validatepackageversion(ITSEulaScreen.this));
						Intent i = new Intent(getApplicationContext(),ITSMainScreen.class);
						startActivity(i);
						finish();
					}else{
						Utils.showAlertMessage(AppConstants.PRODUCT_DENIED_LOGIN, ITSEulaScreen.this);
					}
				}else if (XMLparser.accountverified.equals("false") && XMLparser.errorcode.equals("TermsNotYetAccepted")) {
					//startEula(XMLparser.geterrormsg(),XMLparser.getcompanyID(),XMLparser.gethandleID());
					//Utils.showAlertMessage(errormsg, ITSTruckerLogin.this);	
				}else if (XMLparser.errorcode.equals("InActiveHandle")) {
					Utils.showErrorMessage(AppConstants.LOGOUTUSER_ERRORMSG, ITSEulaScreen.this);
					//Utils.showAlertMessage(AppConstants.LOGOUTUSER_ERRORMSG, ITSEulaScreen.this);
					//Utils.showAlertMessage(errormsg, ITSTruckerLogin.this);	
				}else{
					Utils.showErrorMessage(XMLparser.geterrormsg(), ITSEulaScreen.this);
					//Utils.showAlertMessage(XMLparser.geterrormsg(), ITSEulaScreen.this);	
					//startEula(errormsg,XMLparser.getcompanyID(),XMLparser.gethandleID());
				}
			}
		}
	}*/

}