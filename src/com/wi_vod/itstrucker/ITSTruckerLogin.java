package com.wi_vod.itstrucker;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.wi_vod.itstrucker.SearchMainActivity.ErrorDialogFragment;
import com.wi_vod.itstrucker.model.DatabaseHandler;
import com.wi_vod.itstrucker.model.LoginProducts;
import com.wi_vod.itstrucker.model.XMLparser;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.xmlpull.v1.XmlPullParserException;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.cert.CertPath;
import java.security.cert.CertPathValidatorException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.zip.GZIPInputStream;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.TrustManagerFactory;

public class ITSTruckerLogin extends FragmentActivity implements 
GoogleApiClient.ConnectionCallbacks,
GoogleApiClient.OnConnectionFailedListener {
	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {

		View v = getCurrentFocus();
		boolean ret = super.dispatchTouchEvent(event);

		if (v instanceof EditText) {
			View w = getCurrentFocus();
			int scrcoords[] = new int[2];
			w.getLocationOnScreen(scrcoords);
			float x = event.getRawX() + w.getLeft() - scrcoords[0];
			float y = event.getRawY() + w.getTop() - scrcoords[1];

			if (event.getAction() == MotionEvent.ACTION_UP
					&& (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w
							.getBottom())) {

				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
						.getWindowToken(), 0);
			}
		}
		return ret;
	}
	public static String TAG="ITSTruckerLogin";
	private EditText handle, username, password;
	private TextView locationtext;
	private Button loginbtn;
	private SessionManager session;
	String iTSUsername = "", iTSHandle = "";
	ProgressDialog progDialog;
	String retval, ret, response_str=null;
	StringBuilder builder = new StringBuilder();
	String itshandle, itsusername, itspassword;
	private SharedPreferences prefs;
	public static final String EMPTY_STRING = new String();
	List<LoginProducts> products = new ArrayList<LoginProducts>();
	boolean loginSuccess=false;
	private String errormsg;
	//private VerifyLogin task=null;
	DatabaseHandler db;
	 private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
	 public static boolean googleplayexists;
	//prevents orientation change from reloading results and crashing app
    @Override 
    public void onConfigurationChanged(Configuration newConfig) { 
      
      super.onConfigurationChanged(newConfig); 
    } 
    
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.itstrucker_login);
		UpdateUI();
		db = new DatabaseHandler(this);
		session = new SessionManager(ITSTruckerLogin.this);
		prefs = getSharedPreferences("itstrucker", 0);
		handle.setText(session.getItsHandle());
		username.setText(session.getItsAccountNo());
		
		googleplayexists=checkPlayServices();
		
		// Create a new global location parameters object
	
			// Display the current location in the UI
		//	locationtext.setText(getLatLng(this, location));
		}
	
	

	private void UpdateUI() {
		handle = (EditText) findViewById(R.id.editHandle);
		username = (EditText) findViewById(R.id.editUserName);
		password = (EditText) findViewById(R.id.editPassword);
		loginbtn = (Button) findViewById(R.id.bLogin);
		password.setOnEditorActionListener(new OnEditorActionListener() {
		    

			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				  boolean handled = false;
				// TODO Auto-generated method stub
				 if (actionId == EditorInfo. IME_ACTION_GO) {
					 InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
			            if(googleplayexists){
			        		//Utils.updateGoogleplay(ITSTruckerLogin.this);
			        		sendloginRequest();
			        		}else{
			        			updateGoogleplay();
			        			//Toast.makeText(getApplicationContext(), AppConstants.PLAYSERVICES_NOTFOUND, Toast.LENGTH_LONG).show();
			        		}
					
			            handled = true;
			        }
			        return handled;
			}
		});
	}

	public void signin(View v) {
	if(googleplayexists){
		//Utils.updateGoogleplay(ITSTruckerLogin.this);
		sendloginRequest();
		}else{
			updateGoogleplay();
			//Toast.makeText(getApplicationContext(), AppConstants.PLAYSERVICES_NOTFOUND, Toast.LENGTH_LONG).show();
		}
		
		
		//task.execute();
	}

	
	public void sendloginRequest(){
		itshandle = handle.getText().toString();
		itsusername = username.getText().toString();
		itspassword = password.getText().toString();
		if (itshandle.equals("")&&	itsusername.equals("")&&itspassword.equals("") ) {
			Utils.showAlertMessage(AppConstants.LOGIN_CREDENTIALSREQUIRED, ITSTruckerLogin.this);	
		}else{
			//System.out.println(itshandle+" "+itsusername+"  "+itspassword);
			
			errormsg="";
			products.clear();
			//new VerifyLogin().execute();
			
			String envolpe = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"";
			String uri = " xmlns:tem=\"http://tempuri.org/\">";
			String body = envolpe
					+ uri
					+ "\n<soapenv:Header/>\n"
					+ "<soapenv:Body>\n"
					+ "<tem:VerifyItsAccountHandleAndPasswordGetIdAndProducts>\n"
					+ "<tem:authtoken>\n"
					+ "<tem:UserName>"+ AppConstants.USERNAME+ "</tem:UserName>\n"
					+ "<tem:Password>"+ AppConstants.PASSWORD+ "</tem:Password>\n"
					+ "<tem:IntegrationId>"+ AppConstants.INTEGRARTION_ID+ "</tem:IntegrationId>\n"
					+ "</tem:authtoken>\n"
					+ "<tem:itsAccount>"+itsusername.trim()+ "</tem:itsAccount>\n"
					+ "<tem:handle>"+ itshandle.trim()+ "</tem:handle>\n"
					+ "<tem:password>"+ itspassword+ "</tem:password>\n"
			//	+ "<tem:password>luiulikj</tem:password>\n"
					+ "</tem:VerifyItsAccountHandleAndPasswordGetIdAndProducts>\n"
					+ "</soapenv:Body>\n" + "</soapenv:Envelope>";
			String SOAPACTION="http://tempuri.org/IWebServiceAdmin/VerifyItsAccountHandleAndPasswordGetIdAndProducts";
			
			HttpServiceListener listener = new HttpServiceListener();
			WebServiceConnectionService serv = new WebServiceConnectionService(ITSTruckerLogin.this,body,
					SOAPACTION, listener,AppConstants.LOGIN_PROGRESS);
			serv.execute(AppConstants.LOGIN_TEST_URL);
			
		}
	}
	
	
	protected class HttpServiceListener implements ConnectionServiceListener {

		@Override
		public void onServiceComplete(String response) {
			
			 if (response.equals(AppConstants.INTERNET_CONNECTION_ERROR)) {
					Utils.showAlertMessage(AppConstants.INTERNET_CONNECTION_ERROR, ITSTruckerLogin.this);
					//Toast.makeText(ITSTruckerLogin.this,AppConstants.INTERNET_CONNECTION_ERROR ,Toast.LENGTH_LONG).show();
				}else if (response.equals(AppConstants.SERVER_CONNECTION_ERROR)) {
					Utils.showAlertMessage(AppConstants.SERVER_CONNECTION_ERROR, ITSTruckerLogin.this);
					//Toast.makeText(ITSTruckerLogin.this,AppConstants.SERVER_CONNECTION_ERROR ,Toast.LENGTH_LONG).show();
				}else if (response.equals(AppConstants.PLAYSERVICES_NOTFOUND)) {
		               // Utils.showErrorMessage(AppConstants.PLAYSERVICES_NOTFOUND,SearchDetailActivity.this);
					Utils.updateGoogleplay(ITSTruckerLogin.this);
	               
	            }else {
					try {
						boolean result = XMLparser.parsexml(response);
						
						if (result) {
							if (XMLparser.accountverified.equals("true") && XMLparser.errorcode.equals("OKtoLogin")) {
								products=XMLparser.getproducts();
								Iterator<LoginProducts> iterator = products.iterator();
								while (iterator.hasNext()) {
									LoginProducts loginProductsResults=(LoginProducts)iterator.next();
									if (loginProductsResults.getName().equals("TRUCKPOST")&&loginProductsResults.getProductstatus().equals("Have")) {
										session.updateloadpostaccess();
										
									}
									if (loginProductsResults.getName().equals("LOADSEARCH")&&loginProductsResults.getProductstatus().equals("Have")) {
									
										session.updateloadsearhaccess();
									}
									//System.out.println(err.getName()+"   "+err.getProductstatus());
								}
								//clear user preference if the current logged in user is not same as previous user.
								if (!itshandle.equals(session.getItsHandle())) {
									session.clearprofilevalues();
									db.dropFavoriteTable();
								}
								
								
								session.updatefirsttime(true);
								if (prefs.getInt("accessloadsearch", 0)!=0||prefs.getInt("accessloadpost", 0)!=0) {
									//System.out.println("AppConstants.INTEGRARTION_ID+++"+XMLparser.getintegrationId());
									if (session.isFirstTime()) {
										session.createLoginSession(itshandle, itsusername,itspassword, XMLparser.getintegrationId(),true);
									}else{
										session.createLoginSession(itshandle, itsusername,itspassword, XMLparser.getintegrationId(),false);
									}
									
									//send button click event if login is successful
									 Tracker t = ((ITSApplication) getApplication()).getTracker(ITSApplication.TrackerName.APP_TRACKER);
										
						                t.send(new HitBuilders.EventBuilder().setCategory(getString( R.string.UICategory))
						                        .setAction(getString( R.string.UIAction)).setLabel("Login Button").build());
									
									session.setpackageVersion(Utils.validatepackageversion(ITSTruckerLogin.this));
									
									
									Intent i = new Intent(getApplicationContext(),ITSMainScreen.class);
									startActivity(i);
									finish();
								}else{
									Utils.showAlertMessage(AppConstants.PRODUCT_DENIED_LOGIN, ITSTruckerLogin.this);
								}
							}else if (XMLparser.accountverified.equals("false") && XMLparser.errorcode.equals("TermsNotYetAccepted")) {
								startEula(XMLparser.geterrormsg(),XMLparser.getcompanyID(),XMLparser.gethandleID());
								//Utils.showAlertMessage(errormsg, ITSTruckerLogin.this);	
							}else if (XMLparser.accountverified.equals("true") && XMLparser.errorcode.equals("InActiveHandle")) {
								Utils.showAlertMessage(AppConstants.LOGOUTUSER_ERRORMSG, ITSTruckerLogin.this);
								//Utils.showAlertMessage(errormsg, ITSTruckerLogin.this);	
							}else{
								Utils.showAlertMessage(XMLparser.geterrormsg(), ITSTruckerLogin.this);	
								//startEula(errormsg,XMLparser.getcompanyID(),XMLparser.gethandleID());
							}
						}else{
							Utils.showAlertMessage(AppConstants.SERVER_CONNECTION_ERROR, ITSTruckerLogin.this);
						}
						
						
					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				
				}
			}

		@Override
		public void onServiceComplete(String response, String reference) {
			// TODO Auto-generated method stub
			
		}
		}

	
	

	/*private class VerifyLogin extends AsyncTask<Void, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			  progDialog = new ProgressDialog(ITSTruckerLogin.this);
			  progDialog.setMessage(AppConstants.LOGIN_PROGRESS);
			  progDialog.setCancelable(false); 
			  progDialog.show();xZ
		}

		@Override
		protected String doInBackground(Void... params) {
			try {
				//System.out.println(Utils.isInternetAvailable(ITSTruckerLogin.this));
				if (Utils.isInternetAvailable(ITSTruckerLogin.this)) {
					// Log.i("IN BACKGROUND", "doInBackground");
					
					String url = AppConstants.LOGIN_TEST_URL;
					URL obj = new URL(url);
					HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
					ITSApplication app = ((ITSApplication) getApplicationContext());
					//ITSApplication app= new ITSApplication();
					if (app.certifcate()==null) {
						return AppConstants.SERVER_CONNECTION_ERROR;
					}
					
					con.setSSLSocketFactory(app.certifcate().getSocketFactory());
					con.setRequestMethod("POST");
					con.setRequestProperty("Accept-Encoding", "gzip,deflate");
					con.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
					con.setRequestProperty("SOAPAction","http://tempuri.org/IWebServiceAdmin/VerifyItsAccountHandleAndPasswordGetIdAndProducts");
					con.setRequestProperty("Host", "webservices.truckstop.com");
					con.setRequestProperty("Connection", "Keep-Alive");
					con.setRequestProperty("User-Agent","Apache-HttpClient/4.1.1 (java 1.5)\\");
					
					//HttpPost post = new HttpPost(AppConstants.LOGIN_TEST_URL);
					//Log.i(TAG, AppConstants.LOGIN_TEST_URL);
					int timeoutConnection = 9000;
					HttpParams httpParameters = new BasicHttpParams();
					HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
					// Set the default socket timeout (SO_TIMEOUT) 
					// in milliseconds which is the timeout for waiting for data.
					int timeoutSocket = 5000;
					HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
					DefaultHttpClient client = new DefaultHttpClient(httpParameters);
					post.setHeader("Accept-Encoding", "gzip,deflate");
					post.setHeader("Content-Type", "text/xml;charset=UTF-8");
					post.setHeader("SOAPAction","http://tempuri.org/IWebServiceAdmin/VerifyItsAccountHandleAndPasswordGetIdAndProducts");
					post.setHeader("Host", "webservices.truckstop.com");
					post.setHeader("Connection", "Keep-Alive");
					post.setHeader("User-Agent","Apache-HttpClient/4.1.1 (java 1.5)\\");
		
					String envolpe = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"";
					String uri = " xmlns:tem=\"http://tempuri.org/\">";
					String body = envolpe
							+ uri
							+ "\n<soapenv:Header/>\n"
							+ "<soapenv:Body>\n"
							+ "<tem:VerifyItsAccountHandleAndPasswordGetIdAndProducts>\n"
							+ "<tem:authtoken>\n"
							+ "<tem:UserName>"+ AppConstants.USERNAME+ "</tem:UserName>\n"
							+ "<tem:Password>"+ AppConstants.PASSWORD+ "</tem:Password>\n"
							+ "<tem:IntegrationId>"+ AppConstants.INTEGRARTION_ID+ "</tem:IntegrationId>\n"
							+ "</tem:authtoken>\n"
							+ "<tem:itsAccount>"+ itsusername+ "</tem:itsAccount>\n"
							+ "<tem:handle>"+ itshandle+ "</tem:handle>\n"
							+ "<tem:password>"+ itspassword+ "</tem:password>\n"
					//	+ "<tem:password>luiulikj</tem:password>\n"
							+ "</tem:VerifyItsAccountHandleAndPasswordGetIdAndProducts>\n"
							+ "</soapenv:Body>\n" + "</soapenv:Envelope>";
				//System.out.println("BODY VALUE----" + body);
					con.setConnectTimeout(AppConstants.SOCKETTIMEOUT_ERROR);
					con.setDoOutput(true);
					DataOutputStream wr = new DataOutputStream(con.getOutputStream());
					wr.writeBytes(body);
					wr.flush();
					wr.close();
			 
					int responseCode = con.getResponseCode();
					
					
					
					con.setsetEntity(new StringEntity(body));
					HttpResponse response = client.execute(con);
					StatusLine statuscode = response.getStatusLine();
					String statuscodes = Integer.toString(statuscode.getStatusCode());
				
					if (responseCode!=200) {
						return AppConstants.SERVER_CONNECTION_ERROR;
					}else{
						InputStream stream = response.getEntity().getContent();
						Header contentEncoding = response.getFirstHeader("Content-Encoding");
						
						
						InputStream stream = con.getInputStream();
						String contentEncoding = con.getHeaderField("Content-Encoding");
						
						//Header contentEncoding = con.getHeaderField("Content-Encoding").toString();
						if (contentEncoding != null&& contentEncoding.equalsIgnoreCase("gzip")) {
							final InputStream stream1 = new GZIPInputStream(stream);
							// System.out.println("i am here");
							BufferedReader reader = new BufferedReader(new InputStreamReader(stream1));
							String line;
							StringBuilder sbuilder = new StringBuilder();
							while ((line = reader.readLine()) != null) {
								sbuilder.append(line);
							}
							
							response_str="";
							response_str = sbuilder.toString();
							//System.out.println("in if statement\n      "+response_str);
							boolean result = XMLparser.parsexml(response_str);
							if (result) {
								return "success";
							}
						} else {
							BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
							String line;
							StringBuilder sbuilder = new StringBuilder();
							while ((line = reader.readLine()) != null) {
								sbuilder.append(line);
							}
							response_str="";
							response_str = sbuilder.toString();
					//	System.out.println("retval IN ELSE-----"+ response_str);
							boolean result = XMLparser.parsexml(response_str);
							if (result) {
								return "success";
							}
						}
					}
					
				}else {
					return AppConstants.INTERNET_CONNECTION_ERROR;

				}

			} catch(Exception e){
				return AppConstants.SERVER_CONNECTION_ERROR;
			}
			return AppConstants.SERVER_CONNECTION_ERROR;
		}
		
		
		@Override
		protected String doInBackground(Void... params) {
			try {
				//System.out.println(Utils.isInternetAvailable(ITSTruckerLogin.this));
				if (Utils.isInternetAvailable(ITSTruckerLogin.this)) {
					// Log.i("IN BACKGROUND", "doInBackground");
					HttpPost post = new HttpPost(AppConstants.LOGIN_TEST_URL);
					//Log.i(TAG, AppConstants.LOGIN_TEST_URL);
					int timeoutConnection = 9000;
					HttpParams httpParameters = new BasicHttpParams();
					HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
					// Set the default socket timeout (SO_TIMEOUT) 
					// in milliseconds which is the timeout for waiting for data.
					int timeoutSocket = 5000;
					HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
					DefaultHttpClient client = new DefaultHttpClient(httpParameters);
					post.setHeader("Accept-Encoding", "gzip,deflate");
					post.setHeader("Content-Type", "text/xml;charset=UTF-8");
					post.setHeader("SOAPAction","http://tempuri.org/IWebServiceAdmin/VerifyItsAccountHandleAndPasswordGetIdAndProducts");
					post.setHeader("Host", "webservices.truckstop.com");
					post.setHeader("Connection", "Keep-Alive");
					post.setHeader("User-Agent","Apache-HttpClient/4.1.1 (java 1.5)\\");
		
					String envolpe = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"";
					String uri = " xmlns:tem=\"http://tempuri.org/\">";
					String body = envolpe
							+ uri
							+ "\n<soapenv:Header/>\n"
							+ "<soapenv:Body>\n"
							+ "<tem:VerifyItsAccountHandleAndPasswordGetIdAndProducts>\n"
							+ "<tem:authtoken>\n"
							+ "<tem:UserName>"+ AppConstants.USERNAME+ "</tem:UserName>\n"
							+ "<tem:Password>"+ AppConstants.PASSWORD+ "</tem:Password>\n"
							+ "<tem:IntegrationId>"+ AppConstants.INTEGRARTION_ID+ "</tem:IntegrationId>\n"
							+ "</tem:authtoken>\n"
							+ "<tem:itsAccount>"+ itsusername+ "</tem:itsAccount>\n"
							+ "<tem:handle>"+ itshandle+ "</tem:handle>\n"
							+ "<tem:password>"+ itspassword+ "</tem:password>\n"
					//	+ "<tem:password>luiulikj</tem:password>\n"
							+ "</tem:VerifyItsAccountHandleAndPasswordGetIdAndProducts>\n"
							+ "</soapenv:Body>\n" + "</soapenv:Envelope>";
				System.out.println("BODY VALUE----" + body);
					post.setEntity(new StringEntity(body));
					HttpResponse response = client.execute(post);
					StatusLine statuscode = response.getStatusLine();
					String statuscodes = Integer.toString(statuscode.getStatusCode());
				
					if (!statuscodes.equals("200")) {
						return AppConstants.SERVER_CONNECTION_ERROR;
					}else{
						InputStream stream = response.getEntity().getContent();
						Header contentEncoding = response.getFirstHeader("Content-Encoding");
						if (contentEncoding != null&& contentEncoding.getValue().equalsIgnoreCase("gzip")) {
							final InputStream stream1 = new GZIPInputStream(stream);
							// System.out.println("i am here");
							BufferedReader reader = new BufferedReader(new InputStreamReader(stream1));
							String line;
							StringBuilder sbuilder = new StringBuilder();
							while ((line = reader.readLine()) != null) {
								sbuilder.append(line);
							}
							
							response_str="";
							response_str = sbuilder.toString();
							System.out.println("in if statement\n      "+response_str);
							boolean result = XMLparser.parsexml(response_str);
							if (result) {
								return "success";
							}
						} else {
							BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
							String line;
							StringBuilder sbuilder = new StringBuilder();
							while ((line = reader.readLine()) != null) {
								sbuilder.append(line);
							}
							response_str="";
							response_str = sbuilder.toString();
							//System.out.println("retval IN ELSE-----"+ response_str);
							boolean result = XMLparser.parsexml(response_str);
							if (result) {
								return "success";
							}
						}
					}
					
				}else {
					return AppConstants.INTERNET_CONNECTION_ERROR;

				}

			} catch (Exception e) {
				return AppConstants.SERVER_CONNECTION_ERROR;
			}
			return AppConstants.SERVER_CONNECTION_ERROR;
		}
		protected void onPostExecute(String result) {
		
			progDialog.dismiss();
			
		 if (result.equals(AppConstants.INTERNET_CONNECTION_ERROR)) {
				Utils.showAlertMessage(AppConstants.INTERNET_CONNECTION_ERROR, ITSTruckerLogin.this);
				//Toast.makeText(ITSTruckerLogin.this,AppConstants.INTERNET_CONNECTION_ERROR ,Toast.LENGTH_LONG).show();
			}else if (result.equals(AppConstants.SERVER_CONNECTION_ERROR)) {
				Utils.showAlertMessage(AppConstants.SERVER_CONNECTION_ERROR, ITSTruckerLogin.this);
				//Toast.makeText(ITSTruckerLogin.this,AppConstants.SERVER_CONNECTION_ERROR ,Toast.LENGTH_LONG).show();
			}else {
				
				if (XMLparser.accountverified.equals("true") && XMLparser.errorcode.equals("OKtoLogin")) {
					products=XMLparser.getproducts();
					Iterator<LoginProducts> iterator = products.iterator();
					while (iterator.hasNext()) {
						LoginProducts loginProductsResults=(LoginProducts)iterator.next();
						if (loginProductsResults.getName().equals("TRUCKPOST")&&loginProductsResults.getProductstatus().equals("Have")) {
							session.updateloadpostaccess();
							
						}
						if (loginProductsResults.getName().equals("LOADSEARCH")&&loginProductsResults.getProductstatus().equals("Have")) {
						
							session.updateloadsearhaccess();
						}
						//System.out.println(err.getName()+"   "+err.getProductstatus());
					}
					//clear user preference if the current logged in user is not same as previous user.
					if (!itshandle.equals(session.getItsHandle())) {
						session.clearprofilevalues();
						db.dropFavoriteTable();
					}
					
					
					session.updatefirsttime(true);
					if (prefs.getInt("accessloadsearch", 0)!=0||prefs.getInt("accessloadpost", 0)!=0) {
						//System.out.println("AppConstants.INTEGRARTION_ID+++"+XMLparser.getintegrationId());
						if (session.isFirstTime()) {
							session.createLoginSession(itshandle, itsusername,itspassword, XMLparser.getintegrationId(),true);
						}else{
							session.createLoginSession(itshandle, itsusername,itspassword, XMLparser.getintegrationId(),false);
						}
						
						//send button click event if login is successful
						 Tracker t = ((ITSApplication) getApplication()).getTracker(ITSApplication.TrackerName.APP_TRACKER);
							
			                t.send(new HitBuilders.EventBuilder().setCategory(getString( R.string.UICategory))
			                        .setAction(getString( R.string.UIAction)).setLabel("Login Button").build());
						
						session.setpackageVersion(Utils.validatepackageversion(ITSTruckerLogin.this));
						
						
						Intent i = new Intent(getApplicationContext(),ITSMainScreen.class);
						startActivity(i);
						finish();
					}else{
						Utils.showAlertMessage(AppConstants.PRODUCT_DENIED_LOGIN, ITSTruckerLogin.this);
					}
				}else if (XMLparser.accountverified.equals("false") && XMLparser.errorcode.equals("TermsNotYetAccepted")) {
					startEula(XMLparser.geterrormsg(),XMLparser.getcompanyID(),XMLparser.gethandleID());
					//Utils.showAlertMessage(errormsg, ITSTruckerLogin.this);	
				}else if (XMLparser.accountverified.equals("true") && XMLparser.errorcode.equals("InActiveHandle")) {
					Utils.showAlertMessage(AppConstants.LOGOUTUSER_ERRORMSG, ITSTruckerLogin.this);
					//Utils.showAlertMessage(errormsg, ITSTruckerLogin.this);	
				}else{
					Utils.showAlertMessage(XMLparser.geterrormsg(), ITSTruckerLogin.this);	
					//startEula(errormsg,XMLparser.getcompanyID(),XMLparser.gethandleID());
				}
			}
		}
	}*/

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		password.setText("");
		googleplayexists=checkPlayServices();
	}
	
	
	public   void startEula(String msg,final String companyid,final String handleid){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ITSTruckerLogin.this);
      
        // Setting Dialog Title
        alertDialog.setTitle("Error");
  
        // Setting Dialog Message
        alertDialog.setMessage(msg);
  
        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.delete);
  
        // On pressing Settings button
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
						Intent i = new Intent(getApplicationContext(),ITSEulaScreen.class);
						i.putExtra("COMPANYID", companyid);
						i.putExtra("HANDLEID", handleid);
						i.putExtra("ITSACCOUNT", itsusername);
						i.putExtra("HANDLE", itshandle);
						i.putExtra("PASSWORD", itspassword);
						
						startActivity(i);
						finish();
					}
        });
        // Showing Alert Message
        alertDialog.show();
    }

	
	 /**
     * Method to verify google play services on the device
     * */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
        /*    if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(getApplicationContext(),"This device is not supported.", Toast.LENGTH_LONG).show();
                finish();
            }*/
        	
        	updateGoogleplay();
            return false;
        }
        return true;
    }
	
	
	

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onConnected(Bundle connectionHint) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onConnectionSuspended(int cause) {
		// TODO Auto-generated method stub
		
	}	
	
	
	public void updateGoogleplay() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ITSTruckerLogin.this);
		// set title
		alertDialogBuilder.setTitle("Update Google Play Services");
		// set dialog message
		alertDialogBuilder
		.setMessage(AppConstants.PLAYSERVICES_NOTFOUND)
		.setCancelable(false)
		.setPositiveButton("Update",
		new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int id) {
		callMarketPlace();
		finish();
		 }
		});
		alertDialogBuilder.show();
		}
		public void callMarketPlace() {
		try {
		startActivityForResult(new Intent(Intent.ACTION_VIEW,
		Uri.parse("market://details?id="+ "com.google.android.gms")), 1);
		}
		 catch (android.content.ActivityNotFoundException anfe) {
		startActivityForResult(new Intent(Intent.ACTION_VIEW,
		Uri.parse("https://play.google.com/store/apps/details?id="+ "com.google.android.gms")), 1);
		  }
		 }

}
