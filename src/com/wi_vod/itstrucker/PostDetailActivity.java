/******************************************************************
 * SearchDetail.java
 * 
 * Description: Class for the Search Detail Activity
 * 
 * @author: Wayne Y
 * 
 * @version: 1.0
 * 
 * History:
 * 
 * 
 ******************************************************************
 */
package com.wi_vod.itstrucker;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.zip.GZIPInputStream;

import javax.net.ssl.HttpsURLConnection;

import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.xmlpull.v1.XmlPullParserException;

import android.R.color;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SimpleAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.BufferType;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.wi_vod.itstrucker.R;
import com.wi_vod.itstrucker.PostActivity.HttpServiceListener;
import com.wi_vod.itstrucker.model.CurrentTruckPostsModel;
import com.wi_vod.itstrucker.model.PostDetailLoadModel;
import com.wi_vod.itstrucker.model.PostLoadDetailParser;
import com.wi_vod.itstrucker.model.SearchDetailLoadModel;
import com.wi_vod.itstrucker.model.XMLSearchLoadDetailParser;

public class PostDetailActivity extends ActionBarActivity {

	private static String TAG = "PostDetailActivity";
	private ProgressDialog progDialog;
	String retval, ret, response_str;
	StringBuilder builder = new StringBuilder();
	private String postLoadId, errormsg, poastloadsize;
	private TextView loaddetail;
	ArrayList<PostDetailLoadModel> detailitems = new ArrayList<PostDetailLoadModel>();
	PostDetailLoadModel items;
	private SessionManager session;
	private Tracker t;
	private BroadcastReceiver mRegistrationBroadcastReceiver;

	TextView pdetailcompnayname, pdetailhandle, pdetailcompanyhandle,
			pdetailcompanycontact, pdetailcompanyphone, pdetailcompanyemail,
			pdetailstartingPoint, pdetailstartingPointdate,
			pdetaildestinationPoint, pdetaildestinationPointdate,
			pdetaildestinationPointtime, pdetailloadDetails,
			pdetailspecialInfo, pdetailtraileroptions, pdetailloadsize,
			pdetailloadweight, pdetailloadlength, pdetailloadwidth,
			pdetailloaddistance, pdetailloadextrastops, pdetailEquipmentType,
			pdetailloadquantity, pdetailPostedDate;
	TableLayout loadtable;
	TableRow tablerowtrailer;
	Bundle bundle;

	/*********************************************************
	 * Method: onCreate
	 * 
	 * Description: Called when the Activity is first created
	 * 
	 * @param: Bundle savedInstanceState - Current State
	 * 
	 * @returns: Void
	 * 
	 */

	// prevents orientation change from reloading results and crashing app
	@Override
	public void onConfigurationChanged(Configuration newConfig) {

		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Bundle extras;
		Intent intent = getIntent();
		String urlString;

		// Set up the layout for the activity
		super.onCreate(savedInstanceState);
		setContentView(R.layout.postdetailscreen);
		UpdateUI();
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		session = new SessionManager(PostDetailActivity.this);

		t = ((ITSApplication) getApplication()).getTracker(ITSApplication.TrackerName.APP_TRACKER);
		t.setScreenName("Truck Details Screen");
		t.send(new HitBuilders.AppViewBuilder().build());
		// Create a dialog for a process spinner

		bundle = getIntent().getExtras();
		if (bundle != null) {
			postLoadId = bundle.getString("POSTLOADID");
			poastloadsize = bundle.getString("LOADSIZE");
			session.postLoadId(postLoadId);
		}
		// System.out.println("LoadId-------------->"+postLoadId);

		/*
		 * PostTruckDetail postDetails=new PostTruckDetail();
		 * postDetails.execute();
		 */

		PostTruckDetailrequest();
		mRegistrationBroadcastReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Utils.logoutuser(AppConstants.LOGOUTUSER_ERRORMSG,PostDetailActivity.this);
			}
		};
		// Create the URL command string
	}

	private void UpdateUI() {
		pdetailcompnayname = (TextView) findViewById(R.id.pdcompanyname);
		// companyhandle = (TextView) findViewById(R.id.companyhandle);
		pdetailcompanycontact = (TextView) findViewById(R.id.pdcompanycontact);
		pdetailcompanyphone = (TextView) findViewById(R.id.pdcompanyPhone);
		pdetailcompanyemail = (TextView) findViewById(R.id.pdcompanyEmail);
		pdetailstartingPoint = (TextView) findViewById(R.id.pdstartingpoint);
		pdetailstartingPointdate = (TextView) findViewById(R.id.pdstartingpointdate);
		pdetaildestinationPoint = (TextView) findViewById(R.id.pddestinationpoint);
		// pdetaildestinationPointdate = (TextView)
		// findViewById(R.id.pddestinationpointdate);
		// pdetaildestinationPointtime= (TextView)
		// findViewById(R.id.destinationpointtime);
		// loadDetails = (TextView) findViewById(R.id.loaddetails);
		pdetailspecialInfo = (TextView) findViewById(R.id.pdspecialinfo);
		pdetailtraileroptions = (TextView) findViewById(R.id.pdloadtraileroptions);
		loadtable = (TableLayout) findViewById(R.id.pdloadtableLayout);
		tablerowtrailer = (TableRow) findViewById(R.id.tableRow4);
		pdetailloadweight = (TextView) findViewById(R.id.pdloadweight);
		pdetailloadlength = (TextView) findViewById(R.id.pdloadlength);
		pdetailloadwidth = (TextView) findViewById(R.id.pdloadwidth);
		pdetailEquipmentType = (TextView) findViewById(R.id.pdloadequipmenttype);
		pdetailloadquantity = (TextView) findViewById(R.id.pdloadquantity);
		pdetailPostedDate = (TextView) findViewById(R.id.pdloaddateposted);
		pdetailhandle = (TextView) findViewById(R.id.pdhandle);
		pdetailloadsize = (TextView) findViewById(R.id.pdloadsize);

	}

	private void PostTruckDetailrequest() {

		try {
			// TODO Auto-generated method stub
			String envolpe = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\""
					+ " xmlns:v11=\"http://webservices.truckstop.com/v11\""
					+ " xmlns:web=\"http://schemas.datacontract.org/2004/07/WebServices\""
					+ " xmlns:web1=\"http://schemas.datacontract.org/2004/07/WebServices.Objects\""
					+ ">";
			String body = envolpe + "\n<soapenv:Header/>\n"
					+ "<soapenv:Body>\n" + "<v11:GetTruckDetailResults>\n"
					+ "<v11:detailRequest>\n" + "<web:IntegrationId>"
					+ session.getUserIntegrationID() + "</web:IntegrationId>\n"
					+ "<web:Password>" + AppConstants.PASSWORD
					+ "</web:Password>\n"
					+ "<web:UserName>"
					+ AppConstants.USERNAME
					+ "</web:UserName>\n"
					// + "<web1:TruckId>"+46955822+"</web1:TruckId>\n"
					+ "<web1:TruckId>" + postLoadId + "</web1:TruckId>\n"
					+ "</v11:detailRequest>\n"
					+ "</v11:GetTruckDetailResults>\n" + "</soapenv:Body>\n"
					+ "</soapenv:Envelope>\n";

			// System.out.println("BODY VALUE----" + body);
			String SOAPACTION = "http://webservices.truckstop.com/v11/ITruckPosting/GetTruckDetailResults";

			HttpServiceListener listener = new HttpServiceListener();
			WebServiceConnectionService serv = new WebServiceConnectionService(PostDetailActivity.this, body, SOAPACTION, listener,
					AppConstants.POSTDETAIL_PROGRESS);
			serv.execute(AppConstants.GETTRUCKS_TEST_URL);
		} catch (Exception e) {
			Utils.showAlertMessage("Unable to Process Request",PostDetailActivity.this);
		}

	}

	protected class HttpServiceListener implements ConnectionServiceListener {

		@Override
		public void onServiceComplete(String response) {
			if (response.equals(AppConstants.INTERNET_CONNECTION_ERROR)) {
				Utils.showErrorMessage(AppConstants.INTERNET_CONNECTION_ERROR,PostDetailActivity.this);
				/*
				 * Toast.makeText(PostDetailActivity.this,AppConstants.
				 * INTERNET_CONNECTION_ERROR,Toast.LENGTH_LONG).show();
				 * finish();
				 */
			} else if (response.equals(AppConstants.SERVER_CONNECTION_ERROR)) {
				Utils.showErrorMessage(AppConstants.SERVER_CONNECTION_ERROR,PostDetailActivity.this);
				/*
				 * Toast.makeText(PostDetailActivity.this,AppConstants.
				 * SERVER_CONNECTION_ERROR, Toast.LENGTH_LONG).show(); finish();
				 */
			} else if (response.equals(AppConstants.PLAYSERVICES_NOTFOUND)) {
                //Utils.showErrorMessage(AppConstants.PLAYSERVICES_NOTFOUND,PostDetailActivity.this);
				Utils.updateGoogleplay(PostDetailActivity.this);
                
            }else {

				boolean Postingdetailsucess = false;
				try {
					Postingdetailsucess = PostLoadDetailParser.parsexml(response);
					if (Postingdetailsucess) {
						items = PostLoadDetailParser.getpostdetailItems();

						// Log.i(TAG,
						// Utils.checkIsNull(items.getPd_loadid())+" ++++  "+Utils.checkIsNull(items.getPd_hasFileCheck()));
						pdetailcompnayname.setText(items.getPd_truckCompanyName());
						// compnayname.setText(items.getTruckCompanyName());
						// companycontact.setText(items.getPointOfContact());

						SpannableString ss4 = new SpannableString("Handle:");
						ss4.setSpan(new StyleSpan(Typeface.BOLD), 0,ss4.length(), 0);
						ss4.setSpan(new ForegroundColorSpan(Color.WHITE), 0,ss4.length(),Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
						pdetailcompanycontact.append(ss4);
						pdetailcompanycontact.append(" "+ session.getItsHandle());

						SpannableString ss1 = new SpannableString("\nContact:");
						ss1.setSpan(new StyleSpan(Typeface.BOLD), 0,ss1.length(), 0);
						ss1.setSpan(new ForegroundColorSpan(Color.WHITE), 0,ss1.length(),
								Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
						pdetailcompanycontact.append(ss1);
						pdetailcompanycontact.append(" "+ Utils.checkIsNull(items.getPd_pointOfContact()));

						SpannableString ss2 = new SpannableString("\nPhone:");
						ss2.setSpan(new StyleSpan(Typeface.BOLD), 0,ss2.length(), 0);
						ss2.setSpan(new ForegroundColorSpan(Color.WHITE), 0,ss2.length(),Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
						pdetailcompanyphone.append(ss2);
						pdetailcompanyphone.append(" "+ Utils.checkIsNull(items.getPd_pointOfContactPhone()));

						SpannableString ss3 = new SpannableString("\nEmail:");
						ss3.setSpan(new StyleSpan(Typeface.BOLD), 0,ss3.length(), 0);
						ss3.setSpan(new ForegroundColorSpan(Color.WHITE), 0,ss3.length(),Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
						pdetailcompanyemail.append(ss3);
						pdetailcompanyemail.append(" "+ Utils.checkIsNull(items.getPd_truckCompanyEmail()));

						String originpoint = Utils.formatString(Utils
								.checkIsNull(items.getPd_originCity()+ ","+ Utils.checkIsNull(items.getPd_originState())));
						pdetailstartingPoint.setText(originpoint);

						pdetailstartingPointdate.setText(Utils.checkIsNull((items.getPd_dateTruckAvailable())));
						String destinationpoint = Utils.formatString(Utils.checkIsNull(items.getPd_destinationCity()
										+ ","+ Utils.checkIsNull(items.getPd_destinationState())));

						pdetaildestinationPoint.setText(destinationpoint);
						// pdetaildestinationPointdate.setText(Utils.checkIsNull(items.getPd_dateTruckAvailable()));
						pdetailspecialInfo.setText(Utils.checkIsNull(items.getPd_specInfo()));
						pdetailtraileroptions.setText(Utils.checkIsNull(items.getTrailerOptionType()));

						if (!Utils.checkIsNull(items.getPd_weight()).equals("-")) {
							pdetailloadweight.setText(items.getPd_weight()+ getResources().getString(R.string.unit_lbs));
						} else {
							pdetailloadweight.setText(Utils.checkIsNull(items.getPd_weight()));
						}

						if (!Utils.checkIsNull(items.getPd_length()).equals("-")) {
							pdetailloadlength.setText(items.getPd_length()+ getResources().getString(R.string.unit_feet));
						} else {
							pdetailloadlength.setText(Utils.checkIsNull(items.getPd_length()));
						}

						if (!Utils.checkIsNull(items.getPd_width()).equals("-")) {
							pdetailloadwidth.setText(items.getPd_width()+ getResources().getString(R.string.unit_feet));
						} else {
							pdetailloadwidth.setText(Utils.checkIsNull(items.getPd_width()));
						}

						String newDateString = null;
						if (!Utils.checkIsNull(items.getEntered()).equals("-")) {
							newDateString = Utils.detailsDateFormat(items
									.getEntered());
							pdetailPostedDate.setText(newDateString);
						} else {
							pdetailPostedDate
									.setText(AppConstants.EMPTY_STRING);
						}

						pdetailloadquantity.setText(Utils.checkIsNull(items
								.getPd_quantity()));

						pdetailEquipmentType.setText(Utils.checkIsNull(items
								.getPd_equipmenttypesdescription()));
						// items.setPd_loadsize(poastloadsize);
						pdetailloadsize.setText(Utils
								.checkIsNull(poastloadsize));

						// System.out.println("truckId---->" + items);

					} else {
						errormsg = PostLoadDetailParser.geterrormmsg();
						Utils.showErrorMessage(errormsg,
								PostDetailActivity.this);
						// System.out.println("errormsg IN IF-----"+ errormsg);

					}

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}

		@Override
		public void onServiceComplete(String response, String reference) {
			// TODO Auto-generated method stub

		}

	}

	/*
	 * private class PostTruckDetail extends AsyncTask<Void, Void, String> {
	 * 
	 * @Override protected void onPreExecute() { super.onPreExecute();
	 * progDialog = new ProgressDialog(PostDetailActivity.this);
	 * progDialog.setMessage(AppConstants.POSTDETAIL_PROGRESS);
	 * progDialog.setCancelable(false); progDialog.show(); if
	 * (PostDetailActivity
	 * .this.getResources().getConfiguration().orientation==Configuration
	 * .ORIENTATION_PORTRAIT) {
	 * PostDetailActivity.this.setRequestedOrientation(Configuration
	 * .ORIENTATION_PORTRAIT); }else{
	 * PostDetailActivity.this.setRequestedOrientation
	 * (Configuration.ORIENTATION_LANDSCAPE); }
	 * 
	 * }
	 * 
	 * @Override protected String doInBackground(Void... params) { try {
	 * //System.out.println(Utils.isInternetAvailable(PostActivity.this)); if
	 * (Utils.isInternetAvailable(PostDetailActivity.this)) { HttpPost post =
	 * new HttpPost(AppConstants.GETTRUCKS_TEST_URL); HttpParams httpParameters
	 * = new BasicHttpParams();
	 * HttpConnectionParams.setConnectionTimeout(httpParameters,
	 * AppConstants.TIMEOUTCONNECTION_ERROR);
	 * HttpConnectionParams.setSoTimeout(httpParameters,
	 * AppConstants.SOCKETTIMEOUT_ERROR); DefaultHttpClient client = new
	 * DefaultHttpClient(httpParameters); post.setHeader("Accept-Encoding",
	 * "gzip,deflate"); post.setHeader("Content-Type",
	 * "text/xml;charset=UTF-8");
	 * 
	 * post.setHeader("SOAPAction",
	 * "http://webservices.truckstop.com/v11/ITruckPosting/GetTruckDetailResults"
	 * ); post.setHeader("Host", "webservices.truckstop.com");
	 * post.setHeader("Connection", "Keep-Alive");
	 * post.setHeader("User-Agent","Apache-HttpClient/4.1.1 (java 1.5)\\");
	 * 
	 * 
	 * 
	 * 
	 * String url = AppConstants.GETTRUCKS_TEST_URL; // Log.i(TAG,
	 * AppConstants.GETTRUCKS_TEST_URL); URL obj = new URL(url);
	 * HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
	 * ITSApplication app = ((ITSApplication) getApplicationContext());
	 * //ITSApplication app= new ITSApplication(); if (app.certifcate()==null) {
	 * return AppConstants.SERVER_CONNECTION_ERROR; }
	 * 
	 * con.setSSLSocketFactory(app.certifcate().getSocketFactory());
	 * con.setRequestMethod("POST"); con.setRequestProperty("Accept-Encoding",
	 * "gzip,deflate"); con.setRequestProperty("Content-Type",
	 * "text/xml;charset=UTF-8"); // SOAPAction: //
	 * "http://webservices.truckstop.com/v12/ILoadSearch/GetLoadSearchResults"
	 * con.setRequestProperty("SOAPAction",
	 * "http://webservices.truckstop.com/v11/ITruckPosting/GetTruckDetailResults"
	 * ); con.setRequestProperty("Host", "webservices.truckstop.com");
	 * con.setRequestProperty("Connection", "Keep-Alive");
	 * con.setRequestProperty("User-Agent",
	 * "Apache-HttpClient/4.1.1 (java 1.5)\\");
	 * 
	 * 
	 * String envolpe =
	 * "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\""
	 * +" xmlns:v11=\"http://webservices.truckstop.com/v11\""
	 * +" xmlns:web=\"http://schemas.datacontract.org/2004/07/WebServices\"" +
	 * " xmlns:web1=\"http://schemas.datacontract.org/2004/07/WebServices.Objects\""
	 * +">"; String body = envolpe +"\n<soapenv:Header/>\n" +"<soapenv:Body>\n"
	 * + "<v11:GetTruckDetailResults>\n" +"<v11:detailRequest>\n"
	 * +"<web:IntegrationId>"
	 * +session.getUserIntegrationID()+"</web:IntegrationId>\n"
	 * +"<web:Password>"+AppConstants.PASSWORD+"</web:Password>\n"
	 * +"<web:UserName>"+AppConstants.USERNAME+"</web:UserName>\n" // +
	 * "<web1:TruckId>"+46955822+"</web1:TruckId>\n" +
	 * "<web1:TruckId>"+postLoadId+"</web1:TruckId>\n" +"</v11:detailRequest>\n"
	 * +"</v11:GetTruckDetailResults>\n" +"</soapenv:Body>\n"
	 * +"</soapenv:Envelope>\n";
	 * 
	 * //System.out.println("BODY VALUE----" + body); post.setEntity(new
	 * StringEntity(body)); HttpResponse response = client.execute(post);
	 * StatusLine statuscode = response.getStatusLine(); String statuscodes =
	 * Integer.toString(statuscode.getStatusCode());
	 * //System.out.println("Status coded-------" + statuscodes); if
	 * (!statuscodes.equals("200")) { return
	 * AppConstants.SERVER_CONNECTION_ERROR; } else { InputStream stream =
	 * response.getEntity().getContent(); Header contentEncoding =
	 * response.getFirstHeader("Content-Encoding");
	 * //System.out.println("BODY VALUE----" + body);
	 * con.setConnectTimeout(AppConstants.SOCKETTIMEOUT_ERROR);
	 * con.setDoOutput(true); DataOutputStream wr = new
	 * DataOutputStream(con.getOutputStream());
	 * //System.out.println("BODY VALUE----" + body); wr.writeBytes(body);
	 * wr.flush(); wr.close();
	 * 
	 * int responseCode = con.getResponseCode(); //
	 * System.out.println("Status coded-------" + statuscodes); if
	 * (responseCode!=200) { return AppConstants.SERVER_CONNECTION_ERROR; } else
	 * { InputStream stream = con.getInputStream(); String contentEncoding =
	 * con.getHeaderField("Content-Encoding"); if (contentEncoding != null&&
	 * contentEncoding.getValue().equalsIgnoreCase("gzip")) { if
	 * (contentEncoding != null&& contentEncoding.equalsIgnoreCase("gzip")) {
	 * final InputStream stream1 = new GZIPInputStream(stream); //
	 * System.out.println("i am here"); BufferedReader reader = new
	 * BufferedReader(new InputStreamReader(stream1)); String line;
	 * StringBuilder builder1 = new StringBuilder(); while ((line =
	 * reader.readLine()) != null) { builder1.append(line);
	 * 
	 * } response_str=""; response_str = builder1.toString(); //
	 * System.out.println("retval IN IF-----"+ response_str); boolean
	 * Postingdetailsucess = PostLoadDetailParser.parsexml(response_str);
	 * 
	 * if (Postingdetailsucess) {
	 * items=PostLoadDetailParser.getpostdetailItems();
	 * 
	 * // System.out.println("truckId---->" + items); return "success"; } else {
	 * errormsg = PostLoadDetailParser.geterrormmsg();
	 * //System.out.println("errormsg IN IF-----"+ errormsg); return "error"; }
	 * } else { BufferedReader reader = new BufferedReader(new
	 * InputStreamReader(stream)); String line; StringBuilder builder1 = new
	 * StringBuilder(); while ((line = reader.readLine()) != null) {
	 * builder1.append(line);
	 * 
	 * } response_str=""; response_str = builder1.toString();
	 * //System.out.println("retval IN ELSE-----"+ response_str); boolean
	 * Postingdetailsucess = PostLoadDetailParser.parsexml(response_str);
	 * 
	 * if (Postingdetailsucess) {
	 * items=PostLoadDetailParser.getpostdetailItems();
	 * 
	 * //System.out.println("truckId---->" + items); return "success"; } else {
	 * errormsg = PostLoadDetailParser.geterrormmsg();
	 * //System.out.println("errormsg IN IF-----"+ errormsg); return "error"; }
	 * } } } else { return AppConstants.INTERNET_CONNECTION_ERROR; } } catch
	 * (Exception e) { e.printStackTrace(); return
	 * AppConstants.SERVER_CONNECTION_ERROR; }
	 * 
	 * }
	 * 
	 * protected void onPostExecute(String result) { progDialog.dismiss();
	 * //PostDetailActivity
	 * .this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR); if
	 * (result.equals("error")) { Utils.showErrorMessage(errormsg,
	 * PostDetailActivity.this); } else if
	 * (result.equals(AppConstants.INTERNET_CONNECTION_ERROR)) {
	 * Utils.showErrorMessage(AppConstants.INTERNET_CONNECTION_ERROR,
	 * PostDetailActivity.this);
	 * Toast.makeText(PostDetailActivity.this,AppConstants
	 * .INTERNET_CONNECTION_ERROR,Toast.LENGTH_LONG).show(); finish(); } else if
	 * (result.equals(AppConstants.SERVER_CONNECTION_ERROR)) {
	 * Utils.showErrorMessage(AppConstants.SERVER_CONNECTION_ERROR,
	 * PostDetailActivity.this);
	 * Toast.makeText(PostDetailActivity.this,AppConstants
	 * .SERVER_CONNECTION_ERROR, Toast.LENGTH_LONG).show(); finish(); } else {
	 * 
	 * //Log.i(TAG,
	 * Utils.checkIsNull(items.getPd_loadid())+" ++++  "+Utils.checkIsNull
	 * (items.getPd_hasFileCheck()));
	 * pdetailcompnayname.setText(items.getPd_truckCompanyName());
	 * //compnayname.setText(items.getTruckCompanyName());
	 * //companycontact.setText(items.getPointOfContact());
	 * 
	 * SpannableString ss4= new SpannableString("Handle:"); ss4.setSpan(new
	 * StyleSpan(Typeface.BOLD ), 0, ss4.length(), 0); ss4.setSpan(new
	 * ForegroundColorSpan(Color.WHITE), 0, ss4.length(),
	 * Spannable.SPAN_EXCLUSIVE_EXCLUSIVE); pdetailcompanycontact.append(ss4);
	 * pdetailcompanycontact.append(" "+session.getItsHandle());
	 * 
	 * SpannableString ss1= new SpannableString("\nContact:"); ss1.setSpan(new
	 * StyleSpan(Typeface.BOLD ), 0, ss1.length(), 0); ss1.setSpan(new
	 * ForegroundColorSpan(Color.WHITE), 0, ss1.length(),
	 * Spannable.SPAN_EXCLUSIVE_EXCLUSIVE); pdetailcompanycontact.append(ss1);
	 * pdetailcompanycontact
	 * .append(" "+Utils.checkIsNull(items.getPd_pointOfContact()));
	 * 
	 * SpannableString ss2= new SpannableString("\nPhone:"); ss2.setSpan(new
	 * StyleSpan(Typeface.BOLD ), 0, ss2.length(), 0); ss2.setSpan(new
	 * ForegroundColorSpan(Color.WHITE), 0, ss2.length(),
	 * Spannable.SPAN_EXCLUSIVE_EXCLUSIVE); pdetailcompanyphone.append(ss2);
	 * pdetailcompanyphone
	 * .append(" "+Utils.checkIsNull(items.getPd_pointOfContactPhone()));
	 * 
	 * SpannableString ss3= new SpannableString("\nEmail:"); ss3.setSpan(new
	 * StyleSpan(Typeface.BOLD), 0, ss3.length(), 0); ss3.setSpan(new
	 * ForegroundColorSpan(Color.WHITE), 0, ss3.length(),
	 * Spannable.SPAN_EXCLUSIVE_EXCLUSIVE); pdetailcompanyemail.append(ss3);
	 * pdetailcompanyemail
	 * .append(" "+Utils.checkIsNull(items.getPd_truckCompanyEmail()));
	 * 
	 * String
	 * originpoint=Utils.formatString(Utils.checkIsNull(items.getPd_originCity
	 * ()+","+Utils.checkIsNull(items.getPd_originState())));
	 * pdetailstartingPoint.setText(originpoint);
	 * 
	 * pdetailstartingPointdate.setText(Utils.checkIsNull((items.
	 * getPd_dateTruckAvailable()))); String
	 * destinationpoint=Utils.formatString(
	 * Utils.checkIsNull(items.getPd_destinationCity
	 * ()+","+Utils.checkIsNull(items.getPd_destinationState())));
	 * 
	 * 
	 * pdetaildestinationPoint.setText(destinationpoint);
	 * //pdetaildestinationPointdate
	 * .setText(Utils.checkIsNull(items.getPd_dateTruckAvailable()));
	 * pdetailspecialInfo.setText(Utils.checkIsNull(items.getPd_specInfo()));
	 * pdetailtraileroptions
	 * .setText(Utils.checkIsNull(items.getTrailerOptionType()));
	 * 
	 * if (!Utils.checkIsNull(items.getPd_weight()).equals("-")) {
	 * pdetailloadweight
	 * .setText(items.getPd_weight()+getResources().getString(R.
	 * string.unit_lbs)); }else{
	 * pdetailloadweight.setText(Utils.checkIsNull(items.getPd_weight())); }
	 * 
	 * if (!Utils.checkIsNull(items.getPd_length()).equals("-")) {
	 * pdetailloadlength
	 * .setText(items.getPd_length()+getResources().getString(R.
	 * string.unit_feet)); }else{
	 * pdetailloadlength.setText(Utils.checkIsNull(items.getPd_length())); }
	 * 
	 * if (!Utils.checkIsNull(items.getPd_width()).equals("-")) {
	 * pdetailloadwidth
	 * .setText(items.getPd_width()+getResources().getString(R.string
	 * .unit_feet)); }else{
	 * pdetailloadwidth.setText(Utils.checkIsNull(items.getPd_width())); }
	 * 
	 * String newDateString = null; if
	 * (!Utils.checkIsNull(items.getEntered()).equals("-")){
	 * newDateString=Utils.detailsDateFormat(items.getEntered());
	 * pdetailPostedDate.setText(newDateString); }else{
	 * pdetailPostedDate.setText(AppConstants.EMPTY_STRING); }
	 * 
	 * 
	 * pdetailloadquantity.setText(Utils.checkIsNull(items.getPd_quantity()));
	 * 
	 * pdetailEquipmentType.setText(Utils.checkIsNull(items.
	 * getPd_equipmenttypesdescription())); //
	 * items.setPd_loadsize(poastloadsize);
	 * pdetailloadsize.setText(Utils.checkIsNull(poastloadsize));
	 * 
	 * } } }
	 */

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.postdetail_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		switch (item.getItemId()) {
		/*
		 * case R.id.action_refreshsearch: loadResultPageNumber=1;
		 * sercheditems.clear(); btnLoadMore.setVisibility(View.VISIBLE);
		 * listview.addFooterView(btnLoadMore); SearchLoads loadsearch = new
		 * SearchLoads(); loadsearch.execute(); return true;
		 */
		case android.R.id.home:

			// NavUtils.navigateUpFromSameTask(this);
			finish();
			return true;

		case R.id.edit_truck:
			t.setScreenName("Edit Truck Screen");
			t.send(new HitBuilders.AppViewBuilder().build());
			Intent startposteditscreen = new Intent(PostDetailActivity.this,
					PostActivity.class);
			startposteditscreen.putExtra("POSTLOADDETAILS", items);
			startposteditscreen.putExtra("POSTLOADID", postLoadId);
			int postloadid;
			if (bundle.getString("LOADSIZE").equals("LTL")) {
				postloadid = 2;
			} else {
				postloadid = 1;
			}
			startposteditscreen.putExtra("POSTLOADSIZE", postloadid);

			startposteditscreen.putExtra("postType", 4);
			startActivity(startposteditscreen);
			finish();

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Utils.appcameforeground(PostDetailActivity.this);
		// Utils.startbackgroundservice(PostDetailActivity.this);
		LocalBroadcastManager.getInstance(this).registerReceiver(
				mRegistrationBroadcastReceiver,
				new IntentFilter(Utils.LOGOUT_USER));

	}

	@Override
	public void onPause() {
		LocalBroadcastManager.getInstance(this).unregisterReceiver(
				mRegistrationBroadcastReceiver);

		super.onPause();
	}

	@Override
	protected void onStop() {
		// session.setappbackground(Utils.isAppIsInBackground(SearchMain.this));
		Utils.appwentbackground(PostDetailActivity.this);
		// System.out.println("onStop called");
		// TODO Auto-generated method stub
		super.onStop();
	}

}
