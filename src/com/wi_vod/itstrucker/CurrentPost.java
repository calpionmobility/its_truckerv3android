/******************************************************************
 * CurrentPost.java
 * 
 * Description: Class for the showing the current posting Activity
 * 
 * @author: Wayne Y
 * 
 * @version: 1.0
 * 
 * History:
 * 
 * 
 ******************************************************************
 *//*
package com.wi_vod.itstrucker;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.haarman.listviewanimations.itemmanipulation.AnimateDismissAdapter;
import com.haarman.listviewanimations.itemmanipulation.OnDismissCallback;
import com.wi_vod.itstrucker.model.CurrentTruckPostsModel;
import com.wi_vod.itstrucker.model.XMLCurrentPostsparser;
import com.wi_vod.itstrucker.model.XMLTruckDeleteparser;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.zip.GZIPInputStream;
//import android.view.ActionMode;

*//************************************************************
 * Class: CurrentPost
 * 
 * Inherits: Fragment
 * 
 * Description: Set up the current post activity
 * -----------------------------------------------------------------
 *//*
public class CurrentPost extends ActionBarActivity
{

  
    private static final String TAG="CURRENTPOST.CLASS" ;
    private ArrayList<Map<String, String>> list = new ArrayList<Map<String, String>>();
    ArrayList<CurrentTruckPostsModel> truckpostitems = new ArrayList<CurrentTruckPostsModel>();
    private String deleteID       = null;
    
    private ProgressDialog progDialog; // Progress bar
    private static byte[] buff = new byte[1024];
    String retval, ret, response_str;
    StringBuilder builder = new StringBuilder();
    String itshandle, itsusername, itspassword;
    String errormsg;
    private SessionManager session;
    SampleAdapter adapter ;
    int selectedlistpostion;
    private TextView listerrormsg;
    private ListView currentList;
    public SelectionAdapter<String> animateDismissAdapter;

    private String deleteTruckIds = "" ;
    private TreeMap<Integer, Boolean> mSelection = new TreeMap<Integer, Boolean>();
    private View listItemView ;
    private int[] selecteditems;
    int[] str_Arr;
    ArrayList<Integer> arr_List;
     
     *//*********************************************************
     * Method: onCreate
     * 
     * Description: Called when the Activity is first created
     * 
     * @param: Bundle savedInstanceState - Current State
     * 
     * @returns: Void
     * 
     *//*
    
  //prevents orientation change from reloading results and crashing app
    @Override 
    public void onConfigurationChanged(Configuration newConfig) { 
      
      super.onConfigurationChanged(newConfig); 
    } 
    
    @Override
    protected void onCreate(Bundle arg0) {
    	super.onCreate(arg0);
    	setContentView(R.layout.currentpost);
    
        listerrormsg = (TextView) findViewById(R.id.errormsg);
        currentList = (ListView) findViewById(R.id.currentlist);
        
        // For Multiple Select and Delete feature
        currentList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        
        session = new SessionManager(CurrentPost.this);
        // Create a dialog for a process spinner
        GetCurrentTrucks gettrucks = new GetCurrentTrucks();
        gettrucks.execute();
        Log.i(TAG, "executed oncreateview");
    }
    
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	View v = inflater.inflate(R.layout.currentpost, container, false);
    	listerrormsg=(TextView)v.findViewById(R.id.errormsg);
    	currentList=(ListView)v.findViewById(R.id.currentlist);
    	
    	  session=new SessionManager(CurrentPost.this);
          // Create a dialog for a process spinner
          GetCurrentTrucks gettrucks=new GetCurrentTrucks();
          gettrucks.execute();
          Log.i(TAG, "executed oncreateview");
          
    	// TODO Auto-generated method stub
    	return v;
    }
    

	*//*********************************************************
      * Class RequestDeletePost
      * 
      * Description: Task for processing the HTTP request
      * 
      * 
      *//*
     private class RequestDeletePost extends AsyncTask<Void, Void, String>
     {
    	 @Override
  		protected void onPreExecute() {
  			super.onPreExecute();
  			progDialog = new ProgressDialog(CurrentPost.this);
  			progDialog.setMessage("Deleting Post.Please Wait....");
  			// progDialog.setIndeterminate(false);
  			progDialog.setCancelable(false);
  			progDialog.show();
  		}


		@Override
		protected String doInBackground(Void... params) {
			try {
  				System.out.println(Utils.isInternetAvailable(CurrentPost.this));
  				if (Utils.isInternetAvailable(CurrentPost.this)) {
  					// Log.i("IN BACKGROUND", "doInBackground");
  					HttpPost post = new HttpPost(AppConstants.GETTRUCKS_TEST_URL);
 					HttpParams httpParameters = new BasicHttpParams();
 					HttpConnectionParams.setConnectionTimeout(httpParameters, AppConstants.TIMEOUTCONNECTION_ERROR);
 					HttpConnectionParams.setSoTimeout(httpParameters, AppConstants.TIMEOUTCONNECTION_ERROR);
 					DefaultHttpClient client = new DefaultHttpClient(httpParameters);
  					post.setHeader("Accept-Encoding", "gzip,deflate");
  					post.setHeader("Content-Type", "text/xml;charset=UTF-8");
  					//SOAPAction: "http://webservices.truckstop.com/v12/ILoadSearch/GetLoadSearchResults"
  					post.setHeader("SOAPAction","http://webservices.truckstop.com/v11/ITruckPosting/DeleteTrucks");
  					post.setHeader("Host", "webservices.truckstop.com");
  					post.setHeader("Connection", "Keep-Alive");
  					//post.setHeader("Content-Length", "2811"); 
  					post.setHeader("User-Agent","Apache-HttpClient/4.1.1 (java 1.5)\\");
  		

  					String envolpe = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\""
  		 					+" xmlns:v11=\"http://webservices.truckstop.com/v11\""
  		 					+" xmlns:web=\"http://schemas.datacontract.org/2004/07/WebServices\""+
  		 					" xmlns:web1=\"http://schemas.datacontract.org/2004/07/WebServices.Posting\""
  		 					+" xmlns:arr=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\""
  		 					+">";
  		 					String body = envolpe
  		 							+"\n<soapenv:Header/>\n"
  		 							+"<soapenv:Body>\n"
  		 							+ "<v11:DeleteTrucks>\n"
  		 							+" <v11:trucks>\n"
  		 							+"<web:IntegrationId>"+session.getUserIntegrationID()+"</web:IntegrationId>\n"
  		 							+ "<web:Password>"+AppConstants.PASSWORD+"</web:Password>\n"
  									+ "<web:UserName>"+AppConstants.USERNAME+"</web:UserName>\n"
  		 							+"<web1:Trucks>\n"
  		 							//+"<arr:int>"+deleteID+"</arr:int>\n"
  		 							+ deleteTruckIds  
                                    +"</web1:Trucks>\n"
  		 							+"</v11:trucks>\n"
  		 							+"</v11:DeleteTrucks>\n"
  		 							+"</soapenv:Body>\n"
  		 							+"</soapenv:Envelope>\n";
  					
  					System.out.println("BODY VALUE----" + body);
  					post.setEntity(new StringEntity(body));
  					HttpResponse response = client.execute(post);
  					StatusLine statuscode = response.getStatusLine();
  					String statuscodes = Integer.toString(statuscode.getStatusCode());
  					System.out.println("Status coded-------" + statuscodes);
  					if (!statuscodes.equals("200")) {
  						return AppConstants.SERVER_CONNECTION_ERROR;
  					} else {
  						InputStream stream = response.getEntity().getContent();
  						Header contentEncoding = response.getFirstHeader("Content-Encoding");
  						if (contentEncoding != null&& contentEncoding.getValue().equalsIgnoreCase("gzip")) {
  							final InputStream stream1 = new GZIPInputStream(stream);
  							// System.out.println("i am here");
  							BufferedReader reader = new BufferedReader(new InputStreamReader(stream1));
  							String line;
  							 StringBuilder sbuilder = new StringBuilder();
  							while ((line = reader.readLine()) != null) {
  								
  								sbuilder.append(line);
  								response_str = sbuilder.toString();
  								System.out.println("retval IN IF-----"+ response_str);
  								boolean deletesucess=XMLTruckDeleteparser.parsexml(response_str);
  								
  								if (!deletesucess) {
  									truckpostitems=XMLCurrentPostsparser.getCurrentTrucks();
  									System.out.println("items in details---->"+truckpostitems.toString());
  									return "error";
  								}
  							}
  						} else {
  							BufferedReader reader = new BufferedReader(
  									new InputStreamReader(stream));
  							String line;
  							 StringBuilder sbuilder = new StringBuilder();
  							while ((line = reader.readLine()) != null) {
  								sbuilder.append(line);
  								response_str="";
  								response_str = sbuilder.toString();
  								System.out.println("retval IN ELSE-----"+ response_str);
  								boolean deletesucess = XMLTruckDeleteparser.parsexml(response_str);
  								
  								if (!deletesucess) {
  									truckpostitems=XMLCurrentPostsparser.getCurrentTrucks();
  									System.out.println("items in details---->"+truckpostitems.toString());
  									return "error";
  								}
  							}
  						}
  					}

  				} else {
  					return AppConstants.INTERNET_CONNECTION_ERROR;
  				}

  			} catch (Exception e) {
  				e.printStackTrace();
  				return AppConstants.SERVER_CONNECTION_ERROR;
  			}
  			return "success";
		}


		protected void onPostExecute(String result) {
 			progDialog.dismiss();
 			if (result.equals("error")) {
 				Toast.makeText(CurrentPost.this,"Delete Unsuccessful.Try after some time",Toast.LENGTH_LONG).show();
 				//finish();
 			} else if (result.equals(AppConstants.INTERNET_CONNECTION_ERROR)) {
 				Toast.makeText(CurrentPost.this,AppConstants.INTERNET_CONNECTION_ERROR,Toast.LENGTH_LONG).show();
 				//finish();
 			} else if (result.equals(AppConstants.SERVER_CONNECTION_ERROR)) {
 				Toast.makeText(CurrentPost.this,AppConstants.SERVER_CONNECTION_ERROR, Toast.LENGTH_LONG).show();
 				//finish();
 			} else {
 				
 				System.out.println("");
				list.remove(selectedlistpostion);
				if (list.size()==0) {
					listerrormsg.setVisibility(View.VISIBLE);
					listerrormsg.setText("NO MORE POSTS TO DISPLAY");
				}
				adapter.notifyDataSetChanged();
 				
 					animateDismissAdapter.animateDismiss(arr_List);
			
 				
 				
				
 				
 			   currentList.animate().setDuration(2000).alpha(0)
               .withEndAction(new Runnable() {
                 @Override
                 public void run() {
                  
                   System.out.println("");
    				list.remove(selectedlistpostion);
    				if (list.size()==0) {
    					listerrormsg.setVisibility(View.VISIBLE);
    					listerrormsg.setText("NO MORE POSTS TO DISPLAY");
   				}
    				adapter.notifyDataSetChanged();
    				currentList.setAlpha(1);
                 }
                 
               });
 			}
 			}
  	}
     
     private class GetCurrentTrucks extends AsyncTask<Void, Void, String> {
 		@Override
 		protected void onPreExecute() {
 			super.onPreExecute();
 			progDialog = new ProgressDialog(CurrentPost.this);
 			progDialog.setMessage("Retreiving Item Details.Please Wait....");
 			// progDialog.setIndeterminate(false);
 			progDialog.setCancelable(false);
 			progDialog.show();
 		}

 		@Override
 		protected String doInBackground(Void... params) {
 			try {
 				System.out.println(Utils.isInternetAvailable(CurrentPost.this));
 				if (Utils.isInternetAvailable(CurrentPost.this)) {
 					// Log.i("IN BACKGROUND", "doInBackground");
 					HttpPost post = new HttpPost(AppConstants.GETTRUCKS_TEST_URL);
					HttpParams httpParameters = new BasicHttpParams();
					HttpConnectionParams.setConnectionTimeout(httpParameters, AppConstants.TIMEOUTCONNECTION_ERROR);
					HttpConnectionParams.setSoTimeout(httpParameters, AppConstants.SOCKETTIMEOUT_ERROR);
					DefaultHttpClient client = new DefaultHttpClient(httpParameters);
 					post.setHeader("Accept-Encoding", "gzip,deflate");
 					post.setHeader("Content-Type", "text/xml;charset=UTF-8");
 					//SOAPAction: "http://webservices.truckstop.com/v12/ILoadSearch/GetLoadSearchResults"
 					post.setHeader("SOAPAction","http://webservices.truckstop.com/v11/ITruckPosting/GetTrucks");
 					post.setHeader("Host", "webservices.truckstop.com");
 					post.setHeader("Connection", "Keep-Alive");
 					//post.setHeader("Content-Length", "2811"); 
 					post.setHeader("User-Agent","Apache-HttpClient/4.1.1 (java 1.5)\\");
 	

 					String envolpe = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\""
 					+" xmlns:v11=\"http://webservices.truckstop.com/v11\""
 					+" xmlns:web=\"http://schemas.datacontract.org/2004/07/WebServices\""+">";
					String body = envolpe 
							+ "\n<soapenv:Header/>\n"
							+ "<soapenv:Body>\n" 
							+ " <v11:GetTrucks>\n"
							+ " <v11:listRequest>\n" 
							+ "<web:IntegrationId>" + session.getUserIntegrationID() + "</web:IntegrationId>\n"
							+ "<web:Password>"+AppConstants.PASSWORD+"</web:Password>\n"
							+ "<web:UserName>"+AppConstants.USERNAME+"</web:UserName>\n"
							+ "</v11:listRequest>\n" 
							+ "</v11:GetTrucks>\n"
							+ "</soapenv:Body>\n" + 
							"</soapenv:Envelope>\n";
 					
 					System.out.println("BODY VALUE----" + body);
 					post.setEntity(new StringEntity(body));
 					HttpResponse response = client.execute(post);
 					
 					StatusLine statuscode = response.getStatusLine();
 					String statuscodes = Integer.toString(statuscode.getStatusCode());
 					System.out.println("Status coded-------" + statuscodes);
 					if (!statuscodes.equals("200")) {
 						return AppConstants.SERVER_CONNECTION_ERROR;
 					} else {
 						InputStream stream = response.getEntity().getContent();
 						Header contentEncoding = response.getFirstHeader("Content-Encoding");
 						if (contentEncoding != null&& contentEncoding.getValue().equalsIgnoreCase("gzip")) {
 							final InputStream stream1 = new GZIPInputStream(stream);
 							// System.out.println("i am here");
 							BufferedReader reader = new BufferedReader(new InputStreamReader(stream1));
 							String line;
 							while ((line = reader.readLine()) != null) {
 								builder.append(line);
 								response_str = builder.toString();
 								boolean loadsucess=XMLCurrentPostsparser.parsexml(response_str);
 								
 								if (loadsucess) {
 									truckpostitems.clear();
 									truckpostitems=XMLCurrentPostsparser.getCurrentTrucks();
 									//Log.i("Currentpost","truckpostitems size-------->"+truckpostitems.size());
 									return "success";
 								}else{
 									errormsg=XMLCurrentPostsparser.geterrormsg();
 									return "error";
 								}
 							}
 						} else {
							BufferedReader reader = new BufferedReader( new InputStreamReader(stream));
							String line;
							while ((line = reader.readLine()) != null) {
								builder.append(line);
								response_str = builder.toString();
								boolean loadsucess = XMLCurrentPostsparser.parsexml(response_str);
								if (loadsucess) {
									truckpostitems.clear();
									truckpostitems = XMLCurrentPostsparser.getCurrentTrucks();
									Log.i("Currentpost", "truckpostitems size-------->" + truckpostitems.size());
									return "success";
								} else {
									errormsg = XMLCurrentPostsparser.geterrormsg();
									return "error";
								}
							}
 						}
 					}

 				} else {
 					return AppConstants.INTERNET_CONNECTION_ERROR;
 				}

 			} catch (Exception e) {
 				e.printStackTrace();
 				return AppConstants.SERVER_CONNECTION_ERROR;
 			}
 			return AppConstants.SERVER_CONNECTION_ERROR;
 		}

 		@SuppressLint("NewApi")
		protected void onPostExecute(String result) {
 			progDialog.dismiss();
 			if (result.equals("error")) {
 				Utils.showSettingsAlert(errormsg, CurrentPost.this);
 				Toast.makeText(CurrentPost.this,errormsg,Toast.LENGTH_LONG).show();
 				finish();
 			} else if (result.equals(AppConstants.INTERNET_CONNECTION_ERROR)) {
 				Toast.makeText(CurrentPost.this,AppConstants.INTERNET_CONNECTION_ERROR,Toast.LENGTH_LONG).show();
 				finish();
 			} else if (result.equals(AppConstants.SERVER_CONNECTION_ERROR)) {
 				Toast.makeText(CurrentPost.this,AppConstants.SERVER_CONNECTION_ERROR, Toast.LENGTH_LONG).show();
 				finish();
 			} else {
 				list.clear();
 				
 				if (truckpostitems.size()==0) {
 	 				
 	 					listerrormsg.setText("NO POSTS FOUND");
 					//Toast.makeText(CurrentPost.this,"No Posts Found",Toast.LENGTH_LONG).show();
					//finish();
				}else{
					
	 					listerrormsg.setText("Total Posts:"+truckpostitems.size());
				}
 				Iterator<CurrentTruckPostsModel> iterator = truckpostitems.iterator();
 				
					while (iterator.hasNext()) {
						CurrentTruckPostsModel items=(CurrentTruckPostsModel)iterator.next();
						// Insert the data into the array
                        list.add(putData(items.getOriginCity() + ", " + items.getOriginState(),
                        		items.getDestinationCity() + ", " + items.getDestinationState(),
                        		items.getEquipment(),
                        		items.getDateAvailable(),
                        		items.getIsFull(),
                        		items.getId()));
					}
 				
					// Map the fields to the text view
	                 String[] from = { "origin", "destination", "equipment", "date", "load", "ID" };
	                 int[] to = { R.id.postText1, 
	                              R.id.postText2, 
	                              R.id.postText3, 
	                              R.id.postText4,
	                              R.id.postText5,
	                              R.id.postText6};
	                 // Link the list view adapter to the string array
	                 // android.R.layout.simple_list_item is a generic layout XML file with just one text view
	                 System.out.println("list.size() after filling-------->"+list.size());
//	                 adapter = new SimpleAdapter(CurrentPost.this, list, R.layout.current_post, from, to);
	                 adapter = new SampleAdapter(CurrentPost.this, list, R.layout.current_post, from, to);
	                 
	                // currentList.setAdapter(adapter);	
	                 
                animateDismissAdapter = new SelectionAdapter<String>(adapter,
                        new MyOnDismissCallback());
                animateDismissAdapter.setAbsListView(currentList);
                currentList.setAdapter(animateDismissAdapter);
                
                currentList.setMultiChoiceModeListener(new MultiChoiceModeListener() {
                    
                    private int nr = 0;
                     
                    @Override
                    public boolean onPrepareActionMode(android.view.ActionMode mode , Menu menu) {
                        // TODO Auto-generated method stub
                        return false;
                    }
                     
                    @Override
                    public void onDestroyActionMode(android.view.ActionMode mode ) {
                        // TODO Auto-generated method stub
                        animateDismissAdapter.clearSelection();
                    }
                     
                    @Override
                    public boolean onCreateActionMode(android.view.ActionMode mode , Menu menu) {
                        // TODO Auto-generated method stub
                         
                        nr = 0;
                        MenuInflater inflater = getMenuInflater();
                        inflater.inflate(R.menu.contextual_menu, menu);
                        return true;
                    }
                     
                    @SuppressLint("NewApi")
					@Override
                    public boolean onActionItemClicked(android.view.ActionMode mode , MenuItem item) {
                        // TODO Auto-generated method stub
                        switch (item.getItemId()) {
                         
                            case R.id.item_delete:
//                                nr = 0;
//                                animateDismissAdapter.clearSelection();
                                if(mSelection !=null && mSelection.size()>0){
                                	
                                Iterator< Entry< Integer, Boolean > > it = mSelection.entrySet().iterator();
                                arr_List = new ArrayList<Integer>();
                            	deleteTruckIds = "";
                                    while (it.hasNext()) {
                                    
                                        Map.Entry< Integer,Boolean> entry = (Map.Entry< Integer,Boolean >) it.next();
                                        Integer key = (Integer)entry.getKey();
                                        Boolean val = (Boolean)entry.getValue();
                                        
                                        System.out.println("key,val: " + key + "," + val);
                                        arr_List.add(key);
                                        if(val){
                                            deleteTruckIds = deleteTruckIds +"<arr:int>"+list.get(key).get("ID").substring(17)+"</arr:int>\n";
                                        }
                                    
                                    }
                                }
                               
                                RequestDeletePost requestDeletePost = new RequestDeletePost();
                                requestDeletePost.execute();
                                mode.finish();
                        }
                        return false;
                    }
                     
                     
                 
                    @SuppressLint("NewApi")
					@Override
                    public void onItemCheckedStateChanged(android.view.ActionMode mode , int position,
                            long id, boolean checked) {
                        // TODO Auto-generated method stub
                         if (checked) {
                                nr++;
                                animateDismissAdapter.setNewSelection(position, checked);   
                             
                            } else {
                                nr--;
                                animateDismissAdapter.removeSelection(position);
                            }
                            mode.setTitle(nr + " selected");
                         
                    }

                });

                currentList.setOnItemLongClickListener(new OnItemLongClickListener() {
                    
                    @Override
                    public boolean onItemLongClick(AdapterView<?> parent, View view,
                            int position, long id) {
                        // TODO Auto-generated method stub
                         
                        currentList.setItemChecked(position, !animateDismissAdapter.isPositionChecked(position));
                        return false;
                    }
                });
          

                currentList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

	                     @Override
	                     public void onItemClick(AdapterView<?> parent, final View view,int position, long id) {
	                       //final String item = (String) parent.getItemAtPosition(position);
	                    	   Map<String,String> selected=list.get(position);
	 	                      String postLoadid=  selected.get("ID");
	 	                      String loadsize= selected.get("load");
	 	                      loadsize=loadsize.replace("LoadSize: ", "");
	 	                      postLoadid = postLoadid.replaceAll("\\D+",""); 
		               	Log.i(TAG, postLoadid);
		               		Intent startpostdetailpage = new Intent(CurrentPost.this, PostDetailActivity.class);
		               		startpostdetailpage.putExtra("POSTLOADID", postLoadid);
		               		startpostdetailpage.putExtra("LOADSIZE", loadsize);
		               		
		               		startActivity(startpostdetailpage);
	                     }
	                   });
 			}
 			}
 	}
     
     
    @Override
    protected void onResume() {
    	// TODO Auto-generated method stub
    	super.onResume();
//    	list.clear();
//    	truckpostitems.clear();
    }
     
    private class MyOnDismissCallback implements OnDismissCallback {

		@Override
		public void onDismiss(AbsListView listView, int[] reverseSortedPositions) {
			for (int position : reverseSortedPositions) {
				adapter.
				((Map<String, String>) adapter).remove(position);
			    
				list.remove(position);
//				    adapter.removeItem(position);
				if (list.size() == 0) {
					listerrormsg.setVisibility(View.VISIBLE);
					listerrormsg.setText("NO MORE POSTS TO DISPLAY");
				}else{
					listerrormsg.setText("Total Posts:"+ list.size());
				}
			}
			adapter.notifyDataSetChanged();
			animateDismissAdapter.notifyDataSetChanged();
		}
	}
     
     @Override
    protected void onPause() {
    	// TODO Auto-generated method stub
    	super.onPause();
    	truckpostitems.clear();
    }
     *//*********************************************************
      * Method: putData
      * 
      * Description: This method will be used to put the data into the array
      * 
      * @param: origin       - origin of the load
      * @param: destination  - destination of the load
      * @param: equipment    - equipment type
      * @param: date         - date available
      * @param: load         - load type
      * @param: truckID      - Truck load ID
      * 
      * @returns: Void
      * 
      *//*
     private HashMap<String, String> putData(String origin, String destination, String equipment,String date, String load,  String truckID) 
     {
         HashMap<String, String> item = new HashMap<String, String>();
         item.put("origin",      "Origin: " + origin);
         item.put("destination", "Destination: " + destination);
         item.put("equipment",   "Equipment: " + equipment);
         item.put("date",        "Date Available: " + date);
         item.put("load",        "LoadSize: " + load);
         item.put("ID",          "Truck Posted ID: " + truckID);
         
         return item;
     }
     
     
      @Override
  	public boolean onCreateOptionsMenu(Menu menu) {
  		
  		return true;
  	}
  	@Override
  	public boolean onOptionsItemSelected(MenuItem item) {
  	    switch (item.getItemId()) {
  	    // Respond to the action bar's Up/Home button
  	    case android.R.id.home:
  	        NavUtils.navigateUpFromSameTask(this);
  	      
  	        return true;
  	    }
  	    return super.onOptionsItemSelected(item);
  	}
      
      
  	
  	public class SelectionAdapter<String> extends AnimateDismissAdapter<String> {
  	  
//        private HashMap<Integer, Boolean> mSelection = new HashMap<Integer, Boolean>();
        
        public SelectionAdapter(BaseAdapter bAdapter, OnDismissCallback dismissCallback) {
//            super(context, resource, textViewResourceId, objects);
            super(bAdapter, dismissCallback);
        }
 
        public void setNewSelection(int position, boolean value) {
            mSelection.put(position, value);
            notifyDataSetChanged();
        }
 
        public boolean isPositionChecked(int position) {
            Boolean result = mSelection.get(position);
            return result == null ? false : result;
        }
 
        public Set<Integer> getCurrentCheckedPosition() {
            return mSelection.keySet();
        }
 
        public void removeSelection(int position) {
            mSelection.remove(position);
            notifyDataSetChanged();
        }
 
        public void clearSelection() {
            mSelection = new TreeMap<Integer, Boolean>();
            notifyDataSetChanged();
        }
 
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = super.getView(position, convertView, parent);//let the adapter handle setting up the row views
            v.setBackgroundColor(getResources().getColor(android.R.color.background_light)); //default color
             
            if (mSelection.get(position) != null) {
                v.setBackgroundColor(getResources().getColor(R.color.list_background_highlight));// this is a selected position so make it red
            }
            notifyDataSetChanged();
            return v;
        }
        
       
    }
  	
  	
  	private class SampleAdapter extends SimpleAdapter{

  	    private ArrayList<Map<String, String>> mData = new ArrayList<Map<String, String>>();
  	    
        public SampleAdapter(Context context, List<? extends Map<java.lang.String, ?>> data,
                int resource, java.lang.String[] from, int[] to) {
            super(context, data, resource, from, to);
            mData = (ArrayList<Map<java.lang.String, java.lang.String>>) data ;
        }
        
        public void removeItem(int position){
            mData.remove(position);
            this.notifyDataSetChanged();
        }
  	    
  	}
      
}
     


*/