package com.wi_vod.itstrucker;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.DatePicker;
import android.widget.Toast;

public class CustomDatePickerDialog extends DatePickerDialog implements DatePickerDialog.OnDateSetListener{

    private Context mContext ;
    private Activity mActivity ;
    
    public CustomDatePickerDialog(Context context, OnDateSetListener callBack, int year,
            int monthOfYear, int dayOfMonth) {
        super(context, callBack, year, monthOfYear, dayOfMonth);
        this.mContext = context;
    }
    
    public CustomDatePickerDialog(Activity activity, OnDateSetListener callBack, int year,
            int monthOfYear, int dayOfMonth) {
        super(activity, callBack, year, monthOfYear, dayOfMonth);
        this.mActivity = activity;
    }
    
    @Override
    public void onClick(DialogInterface dialog, int which) {
        // TODO Auto-generated method stub
        super.onClick(dialog, which);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Toast.makeText(mContext, "In CustomDatePickerDialog onDateSet()", Toast.LENGTH_LONG).show();
    
    }
    
    @Override
    public DatePicker getDatePicker() {
        // TODO Auto-generated method stub
        return super.getDatePicker();
    }
    
}
