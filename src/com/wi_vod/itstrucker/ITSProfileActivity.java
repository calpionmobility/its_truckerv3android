package com.wi_vod.itstrucker;

import android.app.ActionBar.LayoutParams;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.wi_vod.itstrucker.model.DataModel;
import com.wi_vod.itstrucker.model.PlacesAutoCompleteAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class ITSProfileActivity extends Fragment implements OnDialogCancelListener{
    
    private View v;
    private SessionManager session;
    private Spinner psequipmenttype, pstrailerOptionType,ppequipmenttype, pptrailerOptionType, pshoursold, pssortOption,countryspinner;
    private EditText profileWidth, profileLength, profileWeight,profileMinDist, profileRatePerMile,profileQuantity;
    private String country,countryvalue,profilehomebase,width,length,weight,mindist,ratepermile,
                    originareaaselectedcode,destareaselectedcode,destinationcountry;
    private int searchequipmentindex,postequipmentindex,searchtraileroptionindex,posttraileroptionindex,psradius,ppradius,
    pshoursindex,pssorttypeindex,psloadsize,pploadsize,origincountryindex,originareaaselected,destareaselected;
    SharedPreferences prefs;
    private SeekBar psradiusbar,ppradiusbar;
    private Button save;
    private ImageButton searcharrowbtn,postarrowbtn;
    private RadioGroup psloadSizegroup,pploadSizegroup;
    private RadioButton psloadsizefull, psloadsizeltl,psloadsizeany, radioLoadButton,pploadsizefull,pploadsizeltl,ppradioLoadButton;
    private RelativeLayout parentlayout;
    private TextView psradiusValue,ppradiusValue, txtPSEquipment;
    public static boolean validated = true;
    private String validationError="",destinationstate,destinationcity;
    private AutoCompleteTextView homeBase;
    private LinearLayout parentLinearLayout,profilesearchlayoutmain,profilepostinglayoutmain;
    private RelativeLayout profilesearchheaderlayout,profilepostingheaderlayout;
    PlacesAutoCompleteAdapter adapter;
    private MultiSelectionSpinner psequipmentType_multiSelect_Spinner,pstrailerOptions_multiSelect_Spinner;
    private MultiSelectionSpinnerposting pptrailerOptions_multiSelect_Spinner,ppequipmentType_multiSelect_Spinner;
    private String psequipmentTypeStr, pstrailerOptionStr,ppequipmentTypeStr, pptrailerOptionStr,psequipmenttypeString,pstraileroptionString;
    private TextView psTxtTrailerOption, ppTxtTrailerOption,ppTxtEquipmentType ;
    private String pssortypeString,pshoursoldString,psloadtypeString;
    

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        session=new SessionManager(getActivity());
        prefs = getActivity().getSharedPreferences("itstrucker", 0);
        v = inflater.inflate(R.layout.itsprofile, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager. LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        UpdateUI();
        UpdateProfileValues();
        return v;
    }

    private void UpdateProfileValues() {
        origincountryindex=prefs.getInt("country", 0);
        profilehomebase = prefs.getString("homebase", "");
        
        //Retrieve search preference values from saved sharedpreference
        psradius=prefs.getInt("radius", 25);
        psloadsize=prefs.getInt("loadsizetype", 0);
        pshoursindex=prefs.getInt("hoursold", 0);
        pssorttypeindex=prefs.getInt("sorttype", 0);
        
        //Retrieve posting preference values from saved sharedpreference
        ppradius=prefs.getInt("ppradius", 25);
        pploadsize=prefs.getInt("pploadsizetype", 0);
        width=prefs.getString("width", "");
        length=prefs.getString("length", "");
        weight=prefs.getString("weight", "");
        mindist=prefs.getString("mindist", "");
        ratepermile=prefs.getString("ratepermile", "");
        
        
        //Set search preference values to the respective views 
        countryspinner.setSelection(origincountryindex);
        homeBase.setText(profilehomebase);
        
        if (psradius==0) {
            psradiusValue.setText("25 mi");
        }else{
            psradiusbar.setProgress(psradius-25);
            psradiusValue.setText(String.valueOf(psradius) + " mi");
        }
        if (psloadsize==1) {
            psloadsizeltl.setChecked(true);
        }else if (psloadsize==2) {
            psloadsizefull.setChecked(true);
            
        }else{
            psloadsizeany.setChecked(true);
        }
        
        pshoursold.setSelection(pshoursindex);
        pssortOption.setSelection(pssorttypeindex);
        
        psequipmentTypeStr = prefs.getString("equipmentindex", AppConstants.EMPTY_STRING);
        pstrailerOptionStr = prefs.getString("traileroptions", AppConstants.EMPTY_STRING);
        
     // To retain the previously selected items
        // To retain the previously selected items
        if(psequipmentTypeStr.length() != 0 && !psequipmentTypeStr.equals("-") ){
            txtPSEquipment.setVisibility(View.GONE);
            psequipmentType_multiSelect_Spinner.setVisibility(View.VISIBLE);
           
            ArrayList<String> selectedEquipmentTypeList = Utils.getSelectedItems(psequipmentTypeStr);
            psequipmentType_multiSelect_Spinner.setSelection(selectedEquipmentTypeList);
            psequipmentType_multiSelect_Spinner.setCount(selectedEquipmentTypeList.size());
            psequipmentType_multiSelect_Spinner.setSelectedIds(Utils.getSelectedEquipmentTypeIndices(getActivity().getApplicationContext(), selectedEquipmentTypeList));
        }else{
            txtPSEquipment.setVisibility(View.VISIBLE);
            psequipmentType_multiSelect_Spinner.setVisibility(View.GONE);
        }
        
        if(pstrailerOptionStr.length() != 0 && !pstrailerOptionStr.equals("-")){
            psTxtTrailerOption.setVisibility(View.GONE);
            pstrailerOptions_multiSelect_Spinner.setVisibility(View.VISIBLE);
            
            ArrayList<String> selectedTrailerOptionList = Utils.getSelectedItems(pstrailerOptionStr,",");
            pstrailerOptions_multiSelect_Spinner.setSelection(selectedTrailerOptionList);
            pstrailerOptions_multiSelect_Spinner.setCount(selectedTrailerOptionList.size());
            pstrailerOptions_multiSelect_Spinner.setSelectedIds(Utils.getSelectedTrailerOptionIndices(getActivity().getApplicationContext(), selectedTrailerOptionList));
        }else{
            psTxtTrailerOption.setVisibility(View.VISIBLE);
            pstrailerOptions_multiSelect_Spinner.setVisibility(View.GONE);
        }
        
        //Set post preference values to the respective views 
//      postequipmentindex = prefs.getInt("ppequipmentindex", 0);
          ppequipmentTypeStr = prefs.getString("ppequipmentindex", AppConstants.EMPTY_STRING);
          pptrailerOptionStr = prefs.getString("pptraileroptions", AppConstants.EMPTY_STRING);

      
   // To retain the previously selected items
//      ppequipmenttype.setSelection(postequipmentindex);
      
      if(ppequipmentTypeStr.length() != 0 && !ppequipmentTypeStr.equals("-") ){
          ppTxtEquipmentType.setVisibility(View.GONE);
          ppequipmentType_multiSelect_Spinner.setVisibility(View.VISIBLE);
         
          ArrayList<String> selectedEquipmentTypeList = Utils.getSelectedItems(ppequipmentTypeStr);
          ppequipmentType_multiSelect_Spinner.setSelection(selectedEquipmentTypeList);
          ppequipmentType_multiSelect_Spinner.setCount(selectedEquipmentTypeList.size());
          ppequipmentType_multiSelect_Spinner.setSelectedIds(Utils.getSelectedEquipmentTypeIndicesForPost(getActivity().getApplicationContext(), selectedEquipmentTypeList));
      }else{
          ppTxtEquipmentType.setVisibility(View.VISIBLE);
          ppequipmentType_multiSelect_Spinner.setVisibility(View.GONE);
      }

        
     // To retain the previously selected items
        //ppequipmenttype.setSelection(postequipmentindex);
        
        if(pptrailerOptionStr.length() != 0 && !pptrailerOptionStr.equals("-")){
            ppTxtTrailerOption.setVisibility(View.GONE);
            pptrailerOptions_multiSelect_Spinner.setVisibility(View.VISIBLE);
            
            ArrayList<String> ppselectedTrailerOptionList = Utils.getSelectedItems(pptrailerOptionStr);
            pptrailerOptions_multiSelect_Spinner.setSelection(ppselectedTrailerOptionList);
            pptrailerOptions_multiSelect_Spinner.setCount(ppselectedTrailerOptionList.size());
            pptrailerOptions_multiSelect_Spinner.setSelectedIds(Utils.getSelectedTrailerOptionIndices(getActivity().getApplicationContext(), ppselectedTrailerOptionList));
        }else{
            ppTxtTrailerOption.setVisibility(View.VISIBLE);
            pptrailerOptions_multiSelect_Spinner.setVisibility(View.GONE);
        }
        
        if (ppradius==0) {
            ppradiusValue.setText("25 mi");
        }else{
            ppradiusbar.setProgress(ppradius-25);
            ppradiusValue.setText(String.valueOf(ppradius) + " mi");
        }
        
        if (pploadsize==2) {
            pploadsizeltl.setChecked(true);
        }else{
            pploadsizefull.setChecked(true);
        }
        
        profileWidth.setText(width);
        profileLength.setText(length);
        profileWeight.setText(weight);
        profileMinDist.setText(mindist);
        profileRatePerMile.setText(ratepermile);
    }

    private void UpdateUI() {
        

        parentLinearLayout=(LinearLayout)v.findViewById(R.id.parentlinearlayout);
        countryspinner = (Spinner) v.findViewById(R.id.profilecountryspinner);
        homeBase = (AutoCompleteTextView) v.findViewById(R.id.homebase);
        searcharrowbtn=(ImageButton)v.findViewById(R.id.psbutton);
        postarrowbtn=(ImageButton)v.findViewById(R.id.ppbutton);
        
        //Initializing Search preference(Section) UI 
        profilesearchheaderlayout = (RelativeLayout) v.findViewById(R.id.profilesearchheader);
        profilesearchlayoutmain = (LinearLayout) v.findViewById(R.id.searchlayoutmain);
        
        String[] equipString = getActivity().getResources().getStringArray(R.array.type_array);
        String[] trailerOptions = getActivity().getResources().getStringArray(R.array.option_array);
        
        txtPSEquipment = (TextView)v.findViewById(R.id.et_spinner_txt);
        psequipmentType_multiSelect_Spinner = (MultiSelectionSpinner) v.findViewById(R.id.psequipmentspinner);
        pstrailerOptions_multiSelect_Spinner = (MultiSelectionSpinner) v.findViewById(R.id.pstraileroptionsspinner);
        
        txtPSEquipment.setOnClickListener(new OnClickListener() {
            
            @Override
            public void onClick(View v) {
                psequipmentType_multiSelect_Spinner.setVisibility(View.VISIBLE);
                txtPSEquipment.setVisibility(View.GONE);
                psequipmentType_multiSelect_Spinner.performClick();  
            }
        });
        
        psTxtTrailerOption = (TextView)v.findViewById(R.id.ps_tr_spinner_txt);
        
        psTxtTrailerOption.setOnClickListener(new OnClickListener() {
            
            @Override
            public void onClick(View v) {
                pstrailerOptions_multiSelect_Spinner.setVisibility(View.VISIBLE);
                psTxtTrailerOption.setVisibility(View.GONE);
                pstrailerOptions_multiSelect_Spinner.performClick();  
            }
        });

        pshoursold = (Spinner) v.findViewById(R.id.pshouroldspinner);
        pssortOption = (Spinner) v.findViewById(R.id.pssortoptions);
        psloadSizegroup=(RadioGroup)v.findViewById(R.id.psroadiogrouploadsize);
        psloadsizefull=(RadioButton)v.findViewById(R.id.psradioloadsizefull);
        psloadsizeltl=(RadioButton)v.findViewById(R.id.psradioloadsizeltl);
        psloadsizeany=(RadioButton)v.findViewById(R.id.psradioloadsizeany);
        psradiusValue=(TextView)v.findViewById(R.id.psseekbarvalue);
        psradiusbar=(SeekBar)v.findViewById(R.id.psradiusseekBar);
        
        
        //Initializing Post preference(Section) UI 
        profilepostingheaderlayout = (RelativeLayout) v.findViewById(R.id.profilepostingheaderlayout);
        profilepostinglayoutmain = (LinearLayout) v.findViewById(R.id.postinglayoutmain);

         ppequipmenttype = (Spinner) v.findViewById(R.id.ppequipmenttypespinner);
            ppequipmentType_multiSelect_Spinner = (MultiSelectionSpinnerposting)v.findViewById(R.id.ppequipmenttypespinner);
            ppTxtEquipmentType = (TextView)v.findViewById(R.id.pp_et_spinner_txt);
            
            ppTxtEquipmentType.setOnClickListener(new OnClickListener() {
                
                @Override
                public void onClick(View v) {
                    ppequipmentType_multiSelect_Spinner.setVisibility(View.VISIBLE);
                    ppTxtEquipmentType.setVisibility(View.GONE);
                    ppequipmentType_multiSelect_Spinner.setUnSelection(0);
                    ppequipmentType_multiSelect_Spinner.performClick();
                }
            });
            
        
        String[] pptrailerOptions = getActivity().getResources().getStringArray(R.array.option_array);
        pptrailerOptions_multiSelect_Spinner = (MultiSelectionSpinnerposting) v.findViewById(R.id.pptraileroptionsspinner);
        
        ppTxtTrailerOption = (TextView)v.findViewById(R.id.pp_tr_spinner_txt);
        
        ppTxtTrailerOption.setOnClickListener(new OnClickListener() {
            
            @Override
            public void onClick(View v) {
                pptrailerOptions_multiSelect_Spinner.setVisibility(View.VISIBLE);
                ppTxtTrailerOption.setVisibility(View.GONE);
                pptrailerOptions_multiSelect_Spinner.performClick();  
            }
        });
        
        ppradiusValue=(TextView)v.findViewById(R.id.ppseekbarvalue);
        ppradiusbar=(SeekBar)v.findViewById(R.id.ppradiusseekBar);
        pploadSizegroup=(RadioGroup)v.findViewById(R.id.pproadiogrouploadsize);
        pploadsizefull=(RadioButton)v.findViewById(R.id.ppradioloadsizefull);
        pploadsizeltl=(RadioButton)v.findViewById(R.id.ppradioloadsizeltl);
        profileWidth = (EditText) v.findViewById(R.id.profilewidth);
        profileLength = (EditText) v.findViewById(R.id.profilelength);
        profileWeight = (EditText) v.findViewById(R.id.profileweight);
        profileMinDist = (EditText) v.findViewById(R.id.profilemindistance);
        profileRatePerMile = (EditText) v.findViewById(R.id.profileratepermile);
        
    
        profileWidth.setOnFocusChangeListener(new DecimalFilter(profileWidth, getActivity()));
        profileLength.setOnFocusChangeListener(new DecimalFilter(profileLength, getActivity() ));
        profileRatePerMile.setOnFocusChangeListener(new DecimalFilter(profileRatePerMile, getActivity() ));
        
        profileWeight.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
             /*     if (profileWeight.getText().toString()!=null&&!profileWeight.getText().toString().equals("")) {
                        double d=Double.valueOf(profileWeight.getText().toString());
                        int i=(int)(Math.round(d));
                        if (i>999999) {
                            profileWeight.setText("999999");
                        }else{
                            profileWeight.setText(String.valueOf((int)(Math.round(d))));
                        }
                        
                    }*/
                    profileWeight.setText(Utils.weightRounding(profileWeight.getText().toString()));
                }
               }
            });
        
        
        profileMinDist.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    /*if (profileMinDist.getText().toString()!=null&&!profileMinDist.getText().toString().equals("")) {
                        double d=Double.valueOf(profileMinDist.getText().toString());
                        int i=(int)(Math.round(d));
                        if (i>9999) {
                            profileMinDist.setText("9999");
                        }else{
                            profileMinDist.setText(String.valueOf((int)(Math.round(d))));
                        }
                        
                    }*/
                    profileMinDist.setText(Utils.mindistanceRounding(profileMinDist.getText().toString()));
                    
                }
               }
            });
        
        
        
        
        //Initializing save button
        save=(Button)v.findViewById(R.id.saveprofile);
        

        //Set initial country and country index to USA and value to 0 for validating states in searchcity method in utils.
        updategooleplaces("us",0);
        
        
        profilesearchheaderlayout.setOnClickListener(new OnClickListener() {
            
            @Override
            public void onClick(View v) {
                if (profilesearchlayoutmain.getVisibility()==View.VISIBLE) {
                    collapse(profilesearchlayoutmain);
                    searcharrowbtn.setSelected(false);
                }else{
                    expand(profilesearchlayoutmain);
                    searcharrowbtn.setSelected(true);
                }
            }
        });
    
        profilepostingheaderlayout.setOnClickListener(new OnClickListener() {
            
            @Override
            public void onClick(View v) {
                
                if (profilepostinglayoutmain.getVisibility()==View.VISIBLE) {
                    collapse(profilepostinglayoutmain);
                    postarrowbtn.setSelected(false);
                }else{
                    expand(profilepostinglayoutmain);
                    postarrowbtn.setSelected(true);
                }
            }
        });
        
        
        save.setOnClickListener(new OnClickListener() {
            
            @Override
            public void onClick(View v) {
                profilehomebase=homeBase.getText().toString();
                psloadsize = psloadSizegroup.getCheckedRadioButtonId();
                pploadsize = pploadSizegroup.getCheckedRadioButtonId();
                 // Selected Equipment OPtions for search
                 psequipmentTypeStr = psequipmentType_multiSelect_Spinner.getSelectedItemsAsString();
                // Selected Equipment OPtions
                    List<Integer> selectedIndices = psequipmentType_multiSelect_Spinner.getSelectedIndicies();
                    psequipmenttypeString = Utils.getSelectedEquipmentTypes(getActivity(), selectedIndices);

                if (psequipmenttypeString != null && psequipmenttypeString.contains(";")) {
                    psequipmenttypeString = psequipmenttypeString.replace( AppConstants.TOKEN_DELIMETER, ",");
                } else if (psequipmenttypeString == null || psequipmenttypeString.length() == 0) {
                    psequipmenttypeString = AppConstants.EMPTY_STRING;
                }
                 
                 
                 // Selected Trailer OPtions
                 pstraileroptionString = pstrailerOptions_multiSelect_Spinner.getSelectedItemsAsString();
                    
                if (pstraileroptionString != null && pstraileroptionString.contains(";")) {
                    pstraileroptionString = pstraileroptionString.replace( AppConstants.TOKEN_DELIMETER, ",");
                } else if (pstraileroptionString == null    || pstraileroptionString.length() == 0) {
                    pstraileroptionString = AppConstants.EMPTY_STRING;
                }
                 
                // Selected Equipment OPtions for posting
                ppequipmentTypeStr = ppequipmentType_multiSelect_Spinner.getSelectedItemsAsString();
                  // Selected Trailer OPtions for posting
                 pptrailerOptionStr = pptrailerOptions_multiSelect_Spinner.getSelectedItemsAsString();
                 
                if (psloadsizefull.isChecked()) {
                    psloadsize = 2;
                    psloadtypeString="Full";
                } else if (psloadsizeltl.isChecked()) {
                    psloadsize = 1;
                    psloadtypeString="Partial";
                } else {
                    psloadsize = 3;
                    psloadtypeString="All";
                }
                
                
                
                

                if (pploadsizefull.isChecked()) {
                    pploadsize = 1;
                } else if (pploadsizeltl.isChecked()) {
                    pploadsize = 2;
                }
                origincountryindex = countryspinner.getSelectedItemPosition();
                //Log.d("ITSPROFILEACTIVITY", String.valueOf(origincountryindex));
              if(profilehomebase.isEmpty()){
                  validated=true;
              }else if  (profilehomebase.contains(",")&& profilehomebase.replaceAll("[^,]","").length()>1) {
                  validated=false;
                }else if (!profilehomebase.contains(",") &&!Utils.searchstate(getActivity(),profilehomebase,origincountryindex)) {
                      validated=false;
                }else{
                    int count=0;
                    StringTokenizer st = new StringTokenizer(profilehomebase, ",");
                    while (st.hasMoreElements()) {
                        String searchdest = (String) st.nextElement();
                        searchdest = searchdest.trim();
                        if (!searchdest.matches("^[a-zA-Z\\s]+")) {
                            validated = false;
                        }else if (count==0 && !Utils.searchstate(getActivity(),searchdest,origincountryindex)) {
                            destinationcity=searchdest;
                        }else{
                            if (!Utils.searchstate(getActivity(),searchdest,origincountryindex)) {
                                validated=false;
                                //validationErrorMessage+=AppConstants.ORIGIN_NOTFOUND;
                            }else{
                                destinationstate=Utils.statevalue;
                            }
                        }
                    count++;
                    }
                }
              
                pssorttypeindex = pssortOption.getSelectedItemPosition();
                String sorttype[] = getResources().getStringArray( R.array.sorttype_array);
                pssortypeString = sorttype[pssorttypeindex];
                pshoursindex = pshoursold.getSelectedItemPosition();

                // storing string resources into Array
                String hoursoldarray[] = getResources().getStringArray( R.array.hoursold_array);
                pshoursoldString = hoursoldarray[pshoursindex];
                if (pshoursoldString.equals("Any")) {
                    pshoursoldString = "0";
                }
              
              
                length=Utils.decimalFormat(profileLength.getText().toString());
                ratepermile=Utils.decimalFormat(profileRatePerMile.getText().toString());
                width=Utils.decimalFormat(profileWidth.getText().toString());
                weight=Utils.weightRounding(profileWeight.getText().toString());
                mindist=Utils.mindistanceRounding(profileMinDist.getText().toString());
                
                 if (validated) {
                    String origincounrty="";
                    String originstate ="";
                    String origincity="";
                    String searchradius=String.valueOf(psradius);
                    String md="";
                    originareaaselectedcode="";
                    destareaselectedcode="";
                    destinationcountry=Utils.getCountryForAreaSelected(getActivity(),origincountryindex);
                    session.createsearchProfile(origincountryindex, profilehomebase, psequipmentTypeStr, psradius, pstraileroptionString, psloadsize, pshoursindex, pssorttypeindex);
                    destinationcity=Utils.checkIsNullforPosts(destinationcity);
                        
                     DataModel fav = new DataModel(origincounrty, originstate,origincity, destinationcountry, destinationcity,
                                destinationstate, searchradius, psequipmenttypeString,"", pstraileroptionString, pshoursoldString, psloadtypeString,
                                pssortypeString, originareaaselectedcode,destareaselectedcode, originareaaselected,destareaselected, md);
                     
                
                     
                    Gson gson = new Gson();
                    String json = gson.toJson(fav);
                        //System.out.println("json.tostring------->" + json);
                     session.updateSearchprofileSearch(json);   
                     session.createpostingProfile(ppequipmentTypeStr, ppradius, pptrailerOptionStr, pploadsize, width, length, weight, mindist, ratepermile);
                    
                     profileLength.setText(length);
                     profileRatePerMile.setText(ratepermile);
                     profileWidth.setText(width);
                     profileWeight.setText(weight);
                     profileMinDist.setText(mindist);
                     
                    Toast.makeText(getActivity(), "Profile was saved successfully!",Toast.LENGTH_SHORT).show();
                //  System.out.println("profileloadsize when saving ----"+ profileloadsize);
                      Tracker t = ((ITSApplication)getActivity(). getApplication()).getTracker(ITSApplication.TrackerName.APP_TRACKER);
                    
                        t.send(new HitBuilders.EventBuilder().setCategory(getString( R.string.UICategory))
                                .setAction(getString( R.string.UIAction)).setLabel("Profile Save Button").build());
                    
                } else {
                     validated=true;
                    // validationError+="\nor "+AppConstants.INVALID_SERACHFORMAT;
                     Utils.showAlertMessage(AppConstants.PROFILEINVALID_SERACHFORMAT, getActivity());
                     validationError="";
                    //Toast.makeText(getActivity(),AppConstants.INVALID_SERACHFORMAT, Toast.LENGTH_SHORT).show();
                }
            }
        });
        
    
        
        //search radius SeekBarChangeListener in search preference section
        psradiusbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar,int progress, boolean fromUser) {
                int yourStep = 25;
                psradius = ((int)Math.round(progress/yourStep ))*yourStep;
                psradius+=25;
                seekBar.setProgress(progress);
                psradiusValue.setText(String.valueOf(psradius) + " mi");
                
            }
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        
        //posting radius SeekBarChangeListener in post preference section
        ppradiusbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar,int progress, boolean fromUser) {
                int yourStep = 25;
                ppradius = ((int)Math.round(progress/yourStep ))*yourStep;
                ppradius+=25;
                seekBar.setProgress(progress);
                ppradiusValue.setText(String.valueOf(ppradius) + " mi");
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        
        
        
        
    
        // Set Adapter for origincountry
        ArrayAdapter<CharSequence> originDataAdapter = ArrayAdapter.createFromResource(getActivity(),R.array.profilecountry_array,
                        android.R.layout.simple_spinner_dropdown_item);
        originDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        countryspinner.setAdapter(originDataAdapter);
        countryspinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,int arg2, long arg3) {
                origincountryindex = arg0.getSelectedItemPosition();
                // storing string resources into Array
                String origincountryarray[] = getResources().getStringArray(R.array.profilecountry_array);
                String origincountryforplaces[] = getResources().getStringArray(R.array.googlecountry_array);
                country = origincountryarray[origincountryindex];
                countryvalue = origincountryforplaces[origincountryindex];
                 if (origincountryindex==prefs.getInt("country", 0) ){
                        if (homeBase.getText().toString().equals(profilehomebase)) {
                            homeBase.setText(profilehomebase);
                        } else
                            homeBase.setText("");
                    } else
                        homeBase.setText("");
                
                    updategooleplaces(countryvalue,origincountryindex);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
        
        if(null != equipString && equipString.length > 0){
            psequipmentType_multiSelect_Spinner.setSelectType(AppConstants.SELECT_LIMIT);
            psequipmentType_multiSelect_Spinner.setItems(equipString);
            psequipmentType_multiSelect_Spinner.setmDialogCancelListener(this);
           
        }
        
        
        if(null != trailerOptions && trailerOptions.length > 0){
            pstrailerOptions_multiSelect_Spinner.setSelectType(AppConstants.SELECT_MAX);
            pstrailerOptions_multiSelect_Spinner.setItems(trailerOptions);
            pstrailerOptions_multiSelect_Spinner.setmDialogCancelListener(this);
        }
        
        
        String[] ppEquipmentOptions = getResources().getStringArray(R.array.posttype_array);
        if (null != ppEquipmentOptions && ppEquipmentOptions.length > 0) {
            ppequipmentType_multiSelect_Spinner.setSelectType(AppConstants.SELECT_ONE);
            ppequipmentType_multiSelect_Spinner.setItems(ppEquipmentOptions);
            ppequipmentType_multiSelect_Spinner.setmDialogCancelListener(this);
        }
        
        if(null != pptrailerOptions && pptrailerOptions.length > 0){
            pptrailerOptions_multiSelect_Spinner.setSelectType(AppConstants.SELECT_MAX);
            pptrailerOptions_multiSelect_Spinner.setItems(pptrailerOptions);
            pptrailerOptions_multiSelect_Spinner.setmDialogCancelListener(this);
        }
        
        // Set Adapter for HoursOld
        ArrayAdapter<CharSequence> hourOldDataAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.hoursold_array,
                        android.R.layout.simple_spinner_dropdown_item);
        hourOldDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        pshoursold.setAdapter(hourOldDataAdapter);
        pshoursold.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
                pshoursindex = arg0.getSelectedItemPosition();
                
                // storing string resources into Array 
                String hoursoldarray[] = getResources().getStringArray(R.array.hoursold_array);
                pshoursoldString=hoursoldarray[pshoursindex];
                if (pshoursoldString.equals("Any")) {
                    pshoursoldString="0";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                
                
            }
        });

        // Set Adapter for sort options
        ArrayAdapter<CharSequence> sortTypeDataAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.sorttype_array_regular,
                        android.R.layout.simple_spinner_dropdown_item);
        sortTypeDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        pssortOption.setAdapter(sortTypeDataAdapter);
        
        pssortOption.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
                pssorttypeindex = arg0.getSelectedItemPosition();

                // storing string resources into Array 
                String sorttype[] = getResources().getStringArray(R.array.sorttype_array);
                pssortypeString=sorttype[pssorttypeindex];

                //Toast.makeText(getActivity(), "You have selected : " +sorttype[sorttypeindex], Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }
    
    /*********************************************************
     *Description: Updates  homebase autocomplete for PlacesAutoCompleteAdapter with new googlecountry component and country to index to search the entered
     *value exists in existing state and abr list(Found in values folder)
     * 
     * 
     */
    
    private void updategooleplaces(String googlecountry,int countryindex) {

        adapter= new PlacesAutoCompleteAdapter(getActivity(),googlecountry,countryindex, R.layout.list_item);

        homeBase.setAdapter(adapter);
        
    }
    public void saveProfile(View v) {
        profilehomebase = homeBase.getText().toString();
    }
    
    

    
    /*********************************************************
     *Description:Expands the views that is embedded within the layout
     * 
     * 
     */
    
    public void expand(final View v) {
        v.measure(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        final int measuredHeight = v.getMeasuredHeight();
     
        v.getLayoutParams().height = 0;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1  ? LayoutParams.WRAP_CONTENT
                        : (int)(measuredHeight * interpolatedTime);
                v.requestLayout();
            }
     
            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };
     
        // 1dp per milliseconds
        a.setDuration((int)(measuredHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }
        
    /*********************************************************
     *Description:collapses the views that is embedded within the layout
     * 
     * 
     */
    public void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();
     
        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if(interpolatedTime == 1){
                    v.setVisibility(View.GONE);
                }else{
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }
     
            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };
     
        // 1dp per milliseconds
        a.setDuration((int)(initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }   
    
    @Override
    public void onDialogCancelled(int spinnerType,int type,boolean flag) {
        if(spinnerType == AppConstants.SPINNER_TYPE_1){
            if(type == AppConstants.EQUIPMENT_TYPE && flag == false){
                psequipmentType_multiSelect_Spinner.setVisibility(View.GONE);
                txtPSEquipment.setVisibility(View.VISIBLE);
            }else if(type == AppConstants.TRAILER_OPTION && flag == false){
                pstrailerOptions_multiSelect_Spinner.setVisibility(View.GONE);
                psTxtTrailerOption.setVisibility(View.VISIBLE);
                
            }
        }else if (spinnerType == AppConstants.SPINNER_TYPE_2){
            if(type == AppConstants.EQUIPMENT_TYPE && flag == false){
                ppequipmentType_multiSelect_Spinner.setVisibility(View.GONE);
                ppTxtEquipmentType.setVisibility(View.VISIBLE);
            }
            if(type == AppConstants.TRAILER_OPTION && flag == false){
                pptrailerOptions_multiSelect_Spinner.setVisibility(View.GONE);
                ppTxtTrailerOption.setVisibility(View.VISIBLE);
                
            }
        }
        
    }
    
}