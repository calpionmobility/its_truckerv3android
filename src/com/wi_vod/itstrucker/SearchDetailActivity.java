/******************************************************************
 * SearchDetail.java
 * 
 * Description: Class for the Search Detail Activity
 * 
 * @author: shaik Chanu 
 * 
 * @version: 1.0
 * 
 * History:
 * 
 * 
 ******************************************************************
 */
package com.wi_vod.itstrucker;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.net.ssl.HttpsURLConnection;


import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.xmlpull.v1.XmlPullParserException;

import android.R.color;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.text.style.SuperscriptSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SimpleAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.BufferType;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.wi_vod.itstrucker.R;
import com.wi_vod.itstrucker.SearchResultActivty.HttpServiceListener;
import com.wi_vod.itstrucker.model.SearchDetailLoadModel;
import com.wi_vod.itstrucker.model.XMLSearchLoadDetailParser;

public class SearchDetailActivity extends ActionBarActivity implements OnClickListener  
{
   
	private static String TAG="SEARCHDETAILACTIVITY";
    private ProgressDialog progressDialog; // Progress bar
	private ProgressDialog progDialog;
	private static byte[] buff = new byte[1024];
	String retval, ret, response_str;

	private String LoadId,errormsg;
	private TextView loaddetail;
	ArrayList<SearchDetailLoadModel> detailitems = new ArrayList<SearchDetailLoadModel>();
	SearchDetailLoadModel items;
	private SessionManager session;
    private String bondInfo             = null;
    private String bondAmount           = null;
    private int bondTypeID              = 0;
    private int colourCode              = 0;
    private static final String CR           =  "\n";
    private static final String COMMA        =  ", ";
    private static final String DIAMOND      = "\u2666";
    int bondTypeValue=9999;
	
	TextView handle, detailLoadPostedDate,compnayname,creditstopheader,companyhandle,companycontact,companyphone,companyfax,companyemail,mileage,paymentAmount,creditStop,brokerExperiencefactor,fuelDesk,diamondBroker,startingPoint,
	startingPointdate,companymobile,startingPointtime,destinationPoint,destinationPointdate,destinationPointtime,loadDetails,specialInfo,loaddetailtraileroptions,
	detailloadsize,detailloadweight,avgdaystopay,detailloadlength,detailloadwidth,detailloaddistance,detailloadextrastops,detailEquipmentType,detailloadquantity,detailLoadPostedDateavgdaystopay,expfactor,postrated,miles,brokermc,carriermc,usdot;
	TableLayout loadtable;
	TableRow tablerowtrailer;
	private String days2pay="";
	private String experiencefactor="";
	private  Tracker t;
	private LinearLayout mobilelayout;
	private BroadcastReceiver mRegistrationBroadcastReceiver;
    /*********************************************************
     * Method: onCreate
     * 
     * Description: Called when the Activity is first created
     * 
     * @param: Bundle savedInstanceState - Current State
     * 
     * @returns: Void
     * 
     */
    
    //prevents orientation change from reloading results and crashing app
    @Override 
    public void onConfigurationChanged(Configuration newConfig) { 
      
      super.onConfigurationChanged(newConfig); 
    } 
    
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        Bundle extras;
        Intent intent = getIntent();
        String urlString;
        
        // Set up the layout for the activity
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modifiedsearchdetailscreen);
        UpdateUI();
        
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        session=new SessionManager(SearchDetailActivity.this);

		t = ((ITSApplication) getApplication()).getTracker(ITSApplication.TrackerName.APP_TRACKER);
		t.setScreenName("Load Details Screen");
		t.send(new HitBuilders.AppViewBuilder().build());
        // Create a dialog for a process spinner
        progressDialog = new ProgressDialog(this);
        
        Bundle bundle = getIntent().getExtras();
		if(bundle != null){
			LoadId=bundle.getString("LOADID");
			days2pay=bundle.getString("days2pay");
			experiencefactor=bundle.getString("experiencefactor");
		}
		//System.out.println("LoadId-------------->"+LoadId);
		/*GetSearchItemDetails searchDetails=new GetSearchItemDetails();
		searchDetails.execute();*/
		GetSearchItemDetails();
		mRegistrationBroadcastReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Utils.logoutuser(AppConstants.LOGOUTUSER_ERRORMSG, SearchDetailActivity.this);
			}
		};
        // Create the URL command string
    }
    
    
	

	private void UpdateUI() {
		
		handle= (TextView) findViewById(R.id.searchhandle);
		mobilelayout= (LinearLayout) findViewById(R.id.mobilenolayout);
		companycontact = (TextView) findViewById(R.id.companycontact);
		companymobile=(TextView)findViewById(R.id.companyMobile);
		companymobile.setOnClickListener(this);
		companyphone = (TextView) findViewById(R.id.companyPhone);
		compnayname = (TextView) findViewById(R.id.companyname);
		companyphone.setOnClickListener(this);
		
		creditstopheader=(TextView) findViewById(R.id.creditstopheader);
		companyfax = (TextView) findViewById(R.id.companyFax);
		companyemail = (TextView) findViewById(R.id.companyEmail);
		companyemail.setOnClickListener(this);
		fuelDesk=(TextView)findViewById(R.id.fueldesk);
		
		
		brokermc=(TextView)findViewById(R.id.brokermc);
		carriermc=(TextView)findViewById(R.id.carriermc);
		usdot=(TextView)findViewById(R.id.usdot);
		

		
		fuelDesk.setOnClickListener(this);
		mileage = (TextView) findViewById(R.id.mileage);
		//postrated=(TextView)findViewById(R.id.postrated);
		avgdaystopay=(TextView)findViewById(R.id.avgdaystopay);
		//miles=(TextView)findViewById(R.id.miles);
		paymentAmount = (TextView) findViewById(R.id.paymentamount);
		creditStop = (TextView) findViewById(R.id.creditstop);
		brokerExperiencefactor= (TextView) findViewById(R.id.experiencefactor);

		diamondBroker = (TextView) findViewById(R.id.diamondbroker);
		startingPoint = (TextView) findViewById(R.id.startingpoint);
		startingPointdate = (TextView) findViewById(R.id.startingpointdate);
		startingPointtime= (TextView) findViewById(R.id.startingpointtime);
		destinationPoint = (TextView) findViewById(R.id.destinationpoint);
		destinationPointdate = (TextView) findViewById(R.id.destinationpointdate);
		destinationPointtime= (TextView) findViewById(R.id.destinationpointtime);
	//	loadDetails = (TextView) findViewById(R.id.loaddetails);
		specialInfo = (TextView) findViewById(R.id.specialinfo);
		loaddetailtraileroptions= (TextView) findViewById(R.id.loadtraileroptions);
		loadtable= (TableLayout) findViewById(R.id.loadtableLayout);
		tablerowtrailer= (TableRow) findViewById(R.id.tableRow4);
		detailloadsize= (TextView) findViewById(R.id.loadsize);
		detailloadweight= (TextView) findViewById(R.id.loadweight);
		detailloadlength= (TextView) findViewById(R.id.loadlength);
		detailloadwidth= (TextView) findViewById(R.id.loadwidth);
		detailloaddistance= (TextView) findViewById(R.id.loaddistance);
		detailloadextrastops= (TextView) findViewById(R.id.loadextrastops);
		detailEquipmentType= (TextView) findViewById(R.id.loadequipmenttype);
		detailloadquantity= (TextView) findViewById(R.id.loadquantity);
		detailLoadPostedDate= (TextView) findViewById(R.id.loaddateposted);
		
		
		
		SpannableStringBuilder cs = new SpannableStringBuilder("CreditStopSM");
		cs.setSpan(new SuperscriptSpan(), 10, 12, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		cs.setSpan(new RelativeSizeSpan(0.75f), 10, 12, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		
		creditstopheader.setText(cs);

		
		//creditstopheader.setText(creditstopheader.getText()+""+Html.fromHtml("5x<sup><small>2</small></sup>\t"));
	}

	
	
	
	private void GetSearchItemDetails() {
		
		String envolpe = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\""
				+" xmlns:v12=\"http://webservices.truckstop.com/v12\""
				+" xmlns:web=\"http://schemas.datacontract.org/2004/07/WebServices\""
				+" xmlns:web1=\"http://schemas.datacontract.org/2004/07/WebServices.Searching\""+">";
				String body = envolpe
						+"\n<soapenv:Header/>\n"
						+"<soapenv:Body>\n"
						+ " <v12:GetLoadSearchDetailResult>\n"
						+" <v12:detailRequest>\n"
						+"<web:IntegrationId>"+session.getUserIntegrationID()+"</web:IntegrationId>\n"  
						
						+"<web:Password>"+AppConstants.PASSWORD+"</web:Password>\n"
						+"<web:UserName>"+AppConstants.USERNAME+"</web:UserName>\n"
						+"<web1:LoadId>"+LoadId+"</web1:LoadId>\n"
						+"</v12:detailRequest>\n"
						+"</v12:GetLoadSearchDetailResult>\n"
						+"</soapenv:Body>\n"
						+"</soapenv:Envelope>\n";
				
				//System.out.println("BODY VALUE----" + body);
				 String SOAPACTION="http://webservices.truckstop.com/v12/ILoadSearch/GetLoadSearchDetailResult";
		         
		         HttpServiceListener listener = new HttpServiceListener();
					WebServiceConnectionService serv = new WebServiceConnectionService(SearchDetailActivity.this,body,
							SOAPACTION, listener,AppConstants.DETAILLOADS_PROGRESS);
					serv.execute(AppConstants.SEARCHLOADS_TEST_URL);         
		// TODO Auto-generated method stub
		
	}
	
	 protected class HttpServiceListener implements ConnectionServiceListener {

		@Override
		public void onServiceComplete(String response) {
			// TODO Auto-generated method stub
			if (response.equals(AppConstants.INTERNET_CONNECTION_ERROR)) {
				Utils.showErrorMessage(AppConstants.INTERNET_CONNECTION_ERROR, SearchDetailActivity.this);
				/*
				 * Toast.makeText(SearchDetailActivity.this,AppConstants.
				 * INTERNET_CONNECTION_ERROR,Toast.LENGTH_LONG).show();
				 * finish();
				 */
			} else if (response.equals(AppConstants.SERVER_CONNECTION_ERROR)) {
				Utils.showErrorMessage(AppConstants.SERVER_CONNECTION_ERROR, SearchDetailActivity.this);

				/*
				 * Toast.makeText(SearchDetailActivity.this,AppConstants.
				 * SERVER_CONNECTION_ERROR, Toast.LENGTH_LONG).show(); finish();
				 */
			}else if (response.equals(AppConstants.PLAYSERVICES_NOTFOUND)) {
               // Utils.showErrorMessage(AppConstants.PLAYSERVICES_NOTFOUND,SearchDetailActivity.this);
				Utils.updateGoogleplay(SearchDetailActivity.this);
               
            } else {
				boolean loadsucess = false;
				try {
					loadsucess = XMLSearchLoadDetailParser.parsexml(response);
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if (loadsucess) {
					//System.out.println("retval IN IF-----"+ response_str);
					items=XMLSearchLoadDetailParser.getSearchItems();
					//System.out.println("items in details---->"+items.toString());
					compnayname.setText(Utils.checkIsNull(items.getTruckCompanyName()));
					companycontact.setText(Utils.checkIsNull(items.getPointOfContact()));
					if (Utils.checkIsNull(items.getPointOfContactPhone()).equals("-")) {
						mobilelayout.setVisibility(View.GONE);
					}else {
						companymobile.setText(Utils.checkIsNull(items.getPointOfContactPhone()));
					}
					
					
					
					
					companyphone.setText(Utils.checkIsNull(items.getTruckCompanyPhone()));
					companyfax.setText(Utils.checkIsNull(items.getTruckCompanyFax()));
					companyemail.setText(Utils.checkIsNull(items.getTruckCompanyEmail()));
					mileage.setText(Utils.checkIsNull(items.getMileage()));
					if (Utils.checkIsNull(items.getPaymentAmount()).equals("-")) {
						paymentAmount.setText(Utils.checkIsNull(items.getPaymentAmount()));
					}else {
						paymentAmount.setText("$"+Utils.checkIsNull(items.getPaymentAmount()));
					}
				
					
					if(Utils.checkIsNull(days2pay).equals("----")||Utils.checkIsNull(days2pay).equals("-"))
						avgdaystopay.setText("-");
					else
						avgdaystopay.setText(Utils.checkIsNull(days2pay));
						
					
					
					
					
				
				
					if(	brokerExperiencefactor.getText().toString()==null & brokerExperiencefactor.getText().toString().trim().length()==0)
						brokerExperiencefactor.setText("-");
					else
						brokerExperiencefactor.setText(Utils.checkIsNull(experiencefactor));
			      // 	brokerExperiencefactor.setText(brokerFactor);
					
					
					
					if (Utils.checkIsNull(items.getFuelCost()).equals("-")) {
						fuelDesk.setText(AppConstants.DETAIL_MOREINFO);
					}else {
						fuelDesk.setText(Utils.checkIsNull(items.getFuelCost()));
					}
					
					
					brokermc.setText(Utils.checkIsNullforPosts(items.getMcNumber()));
					carriermc.setText(Utils.checkIsNullforPosts(items.gettMCNumber()));
					usdot.setText(Utils.checkIsNullforPosts(items.getDotNumber()));
					
					
					
					
					startingPoint.setText(Utils.checkIsNull(items.getOriginCity()+","+items.getOriginState()));
					
					startingPointdate.setText(Utils.checkIsNull(items.getPickupDate()));
					startingPointtime.setText(Utils.checkIsNull(items.getPickupTime()));
					destinationPoint.setText(Utils.checkIsNull(items.getDestinationCity()+","+Utils.checkIsNull(items.getDestinationState())));
					
					destinationPointdate.setText(Utils.checkIsNull(items.getDeliveryDate()));
					destinationPointtime.setText(Utils.checkIsNull(items.getDeliveryTime()));
					specialInfo.setText(Utils.checkIsNull(items.getSpecInfo()));
					loaddetailtraileroptions.setText(Utils.checkIsNull(items.getTrailerOptionType()));
					String newDateString = null;
					if (!Utils.checkIsNull(items.getEntered()).equals("-")){
						newDateString=Utils.detailsDateFormat(items.getEntered());
						detailLoadPostedDate.setText(newDateString);
					}else{
						detailLoadPostedDate.setText(AppConstants.EMPTY_STRING);
					}
					
					detailloadquantity.setText(Utils.checkIsNull(items.getQuantity()));
					detailloadsize.setText(Utils.checkIsNull(items.getLoadType()));
					 if (!Utils.checkIsNull(items.getWeight()).equals("-")) {
						 detailloadweight.setText(items.getWeight()+getResources().getString(R.string.unit_lbs));
					}else{
						 detailloadweight.setText(Utils.checkIsNull(items.getWeight()));
					}
					 
					// Log.i(TAG, "length is ----->"+items.getLength());
					 
					 if (!Utils.checkIsNull(items.getLength()).equals("-")) {
						 detailloadlength.setText(items.getLength()+getResources().getString(R.string.unit_feet));
					}else{
						detailloadlength.setText(Utils.checkIsNull(items.getLength()));
					}
					 
					 
					 
					 if (!Utils.checkIsNull(items.getWidth()).equals("-")) {
						 detailloadwidth.setText(items.getWidth()+getResources().getString(R.string.unit_feet));
					}else{
						 detailloadwidth.setText(Utils.checkIsNull(items.getWidth()));
					}
					
					 if (!Utils.checkIsNull(items.getDistance()).equals("-")) {
						 detailloaddistance.setText(items.getDistance()+getResources().getString(R.string.unit_miles));
					}else{
						detailloaddistance.setText(Utils.checkIsNull(items.getDistance()));
					}
					
					
					detailloadextrastops.setText(Utils.checkIsNull(items.getStops()));
					detailEquipmentType.setText(Utils.checkIsNull(items.getEquipmenttypes().getDescription()));
					
					
					if (!Utils.checkIsNull(items.getBondTypeID()).equals("-")) {
						bondTypeValue =Integer.valueOf(items.getBondTypeID());
					}
					 if (bondTypeValue== 9999) {
		                    bondAmount = "";
		                    bondInfo   = "-";
		                    colourCode = Color.BLACK;
		                }else {
		                    if (bondTypeValue > 1000)
		                    {
		                        // Set the colour to gray
		                        colourCode = Color.GRAY;
		                    }
		                    else if (bondTypeValue > 500)
		                    {
		                        // Set the colour to blue
		                        colourCode = Color.BLUE;
		                    }
		                    else
		                    {
		                        // Set the colour to Red
		                        colourCode = Color.RED;
		                    }
		                    
		                    // Get the amount of the bond
		                    int bondAmountID = bondTypeValue % 100;
		                    
		                    if (bondAmountID == 5)
		                    {
		                        bondAmount = "$10K ";
		                        bondInfo   = DIAMOND;
		                    }
		                    else if (bondAmountID == 4)
		                    {
		                        bondAmount = "$25K ";
		                        bondInfo   = DIAMOND +DIAMOND;
		                    }
		                    else if (bondAmountID == 3)
		                    {
		                        bondAmount = "$50K ";
		                        bondInfo   = DIAMOND +DIAMOND + DIAMOND;
		                    }
		                    else if (bondAmountID == 2)
		                    {
		                        bondAmount = "$100K ";
		                        bondInfo   = DIAMOND +DIAMOND + DIAMOND + DIAMOND;
		                    }
		                    else if (bondAmountID == 1)
		                    {
		                        bondAmount = "$250K ";
		                        bondInfo   = DIAMOND +DIAMOND + DIAMOND + DIAMOND + DIAMOND;
		                    }else {
		                        bondAmount = "";
		                        bondInfo   = "-";
		                    }
		                }
		                
					 if (Utils.checkIsNull(items.getBond()).equals("-")||Utils.checkIsNull(items.getBond()).equals("0")) {
						 diamondBroker.setText("-");
					}else{
						bondAmount="$"+items.getBond()+"K";
						int start = bondAmount.length();
		                int end = start + bondInfo.length();
		                        
		                // User Spannable text view so we can change the colour or the text
		                diamondBroker.setText(bondAmount + bondInfo ,
		                                        BufferType.SPANNABLE);
		                Spannable spannableInfoView = (Spannable)diamondBroker.getText();
		                spannableInfoView.setSpan(new ForegroundColorSpan(colourCode), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
					}
				}else{
					errormsg=XMLSearchLoadDetailParser.geterrormmsg();
					//System.out.println("errormsg IN IF-----"+ errormsg);
					Utils.showErrorMessage(errormsg, SearchDetailActivity.this);
				}
				
			}
			
			
		}

		@Override
		public void onServiceComplete(String response, String reference) {
			// TODO Auto-generated method stub
			
		}
		 
	 }
	 

/*	private class GetSearchItemDetails extends AsyncTask<Void, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			progDialog = new ProgressDialog(SearchDetailActivity.this);
			progDialog.setMessage(AppConstants.DETAILLOADS_PROGRESS);
			progDialog.setCancelable(false);
			progDialog.show();
			if (SearchDetailActivity.this.getResources().getConfiguration().orientation==Configuration.ORIENTATION_PORTRAIT) {
				SearchDetailActivity.this.setRequestedOrientation(Configuration.ORIENTATION_PORTRAIT);
			}else{
				SearchDetailActivity.this.setRequestedOrientation(Configuration.ORIENTATION_LANDSCAPE);
			}

		}

		@Override
		protected String doInBackground(Void... params) {
			try {
				//System.out.println(Utils.isInternetAvailable(SearchDetailActivity.this));
				if (Utils.isInternetAvailable(SearchDetailActivity.this)) {
					// Log.i("IN BACKGROUND", "doInBackground");
					
					HttpPost post = new HttpPost(AppConstants.SEARCHLOADS_TEST_URL);
					//System.out.println("URL ----" + AppConstants.SEARCHLOADS_TEST_URL);
					int timeoutConnection = 10000;
					HttpParams httpParameters = new BasicHttpParams();
					HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
					// Set the default socket timeout (SO_TIMEOUT) 
					// in milliseconds which is the timeout for waiting for data.
					int timeoutSocket = 10000;
					HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
					DefaultHttpClient client = new DefaultHttpClient(httpParameters);
					post.setHeader("Accept-Encoding", "gzip,deflate");
					post.setHeader("Content-Type", "text/xml;charset=UTF-8");
					//SOAPAction: "http://webservices.truckstop.com/v12/ILoadSearch/GetLoadSearchResults"
					post.setHeader("SOAPAction","http://webservices.truckstop.com/v12/ILoadSearch/GetLoadSearchDetailResult");
					post.setHeader("Host", "webservices.truckstop.com");
					post.setHeader("Connection", "Keep-Alive");
					//post.setHeader("Content-Length", "2811"); 
					post.setHeader("User-Agent","Apache-HttpClient/4.1.1 (java 1.5)\\");
					
					
					
					
					
					
					
					
					
					
					 String url = AppConstants.SEARCHLOADS_TEST_URL;
	                   // Log.i(TAG, AppConstants.SEARCHLOADS_TEST_URL);
						URL obj = new URL(url);
						HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
						ITSApplication app = ((ITSApplication) getApplicationContext());
						//ITSApplication app= new ITSApplication();
						if (app.certifcate()==null) {
							return AppConstants.SERVER_CONNECTION_ERROR;
						}
						
						con.setSSLSocketFactory(app.certifcate().getSocketFactory());
						con.setRequestMethod("POST");
						con.setRequestProperty("Accept-Encoding", "gzip,deflate");
						con.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
	                    // SOAPAction:
	                    // "http://webservices.truckstop.com/v12/ILoadSearch/GetLoadSearchResults"
						con.setRequestProperty("SOAPAction","http://webservices.truckstop.com/v12/ILoadSearch/GetLoadSearchDetailResult");
						con.setRequestProperty("Host", "webservices.truckstop.com");
						con.setRequestProperty("Connection", "Keep-Alive");
						con.setRequestProperty("User-Agent", "Apache-HttpClient/4.1.1 (java 1.5)\\");
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					String envolpe = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\""
					+" xmlns:v12=\"http://webservices.truckstop.com/v12\""
					+" xmlns:web=\"http://schemas.datacontract.org/2004/07/WebServices\""
					+" xmlns:web1=\"http://schemas.datacontract.org/2004/07/WebServices.Searching\""+">";
					String body = envolpe
							+"\n<soapenv:Header/>\n"
							+"<soapenv:Body>\n"
							+ " <v12:GetLoadSearchDetailResult>\n"
							+" <v12:detailRequest>\n"
							+"<web:IntegrationId>"+session.getUserIntegrationID()+"</web:IntegrationId>\n"  
							
							+"<web:Password>"+AppConstants.PASSWORD+"</web:Password>\n"
							+"<web:UserName>"+AppConstants.USERNAME+"</web:UserName>\n"
							+"<web1:LoadId>"+LoadId+"</web1:LoadId>\n"
							+"</v12:detailRequest>\n"
							+"</v12:GetLoadSearchDetailResult>\n"
							+"</soapenv:Body>\n"
							+"</soapenv:Envelope>\n";
					
					//System.out.println("BODY VALUE----" + body);
					post.setEntity(new StringEntity(body));

					HttpResponse response = client.execute(post);
					StatusLine statuscode = response.getStatusLine();
					String statuscodes = Integer.toString(statuscode.getStatusCode());
					
					
					
					
					
					 con.setConnectTimeout(AppConstants.SOCKETTIMEOUT_ERROR);
						con.setDoOutput(true);
						DataOutputStream wr = new DataOutputStream(con.getOutputStream());
						//System.out.println("BODY VALUE----" + body);
						wr.writeBytes(body);
						wr.flush();
						wr.close();
				 
						int responseCode = con.getResponseCode();
	                    // System.out.println("Status coded-------" + statuscodes);
						if (responseCode!=200) {
	                        return AppConstants.SERVER_CONNECTION_ERROR;
	                    } else {
	                    	InputStream stream = con.getInputStream();
							String contentEncoding = con.getHeaderField("Content-Encoding");
						if (contentEncoding != null&& contentEncoding.equalsIgnoreCase("gzip")) {
							final InputStream stream1 = new GZIPInputStream(stream);
							// System.out.println("i am here");
							BufferedReader reader = new BufferedReader(new InputStreamReader(stream1));
							String line;
							StringBuilder builder = new StringBuilder();
							while ((line = reader.readLine()) != null) {
								builder.append(line);
							}
							response_str="";
							response_str = builder.toString();
							boolean loadsucess=XMLSearchLoadDetailParser.parsexml(response_str);
							
							if (loadsucess) {
								//System.out.println("retval IN IF-----"+ response_str);
								items=XMLSearchLoadDetailParser.getSearchItems();
								//System.out.println("items in details---->"+items.toString());
								return "success";
							}else{
								errormsg=XMLSearchLoadDetailParser.geterrormmsg();
								//System.out.println("errormsg IN IF-----"+ errormsg);
								return "error";
							}
							
						} else {
							BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
							String line;
							StringBuilder builder = new StringBuilder();
							while ((line = reader.readLine()) != null) {
								builder.append(line);
							}
							String response_str;
							response_str = builder.toString();
							//System.out.println("retval IN ELSE-----"+ response_str);
							boolean loadsucess=XMLSearchLoadDetailParser.parsexml(response_str);
							if (loadsucess) {
								items=XMLSearchLoadDetailParser.getSearchItems();
								//System.out.println("items in details---->"+items.toString());
									return "success";
								}else{
									errormsg=XMLSearchLoadDetailParser.geterrormmsg();
								//	System.out.println("errormsg IN ELSE-----"+ errormsg);
									return "error";
								}
						}
					}

				} else {
					return AppConstants.INTERNET_CONNECTION_ERROR;
				}

			} catch (Exception e) {
				
				e.printStackTrace();
				return AppConstants.SERVER_CONNECTION_ERROR;
			}
			
		}

		protected void onPostExecute(String result) {
			progDialog.dismiss();
			//SearchDetailActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
			if (result.equals("error")) {

				Utils.showErrorMessage(errormsg, SearchDetailActivity.this);
				
				 * Toast.makeText(SearchDetailActivity.this,errormsg,Toast.
				 * LENGTH_LONG).show(); finish();
				 
			} else if (result.equals(AppConstants.INTERNET_CONNECTION_ERROR)) {
				Utils.showErrorMessage(AppConstants.INTERNET_CONNECTION_ERROR, SearchDetailActivity.this);
				
				 * Toast.makeText(SearchDetailActivity.this,AppConstants.
				 * INTERNET_CONNECTION_ERROR,Toast.LENGTH_LONG).show();
				 * finish();
				 
			} else if (result.equals(AppConstants.SERVER_CONNECTION_ERROR)) {
				Utils.showErrorMessage(AppConstants.SERVER_CONNECTION_ERROR, SearchDetailActivity.this);

				
				 * Toast.makeText(SearchDetailActivity.this,AppConstants.
				 * SERVER_CONNECTION_ERROR, Toast.LENGTH_LONG).show(); finish();
				 
			} else {
				
				compnayname.setText(Utils.checkIsNull(items.getTruckCompanyName()));
				companycontact.setText(Utils.checkIsNull(items.getPointOfContact()));
				if (Utils.checkIsNull(items.getPointOfContactPhone()).equals("-")) {
					mobilelayout.setVisibility(View.GONE);
				}else {
					companymobile.setText(Utils.checkIsNull(items.getPointOfContactPhone()));
				}
				
				
				
				
				companyphone.setText(Utils.checkIsNull(items.getTruckCompanyPhone()));
				companyfax.setText(Utils.checkIsNull(items.getTruckCompanyFax()));
				companyemail.setText(Utils.checkIsNull(items.getTruckCompanyEmail()));
				mileage.setText(Utils.checkIsNull(items.getMileage()));
				if (Utils.checkIsNull(items.getPaymentAmount()).equals("-")) {
					paymentAmount.setText(Utils.checkIsNull(items.getPaymentAmount()));
				}else {
					paymentAmount.setText("$"+Utils.checkIsNull(items.getPaymentAmount()));
				}
				
				
				
			if(	avgdaystopay.getText().toString()==null & avgdaystopay.getText().toString().trim().length()==0)
				avgdaystopay.setText("-");
			else
				avgdaystopay.setText(Utils.checkIsNull(days2pay));
				
				
				
				
				if(Utils.checkIsNull(days2pay).equals("----")||Utils.checkIsNull(days2pay).equals("-"))
					avgdaystopay.setText("-");
				else
					avgdaystopay.setText(Utils.checkIsNull(days2pay));
					
				
				
				
				
			
			
				if(	brokerExperiencefactor.getText().toString()==null & brokerExperiencefactor.getText().toString().trim().length()==0)
					brokerExperiencefactor.setText("-");
				else
					brokerExperiencefactor.setText(Utils.checkIsNull(experiencefactor));
		      // 	brokerExperiencefactor.setText(brokerFactor);
				
				
				
				if (Utils.checkIsNull(items.getFuelCost()).equals("-")) {
					fuelDesk.setText(AppConstants.DETAIL_MOREINFO);
				}else {
					fuelDesk.setText(Utils.checkIsNull(items.getFuelCost()));
				}
				
				
				brokermc.setText(Utils.checkIsNullforPosts(items.getMcNumber()));
				carriermc.setText(Utils.checkIsNullforPosts(items.gettMCNumber()));
				usdot.setText(Utils.checkIsNullforPosts(items.getDotNumber()));
				
				
				
				
				startingPoint.setText(Utils.checkIsNull(items.getOriginCity()+","+items.getOriginState()));
				
				startingPointdate.setText(Utils.checkIsNull(items.getPickupDate()));
				startingPointtime.setText(Utils.checkIsNull(items.getPickupTime()));
				destinationPoint.setText(Utils.checkIsNull(items.getDestinationCity()+","+Utils.checkIsNull(items.getDestinationState())));
				
				destinationPointdate.setText(Utils.checkIsNull(items.getDeliveryDate()));
				destinationPointtime.setText(Utils.checkIsNull(items.getDeliveryTime()));
				specialInfo.setText(Utils.checkIsNull(items.getSpecInfo()));
				loaddetailtraileroptions.setText(Utils.checkIsNull(items.getTrailerOptionType()));
				String newDateString = null;
				if (!Utils.checkIsNull(items.getEntered()).equals("-")){
					newDateString=Utils.detailsDateFormat(items.getEntered());
					detailLoadPostedDate.setText(newDateString);
				}else{
					detailLoadPostedDate.setText(AppConstants.EMPTY_STRING);
				}
				
				detailloadquantity.setText(Utils.checkIsNull(items.getQuantity()));
				detailloadsize.setText(Utils.checkIsNull(items.getLoadType()));
				 if (!Utils.checkIsNull(items.getWeight()).equals("-")) {
					 detailloadweight.setText(items.getWeight()+getResources().getString(R.string.unit_lbs));
				}else{
					 detailloadweight.setText(Utils.checkIsNull(items.getWeight()));
				}
				 
				// Log.i(TAG, "length is ----->"+items.getLength());
				 
				 if (!Utils.checkIsNull(items.getLength()).equals("-")) {
					 detailloadlength.setText(items.getLength()+getResources().getString(R.string.unit_feet));
				}else{
					detailloadlength.setText(Utils.checkIsNull(items.getLength()));
				}
				 
				 
				 
				 if (!Utils.checkIsNull(items.getWidth()).equals("-")) {
					 detailloadwidth.setText(items.getWidth()+getResources().getString(R.string.unit_feet));
				}else{
					 detailloadwidth.setText(Utils.checkIsNull(items.getWidth()));
				}
				
				 if (!Utils.checkIsNull(items.getDistance()).equals("-")) {
					 detailloaddistance.setText(items.getDistance()+getResources().getString(R.string.unit_miles));
				}else{
					detailloaddistance.setText(Utils.checkIsNull(items.getDistance()));
				}
				
				
				detailloadextrastops.setText(Utils.checkIsNull(items.getStops()));
				detailEquipmentType.setText(Utils.checkIsNull(items.getEquipmenttypes().getDescription()));
				
				
				if (!Utils.checkIsNull(items.getBondTypeID()).equals("-")) {
					bondTypeValue =Integer.valueOf(items.getBondTypeID());
				}
				
				 if (bondTypeValue== 9999) {
	                    bondAmount = "";
	                    bondInfo   = "-";
	                    colourCode = Color.BLACK;
	                }else {
	                    if (bondTypeValue > 1000)
	                    {
	                        // Set the colour to gray
	                        colourCode = Color.GRAY;
	                    }
	                    else if (bondTypeValue > 500)
	                    {
	                        // Set the colour to blue
	                        colourCode = Color.BLUE;
	                    }
	                    else
	                    {
	                        // Set the colour to Red
	                        colourCode = Color.RED;
	                    }
	                    
	                    // Get the amount of the bond
	                    int bondAmountID = bondTypeValue % 100;
	                    
	                    if (bondAmountID == 5)
	                    {
	                        bondAmount = "$10K ";
	                        bondInfo   = DIAMOND;
	                    }
	                    else if (bondAmountID == 4)
	                    {
	                        bondAmount = "$25K ";
	                        bondInfo   = DIAMOND +DIAMOND;
	                    }
	                    else if (bondAmountID == 3)
	                    {
	                        bondAmount = "$50K ";
	                        bondInfo   = DIAMOND +DIAMOND + DIAMOND;
	                    }
	                    else if (bondAmountID == 2)
	                    {
	                        bondAmount = "$100K ";
	                        bondInfo   = DIAMOND +DIAMOND + DIAMOND + DIAMOND;
	                    }
	                    else if (bondAmountID == 1)
	                    {
	                        bondAmount = "$250K ";
	                        bondInfo   = DIAMOND +DIAMOND + DIAMOND + DIAMOND + DIAMOND;
	                    }else {
	                        bondAmount = "";
	                        bondInfo   = "-";
	                    }
	                }
	                
				 if (Utils.checkIsNull(items.getBond()).equals("-")||Utils.checkIsNull(items.getBond()).equals("0")) {
					 diamondBroker.setText("-");
				}else{
					bondAmount="$"+items.getBond()+"K";
					int start = bondAmount.length();
	                int end = start + bondInfo.length();
	                        
	                // User Spannable text view so we can change the colour or the text
	                diamondBroker.setText(bondAmount + bondInfo ,
	                                        BufferType.SPANNABLE);
	                Spannable spannableInfoView = (Spannable)diamondBroker.getText();
	                spannableInfoView.setSpan(new ForegroundColorSpan(colourCode), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
				}
				 
	                
			}
		}
	}
	
	*/
	
	
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.post, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			finish();

			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	@Override
	 public void onClick(View v) {
	  
	  // TODO Auto-generated method stub
	  switch (v.getId()) {
	  case R.id. companyMobile:
			if (!companymobile.getText().toString().equals("-")) {
				Intent mobileIntent = new Intent(Intent.ACTION_DIAL);
				mobileIntent.setData(Uri.parse("tel:"+ companymobile.getText().toString().trim()));
				//startActivity(mobileIntent);
				pckmanager(mobileIntent);
			}
		   break;

	  case R.id.companyPhone:
			if (!companyphone.getText().toString().equals("-")) {
				Intent callIntent = new Intent(Intent.ACTION_DIAL);
				callIntent.setData(Uri.parse("tel:"+ companyphone.getText().toString().trim()));
				//startActivity(callIntent);
				pckmanager(callIntent);
			}

	   break;
	   
	   
	  case R.id.fueldesk:
			if (fuelDesk.getText().toString().equals(AppConstants.DETAIL_MOREINFO)) {
				Intent callIntent = new Intent(Intent.ACTION_DIAL);
				callIntent.setData(Uri.parse("tel:800-203-2540" ));
				//startActivity(callIntent);
				pckmanager(callIntent);
			}

	   break;
		case R.id.companyEmail:
			Intent intentEmail = new Intent(Intent.ACTION_SEND);
			intentEmail.putExtra(Intent.EXTRA_EMAIL,
					new String[] { companyemail.getText().toString() });
			intentEmail.putExtra(Intent.EXTRA_SUBJECT, "");
			intentEmail.putExtra(Intent.EXTRA_TEXT, "");
			intentEmail.setType("message/rfc822");

			PackageManager packageManager = getPackageManager();
			List<ResolveInfo> activities = packageManager
					.queryIntentActivities(intentEmail, 0);
			boolean isIntentSafe = activities.size() > 0;

			try {
				if (isIntentSafe) {
					startActivity(Intent.createChooser(intentEmail,
							"Choose an email provider :"));
					//Log.i("Finished sending email...", "");
				} else {
					Toast.makeText(SearchDetailActivity.this,
							"There is no email client installed.",
							Toast.LENGTH_SHORT).show();
				}
			} catch (android.content.ActivityNotFoundException ex) {
				Toast.makeText(SearchDetailActivity.this,
						"There is no email client installed.",
						Toast.LENGTH_SHORT).show();
			}

			break;
		}
	}
	public void pckmanager(Intent intent){
		PackageManager packageManager = getPackageManager();
		List<ResolveInfo> activities = packageManager
				.queryIntentActivities(intent, 0);
		boolean isIntentSafe = activities.size() > 0;

		try {
			if (isIntentSafe) {
				startActivity(intent);
			} else {
				Toast.makeText(SearchDetailActivity.this,
						"There is no dial pad installed.",
						Toast.LENGTH_SHORT).show();
			}
		} catch (android.content.ActivityNotFoundException ex) {
			Toast.makeText(SearchDetailActivity.this,
					"There is no dial pad installed.",
					Toast.LENGTH_SHORT).show();
		}

	}
	  @Override
		public void onResume() {
			// TODO Auto-generated method stub
			super.onResume();
			Utils.appcameforeground(SearchDetailActivity.this);
			LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter(Utils.LOGOUT_USER));
			
		}

		@Override
		public void onPause() {
			LocalBroadcastManager.getInstance(this).unregisterReceiver( mRegistrationBroadcastReceiver);
			
			super.onPause();
		}
		
		
		 @Override
		    protected void onStop() {
		    	//session.setappbackground(Utils.isAppIsInBackground(SearchMain.this));
		    	Utils.appwentbackground(SearchDetailActivity.this);
				//System.out.println("onStop called");
		    	// TODO Auto-generated method stub
		    	super.onStop();
		    }
}

