package com.wi_vod.itstrucker;

public interface OnDialogCancelListener {
    
    public void onDialogCancelled(int spinnerType,int type,boolean count);

}
