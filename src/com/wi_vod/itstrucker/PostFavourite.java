package com.wi_vod.itstrucker;

import java.util.ArrayList;


import com.wi_vod.itstrucker.Favourite.CustomfavAdapter;

import com.wi_vod.itstrucker.Favourite.CustomfavAdapter.ViewHolder;
import com.wi_vod.itstrucker.model.DataModel;
import com.wi_vod.itstrucker.model.DatabaseHandler;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class PostFavourite extends ActionBarActivity {
	private static final String TAG = "PostFavourite.CLASS";
	private ListView favList;
	private Resources res;
	ArrayList<DataModel> fav;
	CustomfavAdapter adapter;
	DatabaseHandler db;
	static int checks = 0;
	boolean showdelete;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.favourite);
		updateUI();
		res = getResources();
		db = new DatabaseHandler(this);
		if (db.getpostfavouritesCount() > 0) {
			fav = db.getAllpostfav();
			adapter = new CustomfavAdapter(PostFavourite.this,fav, res);
			favList.setAdapter(adapter);
		}else {
			Toast.makeText(PostFavourite.this, AppConstants.NO_FAVORITE, Toast.LENGTH_SHORT).show();
			finish();
		}
	}
	public void updateUI() {
		favList = (ListView) findViewById(R.id.favlist);
	}
	/*********
	 * Adapter class extends with BaseAdapter and implements with
	 * OnClickListener
	 ************/
	public class CustomfavAdapter extends BaseAdapter implements OnClickListener {

		/*********** Declare Used Variables *********/
		private Activity activity;
		private ArrayList<DataModel> data;
		private LayoutInflater inflater = null;
		public Resources res;
		DataModel items = null;
		int i = 0;

		/************* CustomAdapter Constructor *****************/
		public CustomfavAdapter(Activity a,ArrayList<DataModel> d,Resources resLocal) {

			/********** Take passed values **********/
			activity = a;
			data = d;
			res = resLocal;

			/*********** Layout inflator to call external xml layout () ***********/
			inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		}

		/******** What is the size of Passed Arraylist Size ************/
		public int getCount() {

			if (data.size() <= 0)
				return 1;
			return data.size();
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		/********* Create a holder Class to contain inflated xml file elements *********/
		public class ViewHolder {

			public TextView favouriteText1;
			public TextView favouriteText2;
			public TextView favouriteText3;
			public CheckBox cb;

		}

		/******
		 * Depends upon data size called for each row , Create each ListView row
		 *****/
		public View getView(final int position, View convertView,ViewGroup parent) {

			View vi = convertView;
			ViewHolder holder;

			if (convertView == null) {

				/****** Inflate tabitem.xml file for each row ( Defined below ) *******/
				vi = inflater.inflate(R.layout.favourite_row, null);

				/****** View Holder Object to contain tabitem.xml file elements ******/

				holder = new ViewHolder();
				holder.favouriteText1 = (TextView) vi.findViewById(R.id.favouriteText1);
				holder.favouriteText2 = (TextView) vi.findViewById(R.id.favouriteText2);
				holder.favouriteText3 = (TextView) vi.findViewById(R.id.favouriteText3);
				
				holder.cb = (CheckBox) vi.findViewById(R.id.favcopycheck);

				/************ Set holder with LayoutInflater ************/
				vi.setTag(holder);
			} else
				holder = (ViewHolder) vi.getTag();

			if (data.size() <= 0) {
				holder.favouriteText1.setText("No Data");

			} else {
				/***** Get each Model object from Arraylist ********/
				items = null;
				items = (DataModel) data.get(position);

				/************ Set Model values in Holder elements ***********/
				
				holder.favouriteText1.setText("From:" + Utils.checkIsNull(items.getOrigincity()) + ", "+ Utils.checkIsNull(items.getOriginstate())+ ", "+ Utils.checkIsNull(items.getSearchradius()));
				holder.favouriteText2.setText("To:"+ Utils.checkIsNull(items.getDestinationcity()) + ", "+ Utils.checkIsNull(items.getDestinationstate())+ ", "+ Utils.checkIsNull(items.getSearchradius()));
				holder.favouriteText3.setText("Equipment Type:" + Utils.checkIsNull(items.getEquipmenttype()));
			

				holder.cb.setTag(position);
				boolean setselcted=true;
				if (data.get(position).getIsselected()==1) {
					setselcted=true;
				}else{
					setselcted=false;
				}
				
				holder.cb.setChecked(setselcted);
				holder.cb.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						CheckBox cb = (CheckBox) v;

						int getPosition = (Integer) cb.getTag();

						if (((CheckBox) v).isChecked()) {
							fav.get(getPosition).setIsselected(1);
							checks++;
						} else {
							fav.get(getPosition).setIsselected(0);
							checks--;
						}
						if (checks > 0) {
							showdelete = true;
							supportInvalidateOptionsMenu();
						} else {
							showdelete = false;
							supportInvalidateOptionsMenu();
							
						}
					}
				});

				/******** Set Item Click Listner for LayoutInflater for each row *******/

				vi.setOnClickListener(new OnItemClickListener(position));
			}
			return vi;
		}
		@Override
		public void onClick(View v) {
			//Log.v("CustomAdapter", "=====Row button clicked=====");
		}

		/********* Called when Item click in ListView ************/
		private class OnItemClickListener implements OnClickListener {
			private int mPosition;

			OnItemClickListener(int position) {
				mPosition = position;
			}

			@Override
			public void onClick(View arg0) {

				PostFavourite sct = (PostFavourite) activity;
				sct.onItemClick(mPosition);
			}
		}
	}
	public void onItemClick(int mPosition) {
		DataModel favpost = (DataModel) fav.get(mPosition);
		Intent startsearch = new Intent(PostFavourite.this,PostActivity.class);	
		startsearch.putExtra("searchobject", favpost);
		startsearch.putExtra("postType", 5);
		//startsearch.putExtra("intentorigin", 2);
		startActivity(startsearch);
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.favorite_menu, menu);
		MenuItem item = menu.findItem(R.id.item_favdelete);

		if (showdelete) {
			item.setVisible(true);
		} else {
			item.setVisible(false);
		}

		return super.onCreateOptionsMenu(menu);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);

			return true;

		case R.id.item_favdelete:
			confirmDeleteAlert("Do you want to delete the selected post favorites?");
			
			return true;

		}
		return super.onOptionsItemSelected(item);
	}
	
	
	
	public void deleteFav(){
		ArrayList<DataModel> delList = fav;
		ArrayList<DataModel> delselectedlist= new ArrayList<DataModel>();
		
		//System.out.println("delete list count----->"+delList.size());
		for (int i = 0; i < delList.size(); i++) {
			DataModel selectedfav = delList.get(i);
			if (selectedfav.getIsselected()==1) {
				delselectedlist.add(selectedfav);
				//Log.i(TAG, selectedfav.getDestinationstate()+" , "+selectedfav.getEquipmenttype());
			}
		}
		
		
		if (db.deletepostfavorites(delselectedlist)) {
			fav.removeAll(delselectedlist);
			
			if (fav.size()<=0) {
				Toast.makeText(PostFavourite.this, AppConstants.NO_FAVORITE, Toast.LENGTH_SHORT).show();
				finish();
			}else{
				checks = 0;
				showdelete = false;
				supportInvalidateOptionsMenu();
				adapter.notifyDataSetChanged();
			}
		}
	}
	
	
	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		Intent startsearch = new Intent(PostFavourite.this,PostFavourite.class);
		startsearch.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(startsearch);
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		checks = 0;
	}

	public void confirmDeleteAlert(String msg) {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(PostFavourite.this);

		// Setting Dialog Title
		alertDialog.setTitle("ITS TRUCKER");

		// Setting Dialog Message
		alertDialog.setMessage(msg);

		// Setting Icon to Dialog
		// alertDialog.setIcon(R.drawable.delete);

		// On pressing Ok button profile activity is started to enter profile home base
		alertDialog.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						deleteFav();
					}
				});

		// on pressing cancel button
		alertDialog.setNegativeButton("No",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		// Showing Alert Message
		alertDialog.show();
	}

	
}


