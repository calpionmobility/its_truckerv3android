package com.wi_vod.itstrucker;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnCancelListener;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

public class Utils {
	public static String statevalue = "";

	public static int TYPE_WIFI = 1;
	public static int TYPE_MOBILE = 2;
	public static int TYPE_NOT_CONNECTED = 0;

	public static int CURRENT_NETWORK = ConnectivityManager.TYPE_WIFI;
	public static String NETWORK_FAILURE_REASON = null;

	public static String LOGOUT_USER = "LOGOUT";

	public static String VALIDATION_PATTERN = "([a-zA-Z]+)*,[a-zA-Z]+";
	public static Pattern pattern;
	public static Matcher matcher;
	static String areacode;
	public static int TRACKING_INTERVAL = 1000 * 60 * 1;
	 
	 //public static int TRACKING_INTERVAL = 20000;

	public static boolean isInternetAvailable(Context context) {
		ConnectivityManager conMgr = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		final android.net.NetworkInfo wifi = conMgr
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		final android.net.NetworkInfo mobile = conMgr
				.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

		NETWORK_FAILURE_REASON = null;
		if (wifi != null
				&& (wifi.getState() == NetworkInfo.State.CONNECTED || wifi
						.getState() == NetworkInfo.State.CONNECTING)) {
			CURRENT_NETWORK = ConnectivityManager.TYPE_WIFI;
			return true;

		} else if (mobile != null
				&& (mobile.getState() == NetworkInfo.State.CONNECTED || mobile
						.getState() == NetworkInfo.State.CONNECTING)) {
			CURRENT_NETWORK = ConnectivityManager.TYPE_MOBILE;
			return true;
		} else if ((conMgr.getNetworkInfo(0) != null && conMgr
				.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED)
				|| (conMgr.getNetworkInfo(1) != null && conMgr
						.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED)) {
			return true;

		} else if ((conMgr.getNetworkInfo(0) != null && conMgr
				.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING)
				|| (conMgr.getNetworkInfo(1) != null && conMgr
						.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING)) {
			return true;

		} else if (mobile != null
				&& (mobile.getState() == NetworkInfo.State.DISCONNECTED || mobile
						.getState() == NetworkInfo.State.DISCONNECTING)) {

			NETWORK_FAILURE_REASON = mobile.getReason();
			CURRENT_NETWORK = ConnectivityManager.TYPE_MOBILE;
		} else if (wifi != null
				&& (wifi.getState() == NetworkInfo.State.DISCONNECTED || wifi
						.getState() == NetworkInfo.State.DISCONNECTING)) {
			CURRENT_NETWORK = ConnectivityManager.TYPE_WIFI;
			NETWORK_FAILURE_REASON = wifi.getReason();

		} else if (mobile != null
				&& (mobile.getState() == NetworkInfo.State.DISCONNECTED || mobile
						.getState() == NetworkInfo.State.DISCONNECTING)) {

			NETWORK_FAILURE_REASON = mobile.getReason();
			CURRENT_NETWORK = ConnectivityManager.TYPE_MOBILE;
		} else if (wifi != null
				&& (wifi.getState() == NetworkInfo.State.DISCONNECTED || wifi
						.getState() == NetworkInfo.State.DISCONNECTING)) {
			CURRENT_NETWORK = ConnectivityManager.TYPE_WIFI;
			NETWORK_FAILURE_REASON = wifi.getReason();

		}
		return false;
	}

	public static String checkIsNull(String chkString) {
		if (null == chkString || chkString.equalsIgnoreCase("null")
				|| chkString.equalsIgnoreCase("")) {
			return AppConstants.EMPTY_STRING;
		}
		return chkString;
	}

	public static String checkIsNullforPosts(String chkString) {
		if (null == chkString || chkString.equalsIgnoreCase("null")
				|| chkString.equalsIgnoreCase("")) {
			return AppConstants.POSTEMPTY_STRING;
		}
		return chkString;
	}

	public static boolean searchstate(Context c, String state, int countryvalue) {
		String statestring = state.toLowerCase();
		if (statestring.length() == 2) {
			boolean abv_verified = searchstateab(c, state, countryvalue);
			return abv_verified;
		} else {
			String statearray[] = c.getResources().getStringArray(
					R.array.states_array);
			String mexicoarray[] = c.getResources().getStringArray(
					R.array.mexico_array);
			String canadaarray[] = c.getResources().getStringArray(
					R.array.province_array);

			String stateabrv[] = c.getResources().getStringArray(
					R.array.states_AB_array);
			String mexicoabrv[] = c.getResources().getStringArray(
					R.array.mexico_AB_array);
			String canadaabrv[] = c.getResources().getStringArray(
					R.array.province_AB_array);
			// System.out.println("statestring-------------->"+statestring);
			String[] array[] = { statearray, canadaarray, mexicoarray };
			String[] abvarray[] = { stateabrv, canadaabrv, mexicoabrv };

			/*
			 * for (int i = 0; i < array.length; i++) { int j=0; for (String
			 * curVal : array[i]) { curVal=curVal.toLowerCase(); if
			 * (curVal.equals(statestring)) { statevalue=abvarray[i][j]; if
			 * (countryvalue==i) { return true; } } j++; } }
			 */
			int j = 0;
			for (String curVal : array[countryvalue]) {
				curVal = curVal.toLowerCase();
				if (curVal.equals(statestring)) {
					statevalue = abvarray[countryvalue][j];
					return true;
				}
				j++;
			}
		}

		return false;
	}

	public static String formatString(String str) {

		str = str.startsWith(",") ? str.substring(1) : str;
		return str;
	}

	public static boolean searchstateab(Context c, String state,
			int countryvalue) {

		String stateabrv[] = c.getResources().getStringArray(
				R.array.states_AB_array);
		String mexicoabrv[] = c.getResources().getStringArray(
				R.array.mexico_AB_array);
		String canadaabrv[] = c.getResources().getStringArray(
				R.array.province_AB_array);

		String statestring = state.toLowerCase();
		String[] abvarray[] = { stateabrv, canadaabrv, mexicoabrv };

		int j = 0;
		for (String curVal : abvarray[countryvalue]) {
			curVal = curVal.toLowerCase();
			if (curVal.equals(statestring)) {
				statevalue = abvarray[countryvalue][j];
				return true;
			}
			j++;
		}

		return false;
	}

	public static boolean validate(final String hex) {
		pattern = Pattern.compile(VALIDATION_PATTERN);
		matcher = pattern.matcher(hex);
		return matcher.matches();

	}

	public static final String md5(final String s) {
		final String MD5 = "MD5";
		try {
			// Create MD5 Hash
			MessageDigest digest = java.security.MessageDigest.getInstance(MD5);
			digest.update(s.getBytes());
			byte messageDigest[] = digest.digest();

			// Create Hex String
			StringBuilder hexString = new StringBuilder();
			for (byte aMessageDigest : messageDigest) {
				String h = Integer.toHexString(0xFF & aMessageDigest);
				while (h.length() < 2)
					h = "0" + h;
				hexString.append(h);
			}
			return hexString.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "";
	}

	public static void showAlertMessage(String msg, final Context activity) {
		final Context cn = activity;
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);

		// Setting Dialog Title
		alertDialog.setTitle("Error");

		// Setting Dialog Message
		alertDialog.setMessage(msg);

		// Setting Icon to Dialog
		// alertDialog.setIcon(R.drawable.delete);

		// On pressing Settings button
		alertDialog.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		/*
		 * alertDialog.setOnCancelListener(new OnCancelListener() {
		 * 
		 * @Override public void onCancel(DialogInterface dialog) {
		 * dialog.dismiss(); PackageManager packageManager =
		 * activity.getPackageManager(); try { ActivityInfo info =
		 * packageManager.getActivityInfo(((Activity) cn).getComponentName(),
		 * 0); Log.e("app", "Activity name:" + info.name); ((Activity)
		 * activity).finish(); } catch (NameNotFoundException e) {
		 * e.printStackTrace(); } } });
		 */

		// Showing Alert Message
		alertDialog.show();
	}

	public static void showErrorMessage(String msg, final Context activity) {
		final Context cn = activity;
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);

		// Setting Dialog Title
		alertDialog.setTitle("Error");

		// Setting Dialog Message
		alertDialog.setMessage(msg);

		// Setting Icon to Dialog
		// alertDialog.setIcon(R.drawable.delete);

		// On pressing Settings button
		alertDialog.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
						PackageManager packageManager = activity
								.getPackageManager();
						try {
							ActivityInfo info = packageManager.getActivityInfo(
									((Activity) cn).getComponentName(), 0);
							Log.e("app", "Activity name:" + info.name);
							((Activity) activity).finish();
						} catch (NameNotFoundException e) {
							e.printStackTrace();
						}

					}
				});

		alertDialog.setOnCancelListener(new OnCancelListener() {

			@Override
			public void onCancel(DialogInterface dialog) {
				dialog.dismiss();
				PackageManager packageManager = activity.getPackageManager();
				try {
					ActivityInfo info = packageManager.getActivityInfo(
							((Activity) cn).getComponentName(), 0);
					// Log.e("app", "Activity name:" + info.name);
					((Activity) activity).finish();
				} catch (NameNotFoundException e) {
					e.printStackTrace();
				}

			}
		});
		// Showing Alert Message
		alertDialog.show();
	}

	public static int compareDates(Date date1, Date date2) {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");

		try {
			date1 = sdf.parse(sdf.format(date1));
			date2 = sdf.parse(sdf.format(date2));
		} catch (ParseException e) {
			e.printStackTrace();
			return -2;
		}

		Calendar cal1 = new GregorianCalendar();
		Calendar cal2 = new GregorianCalendar();

		cal1.setTime(date1);
		cal2.setTime(date2);

		if (cal1.equals(cal2)) {
			return 0;
		} else if (cal1.after(cal2)) {
			return 1;
		} else if (cal1.before(cal2)) {
			return -1;
		}

		return -2;
	}

	public static ArrayList<String> autocomplete(String input, String country,
			int countryindex, Context context) {
		ArrayList<String> resultList = null;

		HttpURLConnection conn = null;
		StringBuilder jsonResults = new StringBuilder();
		try {
			StringBuilder sb = new StringBuilder(AppConstants.PLACES_API_BASE
					+ AppConstants.TYPE_AUTOCOMPLETE + AppConstants.OUT_JSON);
			sb.append("?sensor=false&key="
					+ context.getString(R.string.googleplaceproduction));
			sb.append("&components=country:" + country + "&types=(regions)");
			sb.append("&input=" + URLEncoder.encode(input, "utf8"));

			URL url = new URL(sb.toString());
			//Log.i("url--->", url.toString());
			conn = (HttpURLConnection) url.openConnection();
			InputStreamReader in = new InputStreamReader(conn.getInputStream());

			// Load the results into a StringBuilder
			int read;
			char[] buff = new char[1024];
			while ((read = in.read(buff)) != -1) {
				jsonResults.append(buff, 0, read);
			}
		} catch (MalformedURLException e) {
			// Log.e(LOG_TAG, "Error processing Places API URL", e);
			return resultList;
		} catch (IOException e) {
			// Log.e(LOG_TAG, "Error connecting to Places API", e);
			return resultList;
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}

		try {
			// Create a JSON object hierarchy from the results
			JSONObject jsonObj = new JSONObject(jsonResults.toString());
			JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

			// Extract the Place descriptions from the results
			resultList = new ArrayList<String>(predsJsonArray.length());
			for (int i = 0; i < predsJsonArray.length(); i++) {
				// System.out.println("description-->"+predsJsonArray.getJSONObject(i).getString("description"));
				String parsed = parsevalues(predsJsonArray.getJSONObject(i)
						.getString("description"), context, countryindex);
				if (!parsed.equals("")) {
					resultList.add(parsed);
				}

			}
		} catch (JSONException e) {
			// Log.e(LOG_TAG, "Cannot process JSON results", e);
		}

		return resultList;
	}

	public static String parsevalues(String result, Context context,
			int countryindex) {
		String finalstring = "";
		int numberOfCommas = result.replaceAll("[^,]", "").length();
		if (numberOfCommas >= 2) {
			String mystates[] = result.split(",");
			int lenght = mystates.length;

			String city = mystates[lenght - 3];
			String state = mystates[lenght - 2];
			state = state.replaceAll("[^a-zA-Z]+", "");
			if (Utils.searchstate(context, state, countryindex)) {
				finalstring = city + "," + state;
			}
		} else if (numberOfCommas == 1) {
			String mystates[] = result.split(",");
			String state = mystates[0];

			if (Utils.searchstate(context, state, countryindex)) {
				finalstring = state;
			}
		}
		// System.out.println(finalastring);
		return finalstring;
	}

	/**
	 * This method tokenizes mulitple selected items
	 * 
	 * @param selectedOptionsAsString
	 * @return list containing multiple selected options
	 */
	public static ArrayList<String> getSelectedItems(
			String selectedOptionsAsString) {
		ArrayList<String> selectedItemList = new ArrayList<String>();

		if (null != selectedOptionsAsString
				&& selectedOptionsAsString.length() != 0) {
			StringTokenizer tokenizer = new StringTokenizer(
					selectedOptionsAsString, AppConstants.TOKEN_DELIMETER);
			while (tokenizer.hasMoreTokens()) {
				String item = tokenizer.nextToken();
				if (item.equals("PalletExchange"))
					item = "Pallet Exchange";

				selectedItemList.add(item.trim());
			}
			return selectedItemList;
		} else {
			return null;
		}
	}

	/**
	 * This method tokenizes mulitple selected items
	 * 
	 * @param selectedOptionsAsString
	 * @return list containing multiple selected options
	 */
	public static ArrayList<String> getSelectedItems(
			String selectedOptionsAsString, String delimeter) {
		ArrayList<String> selectedItemList = new ArrayList<String>();

		if (null != selectedOptionsAsString
				&& selectedOptionsAsString.length() != 0) {
			StringTokenizer tokenizer = new StringTokenizer(
					selectedOptionsAsString, delimeter);
			while (tokenizer.hasMoreTokens()) {
				String item = tokenizer.nextToken();
				if (item.equals("PalletExchange"))
					item = "Pallet Exchange";

				selectedItemList.add(item.trim());
			}
			return selectedItemList;
		} else {
			return null;
		}
	}

	/**
	 * This method tokenizes mulitple selected items
	 * 
	 * @param selected
	 *            equipment types in abbreivated form
	 * @return String containing multiple selected options with full name
	 */
	public static String getSelectedEquipmentTypeNames(Context ctx,
			String selected_AB_AsString) {
		StringBuilder selectedItemString = new StringBuilder();

		String[] typeABArray = ctx.getResources().getStringArray(
				R.array.type_AB_array);
		String[] typeArray = ctx.getResources().getStringArray(
				R.array.type_array);

		ArrayList<String> typeABList = new ArrayList<String>();
		Collections.addAll(typeABList, typeABArray);

		if (null != selected_AB_AsString && selected_AB_AsString.length() != 0) {
			StringTokenizer tokenizer = new StringTokenizer(
					selected_AB_AsString, ",");
			while (tokenizer.hasMoreTokens()) {
				String item = tokenizer.nextToken();
				if (typeABList.contains(item)) {
					selectedItemString.append(typeArray[typeABList
							.indexOf(item)]);
					selectedItemString.append(AppConstants.TOKEN_DELIMETER);
				}
			}
			selectedItemString.deleteCharAt(selectedItemString.length() - 1);
			return selectedItemString.toString();
		} else {
			return null;
		}
	}

	public static String getSelectedEquipmentTypes(Context ctx,
			List<Integer> selIndices) {
		StringBuilder selectedItems = new StringBuilder();

		String[] typeABList = ctx.getResources().getStringArray(
				R.array.type_AB_array);

		if (null != selIndices && selIndices.size() > 0) {
			for (int item : selIndices) {
				selectedItems.append(typeABList[item]);
				selectedItems.append(AppConstants.TOKEN_DELIMETER);
			}
			selectedItems.deleteCharAt(selectedItems.length() - 1);
			return selectedItems.toString();
		} else {
			return null;
		}
	}

	public static List<Integer> getSelectedEquipmentTypeIndices(Context ctx,
			ArrayList<String> selOptions) {
		List<Integer> selIndices = new ArrayList<Integer>();
		String[] typeArray = ctx.getResources().getStringArray(
				R.array.type_array);
		ArrayList<String> typeList = new ArrayList<String>();
		Collections.addAll(typeList, typeArray);

		if (null != selOptions && selOptions.size() > 0) {
			for (String item : selOptions) {
				if (typeList.contains(item)) {
					selIndices.add(typeList.indexOf(item));
				}
			}
			return selIndices;
		} else {
			return null;
		}
	}

	public static List<Integer> getSelectedTrailerOptionIndices(Context ctx,
			ArrayList<String> selOptions) {
		List<Integer> selIndices = new ArrayList<Integer>();
		String[] optionArray = ctx.getResources().getStringArray(
				R.array.option_array);
		ArrayList<String> optionList = new ArrayList<String>();
		Collections.addAll(optionList, optionArray);

		if (null != selOptions && selOptions.size() > 0) {
			for (String item : selOptions) {
				if (item.equals("PalletExchange"))
					item = "Pallet Exchange";
				if (optionList.contains(item)) {
					selIndices.add(optionList.indexOf(item));
				}
			}
			return selIndices;
		} else {
			return null;
		}
	}

	public enum TrailerOptions {
		Tarps, Hazardous, PalletExchange, Team, Expedited
	}

	/**
	 * This method formats the values with 2 decimal places (if decimal values
	 * exist)
	 * 
	 * @param String
	 *            entered in edit text box
	 * @return String value
	 */
	public static String decimalFormat(String st) {
		if (st != null && !st.isEmpty() && !st.equalsIgnoreCase("null")) {
			double d = Double.valueOf(st);
			double roundOff = (double) Math.round(d * 100) / 100;
			if (roundOff > 100) {
				return "99.99";

			} else {
				return String.valueOf(roundOff);
			}

		}
		return "";

	}

	/**
	 * This method ceils the values if decimal values exist or not
	 * 
	 * @param String
	 *            entered in edit text box
	 * @return String value
	 */
	public static String weightRounding(String st) {
		if (st != null && !st.equals("")) {
			double d = Double.valueOf(st);
			int i = (int) (Math.round(d));
			if (i > 999999) {
				return "999999";
			} else {
				return String.valueOf((int) (Math.round(d)));
			}

		}
		return "";

	}

	public static String mindistanceRounding(String st) {
		if (st != null && !st.equals("")) {
			double d = Double.valueOf(st);
			int i = (int) (Math.round(d));
			if (i > 9999) {
				return "9999";
			} else {
				return String.valueOf((int) (Math.round(d)));
			}

		}
		return "";

	}

	public static String quantityRounding(String st) {
		if (st != null && !st.equals("")) {
			double d = Double.valueOf(st);
			int i = (int) (Math.round(d));
			if (i > 99) {
				return "99";
			} else {
				return String.valueOf((int) (Math.round(d)));
			}

		}
		return "";

	}

	public static String getSortOrder(Context context, String sortBy) {
		Bundle bundle = ResourcesAdditions.getResourcesExtras(
				context.getResources(), R.xml.sort_options);
		if (bundle != null) {
			String flag = (bundle.getInt(sortBy) == 1) ? "true" : "false";
			return flag;
		}
		return null;
	}

	public static String getCountryForAreaSelected(Context c, int index) {
		String country;
		String originareaaselectedcode[] = c.getResources().getStringArray(
				R.array.area_array_select_code);
		areacode = originareaaselectedcode[index];
		if (areacode.equals("Area CW") || areacode.equals("Area CW")
				|| areacode.equals("Area CC") || areacode.equals("CAN")) {
			country = "CAN";
		} else if (areacode.equals("Area Mx") || areacode.equals("MEX")) {
			country = "MEX";
		} else {
			country = "USA";
		}
		return country;
	}

	// ///////////////////////////// For Post Truck

	public static String getSelectedEquipmentTypesForPost(Context ctx,
			List<Integer> selIndices) {
		StringBuilder selectedItems = new StringBuilder();

		String[] typeABList = ctx.getResources().getStringArray(
				R.array.posttype_AB_array);

		if (null != selIndices && selIndices.size() > 0) {
			for (int item : selIndices) {
				selectedItems.append(typeABList[item]);
				selectedItems.append(AppConstants.TOKEN_DELIMETER);
			}
			selectedItems.deleteCharAt(selectedItems.length() - 1);
			return selectedItems.toString();
		} else {
			return null;
		}
	}

	public static List<Integer> getSelectedEquipmentTypeIndicesForPost(
			Context ctx, ArrayList<String> selOptions) {
		List<Integer> selIndices = new ArrayList<Integer>();
		String[] typeArray = ctx.getResources().getStringArray(
				R.array.posttype_array);
		ArrayList<String> typeList = new ArrayList<String>();
		Collections.addAll(typeList, typeArray);

		if (null != selOptions && selOptions.size() > 0) {
			for (String item : selOptions) {
				if (typeList.contains(item)) {
					selIndices.add(typeList.indexOf(item));
				}
			}
			return selIndices;
		} else {
			return null;
		}
	}

	public static ArrayList<String> getSelectedEquipmentTypeNamesForPost(
			Context ctx, String selected_AB_AsString) {
		ArrayList<String> selectedItems = new ArrayList<String>();

		String[] typeABArray = ctx.getResources().getStringArray(
				R.array.posttype_AB_array);
		String[] typeArray = ctx.getResources().getStringArray(
				R.array.posttype_array);

		ArrayList<String> typeABList = new ArrayList<String>();
		Collections.addAll(typeABList, typeABArray);

		if (null != selected_AB_AsString && selected_AB_AsString.length() != 0) {
			StringTokenizer tokenizer = new StringTokenizer(
					selected_AB_AsString, ",");
			while (tokenizer.hasMoreTokens()) {
				String item = tokenizer.nextToken();
				if (typeABList.contains(item)) {
					selectedItems.add(typeArray[typeABList.indexOf(item)]);
				}
			}
			return selectedItems;
		} else {
			return null;
		}
	}

	public static String detailsDateFormat(String date) {
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		Date startDate = null;
		String newDateString = null;
		try {
			startDate = sdf.parse(date);
			newDateString = df.format(startDate);

		} catch (ParseException e) {
			e.printStackTrace();
			return "-";

		}
		return newDateString;

	}

	public static String validatepackageversion(Context ctx) {
		try {
			PackageInfo packageInfo = ctx.getPackageManager().getPackageInfo(
					ctx.getPackageName(), 0);
			String currentversion = packageInfo.versionName;
			return currentversion;
		} catch (NameNotFoundException e) {
			// Handle exception
		}
		return "";
	}
	
	
	
	public static void updateGoogleplay(final Activity activity) {
		final Activity cn = activity;
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(cn);
		// set title
		alertDialogBuilder.setTitle("Update Google Play Services");
		// set dialog message
		alertDialogBuilder
				.setMessage(AppConstants.PLAYSERVICES_NOTFOUND)
				.setCancelable(false)
				.setPositiveButton("Update",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								callMarketPlace(cn);
								try {
									((Activity) activity).finish();
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						});
		alertDialogBuilder.show();
	}

	public static void callMarketPlace(final Activity activity) {
		final Activity cn = activity;
		try {
			cn.startActivityForResult(
					new Intent(Intent.ACTION_VIEW,Uri.parse("market://details?id="+ "com.google.android.gms")), 1);
		} catch (android.content.ActivityNotFoundException anfe) {
			cn.startActivityForResult(new Intent(Intent.ACTION_VIEW,Uri.parse("https://play.google.com/store/apps/details?id="
							+ "com.google.android.gms")), 1);
		}
	}
	
	

	public static void logoutuser(String msg, final Context activity) {

		final Context cn = activity;
		final SessionManager session;
		session = new SessionManager(cn);

		if (!session.getalertinstance()) {

			AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);

			// Setting Dialog Title
			alertDialog.setTitle("Error");
			alertDialog.setCancelable(false);
			// Setting Dialog Message
			alertDialog.setMessage(msg);

			// Setting Icon to Dialog
			// alertDialog.setIcon(R.drawable.delete);

			// On pressing Settings button
			alertDialog.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							 AppConstants.firstlaunch=true;
							PackageManager packageManager = activity
									.getPackageManager();
							try {
								ActivityInfo info = packageManager
										.getActivityInfo(((Activity) cn)
												.getComponentName(), 0);
								Log.e("app", "Activity name:" + info.name);
								stopalarmservice(activity);
								session.logoutUser();
								dialog.cancel();
								((Activity) activity).finish();
							} catch (NameNotFoundException e) {
								e.printStackTrace();
							}

						}
					});
			alertDialog.show();
			session.setalertinstance(true);

		}
		// Showing Alert Message

	}

	// start Alarm timer.Depending on the timer set,Loginbackgrpoound task will
	// be called

	public static void startbackgroundservice(Context context) {

		// System.out.println("-------------->");
		ITSApplication app = ((ITSApplication) context.getApplicationContext());
		app.startAlaramservice();

	}

	public static void stopalarmservice(Context context) {
		ITSApplication app = ((ITSApplication) context.getApplicationContext());
		app.stopAlaramservice();
	}

	// Verify whether the applicatication is in background or foreground.
	public static boolean isAppIsInBackground(Context context) {
		boolean isInBackground = true;
		ActivityManager am = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			List<ActivityManager.RunningAppProcessInfo> runningProcesses = am
					.getRunningAppProcesses();
			for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
				if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
					for (String activeProcess : processInfo.pkgList) {
						if (activeProcess.equals(context.getPackageName())) {
							isInBackground = false;
						}
					}
				}
			}
		} else {
			List<ActivityManager.RunningTaskInfo> taskInfo = am
					.getRunningTasks(1);
			ComponentName componentInfo = taskInfo.get(0).topActivity;
			if (componentInfo.getPackageName().equals(context.getPackageName())) {
				isInBackground = false;
			}
		}

		return isInBackground;
	}

	public static void appwentbackground(Context context) {
		final Context cn = context;
		final SessionManager session;
		session = new SessionManager(cn);
		session.setappbackground(Utils.isAppIsInBackground(cn));
		//System.out.println("appwentbackground method------->"+ Utils.isAppIsInBackground(cn));
	}

	public static void appcameforeground(Context context) {

		final Context cn = context;
		final SessionManager session;
		session = new SessionManager(cn);
		//System.out.println("session.getisappbackground()------->"+ session.getisappbackground());
		if (session.getisappbackground()) {
			//System.out.println("app came from background");
			session.setappbackground(true);
			Utils.startbackgroundservice(context);

		}
	}

}
