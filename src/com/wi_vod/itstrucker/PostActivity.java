package com.wi_vod.itstrucker;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Toast;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;

import com.wi_vod.itstrucker.SearchDetailActivity.HttpServiceListener;
import com.wi_vod.itstrucker.Utils.TrailerOptions;
import com.wi_vod.itstrucker.model.DataModel;
import com.wi_vod.itstrucker.model.DatabaseHandler;
import com.wi_vod.itstrucker.model.PlacesAutoCompleteAdapter;
import com.wi_vod.itstrucker.model.PostDetailLoadModel;
import com.wi_vod.itstrucker.model.XMLTruckPostparser;


import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.xmlpull.v1.XmlPullParserException;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.zip.GZIPInputStream;

import javax.net.ssl.HttpsURLConnection;

public class PostActivity extends ActionBarActivity implements
		OnDialogCancelListener {

	private static String TAG = "POSTACTIVITY";
	private Spinner poriginspinner, pdestinationspinner, pequipmentspinner,
			ptraileroptionspinner/*
								 * , phourOldspinner, poriginareaspinner,
								 * pdestareaspinner
								 */;
	private AutoCompleteTextView postoriginhomebase, postdestinationbase;
	private EditText postwidth, postlength, postweight, postmindist,
			postratepermile, pspecialinfo, postquantity;
	private String statevalue, origincounrty, originstate = "",
			origincity = "", destinationcountry, destinationcity = "",
			destinationstate = "", radius = "0", equipmenttype, searchdate,
			traileroption, hoursold, isloadfull, width = "0", length = "0",
			weight = "0", mindist = "0", ratepermile = "0", specialinfo,
			quantity = "0";
	private Button postdateSearch, posttruck;
	private TextView postradiustext;
	private SeekBar postradiuseekbar;
	private RadioGroup postloadSizegroup;
	private RadioButton postloadsizefull, postloadsizeltl, postloadsizeany,
			radioLoadButton;
	private int equipmenttypeindex, traileroptionindex, searchhoursindex;

	private int origincountryindex, equipmentindex, destinationcountryindex,
			profileradius, profileloadsize, profilehoursindex, posttype,
			postintentcountryindex;
	private SharedPreferences prefs;
	private String postprofilehomebase, postintenthomebase = "";

	private ProgressDialog progDialog; // Progress bar
	// private static byte[] buff = new byte[1024];
	private String response_str;
	private String errormsg;
	private SessionManager session;
	private String truckId;
	private CheckBox favourite;
	private boolean postvalidated = true;
	private boolean postdestinationValidated = true;
	private boolean postdateValidated = true;
	private boolean postETValidated = true;
	private DatabaseHandler db;
	private boolean repostingTruck = false;
	private String validationErrorMessage = "";
	private PostDetailLoadModel postLoadEditObject;
	private String loadid = "0";
	private int loadsizeedit;
	private Date todays;
	PlacesAutoCompleteAdapter adapter;
	String countryforplaces[];
	private String trailerOptionStr, equipmentOptionStr;
	private MultiSelectionSpinner trailerOptions_multiSelect_Spinner;
	private MultiSelectionSpinnerposting equipmentOptions_multiSelect_Spinner;
	private TextView txtTrailerOption, txtEquipmentOption;
	private boolean printDate;
	DatePickerDialog dialog;
	private String corigin, cdest;
	private boolean tempDateFlag;
	int yy = 0, mm = 0, dd = 0;
	private DataModel searchObject;
	boolean originvalidate,dstvalidate,destination,origin;
	private TextView postradiusheader;
	private BroadcastReceiver mRegistrationBroadcastReceiver;


	static final int DATE_DIALOG_ID = 999;
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {

		View v = getCurrentFocus();
		boolean ret = super.dispatchTouchEvent(event);

		if (v instanceof EditText) {
			View w = getCurrentFocus();
			int scrcoords[] = new int[2];
			w.getLocationOnScreen(scrcoords);
			float x = event.getRawX() + w.getLeft() - scrcoords[0];
			float y = event.getRawY() + w.getTop() - scrcoords[1];

			if (event.getAction() == MotionEvent.ACTION_UP
					&& (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w
							.getBottom())) {

				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
						.getWindowToken(), 0);
			}
		}
		return ret;
	}

	// prevents orientation change from reloading results and crashing app
	@Override
	public void onConfigurationChanged(Configuration newConfig) {

		super.onConfigurationChanged(newConfig);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.post_activity);
		UpdateUI();
		 getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		session = new SessionManager(PostActivity.this);
		prefs = getSharedPreferences("itstrucker", 0);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		Bundle bundle = getIntent().getExtras();
		  db = new DatabaseHandler(this);

		if (bundle != null) {
			posttype = bundle.getInt("postType", 0);
			postintenthomebase = bundle.getString("originname");
			postintentcountryindex = bundle.getInt("countryindex");
			postLoadEditObject = bundle.getParcelable("POSTLOADDETAILS");
			loadid = bundle.getString("POSTLOADID");
			loadsizeedit = bundle.getInt("POSTLOADSIZE");
			searchObject = bundle.getParcelable("searchobject");
		}
		if (posttype < 4) {
			UpdatePostValues();
		}

		if (posttype == 5) {
			UpdatePostFavValues();
			repostingTruck = true;
		}
		if (posttype == 4) {
			UpdatePostEditValues();
			repostingTruck = true;
		} else {
			loadid = "0";
			repostingTruck = false;
		}
		mRegistrationBroadcastReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Utils.logoutuser(AppConstants.LOGOUTUSER_ERRORMSG, PostActivity.this);
			}
		};

	}
	 private void UpdatePostFavValues() {
	        String arr[] = getResources().getStringArray(
	                R.array.profilecountry_array);
	        String equipment[] = getResources().getStringArray(
	                R.array.type_AB_array);
	        String trailertype[] = getResources().getStringArray(
	                R.array.option_array);
	        String origincountryarray[] = getResources().getStringArray(
	                R.array.profilecountry_array);
	        if (Utils.checkIsNullforPosts(searchObject.getOrigincounrty()).equals(  "")) {
	            origincountryindex = 0;
	           
	        } else {
	            origincountryindex = Arrays.asList(arr).indexOf(searchObject.getOrigincounrty());
	           
	        }
	        if (Utils.checkIsNullforPosts(searchObject.getDestinationcountry())
	                .equals("")) {
	            destinationcountryindex = 0;
	            destinationcountry = origincountryarray[destinationcountryindex];
	        } else {
	            destinationcountryindex = Arrays.asList(arr).indexOf(
	                    searchObject.getDestinationcountry());
	 
	            destinationcountry = origincountryarray[destinationcountryindex];
	        }
	 
	        if (!Utils.checkIsNullforPosts(searchObject.getSearchdate()).equals("")) {
	            if (searchObject.getSearchdate().equals("0000-00-00")) {
	                postdateSearch.setText("");
	            } else {
	                postdateSearch.setText(searchObject.getSearchdate());
	 
	            }
	        }
	        if (!Utils.checkIsNullforPosts(searchObject.getOriginstate())
	                .equals("")) {
	            Utils.searchstate(PostActivity.this, searchObject.getOriginstate(),
	                    origincountryindex);
	 
	        }
	        corigin = Utils.formatString(Utils.checkIsNullforPosts(searchObject
	                .getOrigincity()) + "," + Utils.statevalue);
	        if (posttype == 5)
	            if (!(postoriginhomebase.getText().equals(corigin)))
	                postoriginhomebase.setText(corigin);
	            else
	                postoriginhomebase.setText("");
	        // postoriginhomebase.setText(Utils.formatString(Utils.checkIsNullforPosts(postLoadEditObject.getPd_originCity())+","+Utils.statevalue));
	 
	        if (!Utils.checkIsNullforPosts(searchObject.getDestinationstate())
	                .equals("")) {
	            Utils.searchstate(PostActivity.this,
	                    searchObject.getDestinationstate(), destinationcountryindex);
	        }
	        cdest = Utils.formatString(Utils.checkIsNullforPosts(searchObject
	                .getDestinationcity()) + "," + Utils.statevalue);
	        if (posttype == 5)
	            if (!(postdestinationbase.getText().equals(cdest)))
	                postdestinationbase.setText(cdest);
	            else
	                postdestinationbase.setText("");
	 
	        if (origincountryindex != -1) {
	            poriginspinner.setSelection(origincountryindex);
	            updategooleplacesfororigin(countryforplaces[origincountryindex],
	                    origincountryindex);
	        }
	 
	        if (destinationcountryindex != -1) {
	            pdestinationspinner.setSelection(destinationcountryindex);
	            updategooleplacesfordest(countryforplaces[destinationcountryindex],
	                    destinationcountryindex);
	        }
	 
	            if (searchObject.getEquipmenttype().length() != 0 && !searchObject.getEquipmenttype().equals("-")) {
	            txtEquipmentOption.setVisibility(View.GONE);
	            equipmentOptions_multiSelect_Spinner.setVisibility(View.VISIBLE);
	 
	            ArrayList<String> selectedEquipmentTypeList = Utils
	                    .getSelectedItems(searchObject.getEquipmenttype());
	            equipmentOptions_multiSelect_Spinner
	                    .setSelection(selectedEquipmentTypeList);
	            equipmentOptions_multiSelect_Spinner
	                    .setCount(selectedEquipmentTypeList.size());
	            equipmentOptions_multiSelect_Spinner
	                    .setSelectedIds(Utils
	                            .getSelectedEquipmentTypeIndicesForPost(
	                                    getApplicationContext(),
	                                    selectedEquipmentTypeList));
	        } else {
	            txtEquipmentOption.setVisibility(View.VISIBLE);
	            equipmentOptions_multiSelect_Spinner.setVisibility(View.GONE);
	        }
	 
	        
	        if (!Utils.checkIsNullforPosts(
	                searchObject.getTraileroption()).equals("")) {
	            if (searchObject.getTraileroption().length() != 0) {
	                txtTrailerOption.setVisibility(View.GONE);
	                trailerOptions_multiSelect_Spinner.setVisibility(View.VISIBLE);
	 
	                ArrayList<String> selectedTrailerOptionList = Utils
	                        .getSelectedItems(
	                                searchObject.getTraileroption(), ",");
	                trailerOptions_multiSelect_Spinner
	                        .setSelection(selectedTrailerOptionList);
	                trailerOptions_multiSelect_Spinner
	                        .setCount(selectedTrailerOptionList.size());
	                trailerOptions_multiSelect_Spinner.setSelectedIds(Utils
	                        .getSelectedTrailerOptionIndices(
	                                getApplicationContext(),
	                                selectedTrailerOptionList));
	            } else {
	                txtTrailerOption.setVisibility(View.VISIBLE);
	                trailerOptions_multiSelect_Spinner.setVisibility(View.GONE);
	            }
	        }
	        Utils.statevalue = "";
	        if (searchObject.getLoadtype().equalsIgnoreCase("false")) {
	            postloadsizeltl.setChecked(true);
	        } else {
	            postloadsizefull.setChecked(true);
	        }
	    
	        if (Integer.valueOf(searchObject.getSearchradius()) == 0) {
	            postradiustext.setText("25 mi");
	            radius="25";
	        } else {
	            postradiuseekbar.setProgress(Integer.valueOf(searchObject.getSearchradius()) - 25);
	        }
	        postwidth.setText(Utils.checkIsNullforPosts(searchObject.getWidth()));
	       // Log.d("Width", searchObject.getWidth().toString());
	        postlength.setText(Utils.checkIsNullforPosts(searchObject.getLength()));
	        //Log.d("getLength", searchObject.getLength().toString());
	        postweight.setText(Utils.checkIsNullforPosts(searchObject.getWeight()));
	       // Log.d("getWeight", searchObject.getWeight().toString());
	        postmindist.setText(Utils.checkIsNullforPosts(searchObject.getMindist()));
	    //  Log.d("getMindist", searchObject.getMindist().toString());
	        postratepermile.setText(Utils.checkIsNullforPosts(searchObject.getRatepermile()));
	       // Log.d("getRatepermile", searchObject.getRatepermile().toString());
	        postquantity.setText(Utils.checkIsNullforPosts(searchObject.getQuantity()));
	        //Log.d("getQuantity", searchObject.getQuantity().toString());
	        pspecialinfo.setText(Utils.checkIsNullforPosts(searchObject.getSpecialInfo()));
	       // Log.d("getSpecialInfo", searchObject.getSpecialInfo().toString());
	 
	    }
	 
	private void UpdatePostEditValues() {

		String arr[] = getResources().getStringArray(R.array.postcountryab_array);
		if (Utils.checkIsNullforPosts(postLoadEditObject.getPd_originCountry()).equals("")) {
			origincountryindex = 0;
			
		} else {
			origincountryindex = Arrays.asList(arr).indexOf(
					postLoadEditObject.getPd_originCountry());
			
		}

		if (Utils.checkIsNullforPosts(
				postLoadEditObject.getPd_destinationCountry()).equals("")) {
			destinationcountryindex = 0;
			
		} else {
			destinationcountryindex = Arrays.asList(arr).indexOf(
					postLoadEditObject.getPd_destinationCountry());

		
		}

		if (!Utils.checkIsNullforPosts(
				postLoadEditObject.getPd_equipmenttypescode()).equals("")) {
			// Log.i(TAG, postLoadEditObject.getPd_equipmenttypescode());
			/*
			 * String id = postLoadEditObject.getPd_equipmenttypescode();
			 * equipmentindex = Arrays.asList(equipment).indexOf(id);
			 */

			if (postLoadEditObject.getEquipment().length() != 0) {
				txtEquipmentOption.setVisibility(View.GONE);
				equipmentOptions_multiSelect_Spinner
						.setVisibility(View.VISIBLE);

				ArrayList<String> selectedEquipmentOptionList = Utils
						.getSelectedEquipmentTypeNamesForPost(
								PostActivity.this,
								postLoadEditObject.getEquipment());
				equipmentOptions_multiSelect_Spinner
						.setSelection(selectedEquipmentOptionList);
				equipmentOptions_multiSelect_Spinner
						.setCount(selectedEquipmentOptionList.size());
				equipmentOptions_multiSelect_Spinner.setSelectedIds(Utils
						.getSelectedEquipmentTypeIndicesForPost(
								getApplicationContext(),
								selectedEquipmentOptionList));
			} else {
				txtEquipmentOption.setVisibility(View.VISIBLE);
				equipmentOptions_multiSelect_Spinner.setVisibility(View.GONE);
			}
		}

		if (!Utils.checkIsNullforPosts(
				postLoadEditObject.getTrailerOptionType()).equals("")) {
			// Log.i(TAG, postLoadEditObject.getTrailerOptionType());
			// traileroptionindex =
			// Arrays.asList(trailertype).indexOf(postLoadEditObject.getTrailerOptionType());
			if (postLoadEditObject.getTrailerOptionType().length() != 0) {
				txtTrailerOption.setVisibility(View.GONE);
				trailerOptions_multiSelect_Spinner.setVisibility(View.VISIBLE);

				ArrayList<String> selectedTrailerOptionList = Utils
						.getSelectedItems(
								postLoadEditObject.getTrailerOptionType(), ",");
				trailerOptions_multiSelect_Spinner
						.setSelection(selectedTrailerOptionList);
				trailerOptions_multiSelect_Spinner
						.setCount(selectedTrailerOptionList.size());
				trailerOptions_multiSelect_Spinner.setSelectedIds(Utils
						.getSelectedTrailerOptionIndices(
								getApplicationContext(),
								selectedTrailerOptionList));
			} else {
				txtTrailerOption.setVisibility(View.VISIBLE);
				trailerOptions_multiSelect_Spinner.setVisibility(View.GONE);
			}
		}

		if (!Utils.checkIsNullforPosts(
				postLoadEditObject.getPd_dateTruckAvailable()).equals("")) {
			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");
			SimpleDateFormat secondformatter = new SimpleDateFormat(
					"yyyy-MM-dd");

			String dateInString = postLoadEditObject.getPd_dateTruckAvailable();
			try {
				Date date = formatter.parse(dateInString);
				/* System.out.println(formatter.format(date)); */

				String seconddate = secondformatter.format(date);
				postdateSearch.setText(seconddate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		postwidth.setText(Utils.checkIsNullforPosts(postLoadEditObject
				.getPd_width()));
		postlength.setText(Utils.checkIsNullforPosts(postLoadEditObject
				.getPd_length()));
		postweight.setText(Utils.checkIsNullforPosts(postLoadEditObject
				.getPd_weight()));
		postmindist.setText(Utils.checkIsNullforPosts(postLoadEditObject
				.getPd_MinMiles()));
		postratepermile.setText(Utils.checkIsNullforPosts(postLoadEditObject
				.getPd_perMile()));
		postquantity.setText(Utils.checkIsNullforPosts(postLoadEditObject
				.getPd_quantity()));
		pspecialinfo.setText(Utils.checkIsNullforPosts(postLoadEditObject
				.getPd_specInfo()));

		if (!Utils.checkIsNullforPosts(postLoadEditObject.getPd_originState())
				.equals("")) {
			Utils.searchstate(PostActivity.this,
					postLoadEditObject.getPd_originState(), origincountryindex);

		}
		// postoriginhomebase.setText(Utils.formatString(Utils.checkIsNullforPosts(postLoadEditObject.getPd_originCity())+","+Utils.statevalue));
		corigin = Utils.formatString(Utils
				.checkIsNullforPosts(postLoadEditObject.getPd_originCity())
				+ "," + Utils.statevalue);
		originvalidate = true;
	/*	if (posttype == 4)
			if (!(postoriginhomebase.getText().equals(corigin)))*/
				postoriginhomebase.setText(corigin);
				
			/*else
				postoriginhomebase.setText("");*/
		Utils.statevalue = "";

		if (!Utils.checkIsNullforPosts(
				postLoadEditObject.getPd_destinationState()).equals("")) {
			Utils.searchstate(PostActivity.this,
					postLoadEditObject.getPd_destinationState(),
					destinationcountryindex);
		}
		// postdestinationbase.setText(Utils.formatString(Utils.checkIsNullforPosts(postLoadEditObject.getPd_destinationCity())+","+Utils.statevalue));
		cdest = Utils.formatString(Utils.checkIsNullforPosts(postLoadEditObject
				.getPd_destinationCity()) + "," + Utils.statevalue);
		dstvalidate = true;
	/*	if (posttype == 4)
			if (!(postdestinationbase.getText().equals(cdest)))*/
				postdestinationbase.setText(cdest);
			/*else
				postdestinationbase.setText("");
*/
		if (origincountryindex != -1) {
			poriginspinner.setSelection(origincountryindex);
			updategooleplacesfororigin(countryforplaces[origincountryindex],
					origincountryindex);
		}

		if (destinationcountryindex != -1) {
			pdestinationspinner.setSelection(destinationcountryindex);
			updategooleplacesfordest(countryforplaces[destinationcountryindex],
					destinationcountryindex);
		}
		/*
		 * if (equipmentindex!=-1) {
		 * pequipmentspinner.setSelection(equipmentindex-1); }
		 */
		/*
		 * if (traileroptionindex!=-1) {
		 * ptraileroptionspinner.setSelection(traileroptionindex); }
		 */
		Utils.statevalue = "";
		if (loadsizeedit == 1) {
			postloadsizefull.setChecked(true);
		} else {
			postloadsizeltl.setChecked(true);
		}
		postradiuseekbar.setProgress(100);
		postradiustext.setText(postradiuseekbar.getProgress() + " mi");
	}

	public void UpdatePostValues() {
		origincountryindex = prefs.getInt("country", 0);
		postprofilehomebase = prefs.getString("homebase", "");
		profileloadsize = prefs.getInt("pploadsizetype", 0);
		postwidth.setText(prefs.getString("width", ""));
		postlength.setText(prefs.getString("length", ""));
		postweight.setText(prefs.getString("weight", ""));
		postmindist.setText(prefs.getString("mindist", ""));
		postratepermile.setText(prefs.getString("ratepermile", ""));
		postquantity.setText(prefs.getString("quantity", ""));
		profileradius = prefs.getInt("ppradius", 0);
		if (posttype == 1) {
			pdestinationspinner.setSelection(origincountryindex);
			postdestinationbase.setText(postprofilehomebase);
			pdestinationspinner.setEnabled(false);
			postdestinationbase.setEnabled(false);
			updategooleplacesfordest(countryforplaces[origincountryindex],
					origincountryindex);
		}

		if (posttype == 1 || posttype == 2) {
			poriginspinner.setSelection(postintentcountryindex);
			postoriginhomebase.setText(postintenthomebase);
			poriginspinner.setEnabled(false);
			postoriginhomebase.setEnabled(false);
			updategooleplacesfordest(countryforplaces[postintentcountryindex],
					postintentcountryindex);
		}
		if (posttype == 3) {
			poriginspinner.setSelection(postintentcountryindex);
			 originvalidate = true ;
			postoriginhomebase.setText(postintenthomebase);
			updategooleplacesfordest(countryforplaces[postintentcountryindex],
					postintentcountryindex);
		}

		// Log.i(TAG, originhomebase.getText().toString());
		// pequipmentspinner.setSelection(prefs.getInt("ppequipmentindex", 0));

		equipmentOptionStr = prefs.getString("ppequipmentindex",
				AppConstants.EMPTY_STRING);
		trailerOptionStr = prefs.getString("pptraileroptions",
				AppConstants.EMPTY_STRING);
		// ptraileroptionspinner.setSelection(prefs.getInt("pptraileroptions",
		// 0));
		/* phourOldspinner.setSelection(prefs.getInt("hoursold", 0)); */

		if (equipmentOptionStr.length() != 0 && !equipmentOptionStr.equals("-")) {
			txtEquipmentOption.setVisibility(View.GONE);
			equipmentOptions_multiSelect_Spinner.setVisibility(View.VISIBLE);

			ArrayList<String> selectedEquipmentTypeList = Utils
					.getSelectedItems(equipmentOptionStr);
			equipmentOptions_multiSelect_Spinner
					.setSelection(selectedEquipmentTypeList);
			equipmentOptions_multiSelect_Spinner
					.setCount(selectedEquipmentTypeList.size());
			equipmentOptions_multiSelect_Spinner
					.setSelectedIds(Utils
							.getSelectedEquipmentTypeIndicesForPost(
									getApplicationContext(),
									selectedEquipmentTypeList));
		} else {
			txtEquipmentOption.setVisibility(View.VISIBLE);
			equipmentOptions_multiSelect_Spinner.setVisibility(View.GONE);
		}

		if (trailerOptionStr.length() != 0) {
			txtTrailerOption.setVisibility(View.GONE);
			trailerOptions_multiSelect_Spinner.setVisibility(View.VISIBLE);

			ArrayList<String> selectedTrailerOptionList = Utils
					.getSelectedItems(trailerOptionStr);
			trailerOptions_multiSelect_Spinner
					.setSelection(selectedTrailerOptionList);
			trailerOptions_multiSelect_Spinner
					.setCount(selectedTrailerOptionList.size());
			trailerOptions_multiSelect_Spinner.setSelectedIds(Utils
					.getSelectedTrailerOptionIndices(getApplicationContext(),
							selectedTrailerOptionList));
		} else {
			txtTrailerOption.setVisibility(View.VISIBLE);
			trailerOptions_multiSelect_Spinner.setVisibility(View.GONE);
		}
		if (profileloadsize == 1) {
			postloadsizefull.setChecked(true);
		} else {
			postloadsizeltl.setChecked(true);
		}
		if (prefs.getInt("ppradius", 25) == 0) {
			postradiustext.setText("25 mi");
			radius="25";
		} else {
			postradiuseekbar.setProgress(profileradius - 25);
		}
		if (posttype != 3 && posttype != 4) {
			posttruck.performClick();
		}

	}

	private void UpdateUI() {

		poriginspinner = (Spinner) findViewById(R.id.postorigincountryspinner);
		pdestinationspinner = (Spinner) findViewById(R.id.postdestcountryspinner);
		// pequipmentspinner = (Spinner)
		// findViewById(R.id.postequipmenttypespinner);
		// ptraileroptionspinner = (Spinner)
		// findViewById(R.id.posttraileroptionsspinner);

		equipmentOptions_multiSelect_Spinner = (MultiSelectionSpinnerposting) findViewById(R.id.postequipmenttypespinner);
		trailerOptions_multiSelect_Spinner = (MultiSelectionSpinner) findViewById(R.id.posttraileroptionsspinner);
		postoriginhomebase = (AutoCompleteTextView) findViewById(R.id.posthomebase);
		postdestinationbase = (AutoCompleteTextView) findViewById(R.id.postdestination);
		postdateSearch = (Button) findViewById(R.id.postpickdate);
		postloadSizegroup = (RadioGroup) findViewById(R.id.postroadiogrouploadsize);
		postloadsizefull = (RadioButton) findViewById(R.id.postradioloadsizefull);
		postloadsizeltl = (RadioButton) findViewById(R.id.postradioloadsizeltl);
		postradiusheader =(TextView)findViewById(R.id.textView4);
		// postloadsizeany = (RadioButton)
		// findViewById(R.id.postradioloadsizeany);
		postradiuseekbar = (SeekBar) findViewById(R.id.postradiusseekBar);
		postradiustext = (TextView) findViewById(R.id.postseekbarvalue);
		favourite = (CheckBox) findViewById(R.id.checkBoxfavorite);
		postwidth = (EditText) findViewById(R.id.pwidth);
		postlength = (EditText) findViewById(R.id.plength);
		postweight = (EditText) findViewById(R.id.pweight);
		postmindist = (EditText) findViewById(R.id.pmindistance);
		postratepermile = (EditText) findViewById(R.id.pratepermile);
		postquantity = (EditText) findViewById(R.id.pquantity);
		pspecialinfo = (EditText) findViewById(R.id.pspecialinfo);
		posttruck = (Button) findViewById(R.id.posttruck);
		countryforplaces = getResources().getStringArray(
				R.array.googlecountry_array);
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		todays = new Date();
		postdateSearch.setText(df.format(todays));

		postwidth.setOnFocusChangeListener(new DecimalFilter(postwidth,
				PostActivity.this));
		postlength.setOnFocusChangeListener(new DecimalFilter(postlength,
				PostActivity.this));
		postratepermile.setOnFocusChangeListener(new DecimalFilter(
				postratepermile, PostActivity.this));

		postweight.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					/*if (postweight.getText().toString() != null&& !postweight.getText().toString().equals("")) {
						double d = Double.valueOf(postweight.getText()
								.toString());
						int i = (int) (Math.round(d));
						if (i > 999999) {
							postweight.setText("999999");
						} else {
							postweight.setText(String.valueOf((int) (Math
									.round(d))));
						}
					}*/
					postweight.setText(Utils.weightRounding(postweight.getText().toString()));
					
				}
			}
		});

		postmindist.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					/*if (postmindist.getText().toString() != null
							&& !postmindist.getText().toString().equals("")) {
						double d = Double.valueOf(postmindist.getText()
								.toString());
						int i = (int) (Math.round(d));
						if (i > 9999) {
							postmindist.setText("9999");
						} else {
							postmindist.setText(String.valueOf((int) (Math
									.round(d))));
						}

					}*/
					postmindist.setText(Utils.mindistanceRounding(postmindist.getText().toString()));
				}
			}
		});

		txtEquipmentOption = (TextView) findViewById(R.id.post_et_spinner_txt);
		txtEquipmentOption.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				equipmentOptions_multiSelect_Spinner
						.setVisibility(View.VISIBLE);
				txtEquipmentOption.setVisibility(View.GONE);
				equipmentOptions_multiSelect_Spinner.performClick();
			}
		});

		txtTrailerOption = (TextView) findViewById(R.id.post_tr_spinner_txt);

		txtTrailerOption.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				trailerOptions_multiSelect_Spinner.setVisibility(View.VISIBLE);
				txtTrailerOption.setVisibility(View.GONE);
				trailerOptions_multiSelect_Spinner.performClick();
			}
		});

		posttruck.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				String origin = postoriginhomebase.getText().toString();
				String destination = postdestinationbase.getText().toString();
				length = Utils.decimalFormat(postlength.getText().toString());
				ratepermile = Utils.decimalFormat(postratepermile.getText()
						.toString());
				width = Utils.decimalFormat(postwidth.getText().toString());
				weight = Utils.weightRounding(postweight.getText().toString());
				mindist = Utils.mindistanceRounding(postmindist.getText()
						.toString());

				specialinfo = pspecialinfo.getText().toString();
				searchdate = postdateSearch.getText().toString();
				quantity = postquantity.getText().toString();

				
				
				
				if (profileradius==0) {
					radius="25";
				}
				
				
				
			String postcountryarray[] = getResources().getStringArray(R.array.postcountryab_array);
				origincountryindex = poriginspinner.getSelectedItemPosition();
			
				origincounrty = postcountryarray[origincountryindex];
				destinationcountryindex = pdestinationspinner.getSelectedItemPosition();
			//	destinationcountry = origincountryarray[destinationcountryindex];
				destinationcountry = postcountryarray[destinationcountryindex];
				// Selected Equipment Options
				equipmenttype = equipmentOptions_multiSelect_Spinner
						.getSelectedItemsAsString();

				// Selected Trailer OPtions
				traileroption = trailerOptions_multiSelect_Spinner
						.getSelectedItemsAsString();

				if (traileroption != null && traileroption.contains(";")) {
					traileroption = traileroption.replace(
							AppConstants.TOKEN_DELIMETER, ",");
				} else if (traileroption == null || traileroption.length() == 0) {
					traileroption = AppConstants.EMPTY_STRING;
				}

				if (quantity.equals("")) {
					quantity = "1";
				}
				if (searchdate.equals(AppConstants.setdate)) {
					postdateValidated = false;
					validationErrorMessage += "Date selection is missing\n";
				}
				//System.out.println("searchdate---------->" + searchdate);
				int profileloadsize = postloadSizegroup
						.getCheckedRadioButtonId();
				// find the radiobutton by returned id
				radioLoadButton = (RadioButton) findViewById(profileloadsize);
				isloadfull = radioLoadButton.getText().toString();
				//System.out.println(isloadfull);
				if (isloadfull.equals("LTL")) {
					isloadfull = "false";
				} else {
					isloadfull = "true";
				}

				if (origin.isEmpty()) {
					originstate = "";
					origincity = "";
				} else if (origin.contains(",")
						&& origin.replaceAll("[^,]", "").length() > 1) {
					postvalidated = false;
					validationErrorMessage += AppConstants.ORIGIN_NOTFOUND;
				} else {
					int count = 0;
					StringBuilder originstr = new StringBuilder();
					StringTokenizer st = new StringTokenizer(origin, ",");
					int len = st.countTokens();
					System.out.println("Len : "+len);
					if(len == 2){
						while (st.hasMoreElements()) {
							String searchorigin = (String) st.nextElement();
							searchorigin = searchorigin.trim();
							if (!searchorigin.matches("^[a-zA-Z\\s]+")) {
								postvalidated = false;
							} else if (count == 0) {
								origincity = searchorigin;
							} else {
								if (!Utils.searchstate(PostActivity.this,
										searchorigin, origincountryindex)) {
									postvalidated = false;
									// validationErrorMessage+=AppConstants.ORIGIN_NOTFOUND;
								} else {
									/*if (Utils.searchstate(PostActivity.this,
													searchorigin,
													origincountryindex)) {
										origincity = searchorigin;
									}*/
									originstr.append("," + Utils.statevalue);
								}
							}
							count++;
						}
					}
					
					
					originstate = Utils.formatString(originstr.toString());
					if (!postvalidated) {
						validationErrorMessage += AppConstants.ORIGIN_NOTFOUND;
					}
				}

				if (destination.isEmpty()) {
					destinationstate = "";
					destinationcity = "";
				} else if (destination.contains(",")
						&& destination.replaceAll("[^,]", "").length() > 1) {
					postdestinationValidated = false;
					validationErrorMessage += "\n"
							+ AppConstants.DESTINATION_NOTFOUND;
				} else {
					int count = 0;
					StringBuilder dststr = new StringBuilder();
					StringTokenizer st = new StringTokenizer(destination, ",");
					while (st.hasMoreElements()) {
						String searchdest = (String) st.nextElement();
						searchdest = searchdest.trim();
						if (!searchdest.matches("^[a-zA-Z\\s]+")) {
							postdestinationValidated = false;
						} else if (count == 0
								&& !Utils.searchstate(PostActivity.this,
										searchdest, destinationcountryindex)) {
							destinationcity = searchdest;
						} else {
							if (!Utils.searchstate(PostActivity.this,
									searchdest, destinationcountryindex)) {
								postdestinationValidated = false;
								// validationErrorMessage+=AppConstants.ORIGIN_NOTFOUND;
							} else {
								if (count == 0
										&& Utils.searchstate(PostActivity.this,
												searchdest,
												destinationcountryindex)) {
									destinationcity = searchdest;
								}
								dststr.append("," + Utils.statevalue);
							}
						}
						count++;
					}
					destinationstate = Utils.formatString(dststr.toString());
					if (!postdestinationValidated) {
						validationErrorMessage += "\n"
								+ AppConstants.DESTINATION_NOTFOUND;
					}
				}

				   if (postvalidated && postdestinationValidated
	                        && postdateValidated) {
	                    StringBuilder strbuilder = new StringBuilder();
	                    strbuilder.append(origincounrty + originstate + origincity
	                            + destinationcountry + destinationcity
	                            + destinationstate + radius + equipmenttype
	                            + searchdate + traileroption + hoursold
	                            + isloadfull + width + length + weight + mindist
	                            + ratepermile+quantity+specialinfo);
	                    String md = Utils.md5(strbuilder.toString());
	 
	                   
	                    DataModel fav = new DataModel(origincounrty, originstate,
	                            origincity, destinationcountry, destinationcity,
	                            destinationstate, radius, equipmenttype,
	                            searchdate, traileroption, hoursold, isloadfull,
	                            width, length, weight, mindist, ratepermile,quantity,specialinfo, md);
	                   String minimumdis= fav.getMindist();
	                 //  Log.d("MINDISTANCE", minimumdis);
	                    //Log.d(TAG, "md in searchmain -->" + md);
	                    if (favourite.isChecked()) {
	 
	                        if (db.addTopostfavourite(fav, md)) {
	                            Toast.makeText(getApplicationContext(),
	                                    AppConstants.ALREADY_FAVORITE,
	                                    Toast.LENGTH_LONG).show();
	                        } else {
	                            Toast.makeText(getApplicationContext(),
	                                    AppConstants.ADD_AS_FAVORITE,
	                                    Toast.LENGTH_LONG).show();
	                        }
	 
	                    }

					//new PostTruck().execute();
	                    postTruckToServer();

				} else {
					postvalidated = true;
					postdestinationValidated = true;
					postdateValidated = true;
					// validationErrorMessage+="\nor "+AppConstants.INVALID_SERACHFORMAT;
					Utils.showAlertMessage(validationErrorMessage,
							PostActivity.this);
					validationErrorMessage = "";
					// Toast.makeText(SearchMain.this,
					// AppConstants.INVALID_SERACHFORMAT
					// ,Toast.LENGTH_SHORT).show();
				}
			}

			

		});

		postdateSearch.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
			    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
    			    DialogFragment newFragment = new SelectDateFragment();
    				newFragment.show(getSupportFragmentManager(), "DatePicker");
			    }else{
			        showDialog(DATE_DIALOG_ID);
			    }
			}
		});

		// Set Adapter for origincountry
		ArrayAdapter<CharSequence> originDataAdapter = ArrayAdapter
				.createFromResource(PostActivity.this,
						R.array.profilecountry_array,
						android.R.layout.simple_spinner_dropdown_item);
		originDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		poriginspinner.setAdapter(originDataAdapter);
		poriginspinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				origincountryindex = arg0.getSelectedItemPosition();

				// storing string resources into Array
				String origincountryarray[] = getResources().getStringArray(
						R.array.profilecountry_array);
				
			/*	if (posttype == 3) {
					if (origincountryindex == postintentcountryindex) {
						if (postoriginhomebase.getText().toString()
								.equals(postintenthomebase))
							postoriginhomebase.setText(postintenthomebase);
						else
							postoriginhomebase.setText("");
					} else
						postoriginhomebase.setText("");
				}
				else*/ if(posttype == 4 || posttype == 3 ){
				if(originvalidate){
					originvalidate=false;
				}
				else
				{
					postoriginhomebase.setText("");
				}
				/*if (posttype == 4||posttype == 5) {

					if (origincountryindex == postintentcountryindex) {
						if (postoriginhomebase.getText().toString()
								.equals(corigin)) {
							postoriginhomebase.setText(corigin);
						} else
							postoriginhomebase.setText("");
					} else
						postoriginhomebase.setText("");

				}*/
				updategooleplacesfororigin(
						countryforplaces[origincountryindex],
						origincountryindex);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});

		// Set Adapter for destinationcountry
		ArrayAdapter<CharSequence> destinationDataAdapter = ArrayAdapter
				.createFromResource(PostActivity.this,
						R.array.profilecountry_array,
						android.R.layout.simple_spinner_dropdown_item);
		destinationDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		pdestinationspinner.setAdapter(destinationDataAdapter);
		pdestinationspinner.setOnItemSelectedListener(new OnItemSelectedListener() {
					@Override
					public void onItemSelected(AdapterView<?> arg0, View arg1,int arg2, long arg3) {
						destinationcountryindex = arg0.getSelectedItemPosition();

						// storing string resources into Array
						String origincountryarray[] = getResources().getStringArray(R.array.profilecountry_array);
						
					/*	if (posttype == 3) {
							postdestinationbase.setText("");
						}
						else*/ if(posttype == 4 || posttype ==3){
							
						if(dstvalidate){
							dstvalidate = false;
						}
						else{
							postdestinationbase.setText("");
						}
					/*	if (posttype == 4||posttype == 5) {

							if (destinationcountryindex == postintentcountryindex) {
								if (postdestinationbase.getText().toString()
										.equals(cdest)) {
									postdestinationbase.setText(cdest);
								} else
									postdestinationbase.setText("");
							} else
								postdestinationbase.setText("");

						}*/
						updategooleplacesfordest(
								countryforplaces[destinationcountryindex],
								destinationcountryindex);
						}
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {
					}

				});
		
		
		
		postoriginhomebase.addTextChangedListener(new TextWatcher() {
			@Override
			   public void onTextChanged(CharSequence s, int start, int before,int count) {
			    String textwatcher = String.valueOf(s);
			    if(textwatcher.equals("")){
			    	origin = false ;
			    	radiusVisibility();
			    }else if(textwatcher.contains(",")){
			     int num = textwatcher.replaceAll("[^,]","").length();
			     if (num==1) {
			      String[] tokens = textwatcher.split(",");
			      if(null != tokens && tokens.length>0){
				      boolean statevalidated= Utils.searchstate(PostActivity.this,tokens[0],origincountryindex);
				      boolean statevalidated2=false;
					      try {
					        statevalidated2=Utils.searchstate(PostActivity.this,tokens[1],origincountryindex);
					      } catch (Exception e) {
					      }
				      
					      if (!statevalidated&&statevalidated2) {
					    	  origin = true;
					    	  radiusVisibility();
					      }else{
					    	  origin = false;
					    	  radiusVisibility();
					      }
			      } else{
			    	  		Utils.showAlertMessage("Invalid Origin City,State", PostActivity.this);
			      		}
			      
			     }
			     
			    }else{
			    	origin = false;
			    	radiusVisibility();
			    }
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}

			

		});
		postdestinationbase.addTextChangedListener(new TextWatcher() {

			@Override
			   public void onTextChanged(CharSequence s, int start, int before,int count) {
			  String  textwatcher = String.valueOf(s);
			    if(textwatcher.equals("")){
			    	destination = false ;
			    	radiusVisibility();
			    }else if(textwatcher.contains(",")){
			     int num = textwatcher.replaceAll("[^,]","").length();
			     if (num==1) {
			      String[] tokens = textwatcher.split(",");
			      if(null != tokens && tokens.length>0){
			    	  boolean statevalidated= Utils.searchstate(PostActivity.this,tokens[0],destinationcountryindex);
			    	  boolean statevalidated2=false;
				      try {
				        statevalidated2=Utils.searchstate(PostActivity.this,tokens[1],destinationcountryindex);
				      } catch (Exception e) {
				      }
				      if (!statevalidated&&statevalidated2) {
				    	  destination = true;
				    	  radiusVisibility();
				      }else{
				    	  destination = false;
				    	  radiusVisibility();
				      }
			      }else{
			    	  		Utils.showAlertMessage("Invalid Destination City,State", PostActivity.this);
			      		}    
			     }
			     
			    }else{
			    	destination = false;
			    	radiusVisibility();
			    }
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		postradiuseekbar
				.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
					public void onProgressChanged(SeekBar seekBar,
							int progress, boolean fromUser) {
						int yourStep = 25;
						int profileradius = ((int) Math.round(progress
								/ yourStep))
								* yourStep;
						profileradius += 25;
						seekBar.setProgress(progress);
						postradiustext.setText(String.valueOf(profileradius)+ " mi");
						radius = String.valueOf(profileradius);
					}

					public void onStartTrackingTouch(SeekBar seekBar) {
					}

					public void onStopTrackingTouch(SeekBar seekBar) {
					}
				});

		// Set Adapter for EquipmentType
		/*
		 * ArrayAdapter<CharSequence> equipmentDataAdapter =
		 * ArrayAdapter.createFromResource(PostActivity.this,
		 * R.array.posttype_array
		 * ,android.R.layout.simple_spinner_dropdown_item);
		 * equipmentDataAdapter.
		 * setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item
		 * ); pequipmentspinner.setAdapter(equipmentDataAdapter);
		 * pequipmentspinner.setOnItemSelectedListener(new
		 * OnItemSelectedListener() {
		 * 
		 * @Override public void onItemSelected(AdapterView<?> arg0, View
		 * arg1,int arg2, long arg3) { equipmenttypeindex =
		 * arg0.getSelectedItemPosition();
		 * 
		 * // storing string resources into Array // String equipmenttypearray[]
		 * = // getResources().getStringArray(R.array.type_array); String
		 * equipmenttypeabrv[] = getResources()
		 * .getStringArray(R.array.posttype_AB_array); equipmenttype =
		 * equipmenttypeabrv[equipmenttypeindex];
		 * System.out.println("equipmenttype----------->"+ equipmenttype);
		 * 
		 * Toast.makeText(SearchMain.this, "You have selected : "
		 * +equipmenttypearray[equipmenttypeindex], Toast.LENGTH_SHORT).show();
		 * 
		 * }
		 * 
		 * @Override public void onNothingSelected(AdapterView<?> arg0) { } });
		 */

		String[] equipmentOptions = getResources().getStringArray(
				R.array.posttype_array);
		if (null != equipmentOptions && equipmentOptions.length > 0) {
			equipmentOptions_multiSelect_Spinner
					.setSelectType(AppConstants.SELECT_ONE);
			equipmentOptions_multiSelect_Spinner.setItems(equipmentOptions);
			equipmentOptions_multiSelect_Spinner.setmDialogCancelListener(this);
		}

		String[] trailerOptions = getResources().getStringArray(
				R.array.option_array);
		if (null != trailerOptions && trailerOptions.length > 0) {
			trailerOptions_multiSelect_Spinner
					.setSelectType(AppConstants.SELECT_MAX);
			trailerOptions_multiSelect_Spinner.setItems(trailerOptions);
			trailerOptions_multiSelect_Spinner.setmDialogCancelListener(this);
		}

	}

	 public void populateSetDate(int year, int month, int day) {
	        SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd");
	        // searchdate=;
	        try {
	            Date date = outFormat.parse(String.valueOf(year) + "-"+ String.valueOf(month) + "-" + String.valueOf(day));
	 
	            searchdate = outFormat.format(date).toString();
	           // System.out.println("populateSetDate using date formattesr--"+ searchdate);
	 
	            postdateSearch.setText(searchdate);
	        } catch (ParseException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }
	 
	    }
	    
	    @SuppressLint("ValidFragment")
	    public  class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
	 
	        @Override
	        public Dialog onCreateDialog(Bundle savedInstanceState) {
	            
	            DateFormat df= new SimpleDateFormat("yyyy-MM-dd");
	            Date startDate=null;
	            searchdate=postdateSearch.getText().toString();
	            if(searchdate!=null && !searchdate.equals(AppConstants.setdate)){
	                try {
	                    startDate = df.parse(searchdate);
	                } catch (ParseException e) {
	                    e.printStackTrace();
	                }
	                
	                int returnvalue=Utils.compareDates(startDate, todays);
	                if (returnvalue==0||returnvalue==-1) {
	                    final Calendar calendar = Calendar.getInstance();
	                     yy = calendar.get(Calendar.YEAR);
	                     mm = calendar.get(Calendar.MONTH);
	                     dd = calendar.get(Calendar.DAY_OF_MONTH);
	                }else{
	                    String[]dateparts = searchdate.split("-");
	                    yy = Integer.parseInt(dateparts[0]);  
	                    mm = Integer.parseInt(dateparts[1])-1;  
	                    dd=Integer.parseInt(dateparts[2]);
	                }
	 
	                if(Build.MANUFACTURER.contains(AppConstants.SAMSUNG) || Build.BRAND.contains(AppConstants.SAMSUNG)){
	                    dialog =new CustomDatePickerDialog(getActivity(), datePickerListener, yy, mm, dd);
	 
	                    dialog.setButton(DialogInterface.BUTTON_NEUTRAL, "Clear", new DialogInterface.OnClickListener() {
	 
	                        public void onClick(DialogInterface dialog, int id) {
	                            tempDateFlag = false ;
	                            postdateSearch.setText(AppConstants.setdate);
	                          /*  DatePicker datePicker = ((DatePickerDialog) dialog).getDatePicker();
	 
	                            datePickerListener.onDateSet(datePicker,
	                                    datePicker.getYear(),
	                                    datePicker.getMonth(),
	                                    datePicker.getDayOfMonth());*/
	                            dialog.dismiss();
	                        }
	                    });
	                    
	                   dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Set", new DialogInterface.OnClickListener() {
	 
	                       @SuppressLint("NewApi")
						public void onClick(DialogInterface dialog, int id) {
	                           tempDateFlag = true;
	                           DatePicker datePicker = ((DatePickerDialog) dialog).getDatePicker();
	 
	                           datePickerListener.onDateSet(datePicker,
	                                   datePicker.getYear(),
	                                   datePicker.getMonth(),
	                                   datePicker.getDayOfMonth());
	                           dialog.dismiss();
	                       }
	                   });
	                }else{
	                    dialog =new CustomDatePickerDialog(getActivity(), this, yy, mm, dd);
	                    dialog.setButton(DialogInterface.BUTTON_NEUTRAL, "Clear", new DialogInterface.OnClickListener() {
	                        public void onClick(DialogInterface dialog, int id) {
	                            tempDateFlag = false ;
	                            postdateSearch.setText(AppConstants.setdate);
	                            dialog.dismiss();
	                        }
	                    });
	                    
	                   dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Set", new DialogInterface.OnClickListener() {
	                       @SuppressLint("NewApi")
						public void onClick(DialogInterface dialog, int id) {
	                           tempDateFlag = true;
	                           DatePicker datePicker = ((DatePickerDialog) dialog).getDatePicker();
	                              datePickerListener.onDateSet(datePicker,
	                                      datePicker.getYear(),
	                                      datePicker.getMonth(),
	                                      datePicker.getDayOfMonth());
	                           dialog.dismiss();
	                       }
	                   });
	                }
	                  
	         }  
	         return  dialog;
	        }
	 
	        @Override
	        public void onDateSet(DatePicker dp, int yy, int mm, int dd) {
	            if(tempDateFlag){
	                populateSetDate(yy, mm + 1, dd);
	            }else{
	                tempDateFlag = true ;
	            }
	        }
	        
	        final DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
	            // when dialog box is closed, below method will be called.
	            public void onDateSet(DatePicker dp, int yy,
	                    int mm, int dd) {
	                if (tempDateFlag) {
	                    populateSetDate(yy, mm + 1, dd);
	                } else {
	                    tempDateFlag = true;
	                }
	            }
	        };
	    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.post, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:

			// NavUtils.navigateUpFromSameTask(this);
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	
	
	
	
	private void postTruckToServer() {
		// TODO Auto-generated method stub
		try {
			String posttrailer = "";
			// Selected Equipment OPtions
			List<Integer> selectedIndices = equipmentOptions_multiSelect_Spinner
					.getSelectedIndicies();
			equipmenttype = Utils.getSelectedEquipmentTypesForPost(
					PostActivity.this, selectedIndices);

			if (equipmenttype != null && equipmenttype.contains(";")) {
				equipmenttype = equipmenttype.replace(
						AppConstants.TOKEN_DELIMETER, ",");
			} else if (equipmenttype == null || equipmenttype.length() == 0) {
				equipmenttype = AppConstants.EMPTY_STRING;
			}

			if (traileroption.equals(AppConstants.EMPTY_STRING)
					|| traileroption.equals(AppConstants.NONE)) {
				posttrailer = "";
			} else {
				List<String> trailerOptions = new ArrayList<String>();
				trailerOptions = Utils.getSelectedItems(traileroption, ",");
				if (null != trailerOptions && trailerOptions.size() > 0) {
					Iterator<String> iterator = trailerOptions.iterator();
					while (iterator.hasNext()) {
						String trailerOpt = iterator.next();

						if (trailerOpt.equals(AppConstants.OPTION_TARPS)) {
							posttrailer += "<truc:TrailerOptionType>"
									+ TrailerOptions.Tarps
									+ "</truc:TrailerOptionType>\n";
						} else if (trailerOpt
								.equals(AppConstants.OPTION_HAZARDOUS)) {
							posttrailer += "<truc:TrailerOptionType>"
									+ TrailerOptions.Hazardous
									+ "</truc:TrailerOptionType>\n";
						} else if (trailerOpt
								.equals(AppConstants.OPTION_PALLETEXCHANGE)) {
							posttrailer += "<truc:TrailerOptionType>"
									+ TrailerOptions.PalletExchange
									+ "</truc:TrailerOptionType>\n";
						} else if (trailerOpt
								.equals(AppConstants.OPTION_TEAM)) {
							posttrailer += "<truc:TrailerOptionType>"
									+ TrailerOptions.Team
									+ "</truc:TrailerOptionType>\n";
						} else if (trailerOpt
								.equals(AppConstants.OPTION_EXPEDITED)) {
							posttrailer += "<truc:TrailerOptionType>"
									+ TrailerOptions.Expedited
									+ "</truc:TrailerOptionType>\n";
						}
					}
				}
			}
			
			String envolpe = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\""
					+ " xmlns:v11=\"http://webservices.truckstop.com/v11\""
					+ " xmlns:web=\"http://schemas.datacontract.org/2004/07/WebServices\""
					+ " xmlns:web1=\"http://schemas.datacontract.org/2004/07/WebServices.Posting\""
					+ " xmlns:web2=\"http://schemas.datacontract.org/2004/07/WebServices.Objects\""
					+ " xmlns:truc=\"http://schemas.datacontract.org/2004/07/Truckstop2.Objects\""
					+ ">";
			String body = envolpe + "\n<soapenv:Header/>\n"
					+ "<soapenv:Body>\n" + "<v11:PostTrucks>\n"
					+ "<v11:trucks>\n" + "<web:IntegrationId>"
					+ session.getUserIntegrationID()
					+ "</web:IntegrationId>\n" + "<web:Password>"
					+ AppConstants.PASSWORD + "</web:Password>\n"
					+ "<web:UserName>" + AppConstants.USERNAME
					+ "</web:UserName>\n" + "<web1:Handle>"
					+ session.getItsHandle() + "</web1:Handle>\n"
					+ "<web1:Trucks>\n" + "<web2:Truck>\n"
					+ "<web2:DateAvailable>" + searchdate
					+ "</web2:DateAvailable>\n"
					+ "<web2:DestinationCity>" + destinationcity
					+ "</web2:DestinationCity>\n"
					+ "<web2:DestinationCountry>" + destinationcountry
					+ "</web2:DestinationCountry>\n"
					+ "<web2:DestinationRadius>" + radius
					+ "</web2:DestinationRadius>\n"
					+ "<web2:DestinationState>" + destinationstate
					+ "</web2:DestinationState>\n"
					+ "<web2:EquipmentOptions>\n" + posttrailer
					+ "</web2:EquipmentOptions>\n"
					+ "<web2:EquipmentType>" + equipmenttype
					+ "</web2:EquipmentType>\n" + "<web2:Handle>"
					+ session.getItsHandle() + "</web2:Handle>\n"
					+ "<web2:IsLoadFull>" + isloadfull
					+ "</web2:IsLoadFull>\n" + "<web2:Length>" + length
					+ "</web2:Length>\n" + "<web2:MinDistance>"
					+ mindist + "</web2:MinDistance>\n"
					+ "<web2:OriginCity>" + origincity
					+ "</web2:OriginCity>\n" + "<web2:OriginCountry>"
					+ origincounrty + "</web2:OriginCountry>\n"
					+ "<web2:OriginRadius>" + radius
					+ "</web2:OriginRadius>\n" + "<web2:OriginState>"
					+ originstate + "</web2:OriginState>\n"
					+ "<web2:Quantity>" + quantity
					+ "</web2:Quantity>\n" + "<web2:RatePerMile>"
					+ ratepermile + "</web2:RatePerMile>\n"
					+ "<web2:SpecInfo>" + specialinfo
					+ "</web2:SpecInfo>\n"
					+ "<web2:TimeAvailable></web2:TimeAvailable>\n"
					+ "<web2:TruckID>" + loadid + "</web2:TruckID>\n"
					+ "<web2:TruckMultiDest></web2:TruckMultiDest>\n"
					+ "<web2:TruckNumber></web2:TruckNumber>\n"
					+ "<web2:Weight>" + weight + "</web2:Weight>\n"
					+ "<web2:Width>" + width + "</web2:Width>\n"
					+ "</web2:Truck>\n" + "</web1:Trucks>\n"
					+ "</v11:trucks>\n" + "</v11:PostTrucks>\n"
					+ "</soapenv:Body>\n" + "</soapenv:Envelope>\n";
//System.out.println("BODY VALUE----" + body);
String SOAPACTION="http://webservices.truckstop.com/v11/ITruckPosting/PostTrucks";

	HttpServiceListener listener = new HttpServiceListener();
	WebServiceConnectionService serv = new WebServiceConnectionService(PostActivity.this,body,
			SOAPACTION, listener,AppConstants.POST_PROGRESS);
	serv.execute(AppConstants.GETTRUCKS_TEST_URL);         
			
		} catch (Exception e) {
			Utils.showAlertMessage("Unable to Process Request",PostActivity.this);
		}
		
	}
	
	
	
	 protected class HttpServiceListener implements ConnectionServiceListener {

		@Override
		public void onServiceComplete(String response) {
			 if (response.equals(AppConstants.INTERNET_CONNECTION_ERROR)) {
					Utils.showAlertMessage(AppConstants.INTERNET_CONNECTION_ERROR,
							PostActivity.this);
					/*
					 * Toast.makeText(PostActivity.this,AppConstants.
					 * INTERNET_CONNECTION_ERROR,Toast.LENGTH_LONG).show();
					 * finish();
					 */
				} else if (response.equals(AppConstants.SERVER_CONNECTION_ERROR)) {
					Utils.showAlertMessage(AppConstants.SERVER_CONNECTION_ERROR,
							PostActivity.this);
					/*
					 * Toast.makeText(PostActivity.this,AppConstants.
					 * SERVER_CONNECTION_ERROR, Toast.LENGTH_LONG).show(); finish();
					 */
				}else if (response.equals(AppConstants.PLAYSERVICES_NOTFOUND)) {
	                //Utils.showErrorMessage(AppConstants.PLAYSERVICES_NOTFOUND,PostActivity.this);
					Utils.updateGoogleplay(PostActivity.this);
	                
	            }  else {
					
					boolean Postingsucess = false;
					try {
						Postingsucess = XMLTruckPostparser.parsexml(response);
					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					if (Postingsucess) {
						truckId = XMLTruckPostparser.getTruckPostedId();
						if (repostingTruck) {
							ShowMessage(AppConstants.REPOST_SUCESS + truckId);
						} else {
							ShowMessage(AppConstants.POST_SUCESS + truckId);
						}
						//System.out.println("truckId---->" + truckId);
						
					} else {
						errormsg = XMLTruckPostparser.geterrormsg();
						System.out.println("errormsg IN IF-----"+ errormsg);
						Utils.showAlertMessage(errormsg,PostActivity.this);
						
					}
			 
				}
			
		}

		@Override
		public void onServiceComplete(String response, String reference) {
			// TODO Auto-generated method stub
			
		}
		 
		 
		 
	 }
	 
	
	
	
	
	
	

	/*********************************************************
	 * Class PostTruck
	 * 
	 * Description: Task for processing the HTTP request
	 *//*

	private class PostTruck extends AsyncTask<Void, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progDialog = new ProgressDialog(PostActivity.this);
			progDialog.setMessage(AppConstants.POST_PROGRESS);
			progDialog.setCancelable(false);
			progDialog.show();
			if (PostActivity.this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
				PostActivity.this
						.setRequestedOrientation(Configuration.ORIENTATION_PORTRAIT);
			} else {
				PostActivity.this
						.setRequestedOrientation(Configuration.ORIENTATION_LANDSCAPE);
			}

		}

		@Override
		protected String doInBackground(Void... params) {
			try {
				String posttrailer = "";
				// Selected Equipment OPtions
				List<Integer> selectedIndices = equipmentOptions_multiSelect_Spinner
						.getSelectedIndicies();
				equipmenttype = Utils.getSelectedEquipmentTypesForPost(
						PostActivity.this, selectedIndices);

				if (equipmenttype != null && equipmenttype.contains(";")) {
					equipmenttype = equipmenttype.replace(
							AppConstants.TOKEN_DELIMETER, ",");
				} else if (equipmenttype == null || equipmenttype.length() == 0) {
					equipmenttype = AppConstants.EMPTY_STRING;
				}

				if (traileroption.equals(AppConstants.EMPTY_STRING)
						|| traileroption.equals(AppConstants.NONE)) {
					posttrailer = "";
				} else {
					List<String> trailerOptions = new ArrayList<String>();
					trailerOptions = Utils.getSelectedItems(traileroption, ",");
					if (null != trailerOptions && trailerOptions.size() > 0) {
						Iterator<String> iterator = trailerOptions.iterator();
						while (iterator.hasNext()) {
							String trailerOpt = iterator.next();

							if (trailerOpt.equals(AppConstants.OPTION_TARPS)) {
								posttrailer += "<truc:TrailerOptionType>"
										+ TrailerOptions.Tarps
										+ "</truc:TrailerOptionType>\n";
							} else if (trailerOpt
									.equals(AppConstants.OPTION_HAZARDOUS)) {
								posttrailer += "<truc:TrailerOptionType>"
										+ TrailerOptions.Hazardous
										+ "</truc:TrailerOptionType>\n";
							} else if (trailerOpt
									.equals(AppConstants.OPTION_PALLETEXCHANGE)) {
								posttrailer += "<truc:TrailerOptionType>"
										+ TrailerOptions.PalletExchange
										+ "</truc:TrailerOptionType>\n";
							} else if (trailerOpt
									.equals(AppConstants.OPTION_TEAM)) {
								posttrailer += "<truc:TrailerOptionType>"
										+ TrailerOptions.Team
										+ "</truc:TrailerOptionType>\n";
							} else if (trailerOpt
									.equals(AppConstants.OPTION_EXPEDITED)) {
								posttrailer += "<truc:TrailerOptionType>"
										+ TrailerOptions.Expedited
										+ "</truc:TrailerOptionType>\n";
							}
						}
					}
				}

				// System.out.println(Utils.isInternetAvailable(PostActivity.this));
				if (Utils.isInternetAvailable(PostActivity.this)) {
					HttpPost post = new HttpPost(
							AppConstants.GETTRUCKS_TEST_URL);
					HttpParams httpParameters = new BasicHttpParams();
					HttpConnectionParams.setConnectionTimeout(httpParameters,
							AppConstants.TIMEOUTCONNECTION_ERROR);
					HttpConnectionParams.setSoTimeout(httpParameters,
							AppConstants.SOCKETTIMEOUT_ERROR);
					DefaultHttpClient client = new DefaultHttpClient(
							httpParameters);
					 String url = AppConstants.GETTRUCKS_TEST_URL;
	                    //Log.i(TAG, AppConstants.GETTRUCKS_TEST_URL);
						URL obj = new URL(url);
						HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
						ITSApplication app = ((ITSApplication) getApplicationContext());
						//ITSApplication app= new ITSApplication();
						if (app.certifcate()==null) {
							return AppConstants.SERVER_CONNECTION_ERROR;
						}
						
						con.setSSLSocketFactory(app.certifcate().getSocketFactory());
						con.setRequestMethod("POST");
						con.setRequestProperty("Accept-Encoding", "gzip,deflate");
						con.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
	                    // SOAPAction:
	                    // "http://webservices.truckstop.com/v12/ILoadSearch/GetLoadSearchResults"
						con.setRequestProperty("SOAPAction","http://webservices.truckstop.com/v11/ITruckPosting/PostTrucks");
						con.setRequestProperty("Host", "webservices.truckstop.com");
						con.setRequestProperty("Connection", "Keep-Alive");
						con.setRequestProperty("User-Agent", "Apache-HttpClient/4.1.1 (java 1.5)\\");
					
					
					
					
				

					String envolpe = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\""
							+ " xmlns:v11=\"http://webservices.truckstop.com/v11\""
							+ " xmlns:web=\"http://schemas.datacontract.org/2004/07/WebServices\""
							+ " xmlns:web1=\"http://schemas.datacontract.org/2004/07/WebServices.Posting\""
							+ " xmlns:web2=\"http://schemas.datacontract.org/2004/07/WebServices.Objects\""
							+ " xmlns:truc=\"http://schemas.datacontract.org/2004/07/Truckstop2.Objects\""
							+ ">";
					String body = envolpe + "\n<soapenv:Header/>\n"
							+ "<soapenv:Body>\n" + "<v11:PostTrucks>\n"
							+ "<v11:trucks>\n" + "<web:IntegrationId>"
							+ session.getUserIntegrationID()
							+ "</web:IntegrationId>\n" + "<web:Password>"
							+ AppConstants.PASSWORD + "</web:Password>\n"
							+ "<web:UserName>" + AppConstants.USERNAME
							+ "</web:UserName>\n" + "<web1:Handle>"
							+ session.getItsHandle() + "</web1:Handle>\n"
							+ "<web1:Trucks>\n" + "<web2:Truck>\n"
							+ "<web2:DateAvailable>" + searchdate
							+ "</web2:DateAvailable>\n"
							+ "<web2:DestinationCity>" + destinationcity
							+ "</web2:DestinationCity>\n"
							+ "<web2:DestinationCountry>" + destinationcountry
							+ "</web2:DestinationCountry>\n"
							+ "<web2:DestinationRadius>" + radius
							+ "</web2:DestinationRadius>\n"
							+ "<web2:DestinationState>" + destinationstate
							+ "</web2:DestinationState>\n"
							+ "<web2:EquipmentOptions>\n" + posttrailer
							+ "</web2:EquipmentOptions>\n"
							+ "<web2:EquipmentType>" + equipmenttype
							+ "</web2:EquipmentType>\n" + "<web2:Handle>"
							+ session.getItsHandle() + "</web2:Handle>\n"
							+ "<web2:IsLoadFull>" + isloadfull
							+ "</web2:IsLoadFull>\n" + "<web2:Length>" + length
							+ "</web2:Length>\n" + "<web2:MinDistance>"
							+ mindist + "</web2:MinDistance>\n"
							+ "<web2:OriginCity>" + origincity
							+ "</web2:OriginCity>\n" + "<web2:OriginCountry>"
							+ origincounrty + "</web2:OriginCountry>\n"
							+ "<web2:OriginRadius>" + radius
							+ "</web2:OriginRadius>\n" + "<web2:OriginState>"
							+ originstate + "</web2:OriginState>\n"
							+ "<web2:Quantity>" + quantity
							+ "</web2:Quantity>\n" + "<web2:RatePerMile>"
							+ ratepermile + "</web2:RatePerMile>\n"
							+ "<web2:SpecInfo>" + specialinfo
							+ "</web2:SpecInfo>\n"
							+ "<web2:TimeAvailable></web2:TimeAvailable>\n"
							+ "<web2:TruckID>" + loadid + "</web2:TruckID>\n"
							+ "<web2:TruckMultiDest></web2:TruckMultiDest>\n"
							+ "<web2:TruckNumber></web2:TruckNumber>\n"
							+ "<web2:Weight>" + weight + "</web2:Weight>\n"
							+ "<web2:Width>" + width + "</web2:Width>\n"
							+ "</web2:Truck>\n" + "</web1:Trucks>\n"
							+ "</v11:trucks>\n" + "</v11:PostTrucks>\n"
							+ "</soapenv:Body>\n" + "</soapenv:Envelope>\n";
System.out.println("BODY VALUE----" + body);
				con.setConnectTimeout(AppConstants.SOCKETTIMEOUT_ERROR);
				con.setDoOutput(true);
				DataOutputStream wr = new DataOutputStream(con.getOutputStream());
				//System.out.println("BODY VALUE----" + body);
				wr.writeBytes(body);
				wr.flush();
				wr.close();
		 
				int responseCode = con.getResponseCode();
                // System.out.println("Status coded-------" + statuscodes);
				if (responseCode!=200) {
                    return AppConstants.SERVER_CONNECTION_ERROR;
                } else {
                	InputStream stream = con.getInputStream();
					String contentEncoding = con.getHeaderField("Content-Encoding");
						if (contentEncoding != null&& contentEncoding.equalsIgnoreCase("gzip")) {
							final InputStream stream1 = new GZIPInputStream(stream);
							// System.out.println("i am here");
							BufferedReader reader = new BufferedReader(new InputStreamReader(stream1));
							String line;
							StringBuilder builder1 = new StringBuilder();
							while ((line = reader.readLine()) != null) {
								builder1.append(line);
								response_str = builder1.toString();
								//System.out.println("retval IN IF-----"+ response_str);
								boolean Postingsucess = XMLTruckPostparser
										.parsexml(response_str);

								if (Postingsucess) {
									truckId = XMLTruckPostparser
											.getTruckPostedId();
									//System.out.println("truckId---->" + truckId);
									return "success";
								} else {
									errormsg = XMLTruckPostparser.geterrormsg();
							//System.out.println("errormsg IN IF-----"+ errormsg);
									return "error";
								}
							}
						} else {
							BufferedReader reader = new BufferedReader(
									new InputStreamReader(stream));
							String line;
							StringBuilder builder1 = new StringBuilder();
							while ((line = reader.readLine()) != null) {
								builder1.append(line);
								response_str = builder1.toString();
								//System.out.println("retval IN ELSE-----"+ response_str);
								boolean Postingsucess = XMLTruckPostparser
										.parsexml(response_str);
								if (Postingsucess) {
									truckId = XMLTruckPostparser
											.getTruckPostedId();
									//System.out.println("truckId---->" + truckId);
									return "success";
								} else {
									errormsg = XMLTruckPostparser.geterrormsg();
									//System.out.println("errormsg IN IF-----"+ errormsg);
									return "error";
								}
							}
						}
					}
				} else {
					return AppConstants.INTERNET_CONNECTION_ERROR;
				}
			} catch (Exception e) {
				e.printStackTrace();
				return AppConstants.SERVER_CONNECTION_ERROR;
			}
			return AppConstants.SERVER_CONNECTION_ERROR;
		}

		protected void onPostExecute(String result) {
			progDialog.dismiss();
			//PostActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
			if (result.equals("error")) {
				Utils.showAlertMessage(errormsg, PostActivity.this);
			} else if (result.equals(AppConstants.INTERNET_CONNECTION_ERROR)) {
				Utils.showAlertMessage(AppConstants.INTERNET_CONNECTION_ERROR,
						PostActivity.this);
				
				 * Toast.makeText(PostActivity.this,AppConstants.
				 * INTERNET_CONNECTION_ERROR,Toast.LENGTH_LONG).show();
				 * finish();
				 
			} else if (result.equals(AppConstants.SERVER_CONNECTION_ERROR)) {
				Utils.showAlertMessage(AppConstants.SERVER_CONNECTION_ERROR,
						PostActivity.this);
				
				 * Toast.makeText(PostActivity.this,AppConstants.
				 * SERVER_CONNECTION_ERROR, Toast.LENGTH_LONG).show(); finish();
				 
			} else {
				if (repostingTruck) {
					ShowMessage(AppConstants.REPOST_SUCESS + truckId);
				} else {
					ShowMessage(AppConstants.POST_SUCESS + truckId);
				}
			}
		}
	}*/

	public void ShowMessage(String Message) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				PostActivity.this);
		alertDialogBuilder.setTitle("Posting Result");// set title
		// set dialog message
		alertDialogBuilder.setMessage(Message).setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						Intent i = new Intent(PostActivity.this,
								CurrentPostScreen.class);
						i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(i);
						finish();
					}
				});
		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.show();

	}

	/*********************************************************
	 * Description: Updates origin autocomplete for PlacesAutoCompleteAdapter
	 * with new googlecountry component and country to index to search the
	 * entered value exists in existing state and abr list(Found in values
	 * folder)
	 * 
	 * 
	 */

	private void updategooleplacesfororigin(String googlecountry,
			int countryindex) {
		adapter = new PlacesAutoCompleteAdapter(PostActivity.this,
				googlecountry, countryindex, R.layout.list_item);
		postoriginhomebase.setAdapter(adapter);

	}

	/*********************************************************
	 * Description: Updates destination autocomplete for
	 * PlacesAutoCompleteAdapter with new googlecountry component and country to
	 * index to search the entered value exists in existing state and abr
	 * list(Found in values folder)
	 * 
	 * 
	 */

	private void updategooleplacesfordest(String googlecountry, int countryindex) {
		// adapter=new
		// PlacesAutoCompleteAdapter(PostActivity.this,googlecountry,countryindex,R.layout.autocomplete_list,
		// R.id.list_item);
		adapter = new PlacesAutoCompleteAdapter(PostActivity.this,
				googlecountry, countryindex, R.layout.list_item);
		postdestinationbase.setAdapter(adapter);

	}

	@Override
	public void onDialogCancelled(int spinnerType, int type, boolean flag) {
		if (type == AppConstants.TRAILER_OPTION && flag == false) {
			trailerOptions_multiSelect_Spinner.setVisibility(View.GONE);
			txtTrailerOption.setVisibility(View.VISIBLE);
		} else if (type == AppConstants.EQUIPMENT_TYPE && flag == false) {
			equipmentOptions_multiSelect_Spinner.setVisibility(View.GONE);
			txtEquipmentOption.setVisibility(View.VISIBLE);
		}
	}
	
	
	/**
	 * DatePicker Dialog support for Android API level below 11
	 */
	
 private DatePickerDialog dateDialog ;
     
     @Override
        protected Dialog onCreateDialog(int id) {
            switch (id) {
            case DATE_DIALOG_ID:
               // set date picker as current date
                searchdate=postdateSearch.getText().toString();
                if(searchdate!=null && !searchdate.equals(AppConstants.setdate)){
                    
                    if (searchdate.equals(AppConstants.setdate)) {
                        final Calendar calendar = Calendar.getInstance();
                        yy = calendar.get(Calendar.YEAR);
                        mm = calendar.get(Calendar.MONTH);
                        dd = calendar.get(Calendar.DAY_OF_MONTH);
                    }else{
                        String[]dateparts = searchdate.split("-");
                        yy = Integer.parseInt(dateparts[0]); //  
                        mm = Integer.parseInt(dateparts[1])-1; //  
                        dd=Integer.parseInt(dateparts[2]);
                    }
                }    
               dateDialog = new DatePickerDialog(this, datePickerListener, 
                             yy, mm, dd);
               dateDialog.setCancelable(false);
               dateDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override 
                            public void onClick(DialogInterface dialog, int which) {
                                postdateSearch.setText(AppConstants.setdate);
                             dialog.dismiss();
     
                            } 
                        }); 
                
                return dateDialog;
            }
            return null;
        }
     
     private DatePickerDialog.OnDateSetListener datePickerListener 
         = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                int selectedMonth, int selectedDay) {
            int year = selectedYear;
            int month = selectedMonth;
            int day = selectedDay;
        
            try {
                SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date date = outFormat.parse(String.valueOf(year)+"-"+String.valueOf(month+ 1)+"-"+String.valueOf(day));
                searchdate=outFormat.format(date).toString();
                //System.out.println("populateSetDate using date formattesr--"+searchdate);
                postdateSearch.setText(searchdate);
                //cleardate.setVisibility(View.VISIBLE);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        
        }
     };
     
     protected void radiusVisibility() {
         // TODO Auto-generated method stub
         if (origin || destination) {
        	 postradiusheader.setVisibility(View.VISIBLE);
        	 postradiuseekbar.setVisibility(View.VISIBLE);
        	 postradiustext.setVisibility(View.VISIBLE);
         } else {
        	 postradiusheader.setVisibility(View.GONE);
             postradiuseekbar.setVisibility(View.GONE);
             postradiustext.setVisibility(View.GONE);
         }
     }
     @Override
		public void onResume() {
			// TODO Auto-generated method stub
			super.onResume();
			Utils.appcameforeground(PostActivity.this);
			//Utils.startbackgroundservice(PostActivity.this);
			LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter(Utils.LOGOUT_USER));
			
		}

		@Override
		public void onPause() {
			LocalBroadcastManager.getInstance(this).unregisterReceiver( mRegistrationBroadcastReceiver);
			
			super.onPause();
		}
		 @Override
		    protected void onStop() {
		    	//session.setappbackground(Utils.isAppIsInBackground(SearchMain.this));
		    	Utils.appwentbackground(PostActivity.this);
				//System.out.println("onStop called");
		    	// TODO Auto-generated method stub
		    	super.onStop();
		    }
}