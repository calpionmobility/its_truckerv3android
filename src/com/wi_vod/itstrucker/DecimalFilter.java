package com.wi_vod.itstrucker;

import android.app.Activity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.widget.EditText;
import android.widget.Toast;

public class DecimalFilter implements OnFocusChangeListener {

	int count = -1;
	EditText et;
	Activity activity;
	int inputtype;

	public DecimalFilter(EditText edittext, Activity activity) {
		et = edittext;
		this.activity = activity;

	}

	

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		if (!hasFocus) {
			if (et.getText().toString() != null && !et.getText().toString().equals("")) {
				double d = Double.valueOf(et.getText().toString());
				double roundOff = (double) Math.round(d * 100) / 100;
				if (roundOff>100) {
					et.setText( "99.99");
					
				}else{
					et.setText(String.valueOf(roundOff));
				}
				
			}
		}

	}

}
