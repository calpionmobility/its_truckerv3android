package com.wi_vod.itstrucker;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.wi_vod.itstrucker.SearchMainActivity.ErrorDialogFragment;
import com.wi_vod.itstrucker.SearchMainActivity.GetAddressTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.AsyncTask.Status;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class PostTruckMain extends Fragment implements LocationListener,
		GoogleApiClient.ConnectionCallbacks,
		GoogleApiClient.OnConnectionFailedListener,
		android.location.LocationListener {

	private static String TAG = "PostTruckMain.class";
	private ListView postItemList;
	private PostMainarrayAdapter adapter;
	SharedPreferences postprefs;
	String posthomebase;
	private SessionManager session;
	private ListView mDrawerList;
	private String[] navMenuTitles;
	private CharSequence mTitle;
	private SharedPreferences prefs;
	private String homebase;
	private String homebaseaddress;
	private String homebasetext;
	private String homebaseaddresstext;
	private int geocounrtyindex;
	static boolean geofound = false;
	private DrawerLayout mDrawerLayout;
	private String addressText1;
	private LocationManager locationManager;
	private GetAddressTask posttruck;
	private String ppequipmentTypeStr;
	private String locality;
	private String adminarea;

	public PostTruckMain() {
		homebaseaddress = "";
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.searchmain, container, false);
		postItemList = (ListView) v.findViewById(R.id.listviewmain);
		mDrawerList = (ListView) getActivity().findViewById(
				R.id.list_slidermenu);
		navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

		prefs = getActivity().getSharedPreferences("itstrucker", 0);
		homebase = prefs.getString("homebase", "");
		ppequipmentTypeStr = prefs.getString("ppequipmentindex",AppConstants.EMPTY_STRING);
		if (!homebase.equals("")) {
			homebasetext = "(" + homebase + ")";
		}

		if (servicesConnected()) {
			try {
				locationManager = (LocationManager) getActivity().getSystemService(
						Context.LOCATION_SERVICE);
				// Creating a criteria object to retrieve provider
				Criteria criteria = new Criteria();
				// Getting the name of the best provider
				String provider = locationManager.getBestProvider(criteria, true);
				// Getting Current Location
				Location location = locationManager.getLastKnownLocation(provider);
				locationManager.requestLocationUpdates(provider, 0, 0, this);
				if (location != null) {
					posttruck = new GetAddressTask(getActivity());
					posttruck.execute(location);
				} else {
					geofound = false;
				}
			} catch (Exception e) {
				geofound = false;
			}
			
		}

		adapter = new PostMainarrayAdapter(getActivity(),
				AppConstants.POST_ITEM_LIST);
		postItemList.setAdapter(adapter);
		postItemList.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> adapterView,
							View view, int postion, long id) {
						Intent i;
						switch (postion) {

						case 0:

							if (homebase.equals("")&& ppequipmentTypeStr
											.equals(AppConstants.POSTEMPTY_STRING)) {
								showSettingsAlert(AppConstants.PROFILE_SETTINGERRORMSG);
							} else if (homebase.equals("")) {
								showSettingsAlert(AppConstants.HOMEBASE_ERRORMESSAGE);
							} else if (ppequipmentTypeStr
									.equals(AppConstants.POSTEMPTY_STRING)) {
								showSettingsAlert(AppConstants.SEARCHHOMEEQP_ERRORMESSAGE);
							} else if (!homebase.equals("") && geofound) {
								i = new Intent(getActivity(),
										PostActivity.class);
								// homebaseaddress="Akron,al";
								i.putExtra("originname", homebaseaddress);
								i.putExtra("countryindex", geocounrtyindex);
								i.putExtra("postType", 1);
								startActivity(i);
								
								 setupEvent( R.string.UICategory, R.string.UIAction,
							                R.string.postlabel,"GetMeHome");
							} else {
								// Utils.showAlertMessage(AppConstants.NOGPSFOUND_ERROR,getActivity());

								Toast.makeText(getActivity(),
										AppConstants.NOGPSFOUND_ERROR,
										Toast.LENGTH_SHORT).show();
							}

							break;

						case 1:
							if (ppequipmentTypeStr.equals(AppConstants.POSTEMPTY_STRING)) {
								showSettingsAlert(AppConstants.SEARCHHOMEEQP_ERRORMESSAGE);
							} else if (geofound) {
								 setupEvent( R.string.UICategory, R.string.UIAction,
							                R.string.postlabel,"GoAnywhere");
								i = new Intent(getActivity(),
										PostActivity.class);
								i.putExtra("postType", 2);
								// /homebaseaddress="Akron,al";
								i.putExtra("originname", homebaseaddress);
								i.putExtra("countryindex", geocounrtyindex);
								startActivity(i);
							} else {
								// Utils.showAlertMessage(AppConstants.NOGPSFOUND_ERROR,getActivity());
								// Toast.makeText(getActivity(),R.string.no_address_found,Toast.LENGTH_SHORT).show();

								Toast.makeText(getActivity(),
										AppConstants.NOGPSFOUND_ERROR,
										Toast.LENGTH_SHORT).show();
							}
							break;

						case 2:
							 setupEvent( R.string.UICategory, R.string.UIAction,
						                R.string.postlabel,"Current");
							i = new Intent(getActivity(),
									CurrentPostScreen.class);
							startActivity(i);
							break;
						/*case 3:
							i = new Intent(getActivity(), PostFavourite.class);
							startActivity(i);
							break;*/
						case 3:
							 setupEvent( R.string.UICategory, R.string.UIAction,
						                R.string.postlabel,"Custom");
							i = new Intent(getActivity(), PostActivity.class);
							i.putExtra("postType", 3);
							i.putExtra("originname", homebaseaddress);
							i.putExtra("countryindex", geocounrtyindex);
							startActivity(i);

							break;
						default:
							break;
						}
					}
				});
		return v;
	}

	public class PostMainarrayAdapter extends ArrayAdapter<String> {
		private final Context context;
		private final String[] values;

		public PostMainarrayAdapter(Context context, String[] values) {
			super(context, R.layout.list_items_main, values);
			this.context = context;
			this.values = values;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			View rowView = inflater.inflate(R.layout.list_items_main, parent,
					false);
			TextView textView = (TextView) rowView
					.findViewById(R.id.maintextitem);
			ImageView imageView = (ImageView) rowView
					.findViewById(R.id.listimageView);
			TextView hometext = (TextView) rowView
					.findViewById(R.id.homebasetext);
			textView.setText(values[position]);
			if (position == 0) {
				hometext.setText(homebasetext);
			} else if (position == 1) {
				hometext.setText(homebaseaddresstext);
			}
			// Change icon based on name
			return rowView;
		}
	}

	private boolean servicesConnected() {
		// Check that Google Play services is available
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
		// If Google Play services is available
		if (ConnectionResult.SUCCESS == resultCode) {
			// In debug mode, log the status
			// Log.d(LocationUtils.APPTAG,
			// getString(R.string.play_services_available));
			// Continue
			return true;
			// Google Play services was not available for some reason
		} else {
			// Display an error dialog
			Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode,getActivity(), 0);
			if (dialog != null) {
				ErrorDialogFragment errorFragment = new ErrorDialogFragment();
				errorFragment.setDialog(dialog);
				errorFragment.show(getActivity().getSupportFragmentManager(), "");
			}
			return false;
		}
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onConnected(Bundle connectionHint) {
		// TODO Auto-generated method stub

	}

	
	public void onDisconnected() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub

	}

	/**
	 * Define a DialogFragment to display the error dialog generated in
	 * showErrorDialog.
	 */
	public static class ErrorDialogFragment extends DialogFragment {

		// Global field to contain the error dialog
		private Dialog mDialog;

		/**
		 * Default constructor. Sets the dialog field to null
		 */
		public ErrorDialogFragment() {
			super();
			mDialog = null;
		}

		/**
		 * Set the dialog to display
		 * 
		 * @param dialog
		 *            An error dialog
		 */
		public void setDialog(Dialog dialog) {
			mDialog = dialog;
		}

		/*
		 * This method must return a Dialog to the DialogFragment.
		 */
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			 if (mDialog == null)
				    super.setShowsDialog (false);
			return mDialog;
		}
	}

	public static String getLatLng(Context context, Location currentLocation) {
		// If the location is valid
		if (currentLocation != null) {

			// Return the latitude and longitude as strings
			return context.getString(R.string.latitude_longitude,
					currentLocation.getLatitude(),
					currentLocation.getLongitude());
		} else {

			// Otherwise, return the empty string
			return AppConstants.EMPTY_STRING;
		}
	}

	protected class GetAddressTask extends AsyncTask<Location, Void, String> {

		// Store the context passed to the AsyncTask when the system
		// instantiates it.
		Context localContext;

		// Constructor called by the system to instantiate the task
		public GetAddressTask(Context context) {

			// Required by the semantics of AsyncTask
			super();

			// Set a Context for the background task
			localContext = context;
		}

		/**
		 * Get a geocoding service instance, pass latitude and longitude to it,
		 * format the returned address, and return the address to the UI thread.
		 */
		@Override
		protected String doInBackground(Location... params) {
			

			// Create a list to contain the result address
			List<Address> addresses = null;

			// Try to get an address for the current location. Catch IO or
			// network problems.
			try {
				/*
				 * Get a new geocoding service instance, set for localized
				 * addresses. This example uses android.location.Geocoder, but other
				 * geocoders that conform to address standards can also be used.
				 */
				Geocoder geocoder = new Geocoder(localContext, Locale.getDefault());

				// Get the current location from the input parameter list
				Location location = params[0];
				/*
				 * Call the synchronous getFromLocation() method with the
				 * latitude and longitude of the current location. Return at
				 * most 1 address.
				 */
				addresses = geocoder.getFromLocation(location.getLatitude(),
						location.getLongitude(), 1);
				// If the reverse geocode returned an address
				if (addresses != null && addresses.size() > 0) {
					try {
						// Get the first address
						Address address = addresses.get(0);
						// String addressText1=address.getLocality()+","+
						// address.getCountryName()+","+address.getPostalCode()+"\n"+address.getAdminArea();
						/*
						 * addressText1 = address.getLocality() + ","+
						 * address.getAdminArea() +","+ address.getCountryName()
						 */;
						locality = Utils.checkIsNullforPosts(address.getLocality());
						adminarea = Utils.checkIsNullforPosts(address.getAdminArea());

						if (locality.equals("")) {
							addressText1 = adminarea;
						} else {
							addressText1 = locality + "," + adminarea;
						}
						// Log.i(TAG, addressText1);

						if (address.getCountryName().toLowerCase().equals("united states")|| address.getCountryName().toLowerCase().equals("united states of america")) {
							geocounrtyindex = 0;
						} else if (address.getCountryName().toLowerCase().equals("canada")) {
							geocounrtyindex = 1;
						} else if (address.getCountryName().toLowerCase().equals("mexico")) {
							geocounrtyindex = 2;
						} else {
							geofound = false;
							return AppConstants.NO_ADDRESS_FOUND;
						}/*
						 * else {
						 * 
						 * geocounrtyindex = 0; geofound = true; }
						 */
						// Return the text
						geofound = true;
						return addressText1;
						// If there aren't any addresses, post a message
					} catch (Exception e) {
						return AppConstants.NO_ADDRESS_FOUND;
					}
					
				} else {
					return AppConstants.NO_ADDRESS_FOUND;
				}

				// Catch network or other I/O problems.
			} catch (IOException exception1) {

				// Log an error and return an error message
				// Log.e(TAG, getString(R.string.IO_Exception_getFromLocation));

				// print the stack trace
				//exception1.printStackTrace();
				return AppConstants.NO_ADDRESS_FOUND;
				// Return an error message
				// return (getString(R.string.IO_Exception_getFromLocation));

				// Catch incorrect latitude or longitude values
			} catch (IllegalArgumentException exception2) {

				// Construct a message containing the invalid arguments
				/*String errorString = getString(
						R.string.illegal_argument_exception,
						location.getLatitude(), location.getLongitude());
				// Log the error and print the stack trace
				// Log.e(TAG, errorString);
				exception2.printStackTrace();

				//
				return errorString;*/ 
				return AppConstants.NO_ADDRESS_FOUND;
			}catch (Exception e) {
                return AppConstants.NO_ADDRESS_FOUND;
            }
		}

		/**
		 * A method that's called once doInBackground() completes. Set the text
		 * of the UI element that displays the address. This method runs on the
		 * UI thread.
		 */
		@Override
		protected void onPostExecute(String address) {
			// locationtext.setText(address);
			/*
			 * homebaseaddress=address;
			 * //homebaseaddresstext="("+homebaseaddress+")";
			 * adapter.notifyDataSetChanged();
			 */
			if (!address.equals(AppConstants.NO_ADDRESS_FOUND) && geofound) {
				// Toast.makeText(getActivity(), address,
				// Toast.LENGTH_SHORT).show();
				homebaseaddress = addressText1;
				homebaseaddresstext = "(" + homebaseaddress + ")";
				adapter.notifyDataSetChanged();
				// geofound = true;
			} else {
				// Utils.showSettingsAlert(AppConstants.RECENTSEARCH_ERRORMESSAGE,getActivity());
				geofound = false;
				// To ast.makeText(getActivity(), address,
				// Toast.LENGTH_SHORT).show();
			}
		}
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		

	}

	@Override
	public void onProviderDisabled(String provider) {
		

	}

	public void showSettingsAlert(String msg) {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

		// Setting Dialog Title
		alertDialog.setTitle("ITS TRUCKER");

		// Setting Dialog Message
		alertDialog.setMessage(msg);

		// Setting Icon to Dialog
		// alertDialog.setIcon(R.drawable.delete);

		// On pressing Settings button
		alertDialog.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						Fragment fg = new ITSProfileActivity();
						FragmentTransaction fragmentTransaction = getFragmentManager()
								.beginTransaction();
						fragmentTransaction.replace(R.id.frame_container, fg);
						// fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
						fragmentTransaction.commit();
						try {
							if (mDrawerList.getCount() == 4) {
								mDrawerList.setItemChecked(2, true);
								mDrawerList.setSelection(2);
								getActivity().setTitle(navMenuTitles[2]);
							} else {
								mDrawerList.setItemChecked(1, true);
								mDrawerList.setSelection(1);
								getActivity().setTitle(navMenuTitles[2]);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}

					}
				});

		// on pressing cancel button
		alertDialog.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		// Showing Alert Message
		alertDialog.show();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//Log.i(TAG, "onResume called");
		try {
			if (servicesConnected()) {
				locationManager = (LocationManager) getActivity().getSystemService(
						Context.LOCATION_SERVICE);
				// Creating a criteria object to retrieve provider
				Criteria criteria = new Criteria();
				// Getting the name of the best provider
				String provider = locationManager.getBestProvider(criteria, true);
				// Getting Current Location
				Location location = locationManager.getLastKnownLocation(provider);
				locationManager.requestLocationUpdates(provider, 0, 0, this);
				if (location != null) {
					posttruck = new GetAddressTask(getActivity());
					posttruck.execute(location);
					// (new
					// SearchMainActivity.GetAddressTask(getActivity())).execute(location);
				} else {
					geofound = false;

				}
			}
		} catch (Exception e) {
			geofound = false;
		}
		
	}

	@Override
	public void onDestroyView() {
		//Log.i(TAG, "onDestroyView called");
		super.onDestroyView();
		
		try {
			posttruck = null;
			if (posttruck != null && posttruck.getStatus() == Status.RUNNING) {
				posttruck.cancel(true);
			}
			locationManager.removeUpdates(this);

		} catch (Exception e) {
			
		}
		
	}

	
    private void setupEvent( final int categoryId, final int actionId,
            final int labelId,String Dimension) {
                Tracker t = ((ITSApplication) getActivity().getApplication()).getTracker(ITSApplication.TrackerName.APP_TRACKER);
                t.send(new HitBuilders.EventBuilder().setCategory(getString(categoryId))
                        .setAction(getString(actionId)).setLabel(getString(labelId)).setCustomDimension(2, Dimension).build());
         
    }

	@Override
	public void onConnectionSuspended(int arg0) {
		// TODO Auto-generated method stub
		
	}

	
}
