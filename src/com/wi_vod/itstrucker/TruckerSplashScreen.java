package com.wi_vod.itstrucker;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class TruckerSplashScreen extends Activity {
	// Splash screen timer
	private static int SPLASH_TIME_OUT = 2000;
	private SessionManager session;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
	
		session=new SessionManager(TruckerSplashScreen.this);
		session.updatefirsttime(true);
		new Handler().postDelayed(new Runnable() {
			/*
			 * Showing splash screen with a timer showingg splash image
			 */

			@Override
			public void run() {
				if (session.isLoggedIn()) {
					Intent i = new Intent(TruckerSplashScreen.this, ITSMainScreen.class);
					startActivity(i);
					// close this activity
					finish();
				}else{
					Intent i = new Intent(TruckerSplashScreen.this, ITSTruckerLogin.class);
					startActivity(i);
					// close this activity
					finish();
				}
			}
		}, SPLASH_TIME_OUT);
	}
	
	
	
}