package com.wi_vod.itstrucker;

public interface ConnectionServiceListener {

	public void onServiceComplete(String response);
	
	public void onServiceComplete(String response,String reference);
}
